package com.gitee.qdbp.staticize.test.tags.ext;

import java.io.IOException;
import java.util.regex.Pattern;
import com.gitee.qdbp.staticize.annotation.AttrRequired;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.NextStep;
import com.gitee.qdbp.staticize.test.tags.utils.TagConfig;
import com.gitee.qdbp.tools.utils.VerifyTools;

/**
 * 图片标签, 生成图片绝对地址
 *
 * @author zhaohuihua
 * @version 140724
 */
public class UrlImageTag extends BaseTag {

    /** 分隔符 **/
    private static final Pattern SPLITOR = Pattern.compile(",");

    /** 变量名, 如果为空直接将图片地址输出到页面, 否则将图片地址保存到变量中 **/
    private String var;

    /** 图片路径 **/
    private String src;

    /**
     * 设置变量名
     *
     * @param var 变量名
     */
    @AttrRequired
    public void setVar(String var) {
        this.var = var;
    }

    /**
     * 设置图片地址
     *
     * @param src 图片地址
     */
    public void setSrc(String src) {
        this.src = src;
    }

    /**
     * 标签处理方法
     *
     * @return 下一步处理方式
     */
    @Override
    public NextStep doHandle() throws TagException, IOException {
        TagConfig config = TagConfig.getInstance();

        // 获取文件服务器地址
        String fs = config.getString("file.server.root.path");

        // 获取默认图片的绝对路径
        String def = config.getString("image.default.path");

        String url = VerifyTools.isBlank(src) ? fs + def : fs + src;

        // 如果src含有逗号, 则拆分并取第1个
        if (SPLITOR.matcher(url).find()) {
            url = SPLITOR.split(url)[0];
        }

        // 没有指定var, 直接输出
        if (var == null) {
            // 将图片标签输出
            print(url);
            return NextStep.SKIP_BODY;
        }
        // 指定了var, 将href保存到值栈中
        else {
            this.addStackValue(var, url);
            return NextStep.EVAL_BODY;
        }
    }
}
