<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>title</title>
</head>
<body>
	<core:each var="i" status="s" begin="10" rows="1">
		10 --- ${i}
		<core:each var="i" status="s" begin="100" rows="1">
			100 --- ${i}
			<core:each var="i" status="s" begin="1000" rows="1">
				1000 --- ${i}
			</core:each>
			100 --- ${i}
			<core:each var="i" status="s" begin="1100" rows="1">
				1100 --- ${i}
			</core:each>
			100 --- ${i}
			<core:each var="i" status="s" begin="1200" rows="1">
				1200 --- ${i}
			</core:each>
			100 --- ${i}
		</core:each>
		10 --- ${i}
		<core:each var="i" status="s" begin="200" rows="1">
			200 --- ${i}
			<core:each var="i" status="s" begin="2000" rows="1">
				2000 --- ${i}
			</core:each>
			200 --- ${i}
			<core:each var="i" status="s" begin="2100" rows="1">
				2100 --- ${i}
			</core:each>
			200 --- ${i}
			<core:each var="i" status="s" begin="2200" rows="1">
				2200 --- ${i}
			</core:each>
			200 --- ${i}
		</core:each>
		10 --- ${i}
	</core:each>
</body>
</html>
