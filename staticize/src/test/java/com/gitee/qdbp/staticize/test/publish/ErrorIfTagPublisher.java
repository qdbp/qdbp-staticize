package com.gitee.qdbp.staticize.test.publish;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import com.gitee.qdbp.staticize.common.IMetaData;
import com.gitee.qdbp.staticize.io.FileInputCreator;
import com.gitee.qdbp.staticize.io.IReaderCreator;
import com.gitee.qdbp.staticize.parse.TagParser;
import com.gitee.qdbp.staticize.publish.FilePublisher;
import com.gitee.qdbp.tools.files.PathTools;

/**
 * 发布测试类
 * 
 * @author zhaohuihua
 * @version 140605
 */
public class ErrorIfTagPublisher {

    public static void main(String[] args) throws Exception {

        URL path = PathTools.findClassResource(ErrorIfTagPublisher.class, "../template/");
        String folder = PathTools.toUriPath(path);

        String in = "error/if.tpl";
        String out = "error/if.html";

        System.out.println(folder);

        IMetaData data;
        try {
            IReaderCreator input = new FileInputCreator(folder);
            TagParser parser = new TagParser(input);
            data = parser.parse(in);
        } catch (Exception e) {
            System.out.println("模板解析失败: " + in);
            e.printStackTrace();
            return;
        }

        try {
            Map<String, Object> map = new HashMap<>();
            map.put("i", 666);

            FilePublisher publisher = new FilePublisher(data, folder);
            publisher.publish(map, out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
