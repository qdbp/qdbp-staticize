package #{java.vars.current.packages};

<core:each items="#{imports}" var="item">
${item}
</core:each>

/**
 * #{table.comment}查询类
 *
 * @author CodeGenerater
 * @version <fmt:date pattern="yyMMdd" />
 */
public class #{java.vars.current.capitalizeName} extends #{java.vars.model.bean.capitalizeName} {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;
    <core:each items="#{table.fields}" var="field">
    <core:if test="${!field.primaryKey && !field.businessKey && !field.logicallyDeleteField && !field.createTimeField && !field.updateTimeField}">
    /** ${field.comment}空值/非空值查询 **/
    private Boolean ${field.normativeName}IsNull;
    </core:if>
    <core:if test="${field.listData != null}">
    /** ${field.comment}列表 **/
    private List<${field.dataType.typedName}> ${field.listData.normativeName};
    </core:if>
    <core:if test="${field.dataType.stringType}">
    /** ${field.comment}前模匹配条件 **/
    private ${field.dataType.typedName} ${field.normativeName}Starts;
    /** ${field.comment}后模匹配条件 **/
    private ${field.dataType.typedName} ${field.normativeName}Ends;
    /** ${field.comment}模糊查询条件 **/
    private ${field.dataType.typedName} ${field.normativeName}Like;
    </core:if>
    <core:if test="${field.dataType.typedName == 'Date'}">
    /** 最小${field.comment} **/
    private ${field.dataType.typedName} ${field.normativeName}Min;
    /** 最大${field.comment} **/
    private ${field.dataType.typedName} ${field.normativeName}Max;
    /** 最小${field.comment} **/
    private ${field.dataType.typedName} ${field.normativeName}MinWithDay;
    /** 最大${field.comment} **/
    private ${field.dataType.typedName} ${field.normativeName}MaxWithDay;
    </core:if>
    <core:if test="${field.dataType.typedName == 'Integer' || field.dataType.typedName == 'Long' || field.dataType.typedName == 'Double' || field.dataType.typedName == 'Float'}">
    /** 最小${field.comment} **/
    private ${field.dataType.typedName} ${field.normativeName}Min;
    /** 最大${field.comment} **/
    private ${field.dataType.typedName} ${field.normativeName}Max;
    </core:if>
    </core:each>
    <core:each items="#{table.indexes}" var="index">
    	<core:if test="${index.fulltext}">
    /** 按${index.capitalizeName}搜索 **/
    private String searchBy${index.capitalizeName};
    	</core:if>
    </core:each>
    <core:each items="#{table.fields}" var="field">
    <core:if test="${!field.primaryKey && !field.businessKey && !field.logicallyDeleteField && !field.createTimeField && !field.updateTimeField}">

    /** 判断${field.comment}是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean get${field.capitalizeName}IsNull() {
        return ${field.normativeName}IsNull;
    }
    <core:if test="${field.notNullField}">

    /**
     * 设置${field.comment}空值查询(true:空值查询|false:非空值查询)
     *
     * @param ${field.normativeName}IsNull ${field.comment}空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    </core:if>
    <core:else>

    /** 设置${field.comment}空值查询(true:空值查询|false:非空值查询) **/
    </core:else>
    public void set${field.capitalizeName}IsNull(Boolean ${field.normativeName}IsNull) {
        this.${field.normativeName}IsNull = ${field.normativeName}IsNull;
    }
    </core:if>
    <core:if test="${field.listData != null}">

    /** 获取${field.comment}列表 **/
    public List<${field.dataType.typedName}> get${field.listData.capitalizeName}() {
        return ${field.listData.normativeName};
    }

    /** 设置${field.comment}列表 **/
    public void set${field.listData.capitalizeName}(List<${field.dataType.typedName}> ${field.listData.normativeName}) {
        this.${field.listData.normativeName} = ${field.listData.normativeName};
    }

    /** 增加${field.comment} **/
    public void add${field.capitalizeName}(${field.dataType.typedName}... ${field.listData.normativeName}) {
        if (this.${field.listData.normativeName} == null) {
            this.${field.listData.normativeName} = new ArrayList<>();
        }
        this.${field.listData.normativeName}.addAll(Arrays.asList(${field.listData.normativeName}));
    }
    </core:if>
    <core:if test="${field.dataType.stringType}">

    /** 获取${field.comment}前模匹配条件 **/
    public ${field.dataType.typedName} get${field.capitalizeName}Starts() {
        return ${field.normativeName}Starts;
    }

    /** 设置${field.comment}前模匹配条件 **/
    public void set${field.capitalizeName}Starts(${field.dataType.typedName} ${field.normativeName}Starts) {
        this.${field.normativeName}Starts = ${field.normativeName}Starts;
    }

    /** 获取${field.comment}后模匹配条件 **/
    public ${field.dataType.typedName} get${field.capitalizeName}Ends() {
        return ${field.normativeName}Ends;
    }

    /** 设置${field.comment}后模匹配条件 **/
    public void set${field.capitalizeName}Ends(${field.dataType.typedName} ${field.normativeName}Ends) {
        this.${field.normativeName}Ends = ${field.normativeName}Ends;
    }

    /** 获取${field.comment}模糊查询条件 **/
    public ${field.dataType.typedName} get${field.capitalizeName}Like() {
        return ${field.normativeName}Like;
    }

    /** 设置${field.comment}模糊查询条件 **/
    public void set${field.capitalizeName}Like(${field.dataType.typedName} ${field.normativeName}Like) {
        this.${field.normativeName}Like = ${field.normativeName}Like;
    }
    </core:if>
    <core:if test="${field.dataType.typedName == 'Date'}">

    /** 获取最小${field.comment} **/
    public ${field.dataType.typedName} get${field.capitalizeName}Min() {
        return ${field.normativeName}Min;
    }

    /** 设置最小${field.comment} **/
    public void set${field.capitalizeName}Min(${field.dataType.typedName} ${field.normativeName}Min) {
        this.${field.normativeName}Min = ${field.normativeName}Min;
    }

    /** 获取最大${field.comment} **/
    public ${field.dataType.typedName} get${field.capitalizeName}Max() {
        return ${field.normativeName}Max;
    }

    /** 设置最大${field.comment} **/
    public void set${field.capitalizeName}Max(${field.dataType.typedName} ${field.normativeName}Max) {
        this.${field.normativeName}Max = ${field.normativeName}Max;
    }

    /** 获取最小${field.comment} **/
    public ${field.dataType.typedName} get${field.capitalizeName}MinWithDay() {
        return ${field.normativeName}MinWithDay;
    }

    /** 设置最小${field.comment} **/
    public void set${field.capitalizeName}MinWithDay(${field.dataType.typedName} ${field.normativeName}MinWithDay) {
        this.${field.normativeName}MinWithDay = ${field.normativeName}MinWithDay;
    }

    /** 获取最大${field.comment} **/
    public ${field.dataType.typedName} get${field.capitalizeName}MaxWithDay() {
        return ${field.normativeName}MaxWithDay;
    }

    /** 设置最大${field.comment} **/
    public void set${field.capitalizeName}MaxWithDay(${field.dataType.typedName} ${field.normativeName}MaxWithDay) {
        this.${field.normativeName}MaxWithDay = ${field.normativeName}MaxWithDay;
    }
    </core:if>
    <core:if test="${field.dataType.typedName == 'Integer' || field.dataType.typedName == 'Long' || field.dataType.typedName == 'Double' || field.dataType.typedName == 'Float'}">

    /** 获取最小${field.comment} **/
    public ${field.dataType.typedName} get${field.capitalizeName}Min() {
        return ${field.normativeName}Min;
    }

    /** 设置最小${field.comment} **/
    public void set${field.capitalizeName}Min(${field.dataType.typedName} ${field.normativeName}Min) {
        this.${field.normativeName}Min = ${field.normativeName}Min;
    }

    /** 获取最大${field.comment} **/
    public ${field.dataType.typedName} get${field.capitalizeName}Max() {
        return ${field.normativeName}Max;
    }

    /** 设置最大${field.comment} **/
    public void set${field.capitalizeName}Max(${field.dataType.typedName} ${field.normativeName}Max) {
        this.${field.normativeName}Max = ${field.normativeName}Max;
    }
    </core:if>
    </core:each>
    <core:each items="#{table.indexes}" var="index">
    	<core:if test="${index.fulltext}">

    /** 获取按${index.capitalizeName}搜索的条件 **/
    public String getSearchBy${index.capitalizeName}() {
        return searchBy${index.capitalizeName};
    }

    /** 设置按${index.capitalizeName}搜索的条件 **/
    public void setSearchBy${index.capitalizeName}(String searchBy${index.capitalizeName}) {
        this.searchBy${index.capitalizeName} = searchBy${index.capitalizeName};
    }
    	</core:if>
    </core:each>

    @Override
    public String toString() {
        return JsonTools.toLogString(this);
    }
}