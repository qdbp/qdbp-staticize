<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="column" uri="http://www.jeshing.com/tags/column/"%>
<%@ taglib prefix="content" uri="http://www.jeshing.com/tags/content/"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>
<%@ taglib prefix="util" uri="http://www.jeshing.com/tags/util/"%>
<%@ taglib prefix="url" uri="http://www.jeshing.com/tags/url/"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/base/include/meta.tpl"%>
	<meta name="keywords" content="乐居中国" />
	<meta name="description" content="乐居中国" />
	<title>装修宝典</title>
	<%@ include file="/base/include/head.tpl"%>
	<script type="text/javascript" src="<url:link href='resources/base/js/cyclic/zhh.cyclic.js'/>"></script>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/home.css'/>">
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/decoration_collection.css'/>">
	<script type="text/javascript" src="<url:link href='resources/site/js/zhuangxiu.js'/>"></script>
</head>
<body>
	<jsp:include page="/base/include/top.tpl" >
		<jsp:param name="column" value="zhuangxiu" />
	</jsp:include>

	<div class="clear"></div>
    <div class="centern-big center-dapei">
		<div class="daily-left daily-left-dapei daily-left-dapei-ch">
			<div class="marquee-box marquee-box-dapei hover mq-box">
			<position:detail key="zhuangxiu|lunbotu">
				<position:content key="${position.id}" rows="3">
				<div>
					<ul class="marquee-list marquee-lis-dapei mq-list  zhh-cyclic" data-box=".marquee-box" data-pointer-item=".mq-pointer">
							<core:forEach var="item" items="${list}" varStatus="s">
							<li>
								<div class="marquee-item">
									<a href="<url:link href='${item.url}'/>" title="${item.title }" target="_blank">
										<img src="<url:image src='${item.image.src}'/>" width="845" height="420" alt="${item.title }">
									</a>
								</div>
							</li>
							</core:forEach>
					</ul>
					<ul class="pointer pointer-ch">
						<core:forEach var="item" items="${list}" varStatus="s">
							<li class="mq-pointer <core:if test="${s.index==0}">circle-icon-selected</core:if>"><a href="#${s.index}" title="${item.title}"></a></li>
						</core:forEach>
					</ul>
				</div>
				<div class="clear"></div>
				</position:content>
				</position:detail>
			</div>
		</div>

		<div class="daily_right_decoration"  >

			<div class="daily-right-head"  >
				<span class="daily-right-head-icon"  ></span>
				<span class="daily-right-head-title" >装修招标</span>
			</div>
			<div class="daily_right_decoration_top"  ></div>
			<div class="daily_right_decoration_body"  >
				<input type="hidden"  id="getCountUrl"   data-src="<url:link href="web/site/decorate/getCount.json"/>"  >
				<input type="hidden"  id="getAcreageUrl"  data-type="HouseArea"  data-src="<url:link href="web/site/company/listEnumType.json"/>"  >
				<input type="hidden"  id="getDecorateBudgetTypeUrl" data-type="BudgetType"  data-src="<url:link href="web/site/company/listEnumType.json"/>"  >

				<div class="decoration_right_title">已有<span class="decoration_number"></span>位业主通过乐居中国发布了装修招标。</div>
				<form id="requestDecorateForm" action="<url:link href="web/site/decorate/saveDecorate.json"/>">
				<ul class="decoration_right_forms">
                	<li><span>姓氏：</span><input class="input_text" type="text" name="requestName"></li>
                    <li><span>性别：</span><input type="radio" name="sex" value="1"><font>先生</font><input type="radio"  name="sex" value="0" ><font>女士</font></li>
                    <li><span>手机：</span><input class="input_text" type="text" name="mobilePhone" ></li>
                    <li><span>面积：</span><select name="acreage"></select></li>
                    <li><span>预算：</span><select name="decorateBudgetType"></select></li>
                </ul>
                </form>
                <div class="decoration_right_button"><a class="bg5" href="javascript:void(0)" name="requestDecorate">立即预约</a></div>
			</div>
			<div class="daily_right_decoration_bottom"  ></div>
		</div>
	    <div class="clear"></div>
	</div>

	<position:detail key="zhuangxiu|qingsongjiubu" >
		<div class="centern-big step-bottom clear_box">
			 <span class="step-nine-info step-nine-font fl">
				<a class="bg4 step-nine-icon fl"></a>
				<font class="step_nine_f fl">${position.title }</font>
			 </span>
			 <span class="step-nine-info fl">
				${position.shortTitle}
			 </span>
		</div>
		<div class="centern-big step-nine-content step_box following" data-items="a" data-current="step-selected">
			<position:content key="${position.id}" rows="9">
				<core:forEach var="item" items="${list}" varStatus="s">
					<a href="<url:link href='${item.url}'/>" title="${item.title}" <core:if test="${s.last}"> class="margin0-r" </core:if> style="background:url(<url:image src='${item.image.src}'/>) no-repeat">${item.title}</a>
				</core:forEach>
			</position:content>
			<div class="following-block"></div>
		</div>
	</position:detail>

	<%-- 装修必读 --%>
	<div class="centern-big center-dapei">
		<position:detail key="zhuangxiu|zhuangxiubidu">
			<position:content key="${position.id}" rows="9">
			<div class="decoration_have_box fl">
	        	<div class="decoration_title_box">
	                <div class="decoration_have_top">
	                   <div class="bg5 decoration_have_head fl" >
	                    <a class="daily-left-head-titlebig" href="<url:link href='${position.url}'/>">${position.title}</a>
	                    <a class="daily-left-head-titlesmall" href="<url:link href='${position.url}'/>">${position.subTitle}</a>
	                	</div>
		                <core:if test="${position.url!=null}">
		                    <a class="fr more bg5" href="<url:link href='${position.url}'/>" ></a>
		                </core:if>
	                </div>
		            <div class=" decoration_left_border fr"></div>
	            </div>

               	<div class="decoration_musthave_next">
                   	<div class="fl decoration_musthave_next_t"><a href="#" class="prev"><span class="bg5"></span>上一步</a></div>
                    <div class="fl decoration_musthave_next_title"><core:out value="装修步骤1：${list[0].title}"></core:out></div>
                    <div class="fl decoration_musthave_next_dian">
                    	<ul>
                    		<core:forEach var="po" items="${list}" varStatus="s">
                         	<li><a href="#" class="bg5 <core:if test="${s.index==0}">on</core:if>" title="装修步骤${s.index+1}：${po.title }"></a></li>
                            </core:forEach>
                        </ul>
                    </div>
                    <div class="fr decoration_musthave_next_b"><a href="#" class="next">下一步<span class="bg5"></span></a></div>
                </div>

				<div class="decoration_musthave">
					<div class="musthave_wrap">
						<core:forEach var="po" items="${list}" >
		                    <div class="musthave_item">
				            	<div class="decoration_musthave_top">
				                	<div class="fl decoration_musthave_top_left"><a href="<url:link href='${po.url}'/>" title="${po.title}"><img src="<url:image src='${po.image.src}'/>" width="410" height="244" alt="${po.image.title}"/><span class="bg5"></span></a></div>
				                    <div class="fl decoration_musthave_top_right">
				                    	<h3>${po.title}</h3>
				                        <p>${po.digest}</p>
				                    </div>
				                </div>
				                <div class="decoration_musthave_top_b">
				                <position:content key="${po.positionId}" rows="2">
									<core:forEach var="item" items="${list}" >
					                	<div class="fl decoration_musthave_list">
					                        <a href=" <url:link href='${item.url}'/>" title="${item.title}"><img src="<url:image src='${item.image.src}'/>" width="410" height="185" alt="${item.image.title}"/></a>
					                        <span><a href="<url:link href='${item.url}'/> " title="${item.title}">${item.title}</a></span>
					                        <p><a href="<url:link href='${item.url}'/> " title="${item.title}">${item.digest}</a></p>
					                    </div>
				                    </core:forEach>
				                </position:content>
				                </div>
			                </div>
						</core:forEach>
					</div>
	            </div>
			</div>
		</position:content>
		</position:detail>

		<div class="fr decoration_have_right_box">
	         <div class="marquee-box decoration-content-right hover mq-box marquee-box-ch">
				<div>
					<position:detail key="zhuangxiu|youceguanggao">
						<position:content key="${position.id}" rows="3">
						<div class="marquee-list decoration-marquee-list-ch zhh-marquee" data-box=".mq-box" data-pointer-item=".mq-pointer" data-pointer-curr="circle-icon-selected">

							<core:forEach var="item" items="${list}" varStatus="s">
							<div class="decoration-content-right-img">
								<div class="marquee-item decoration-marquee-item-dp" >
									<img src="<url:image src='${item.image.src}'/>" width="315" height="305" alt="${item.title }" />
									<div class="prompty">
										<span><a href="<url:link href='${item.url}'/>" >${item.title }</a></span>
									</div>
									<a class="decoration-bg" href="<url:link href='${item.url}'/>"  title="${item.title}"></a>
								</div>
							</div>
							</core:forEach>
					    </div>
						<div class="decoration-circle marquee-circle-dp">
							<core:forEach var="item" items="${list}" varStatus="s">
								<a class="bg1 circle-icon mq-pointer <core:if test="${s.first }">circle-icon-selected</core:if>"></a>
							</core:forEach>
						</div>
						<div class="clear"></div>
						</position:content>
					</position:detail>
				</div>
			</div>


            <div class="decoration_tools">
            	<position:detail key="zhuangxiu|zhuangxiugongju">
	            	<position:content key="${position.id}" rows="6">
	            	<div class="decoration_tools_title"><span class="bg1"></span><h3>${position.title}</h3></div>
	                <div class="decoration_tools_main">
	                	<ul>
	                		<core:forEach var="item" items="${list}">
		                    	<li><a href="<url:link href='${item.url}'/>"  ><span class="decoration_tools_main_a" style="background:url(<url:image src='${item.image.src}'/>)"></span><span class="decoration_tools_main_name">${item.title}</span></a></li>
	                    	</core:forEach>
	                    </ul>
	                </div>
	                </position:content>
                </position:detail>
            </div>

		</div>
		<div class="clear"></div>
	</div>
    <%-- 设计百科 --%>
    <div class="design_encyclopedia centern-big">
    	<position:detail key="zhuangxiu|shejibaike">
    	<div class="design_encyclopedia_head">
            <div class="design_encyclopedia_title fl"><span class="bg1"></span><h3><b>${position.title}</b>${position.shortTitle}</h3></div>
            <core:if test="${position.url!=null}">
            	<a class="fr more bg5" href="<url:link href='${position.url}'/>"></a>
            </core:if>
        </div>
        <position:content key="${position.id}">
        <div class="design_encyclopedia_box">
        	<core:forEach var="po" items="${list}" varStatus="s">
        		<core:if test="${s.index==0}">
        			 <div class="design_encyclopedia_main fl">
        			 	<position:content key="${po.positionId}" rows="9">
		            	<span class="fl"><a href="<url:link href='${position.url}'/>">${po.title}</a></span>
		            	<div class="fl design_encyclopedia_list">
		            		<core:forEach var="item" items="${list}" varStatus="s">
		            			<core:if test="${s.index==0}">
		            				<a href="<url:link href='${item.url}'/>" >${item.title}</a>
		            			</core:if>
		            			<core:if test="${s.index>0}">
		            				|<a href="<url:link href='${item.url}'/>" >${item.title}</a>
		            			</core:if>
		            		</core:forEach>
		            	</div>
						</position:content>
        			 </div>
        		</core:if>

        		<core:if test="${s.index==1}">
        			 <div class="design_encyclopedia_main fl design_encyclopedia_mg">
        			 	<position:content key="${po.positionId}" rows="9">
		            	<span class="fl"><a href="<url:link href='${position.url}'/>">${po.title}</a></span>
		            	<div class="fl design_encyclopedia_list">
		            		<core:forEach var="item" items="${list}" varStatus="s">
		            			<core:if test="${s.index==0}">
		            				<a href="<url:link href='${item.url}'/>" >${item.title}</a>
		            			</core:if>
		            			<core:if test="${s.index>0}">
		            				|<a href="<url:link href='${item.url}'/>" >${item.title}</a>
		            			</core:if>
		            		</core:forEach>
		            	</div>
						</position:content>
        			 </div>
        		</core:if>

        		<core:if test="${s.index==2}">
        			 <div class="design_encyclopedia_main fr">
        			 	<position:content key="${po.positionId}" rows="9">
		            	<span class="fl"><a href="<url:link href='${position.url}'/>">${po.title}</a></span>
		            	<div class="fl design_encyclopedia_list">
		            		<core:forEach var="item" items="${list}" varStatus="s">
		            			<core:if test="${s.index==0}">
		            				<a href="<url:link href='${item.url}'/>" >${item.title}</a>
		            			</core:if>
		            			<core:if test="${s.index>0}">
		            				|<a href="<url:link href='${item.url}'/>" > ${item.title}</a>
		            			</core:if>
		            		</core:forEach>
		            	</div>
						</position:content>
        			 </div>
        		</core:if>

        	</core:forEach>

        </div>
        </position:content>
        </position:detail>
    </div>
    <div class="clear"></div>
	<%--  装修技巧 --%>
	 <div class="centern-big decoration-centern"  >
		<div class="fl mq-box">
			<position:detail key="zhuangxiu|zhuangxiujiqiao">
				<position:content key="${position.id}" rows="9">
		        	<div class="decoration_title_box">
		                <div class="decoration_have_top">
		                    <div class="bg5 decoration_skills_head fl" >
		                        <a class="daily-left-head-titlebig" href="<url:link href='${position.url}'/>">${position.title}</a>
		                        <a class="daily-left-head-titlesmall" href="<url:link href='${position.url}'/>">${position.subTitle}</a>
		                    </div>
		                    <core:if test="${position.url!=null}">
		                    	<a class="fr more bg5" href="<url:link href='${position.url}'/>" ></a>
		                    </core:if>
		                </div>
		                <div class=" decoration_left_border fr"></div>
		                <div class="clear"></div>
		            </div>
					<div class="decoration-left-pic-warp zhh-marquee" data-box=".mq-box" data-pointer-item=".mq-pointer" data-pointer-curr="circle-icon-selected">
						<core:forEach var="item" items="${list}" varStatus="s">
							<core:if test="${s.index==0||s.index==3||s.index==6}">
							<div class="decoration-left-pic-warp-block">
					        </core:if>
							<div class="decoration-left-pic">
								<core:if test="${s.index==0||s.index==3||s.index==6||s.index==2||s.index==5||s.index==8}">
								<core:if test="${s.index==0||s.index==3||s.index==6}"><a class="bg1 icon-share icon-pink-up1-share" ></a></core:if>
								<core:if test="${s.index==2||s.index==5||s.index==8}"><a class="bg1 icon-share icon-blue-up3-share" ></a></core:if>
								<a class="img-a"  href="<url:link href='${item.url}'/>"   style="background:url(<url:image src='${item.image.src}'/>)"></a>
								<a href="<url:link href='${item.url}'/>"
								<core:if test="${s.index==0||s.index==3||s.index==6}">class="txt-a pinkbg"</core:if>
								<core:if test="${s.index==2||s.index==5||s.index==8}">class="txt-a bluebg"</core:if>
								>${item.title }</a>
							    </core:if>
								<core:if test="${s.index==1||s.index==4||s.index==7}">
									<a class="bg1 icon-share icon-green-down2-share" ></a>
									<a href="<url:link href='${item.url}'/>" class="txt-a greenbg"	>${item.title }</a>
									<a class="img-a"  href="<url:link href='${item.url}'/>"   style="background:url(<url:image src='${item.image.src}'/>)"></a>
								</core:if>
						    </div>
						    <core:if test="${s.index==2||s.index==5||s.index==8||s.last}">
							</div>
					        </core:if>
						</core:forEach>
					</div>
					<div class="decoration-circle decoration_left_width" >
						<core:forEach var="x" begin="1" end="${list.size()%3==0?list.size()/3:(list.size()/3)+1}" step="1" varStatus="s">
							<a class="bg1 circle-icon mq-pointer <core:if test="${s.first }">circle-icon-selected</core:if>"></a>
						</core:forEach>
					</div>
				</position:content>
			</position:detail>

			<div class="decoration_left_middle_rewarp">
				<position:detail key="zhuangxiu|zhuangxiujiqiao|zuo">
					<position:content key="${position.id}" rows="4">
						<core:forEach var="item" items="${list}" begin="0" end="0">
							<div class="decoration-left-middle-left"> <a href="<url:link href='${item.url}'/>" title="${item.title}"><img src="<url:image src='${item.image.src}'/>" width="130" height="100" alt="${item.image.title}"> </a></div>
						</core:forEach>
							<div class="decoration-left-middle-right">
							<core:forEach var="item" items="${list}" varStatus="s">
								<core:if test="${s.index==0 }">
									<div class="decoration-left-middle-right-title"><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></div>
								</core:if>
								<core:if test="${s.index>0 }">
									<div class="decoration-left-middle-right-content"><a class="bg1"></a> <span><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></span></div>
								</core:if>
							</core:forEach>
							</div>
					</position:content>
				</position:detail>
			</div>
			<div class="decoration_left_middle_rewarp">
				<position:detail key="zhuangxiu|zhuangxiujiqiao|you">
					<position:content key="${position.id}" rows="4">
						<core:forEach var="item" items="${list}" begin="0" end="0">
							<div class="decoration-left-middle-left"> <a href="<url:link href='${item.url}'/>" title="${item.title}"><img src="<url:image src='${item.image.src}'/>" width="130" height="100" alt="${item.image.title}"> </a></div>
						</core:forEach>
							<div class="decoration-left-middle-right">
							<core:forEach var="item" items="${list}" varStatus="s">
								<core:if test="${s.index==0 }">
									<div class="decoration-left-middle-right-title"><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></div>
								</core:if>
								<core:if test="${s.index>0 }">
									<div class="decoration-left-middle-right-content"><a class="bg1"></a> <span><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></span></div>
								</core:if>
							</core:forEach>
							</div>
					</position:content>
				</position:detail>
			</div>
			<div class="clear"></div>

		</div>
		<div class="decoration_re_right fr" >
		<position:detail key="zhuangxiu|remenzhuangxiugongsi">
			<div class="bg3 decoration-right-head" >
				<span class="decoration-right-head-title" ><a href="<url:link href='${position.url}'/>">${position.title}</a></span>
			</div>
            <div class="decoration_right_height"></div>
			<div class="decoration-right-head-body" >
				<position:content key="${position.id}" rows="6">
					<core:forEach var="item" items="${list}" >
						<a href="<url:link href='${item.url}'/>" title="${item.title}"><img  class="decoration-right-head-bodyimg" src="<url:image src='${item.image.src}'/>" width="112"  height="112" alt="${item.image.title}"> </a>
					</core:forEach>
				</position:content>
			</div>
        </position:detail>
		</div>
	</div>
	<div class="clear"></div>
     <%-- 装修百科 --%>
	<div class="design_encyclopedia centern-big">
    	<position:detail key="zhuangxiu|zhuangxiubaike">
    	<div class="design_encyclopedia_head">
            <div class="design_encyclopedia_title fl"><span class="bg1"></span><h3><b>${position.title}</b>${position.shortTitle}</h3></div>
            <core:if test="${position.url!=null}">
            	<a class="fr more bg5" href="<url:link href='${position.url}'/>"></a>
            </core:if>
        </div>
        <position:content key="${position.id}">
        <div class="design_encyclopedia_box">
        	<core:forEach var="po" items="${list}" varStatus="s">
        		<core:if test="${s.index==0}">
        			 <div class="design_encyclopedia_main fl">
        			 	<position:content key="${po.positionId}" rows="9">
		            	<span class="fl"><a href="<url:link href='${position.url}'/>">${po.title}</a></span>
		            	<div class="fl design_encyclopedia_list">
		            		<core:forEach var="item" items="${list}" varStatus="s">
		            			<core:if test="${s.index==0}">
		            				<a href="<url:link href='${item.url}'/>" >${item.title}</a>
		            			</core:if>
		            			<core:if test="${s.index>0}">
		            				|<a href="<url:link href='${item.url}'/>" >${item.title}</a>
		            			</core:if>
		            		</core:forEach>
		            	</div>
						</position:content>
        			 </div>
        		</core:if>

        		<core:if test="${s.index==1}">
        			 <div class="design_encyclopedia_main fl">
        			 	<position:content key="${po.positionId}" rows="9">
		            	<span class="fl"><a href="<url:link href='${position.url}'/>">${po.title}</a></span>
		            	<div class="fl design_encyclopedia_list">
		            		<core:forEach var="item" items="${list}" varStatus="s">
		            			<core:if test="${s.index==0}">
		            				<a href="<url:link href='${item.url}'/>" >${item.title}</a>
		            			</core:if>
		            			<core:if test="${s.index>0}">
		            				|<a href="<url:link href='${item.url}'/>" >${item.title}</a>
		            			</core:if>
		            		</core:forEach>
		            	</div>
						</position:content>
        			 </div>
        		</core:if>

        		<core:if test="${s.index==2}">
        			 <div class="design_encyclopedia_main fr">
        			 	<position:content key="${po.positionId}" rows="9">
		            	<span class="fl"><a href="<url:link href='${position.url}'/>">${po.title}</a></span>
		            	<div class="fl design_encyclopedia_list">
		            		<core:forEach var="item" items="${list}" varStatus="s">
		            			<core:if test="${s.index==0}">
		            				<a href="<url:link href='${item.url}'/>" >${item.title}</a>
		            			</core:if>
		            			<core:if test="${s.index>0}">
		            				|<a href="<url:link href='${item.url}'/>" >${item.title}</a>
		            			</core:if>
		            		</core:forEach>
		            	</div>
						</position:content>
        			 </div>
        		</core:if>

        	</core:forEach>

        </div>
        </position:content>
        </position:detail>
    </div>
    <div class="clear"></div>

	<%-- 精挑细选 --%>
	<div class="centern-big case-centern">
		<position:detail key="zhuangxiu|jingtiaoxixuan">
	    	<div>
	        	<div class="decoration_have_top">
	            	<div class="bg5 decoration_guide_head fl" >
	                	<a class="daily-left-head-titlebig" href="<url:link href='${position.url}'/>">${position.title}</a>
	                    <a class="daily-left-head-titlesmall" href="<url:link href='${position.url}'/>">${position.subTitle }</a>
	                </div>
	                <core:if test="${position.url!=null}">
	                	<a class="fr more bg5" href="<url:link href='${position.url}'/>"></a>
	                </core:if>
	            </div>
	            <div class="decoration_border fr"></div>
	            <div class="clear"></div>
	        </div>
	        <position:content key="${position.id}" rows="8">
				<div class="shopping_guide mq-box">
					<div class="fl shopping_guide_left mq-left"><a class="bg5" href="#"></a></div>
					<div class="fl shopping_guide_wrap">
			            <div class="fl shopping_guide_main zhh-marquee" data-box=".mq-box" data-pages-left=".mq-right a" data-pages-right=".mq-left a">
			            	<ul>
			            		<core:forEach var="item" items="${list}" >
			                		<li><a class="shopping_guide_box" href="<url:link href='${item.url}'/>" title="${item.title}"><img src="<url:image src='${item.image.src}'/>" width="276" height="197" alt="${item.image.title}"/><span class="bg5 shopping_guide_main_triangle"></span><span class="shopping_guide_main_bg"></span><span class="shopping_guide_main_name">${item.title}</span></a></li>
			                	</core:forEach>
			                </ul>
			            </div>
		            </div>
		            <div class="fr shopping_guide_right mq-right"><a class="bg5" href="#"></a></div>

				</div>
			</position:content>
	    </position:detail>
	</div>
	<div class="clear"></div>

	<%--  装修改造王 --%>
	<div class="centern-big case-centern">
		<div class="case-left">
				<div class="decoration_title_box">
	                <div class="decoration_have_top">
	                <position:detail key="zhuangxiu|jiazhuanggaizaowang|datu">
	                    <div class="bg5 decoration_evolution_head fl" >
	                        <a class="daily-left-head-titlebig" href="<url:link href='${position.url}'/>">${position.title}</a>
	                        <a class="daily-left-head-titlesmall" href="<url:link href='${position.url}'/>">${position.subTitle}</a>
	                    </div>
	                    <core:if test="${position.url!=null}">
	                    	<a class="fr more bg5" href="<url:link href='${position.url}'/>"></a>
	                    </core:if>
	                </position:detail>
	                </div>
	                <div class=" decoration_left_border fr"></div>
	                <div class="clear"></div>
	            </div>

				<div class="decoration_evolution_box">
					<position:detail key="zhuangxiu|jiazhuanggaizaowang|datu">
		            	<div class="fl decoration_evolution_box_left">
		            		<core:forEach var="item" items="${list}" begin="0" end="0">
			                	<div class="decoration_evolution_box_left_img">
			                    	<a href="<url:link href='${item.url}'/>" title="${item.title}"><img src="<url:image src='${item.image.src}'/>" width="410" height="510" alt="${item.image.title}"/></a>
			                        <span class="decoration_evolution_box_left_imgbg"></span>
			                    </div>
			                    <div  class="decoration_evolution_box_left_a"><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></div>
			                    <p  class="decoration_evolution_box_left_p"><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.digest}</a></p>
			                    <div class="decoration_musthave_top_tips">
				                    <content:detail key="${item.contentId}">
								     <core:if test="${content.tagword!=null}">
										<util:split string="${content.tagword}" var="tagwords" >
											<core:forEach var="word" items="${tagwords}" varStatus="s" begin="0" end="3">
												<a <core:if test="${s.index==0}">class="first"</core:if> href="<url:link href='${item.url}'/>">${word}</a>
											</core:forEach>
										</util:split>
									</core:if>
							 		</content:detail>
			                    </div>
		                	</core:forEach>
		                </div>
		            </position:detail>

					<position:detail key="zhuangxiu|jiazhuanggaizaowang|xiaotu">
		                <div class="fr">
		                	<position:content key="${position.id}" rows="2">
			                	<div class="decoration_musthave_list">
			                	<core:forEach var="item" items="${list}" >
			                        <a href="<url:link href='${item.url}'/>" title="${item.title}"><img src="<url:image src='${item.image.src}'/>" width="411" height="226" alt="${item.image.title}"/></a>
			                        <span><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></span>
			                        <p><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.digest}</a></p>
			                     </core:forEach>
			                    </div>
		                    </position:content>
		                </div>
	                </position:detail>
	            </div>

		</div>
		<div class="case_re_right">
			<position:detail key="zhuangxiu|renqishejishi">
				<div class="bg3 case-right-head-body-top" >
					<span class="case-right-head-body-toptxt" >${position.title}</span>
				</div>
				<position:content key="${position.id}" rows="6">
					<core:forEach var="item" items="${list}" begin="0" end="1">
						<div class="case-right-item-warp" >
							<a class="bg2 case-right-item-icon" >特约</a>
							<a href="<url:link href='${item.url}'/>" title="${item.title}"><img src="<url:image src='${item.image.src}'/>" class="case-right-item-pic" width="130" height="130" alt="${item.image.title}"></a>
							<a href="<url:link href='${item.url}'/>" title="${item.title}"><span  class="case-right-item-name" >${item.title}</span></a>
							<a href="<url:link href='${item.url}'/>" title="${item.title}"><span class="case-right-item-txt" >${item.shortTitle}</span></a>
						</div>
					</core:forEach>
					<%--<div class="case-right-item-splitline"></div>--%>
					<core:forEach var="item" items="${list}" begin="2" end="5">
						<div class="case-right-item-warp case_re_right_mt" >
							<a class="bg2 case-right-item-icon" >特约</a>
							<a href="<url:link href='${item.url}'/>" title="${item.title}"><img src="<url:image src='${item.image.src}'/>" class="case-right-item-pic" width="130" height="130"></a>
							<a href="<url:link href='${item.url}'/>" title="${item.title}"><span  class="case-right-item-name" >${item.title}</span></a>
							<a href="<url:link href='${item.url}'/>" title="${item.title}"><span class="case-right-item-txt" >${item.shortTitle}</span></a>
						</div>
					</core:forEach>
				</position:content>
			</position:detail>
		</div>
	</div>
    <div class="clear"></div>

    <%-- 装修风水 --%>
	<div class="centern-big">
		<position:detail key="zhuangxiu|zhuangxiufengshui">
	    	<div>
	        	<div class="decoration_have_top">
	            	<div class="bg5 decoration_fengshui_head fl" >
	                	<a class="daily-left-head-titlebig" href="<url:link href='${position.url}'/>">${position.title}</a>
	                    <a class="daily-left-head-titlesmall" href="<url:link href='${position.url}'/>">${position.subTitle}</a>
	                </div>
	                <core:if test="${position.url!=null}">
	                	<a class="fr more bg5" href="<url:link href='${position.url}'/>"></a>
	                </core:if>
	            </div>
	            <div class="decoration_border fr"></div>
	            <div class="clear"></div>
	        </div>
			<div class="decoration_fengshui_list_main">
			<position:content key="${position.id}" rows="3">
				<core:forEach var="item" items="${list}" varStatus="s">
				<div class="fl decoration_fengshui_list <core:if test="${s.index!=0}">decoration_fengshui_ml</core:if>">
	                <a href="<url:link href='${item.url}'/>" title="${item.title}"><img src="<url:image src='${item.image.src}'/>" width="384" height="226" alt="${item.image.title}"/></a>
	                <span><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></span>
	            	<p><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.digest}</a></p>
	            </div>
	            </core:forEach>
	        </position:content>
	        </div>
		</position:detail>
     </div>
	<div class="clear"></div>

	<%--  底部 --%>
	<%@ include file="/base/include/foot.tpl"%>
</body>
</html>

