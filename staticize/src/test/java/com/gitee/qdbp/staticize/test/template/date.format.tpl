<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>date format</title>
</head>
<body>
	<fmt:date var="d" value="yesterday" pattern="date">昨天    ${d}</fmt:date><br>
	当前时间    <fmt:date /><br>
	pattern="date"    <fmt:date value="yesterday" pattern="date" /><br>
	pattern="time"    <fmt:date value="${yesterday}" pattern="time" /><br>
	自定义    <fmt:date value="#{yesterday}" pattern="yyyy-MM-dd HH:mm:ss.SSS" /><br>
</body>
</html>
