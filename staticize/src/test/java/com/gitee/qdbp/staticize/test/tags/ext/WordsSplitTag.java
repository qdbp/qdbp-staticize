package com.gitee.qdbp.staticize.test.tags.ext;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import com.gitee.qdbp.staticize.annotation.AttrRequired;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.NextStep;
import com.gitee.qdbp.staticize.test.tags.utils.TagConfig;
import com.gitee.qdbp.tools.utils.VerifyTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 字符串拆分标签
 *
 * @author 邓二宝
 * @version 140612
 */
public class WordsSplitTag extends BaseTag {

    /** 日志对象 **/
    private static final Logger log = LoggerFactory.getLogger(WordsSplitTag.class);

    /** 返回变量名 **/
    private String var;

    /** 待分割的字符串, 如果为空则不处理 **/
    private String string;

    /** 分隔符, 可为空 (如果不设置, 从配置文件中读取) (如果设置为空字符串则逐个字符拆分) **/
    private String separator;

    /** 如果分隔符设置为空字符串则逐个字符拆分, 如 "123"返回["1", "2", "3"] **/
    private boolean isToCharArray;

    /**
     * 设置变量名
     *
     * @param var 变量名
     */
    @AttrRequired
    public void setVar(String var) {
        this.var = var;
    }

    /**
     * 设置待分割的字符串
     *
     * @param string 待分割的字符串
     */
    @AttrRequired
    public void setString(String string) {
        this.string = string;
    }

    /**
     * 设置分隔符
     *
     * @param separator 分隔符
     */
    public void setSeparator(String separator) {
        this.separator = separator;
        this.isToCharArray = VerifyTools.isBlank(separator);
    }

    /**
     * 标签处理方法
     *
     * @return 下一步处理方式
     */
    @Override
    public NextStep doHandle() throws TagException {
        if (VerifyTools.isBlank(string)) {
            return NextStep.EVAL_BODY;
        }

        // 如果分隔符设置为空字符串则逐个字符拆分, 如 "123"返回["1", "2", "3"]
        if (isToCharArray) {
            char[] list = string.toCharArray();
            this.addStackValue(var, list);
            return NextStep.EVAL_BODY;
        }

        Pattern pattern;
        if (separator == null) {
            // 没有指定分隔符, 从配置文件中读取
            TagConfig config = TagConfig.getInstance();
            pattern = Pattern.compile(config.getString("words.separator"));
        } else {
            pattern = Pattern.compile(separator);
        }

        try {
            // 拆分字符串
            String[] words = pattern.split(string);
            List<String> list = Arrays.asList(words);
            // 保存到变量中
            this.addStackValue(var, list);
        } catch (Exception e) {
            log.error("字符串分割错误", e);
        }

        return NextStep.EVAL_BODY;
    }

}
