<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>each tag</title>
</head>
<body>
	#{number} -->  [111, 222, 333, 444, 555, 666, 777, 888, 999, 1000]
	#{ascii}  -->  [ascii]
	--- inline
		111, 222, 333, 444, 555, 666, 777, 888, 999, 1000
	--- {test.number}
		index:0 count:1 begin:0 end:9 first:true last:false value:111
		index:1 count:2 begin:0 end:9 first:false last:false value:222
		index:2 count:3 begin:0 end:9 first:false last:false value:333
		index:3 count:4 begin:0 end:9 first:false last:false value:444
		index:4 count:5 begin:0 end:9 first:false last:false value:555
		index:5 count:6 begin:0 end:9 first:false last:false value:666
		index:6 count:7 begin:0 end:9 first:false last:false value:777
		index:7 count:8 begin:0 end:9 first:false last:false value:888
		index:8 count:9 begin:0 end:9 first:false last:false value:999
		index:9 count:10 begin:0 end:9 first:false last:true value:1000
	--- begin="6" rows="5"
		index:6 count:1 begin:6 end:10 first:true last:false value:6
		index:7 count:2 begin:6 end:10 first:false last:false value:7
		index:8 count:3 begin:6 end:10 first:false last:false value:8
		index:9 count:4 begin:6 end:10 first:false last:false value:9
		index:10 count:5 begin:6 end:10 first:false last:true value:10
	--- {number}
		index:0 count:1 begin:0 end:9 first:true last:false value:111
		index:1 count:2 begin:0 end:9 first:false last:false value:222
		index:2 count:3 begin:0 end:9 first:false last:false value:333
		index:3 count:4 begin:0 end:9 first:false last:false value:444
		index:4 count:5 begin:0 end:9 first:false last:false value:555
		index:5 count:6 begin:0 end:9 first:false last:false value:666
		index:6 count:7 begin:0 end:9 first:false last:false value:777
		index:7 count:8 begin:0 end:9 first:false last:false value:888
		index:8 count:9 begin:0 end:9 first:false last:false value:999
		index:9 count:10 begin:0 end:9 first:false last:true value:1000
	--- {number} begin="5" rows="5"
		index:5 count:1 begin:5 end:9 first:true last:false value:666
		index:6 count:2 begin:5 end:9 first:false last:false value:777
		index:7 count:3 begin:5 end:9 first:false last:false value:888
		index:8 count:4 begin:5 end:9 first:false last:false value:999
		index:9 count:5 begin:5 end:9 first:false last:true value:1000
	--- {number} begin="8" rows="5"
		index:8 count:1 begin:8 end:9 first:true last:false value:999
		index:9 count:2 begin:8 end:9 first:false last:true value:1000
	--- {number} begin="0" rows="0"
	--- {number} begin="0" rows="1"
		index:0 count:1 begin:0 end:0 first:true last:true value:111
	--- {number} begin="2" rows="3"
		index:2 count:1 begin:2 end:4 first:true last:false value:333
		index:3 count:2 begin:2 end:4 first:false last:false value:444
		index:4 count:3 begin:2 end:4 first:false last:true value:555
	--- {number} begin="9" rows="1"
		index:9 count:1 begin:9 end:9 first:true last:true value:1000
	--- {number} begin="20" rows="1"
	--- {number} step="3"
		index:0 count:1 begin:0 end:9 first:true last:false value:111
		index:3 count:2 begin:0 end:9 first:false last:false value:444
		index:6 count:3 begin:0 end:9 first:false last:false value:777
		index:9 count:4 begin:0 end:9 first:false last:true value:1000
	--- {number} begin="1" step="3"
		index:1 count:1 begin:1 end:9 first:true last:false value:222
		index:4 count:2 begin:1 end:9 first:false last:false value:555
		index:7 count:3 begin:1 end:9 first:false last:true value:888
	--- {number} begin="3" step="3"
		index:2 count:1 begin:2 end:9 first:true last:false value:333
		index:5 count:2 begin:2 end:9 first:false last:false value:666
		index:8 count:3 begin:2 end:9 first:false last:true value:999
	--- begin="6" end="5"
		index:6 count:1 begin:6 end:5 first:true last:false value:6
		index:5 count:2 begin:6 end:5 first:false last:true value:5
	--- begin="-1" end="-5"
		index:-1 count:1 begin:-1 end:-5 first:true last:false value:-1
		index:-2 count:2 begin:-1 end:-5 first:false last:false value:-2
		index:-3 count:3 begin:-1 end:-5 first:false last:false value:-3
		index:-4 count:4 begin:-1 end:-5 first:false last:false value:-4
		index:-5 count:5 begin:-1 end:-5 first:false last:true value:-5
	--- begin="-1" end="-10" step="-3"
		index:-1 count:1 begin:-1 end:-10 first:true last:false value:-1
		index:-4 count:2 begin:-1 end:-10 first:false last:false value:-4
		index:-7 count:3 begin:-1 end:-10 first:false last:false value:-7
		index:-10 count:4 begin:-1 end:-10 first:false last:true value:-10
	--- begin="-1" end="-9" step="-3"
		index:-1 count:1 begin:-1 end:-9 first:true last:false value:-1
		index:-4 count:2 begin:-1 end:-9 first:false last:false value:-4
		index:-7 count:3 begin:-1 end:-9 first:false last:true value:-7
	--- {number} step="-1"
		index:9 count:1 begin:9 end:0 first:true last:false value:1000
		index:8 count:2 begin:9 end:0 first:false last:false value:999
		index:7 count:3 begin:9 end:0 first:false last:false value:888
		index:6 count:4 begin:9 end:0 first:false last:false value:777
		index:5 count:5 begin:9 end:0 first:false last:false value:666
		index:4 count:6 begin:9 end:0 first:false last:false value:555
		index:3 count:7 begin:9 end:0 first:false last:false value:444
		index:2 count:8 begin:9 end:0 first:false last:false value:333
		index:1 count:9 begin:9 end:0 first:false last:false value:222
		index:0 count:10 begin:9 end:0 first:false last:true value:111
	--- {number} step="-3"
		index:9 count:1 begin:9 end:0 first:true last:false value:1000
		index:6 count:2 begin:9 end:0 first:false last:false value:777
		index:3 count:3 begin:9 end:0 first:false last:false value:444
		index:0 count:4 begin:9 end:0 first:false last:true value:111
	--- {number} begin="8" end="5"
		index:8 count:1 begin:8 end:5 first:true last:false value:999
		index:7 count:2 begin:8 end:5 first:false last:false value:888
		index:6 count:3 begin:8 end:5 first:false last:false value:777
		index:5 count:4 begin:8 end:5 first:false last:true value:666
	--- {number} end="3" step="-1"
		index:9 count:1 begin:9 end:3 first:true last:false value:1000
		index:8 count:2 begin:9 end:3 first:false last:false value:999
		index:7 count:3 begin:9 end:3 first:false last:false value:888
		index:6 count:4 begin:9 end:3 first:false last:false value:777
		index:5 count:5 begin:9 end:3 first:false last:false value:666
		index:4 count:6 begin:9 end:3 first:false last:false value:555
		index:3 count:7 begin:9 end:3 first:false last:true value:444
	--- {ascii}
		index:0 count:1 begin:0 end:0 first:true last:true value:ascii
	--- {ascii} begin="0" rows="1"
		index:0 count:1 begin:0 end:0 first:true last:true value:ascii
	--- {ascii} begin="0" rows="2"
		index:0 count:1 begin:0 end:0 first:true last:true value:ascii
	--- {ascii} begin="1" rows="2"
	--- {ascii} 20
	--- begin="6" rows="5"
		index:6 count:1 begin:6 end:10 first:true last:false value:6
		index:7 count:2 begin:6 end:10 first:false last:false value:7
		index:8 count:3 begin:6 end:10 first:false last:false value:8
		index:9 count:4 begin:6 end:10 first:false last:false value:9
		index:10 count:5 begin:6 end:10 first:false last:true value:10
	--- begin="3" rows="3" x begin="600" rows="4"
			index:600 count:1 begin:600 end:603 first:true last:false value:600
			index:601 count:2 begin:600 end:603 first:false last:false value:601
			index:602 count:3 begin:600 end:603 first:false last:false value:602
			index:603 count:4 begin:600 end:603 first:false last:true value:603
		index:3 count:1 begin:3 end:5 first:true last:false value:3
			index:600 count:1 begin:600 end:603 first:true last:false value:600
			index:601 count:2 begin:600 end:603 first:false last:false value:601
			index:602 count:3 begin:600 end:603 first:false last:false value:602
			index:603 count:4 begin:600 end:603 first:false last:true value:603
		index:4 count:2 begin:3 end:5 first:false last:false value:4
			index:600 count:1 begin:600 end:603 first:true last:false value:600
			index:601 count:2 begin:600 end:603 first:false last:false value:601
			index:602 count:3 begin:600 end:603 first:false last:false value:602
			index:603 count:4 begin:600 end:603 first:false last:true value:603
		index:5 count:3 begin:3 end:5 first:false last:true value:5
</body>
</html>
