<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>
<%@ taglib prefix="util" uri="http://www.jeshing.com/tags/util/"%>
<position:detail key="guanyuwomen|huodongtuijian">
	<position:content key="${position.id }">
		<core:forEach var="item" items="${list}" varStatus="s">
			<div class="huadong_pictrue_div aboutus <core:if test="${s.index >0 }">ab_jianju</core:if>">
				<div class="zx_right_top zx_right_top_m">
					<a href="${item.url}">
					<img src="<url:image src='${item.image.src}'/>" alt="${item.image.title}" width="230" height="130"/></a>
				</div>
				<div class="huadong_text huadong-aboutus">
					<div class="font zx_right_top_1_nr1 aboutus_font">
						<div class="about_line">
							<a href="${item.url}">${item.title}</a>
						</div>
					</div>
				</div>
			</div>
		</core:forEach>
	</position:content>
</position:detail>