D:/qdbp/qdbp-staticize/staticize/target/test-classes/com/gitee/qdbp/staticize/test/template/code/
0(0)	<{root}>
package 
1(9)	#{java.vars.current.packages}
;


3(1)	<core:each
	items="#{imports}"	(String)
	var="item"	(String)
3	>


4(1)	${item}


3:5	</core:each>


/**
 * 
8(4)	#{table.comment}
查询类
 *
 * @author CodeGenerater
 * @version 
11(13)	<fmt:date
	pattern="yyMMdd"	(String)
11:11	/>

 */
public class 
13(14)	#{java.vars.current.capitalizeName}
 extends 
13(58)	#{java.vars.model.bean.capitalizeName}
 {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;
    
17(5)	<core:each
	items="#{table.fields}"	(String)
	var="field"	(String)
17	>

    
18(5)	<core:if
	test="${!field.primaryKey && !field.businessKey && !field.logicallyDeleteField && !field.createTimeField && !field.updateTimeField}"	(String)
18	>

    /** 
19(9)	${field.comment}
空值/非空值查询 **/
    private Boolean 
20(21)	${field.normativeName}
IsNull;
    
18:21	</core:if>

    
22(5)	<core:if
	test="${field.listData != null}"	(String)
22	>

    /** 
23(9)	${field.comment}
列表 **/
    private List<
24(18)	${field.dataType.typedName}
> 
24(47)	${field.listData.normativeName}
;
    
22:25	</core:if>

    
26(5)	<core:if
	test="${field.dataType.stringType}"	(String)
26	>

    /** 
27(9)	${field.comment}
前模匹配条件 **/
    private 
28(13)	${field.dataType.typedName}
 
28(41)	${field.normativeName}
Starts;
    /** 
29(9)	${field.comment}
后模匹配条件 **/
    private 
30(13)	${field.dataType.typedName}
 
30(41)	${field.normativeName}
Ends;
    /** 
31(9)	${field.comment}
模糊查询条件 **/
    private 
32(13)	${field.dataType.typedName}
 
32(41)	${field.normativeName}
Like;
    
26:33	</core:if>

    
34(5)	<core:if
	test="${field.dataType.typedName == 'Date'}"	(String)
34	>

    /** 最小
35(11)	${field.comment}
 **/
    private 
36(13)	${field.dataType.typedName}
 
36(41)	${field.normativeName}
Min;
    /** 最大
37(11)	${field.comment}
 **/
    private 
38(13)	${field.dataType.typedName}
 
38(41)	${field.normativeName}
Max;
    /** 最小
39(11)	${field.comment}
 **/
    private 
40(13)	${field.dataType.typedName}
 
40(41)	${field.normativeName}
MinWithDay;
    /** 最大
41(11)	${field.comment}
 **/
    private 
42(13)	${field.dataType.typedName}
 
42(41)	${field.normativeName}
MaxWithDay;
    
34:43	</core:if>

    
44(5)	<core:if
	test="${field.dataType.typedName == 'Integer' || field.dataType.typedName == 'Long' || field.dataType.typedName == 'Double' || field.dataType.typedName == 'Float'}"	(String)
44	>

    /** 最小
45(11)	${field.comment}
 **/
    private 
46(13)	${field.dataType.typedName}
 
46(41)	${field.normativeName}
Min;
    /** 最大
47(11)	${field.comment}
 **/
    private 
48(13)	${field.dataType.typedName}
 
48(41)	${field.normativeName}
Max;
    
44:49	</core:if>

    
17:50	</core:each>

    
51(5)	<core:each
	items="#{table.indexes}"	(String)
	var="index"	(String)
51	>

    	
52(9)	<core:if
	test="${index.fulltext}"	(String)
52	>

    /** 按
53(10)	${index.capitalizeName}
搜索 **/
    private String searchBy
54(28)	${index.capitalizeName}
;
    	
52:55	</core:if>

    
51:56	</core:each>

    
57(5)	<core:each
	items="#{table.fields}"	(String)
	var="field"	(String)
57	>

    
58(5)	<core:if
	test="${!field.primaryKey && !field.businessKey && !field.logicallyDeleteField && !field.createTimeField && !field.updateTimeField}"	(String)
58	>


    /** 判断
60(11)	${field.comment}
是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean get
61(23)	${field.capitalizeName}
IsNull() {
        return 
62(16)	${field.normativeName}
IsNull;
    }
    
64(5)	<core:if
	test="${field.notNullField}"	(String)
64	>


    /**
     * 设置
67(10)	${field.comment}
空值查询(true:空值查询|false:非空值查询)
     *
     * @param 
69(15)	${field.normativeName}
IsNull 
69(44)	${field.comment}
空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    
64:73	</core:if>

    
74(5)	<core:else
74	>


    /** 设置
76(11)	${field.comment}
空值查询(true:空值查询|false:非空值查询) **/
    
74:77	</core:else>

    public void set
78(20)	${field.capitalizeName}
IsNull(Boolean 
78(58)	${field.normativeName}
IsNull) {
        this.
79(14)	${field.normativeName}
IsNull = 
79(45)	${field.normativeName}
IsNull;
    }
    
58:81	</core:if>

    
82(5)	<core:if
	test="${field.listData != null}"	(String)
82	>


    /** 获取
84(11)	${field.comment}
列表 **/
    public List<
85(17)	${field.dataType.typedName}
> get
85(49)	${field.listData.capitalizeName}
() {
        return 
86(16)	${field.listData.normativeName}
;
    }

    /** 设置
89(11)	${field.comment}
列表 **/
    public void set
90(20)	${field.listData.capitalizeName}
(List<
90(58)	${field.dataType.typedName}
> 
90(87)	${field.listData.normativeName}
) {
        this.
91(14)	${field.listData.normativeName}
 = 
91(48)	${field.listData.normativeName}
;
    }

    /** 增加
94(11)	${field.comment}
 **/
    public void add
95(20)	${field.capitalizeName}
(
95(44)	${field.dataType.typedName}
... 
95(75)	${field.listData.normativeName}
) {
        if (this.
96(18)	${field.listData.normativeName}
 == null) {
            this.
97(18)	${field.listData.normativeName}
 = new ArrayList<>();
        }
        this.
99(14)	${field.listData.normativeName}
.addAll(Arrays.asList(
99(67)	${field.listData.normativeName}
));
    }
    
82:101	</core:if>

    
102(5)	<core:if
	test="${field.dataType.stringType}"	(String)
102	>


    /** 获取
104(11)	${field.comment}
前模匹配条件 **/
    public 
105(12)	${field.dataType.typedName}
 get
105(43)	${field.capitalizeName}
Starts() {
        return 
106(16)	${field.normativeName}
Starts;
    }

    /** 设置
109(11)	${field.comment}
前模匹配条件 **/
    public void set
110(20)	${field.capitalizeName}
Starts(
110(50)	${field.dataType.typedName}
 
110(78)	${field.normativeName}
Starts) {
        this.
111(14)	${field.normativeName}
Starts = 
111(45)	${field.normativeName}
Starts;
    }

    /** 获取
114(11)	${field.comment}
后模匹配条件 **/
    public 
115(12)	${field.dataType.typedName}
 get
115(43)	${field.capitalizeName}
Ends() {
        return 
116(16)	${field.normativeName}
Ends;
    }

    /** 设置
119(11)	${field.comment}
后模匹配条件 **/
    public void set
120(20)	${field.capitalizeName}
Ends(
120(48)	${field.dataType.typedName}
 
120(76)	${field.normativeName}
Ends) {
        this.
121(14)	${field.normativeName}
Ends = 
121(43)	${field.normativeName}
Ends;
    }

    /** 获取
124(11)	${field.comment}
模糊查询条件 **/
    public 
125(12)	${field.dataType.typedName}
 get
125(43)	${field.capitalizeName}
Like() {
        return 
126(16)	${field.normativeName}
Like;
    }

    /** 设置
129(11)	${field.comment}
模糊查询条件 **/
    public void set
130(20)	${field.capitalizeName}
Like(
130(48)	${field.dataType.typedName}
 
130(76)	${field.normativeName}
Like) {
        this.
131(14)	${field.normativeName}
Like = 
131(43)	${field.normativeName}
Like;
    }
    
102:133	</core:if>

    
134(5)	<core:if
	test="${field.dataType.typedName == 'Date'}"	(String)
134	>


    /** 获取最小
136(13)	${field.comment}
 **/
    public 
137(12)	${field.dataType.typedName}
 get
137(43)	${field.capitalizeName}
Min() {
        return 
138(16)	${field.normativeName}
Min;
    }

    /** 设置最小
141(13)	${field.comment}
 **/
    public void set
142(20)	${field.capitalizeName}
Min(
142(47)	${field.dataType.typedName}
 
142(75)	${field.normativeName}
Min) {
        this.
143(14)	${field.normativeName}
Min = 
143(42)	${field.normativeName}
Min;
    }

    /** 获取最大
146(13)	${field.comment}
 **/
    public 
147(12)	${field.dataType.typedName}
 get
147(43)	${field.capitalizeName}
Max() {
        return 
148(16)	${field.normativeName}
Max;
    }

    /** 设置最大
151(13)	${field.comment}
 **/
    public void set
152(20)	${field.capitalizeName}
Max(
152(47)	${field.dataType.typedName}
 
152(75)	${field.normativeName}
Max) {
        this.
153(14)	${field.normativeName}
Max = 
153(42)	${field.normativeName}
Max;
    }

    /** 获取最小
156(13)	${field.comment}
 **/
    public 
157(12)	${field.dataType.typedName}
 get
157(43)	${field.capitalizeName}
MinWithDay() {
        return 
158(16)	${field.normativeName}
MinWithDay;
    }

    /** 设置最小
161(13)	${field.comment}
 **/
    public void set
162(20)	${field.capitalizeName}
MinWithDay(
162(54)	${field.dataType.typedName}
 
162(82)	${field.normativeName}
MinWithDay) {
        this.
163(14)	${field.normativeName}
MinWithDay = 
163(49)	${field.normativeName}
MinWithDay;
    }

    /** 获取最大
166(13)	${field.comment}
 **/
    public 
167(12)	${field.dataType.typedName}
 get
167(43)	${field.capitalizeName}
MaxWithDay() {
        return 
168(16)	${field.normativeName}
MaxWithDay;
    }

    /** 设置最大
171(13)	${field.comment}
 **/
    public void set
172(20)	${field.capitalizeName}
MaxWithDay(
172(54)	${field.dataType.typedName}
 
172(82)	${field.normativeName}
MaxWithDay) {
        this.
173(14)	${field.normativeName}
MaxWithDay = 
173(49)	${field.normativeName}
MaxWithDay;
    }
    
134:175	</core:if>

    
176(5)	<core:if
	test="${field.dataType.typedName == 'Integer' || field.dataType.typedName == 'Long' || field.dataType.typedName == 'Double' || field.dataType.typedName == 'Float'}"	(String)
176	>


    /** 获取最小
178(13)	${field.comment}
 **/
    public 
179(12)	${field.dataType.typedName}
 get
179(43)	${field.capitalizeName}
Min() {
        return 
180(16)	${field.normativeName}
Min;
    }

    /** 设置最小
183(13)	${field.comment}
 **/
    public void set
184(20)	${field.capitalizeName}
Min(
184(47)	${field.dataType.typedName}
 
184(75)	${field.normativeName}
Min) {
        this.
185(14)	${field.normativeName}
Min = 
185(42)	${field.normativeName}
Min;
    }

    /** 获取最大
188(13)	${field.comment}
 **/
    public 
189(12)	${field.dataType.typedName}
 get
189(43)	${field.capitalizeName}
Max() {
        return 
190(16)	${field.normativeName}
Max;
    }

    /** 设置最大
193(13)	${field.comment}
 **/
    public void set
194(20)	${field.capitalizeName}
Max(
194(47)	${field.dataType.typedName}
 
194(75)	${field.normativeName}
Max) {
        this.
195(14)	${field.normativeName}
Max = 
195(42)	${field.normativeName}
Max;
    }
    
176:197	</core:if>

    
57:198	</core:each>

    
199(5)	<core:each
	items="#{table.indexes}"	(String)
	var="index"	(String)
199	>

    	
200(9)	<core:if
	test="${index.fulltext}"	(String)
200	>


    /** 获取按
202(12)	${index.capitalizeName}
搜索的条件 **/
    public String getSearchBy
203(30)	${index.capitalizeName}
() {
        return searchBy
204(24)	${index.capitalizeName}
;
    }

    /** 设置按
207(12)	${index.capitalizeName}
搜索的条件 **/
    public void setSearchBy
208(28)	${index.capitalizeName}
(String searchBy
208(67)	${index.capitalizeName}
) {
        this.searchBy
209(22)	${index.capitalizeName}
 = searchBy
209(56)	${index.capitalizeName}
;
    }
    	
200:211	</core:if>

    
199:212	</core:each>


    @Override
    public String toString() {
        return JsonTools.toLogString(this);
    }
}
0:218	</{root}>
