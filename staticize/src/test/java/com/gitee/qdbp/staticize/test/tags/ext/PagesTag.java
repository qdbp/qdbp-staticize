package com.gitee.qdbp.staticize.test.tags.ext;

import java.io.IOException;
import java.text.MessageFormat;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.NextStep;
import com.gitee.qdbp.staticize.test.tags.constant.ContextKeys;
import com.gitee.qdbp.staticize.test.tags.utils.TagConfig;
import com.gitee.qdbp.staticize.test.tags.utils.UrlData;
import com.gitee.qdbp.staticize.utils.TagUtils;

public class PagesTag extends BaseTag {

    /** 分页div的id **/
    private String id;

    /** 分页div的name **/
    private String name;

    /** 分页div的class **/
    private String classes;

    /** 分页div的style **/
    private String style;

    /** 当前是第几页 **/
    private int index = 0;

    /** 一共多少行 **/
    private int count = 0;

    /** 是否需要计算总页数, 如果设置了总行数则需要计算 **/
    private boolean isEvalPages;

    /** 当前页前后显示几页, 其余的用省略号 **/
    private int number = 2;

    /** 显示行数(可为空, 默认=10) **/
    private int rows = 10;

    /**
     * 取得row
     *
     * @return 返回row。
     */
    public int getRows() {
        return rows;
    }

    /**
     * 设置显示行数
     *
     * @param rows 要设置的显示行数
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

    /**
     * 设置分页div的id
     *
     * @param id 要设置的id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 设置分页div的name
     *
     * @param name 要设置的name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 设置分页div的class
     *
     * @param classes 要设置的class
     */
    public void setClass(String classes) {
        this.classes = classes;
    }

    /**
     * 设置分页div的style
     *
     * @param style 要设置的style
     */
    public void setStyle(String style) {
        this.style = style;
    }

    /**
     * 设置当前是第几页
     *
     * @param index 要设置的页数
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * 设置一共多少行
     *
     * @param count 要设置的行数
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * 设置当前页前后各显示多少页
     *
     * @param number 要设置的number页数
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /** {@inheritDoc} **/
    @Override
    public NextStep doHandle() throws TagException, IOException {

        TagConfig config = TagConfig.getInstance();
        // 获取配置文件中的内容
        String box = config.getString("pages.box");

        // 用来设置div的属性
        StringBuilder attrs = new StringBuilder();
        attrs.append(TagUtils.createAttribute("id", id));
        attrs.append(TagUtils.createAttribute("name", name));
        attrs.append(TagUtils.createAttribute("class", classes));
        attrs.append(TagUtils.createAttribute("style", style));

        int pages = 0;
        // 如果未设置总行数则从预置数据中获取总页数
        if (!isEvalPages) {
            pages = getPresetValue(ContextKeys.PAGING_COUNT, Integer.class);
        }
        // 根据总行数计算总页数
        else if (count == 0) {
            pages = count % rows == 0 ? count / rows : count / rows + 1;
        }

        if (index == 0) {
            Integer i = getPresetValue(ContextKeys.PAGING_INDEX, Integer.class);
            index = Math.max(1, Math.min(pages, i == null ? 1 : i));
        }

        // 首页
        String first = getConfig(index > 1, "pages.first.active", "pages.first");
        // 上一页
        String prev = getConfig(index > 1, "pages.prev.active", "pages.prev");
        // 下一页
        String next = getConfig(index < pages, "pages.next.active", "pages.next");
        // 末页
        String last = getConfig(index < pages, "pages.last.active", "pages.last");
        // 省略号
        String ellipsis = config.getString("pages.ellipsis");

        UrlData data = getPresetValue(ContextKeys.URL_DATA, UrlData.class);

        // 分页内容
        StringBuilder content = new StringBuilder();
        // 生成首页
        content.append(createPage(data, first, 1));
        // 生成上一页
        content.append(createPage(data, prev, index - 1));

        // 计算开始页数和结束页数
        int start = Math.max(1, index - number);
        int end = Math.min(pages, index + number);
        if (start - end < number * 2) {
            // 靠近首页
            if (index <= number) {
                end = Math.min(pages, number * 2 + 1);
            } else if (index >= pages - number) {
                start = Math.max(1, pages - number * 2 - 1);
            }
        }
        // 如果只省略了第1页, 就不显示省略号
        if (start <= 2) {
            start = 1;
        }
        // 如果只省略了最后1页, 就不显示省略号
        if (end >= pages - 1) {
            end = pages;
        }

        // 生成页码之前的省略号
        if (start > 2) {
            content.append(ellipsis);
        }
        // 生成页码
        for (int i = start; i <= end; i++) {
            String page = getConfig(index == i, "pages.page.active", "pages.page");
            content.append(createPage(data, page, i));
        }
        // 生成页码之后的省略号
        if (end < pages - 1) {
            content.append(ellipsis);
        }

        // 生成下一页
        content.append(createPage(data, next, index + 1));
        // 生成末页
        content.append(createPage(data, last, pages));

        // 拼接好的分页div
        String div = MessageFormat.format(box, attrs, content);
        // 输出内容
        this.print(div);

        return NextStep.SKIP_BODY;
    }

    /**
     * 生成分页的数字页
     *
     * @param data URL相关数据
     * @param linkPattern 分页格式
     * @param index 页数
     * @return 分页URL
     */
    private String createPage(UrlData data, String linkPattern, int index) {
        String url = data.getPageUrl(index);
        // String.valueOf(index), 因为数字过千位时会出现,号分隔符
        return MessageFormat.format(linkPattern, url, String.valueOf(index));
    }

    /**
     * 根据条件取配置文件内容
     *
     * @param match 是否匹配
     * @param one 如果匹配返回该内容
     * @param two 如果不匹配返回该内容
     * @return 配置内容
     */
    private String getConfig(boolean match, String one, String two) {
        TagConfig config = TagConfig.getInstance();
        return match ? config.getString(one) : config.getString(two);
    }
}
