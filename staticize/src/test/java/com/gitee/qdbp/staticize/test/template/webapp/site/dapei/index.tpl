<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="column" uri="http://www.jeshing.com/tags/column/"%>
<%@ taglib prefix="content" uri="http://www.jeshing.com/tags/content/"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>
<%@ taglib prefix="util" uri="http://www.jeshing.com/tags/util/"%>
<%@ taglib prefix="url" uri="http://www.jeshing.com/tags/url/"%>

<!DOCTYPE html>
<html>
<head>
<%@ include file="/base/include/meta.tpl"%>
<column:detail key="shouye">
	<meta name="keywords" content="${column.seo.key}" />
	<meta name="description" content="${column.seo.desc}" />
	<title>${column.seo.title}</title>
</column:detail>
<%@ include file="/base/include/head.tpl"%>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/home.css'/>">
    <link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/decoration_collection.css'/>">
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/dapei.index.css'/>">
	<script type="text/javascript" src="<url:link href='resources/base/js/cyclic/zhh.cyclic.js'/>"></script>
</head>
<body>

	<jsp:include page="/base/include/top.tpl">
		<jsp:param name="column" value="dapei" />
	</jsp:include>

	<div class="clear"></div>
	<div class="centern-big center-dapei">
		<div class="daily-left daily-left-dapei">
			<div class="marquee-box marquee-box-dapei hover mq-box">
			<position:detail key="dapei|lunbotu">
				<position:content key="${position.id}" rows="3">
				<div>
					<ul class="marquee-list marquee-lis-dapei  zhh-cyclic" data-box=".marquee-box" data-pointer-item=".mq-pointer">
						<div style="width: 100%;">
							<core:forEach var="item" items="${list}" varStatus="s">
							<li>
								<div class="marquee-item">
									<a href="<url:link href='${item.url}'/>" title="${item.title }" target="_blank">
										<img  src="<url:image src='${item.image.src}'/>" width="845" height="385" alt="${item.title }">
									</a>
								</div>
							</li>
							</core:forEach>
						</div>
					</ul>
					<ul class="pointer pointer-ch">
						<core:forEach var="item" items="${list}" varStatus="s">
							<li class="mq-pointer"><a href="#${s.index}" title="${item.title}"></a></li>
						</core:forEach>
					</ul>
				</div>
				<div class="clear"></div>
				</position:content>
				</position:detail>
			</div>
		</div>

		<div class="daily-right daily-right-dapei">
			<position:detail key="dapei|tebiecehua">
			<position:content key="${position.id}" rows="7">
			<div class="daily-right-head daily-right-head-dapei">
				<span class="daily-right-head-icon daily-right-head-icon-dapei"></span>
				<span class="daily-right-head-title">${position.title }</span>
			</div>
			<core:forEach var="item" items="${list}" varStatus="s">
			<core:if test="${s.index==0 }">
				<div class="tuwen">
					<a class="images" href="<url:link href='${item.url}'/>" title="${item.title }">
						<img src="<url:image src='${item.image.src}'/>" width="121" height="91" alt="${item.image.title }" />
					</a>
					<a class="title" href="<url:link href='${item.url}'/>" title="${item.title}" >${item.title }</a>
					<a class="text"  href="<url:link href='${item.url}'/>" title="${item.title}">${item.digest }</a>
				</div>
			</core:if>
			</core:forEach>

			<div class="list">
				<core:forEach var="item" items="${list}" varStatus="s">
					<core:if test="${s.index > 0 }">
						<div class="list-title">
							<span>
								<a href="<url:link href='${item.url}'/>" title="${item.title}" class="lm-item">${item.shortTitle}</a>
							</span>
							<a href="<url:link href='${item.url}'/>" class="lm-item">${item.title}</a>
						</div>
						<core:if test="${!s.last }">
							<div class="line"></div>
						</core:if>
					</core:if>
				</core:forEach>
			</div>
			</position:content>
			</position:detail>
		</div>
	</div>
	<div class="clear"></div>
	<!--  装饰精粹 -->
	<div class="centern-big center-dapei">
		<position:detail key="dapei|zhuangshijingcui|datu">
			<div>
				<div class="decoration_have_top">
					<div class="daily-left-head-dapei fl">
						<a class="daily-left-head-titlebig" href="<url:link href='${position.url}'/>" >${position.title }</a>
						<a class="daily-left-head-titlesmall" href="<url:link href='${position.url}'/>" >${position.subTitle }</a>
					</div>
					<core:if test="${!empty position.url }">
					<a class="fr more bg5" href="<url:link href='${position.url}'/>"></a>
					</core:if>
				</div>
				<div class="decoration_border fr"></div>
				<div class="clear"></div>
			</div>
		</position:detail>
		<div class="decoration-content">
			<div class="decoration-content-left">
				<position:detail key="dapei|zhuangshijingcui|datu">
				<position:content key="${position.id}" rows="1">
					<core:forEach var="item" items="${list}" varStatus="s">
					<a class="images" href="<url:link href='${item.url}'/>" title="${item.title }">
						<img src="<url:image src='${item.image.src}'/>" width="410" height="242" alt="${item.title }" />
					</a>
					<div class="content">
						<div class="title">
							<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title }</a>
						</div>
						<div class="text">${item.digest }</div>
						<content:detail key="${item.contentId}">
						<div class="tags">
							<core:if test="${content.tagword!=null}">
								<util:split string="${content.tagword}" var="tagwords">
									<span>相关标签：</span>
										<core:forEach var="word" items="${tagwords}" varStatus="s" begin="0" end="5">
											<a   href="<url:link href='${item.url}'/>"   >${word}</a>
										</core:forEach>
									</util:split>
								</core:if>
						</div>
						</content:detail>
					</div>
					</core:forEach>
				</position:content>
				</position:detail>
				<position:detail key="dapei|zhuangshijingcui|xiaotu">
				<position:content key="${position.id}" rows="3">
					<core:forEach var="item" items="${list}" varStatus="s">
						<div class="content-daipei <core:if test="${s.last}"> content-daipei-last </core:if> ">
							<a class="images-dp" href="<url:link href='${item.url}'/>" title="${item.title }">
								<img src="<url:image src='${item.image.src}'/>" width="265" height="185" alt="${item.title }" />
							</a>
							<div class="title-dp">
								<a href="<url:link href='${item.url}'/>"  >${item.title }</a>
							</div>
							<div class="text-dp">
								<a href="<url:link href='${item.url}'/>"  >${item.digest }</a>
							</div>
						</div>
					</core:forEach>
				</position:content>
				</position:detail>

			</div>

			<div class="dapei-content-right  ">

				<div class="marquee-box hover mq-box marquee-box-ch">
					<position:detail key="dapei|youceguanggao">
					<position:content key="${position.id}" rows="3">
					<div class="marquee-list marquee-list-ch zhh-marquee" data-box=".mq-box" data-pointer-item=".mq-pointer" data-pointer-curr="circle-icon-selected">

							<core:forEach var="item" items="${list}" varStatus="s">
							<div class="dapei-content-right-img">
								<a class="marquee-item marquee-item-dp" href="<url:link href='${item.url}'/>" title="${item.title }" >
									<img src="<url:image src='${item.image.src}'/>" width="316" height="245" alt="${item.title }" />
									<div class="prompty">
										<span><a href="<url:link href='${item.url}'/>" title="${item.title}" >${item.title }</a></span>
									</div>
								</a>
							</div>
							</core:forEach>

					</div>
					<div class="decoration-circle decoration-circle-dp">
						<core:forEach var="item" items="${list}" varStatus="s">
							<a class="bg1 circle-icon mq-pointer <core:if test="${s.first}">circle-icon-selected</core:if>"></a>
						</core:forEach>
					</div>
					<div class="clear"></div>
					</position:content>
					</position:detail>
				</div>
				<div class="hot-tags">
					<position:detail key="dapei|remenbiaoqian">
					<div class="tags-name png">
						<span class="icon"></span><span>${position.title }</span>
					</div>
					<div class="tags-a">
						<position:content key="${position.id}" rows="100">
						<core:forEach var="item" items="${list}" varStatus="s">
							<a href="<url:link href='${item.url}'/>" title="${item.title}" >${item.title }</a>
						</core:forEach>
						</position:content>
					</div>
					</position:detail>
				</div>
			</div>
		</div>

		<div class="clear"></div>

		<!-- 搭配技巧 -->
		<div class="centern-big collocation-centern">
			<div class="collocation-left collocation-right-ch">
				<position:detail key="dapei|dapeijiqiao|datu">
				<div class="decoration_have_top collocation-left collocation-left-dp-ch">
					<div class="bg5 decoration_evolution_head fl">
						<a class="daily-left-head-titlebig" href="<url:link href='${position.url}'/>" >${position.title }</a>
						<a class="daily-left-head-titlesmall" href="<url:link href='${position.url}'/>" >${position.subTitle }</a>
					</div>
					<core:if test="${!empty position.url }">
					<a class="fr more bg5" href="<url:link href='${position.url}'/>"></a>
					</core:if>
				</div>
				</position:detail>
				<div class="decoration_left_border decoration_left_border-ch fr"></div>
				<div class="clear"></div>

				<div class="tags tags-skills tags-skills-1line">
					<position:detail key="dapei|dapeijiqiaobiaoqian">
					<span>热门标签：</span>
					<position:content key="${position.id}" >
						<core:forEach var="item" items="${list}" varStatus="s">
							<a href="<url:link href='${item.url}'/>" title="${item.title }" >${item.title }</a>
						</core:forEach>
					</position:content>
					</position:detail>
				</div>
				<div class="skills-content">
					<position:detail key="dapei|dapeijiqiao|datu">
					<position:content key="${position.id}" rows="1">
					<core:forEach var="item" items="${list}" varStatus="s">
						<div class="skills-content-left skills-height">
							<a class="images" href="<url:link href='${item.url}'/>"  title="${item.title }">
								<img src="<url:image src='${item.image.src}'/>" width="510" height="335" alt="${item.title }" />
							</a>
							<div class="title margin-bt10" >
								<a href="<url:link href='${item.url}'/>" title="${item.title }">${item.title }</a>
							</div>
							<div class="text margin-bt25">
								<a href="<url:link href='${item.url}'/>" title="${item.title }">${item.digest }</a>
							</div>
							<div class="tags tags-skills tags-skills-ch">
								<content:detail key="${item.contentId}">
									<core:if test="${content.tagword!=null}">
										<util:split string="${content.tagword}" var="tagwords">
												<core:forEach var="word" items="${tagwords}" varStatus="s" begin="0" end="5">
													<a   href="<url:link href='${item.url}'/>" title="${item.title }" >${word}</a>
												</core:forEach>
											</util:split>
										</core:if>
								</content:detail>
							</div>
						</div>
					</core:forEach>
					</position:content>
					</position:detail>

					<position:detail key="dapei|dapeijiqiao|xiaotu">
					<position:content key="${position.id}" rows="2">
					<core:forEach var="item" items="${list}" varStatus="s">

					<div class="skills-content-left skills-content-right">
						<a class="images" href="<url:link href='${item.url}'/>" title="${item.title }">
							<img src="<url:image src='${item.image.src}'/>" width="310"
								height="152" alt="${item.title }" />
						</a>
						<div class="title ">
							<a href="<url:link href='${item.url}'/>" title="${item.title }">${item.title }</a>
						</div>
						<div class="text ">
							<a href="<url:link href='${item.url}'/>" title="${item.title }">${item.digest }</a>
						</div>
					</div>

					</core:forEach>
					</position:content>
					</position:detail>

					<div class="clear"></div>
				</div>
			</div>


			<div class="collocation-right collocation-right-ch">

				<position:detail key="dapei|dapei|lepintuijian">
				<div class="bg3 collocation-right-head">
					<a href="<url:link href='${position.url}'/>"  class="collocation-right-head-title white white-hover">${position.title }</a>
				</div>

				<position:content key="${position.id}" rows="3">
				<div class="decoration_right_height decoration_right_height-dp"></div>
				<div class="collocation-right-head-body-warp" >
					<div class="collocation-right-head-body-list">

						<core:forEach var="item" items="${list}" varStatus="s">
							<div class="collocation-right-head-body">
								<div class="collocation-right-head-body-pic">
									<a title="${item.title }" class="collocation-right-head-body-pic-img" href="<url:link href='${item.url}'/>"  >
										<img src="<url:image src='${item.image.src}'/>" width="140" height="118" alt="${item.title }"/>
									</a>
								</div>
								<div class="collocation-right-head-body-info"  >
									<a class="collocation-right-head-body-info-title" title="${item.title }" >${item.title }</a>
									<span class="collocation-right-head-body-info-oldprice" >参考价：${item.shortTitle }元</span>
									<span class="collocation-right-head-body-info-newprice" >惊爆价：</span>
									<span class="collocation-right-head-body-info-price" >￥${item.digest }</span>
									<a href="<url:link href='${item.url}'/>"  class="bg3 collocation-right-head-body-info-btn" title="${item.title }">去看看</a>
								</div>
								<div class="clear"></div>
							</div>
						</core:forEach>

					</div>
				</div>
				</position:content>
				</position:detail>

			</div>

			<div class="centern-big dp-box">
				<position:detail key="dapei|dapeixiaozhishi">
				<div class="title">
					<span class="icon"></span>
					<util:split string="${position.title }" separator="" var="strs">
					<span class="small-zhishi">
						<core:forEach var="str" items="${strs}" varStatus="ss">
							<core:if test="${ss.index < 2}">
								${str }
							</core:if>
						</core:forEach>
					</span>
					<core:forEach var="str" items="${strs}" varStatus="ss">
							<core:if test="${ss.index > 1}">
								${str }
							</core:if>
					</core:forEach>
					</util:split>
				</div>
				<div class="clear"></div>
				<position:content key="${position.id}" rows="6">
				<div class="dp-box-list">
					<ul>
						<core:forEach var="item" items="${list}" varStatus="s">
						<li class=" <core:if test="${s.index == 0|| s.index ==3}">first </core:if> <core:if test="${s.index < 3}"> border-bottom-line</core:if> " >
							<span class="icon png"></span>
							<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title }</a>
						</li>
						</core:forEach>
						<div class="clear"></div>
					</ul>
				</div>
				</position:content>
				</position:detail>
			</div>
		</div>
		<div class="clear"></div>

		<!-- 收纳技巧 -->
		<div class="centern-big collocation-centern">
			<div class="collocation-left collocation-right-ch">
				<position:detail key="dapei|shounajiqiao|datu">
				<div class="decoration_have_top collocation-left collocation-left-dp-ch">
					<div class="bg5 decoration_evolution_head fl">
						<a class="daily-left-head-titlebig" href="<url:link href='${position.url}'/>" >${position.title }</a>
						<a class="daily-left-head-titlesmall" href="<url:link href='${position.url}'/>" >${position.subTitle }</a>
					</div>
					<core:if test="${!empty position.url }">
					<a class="fr more bg5" href="<url:link href='${position.url}'/>"></a>
					</core:if>
				</div>
				</position:detail>

				<div class="decoration_left_border decoration_left_border-ch fr"></div>
				<div class="clear"></div>

				<div class="tags tags-skills tags-skills-1line">
					<position:detail key="dapei|shounajiqiaobiaoqian">
					<span>热门标签：</span>
					<position:content key="${position.id}" >
						<core:forEach var="item" items="${list}" varStatus="s">
							<a href="<url:link href='${item.url}'/>">${item.title }</a>
						</core:forEach>
					</position:content>
					</position:detail>
				</div>
				<div class="skills-content">
					<position:detail key="dapei|shounajiqiao|datu">
					<position:content key="${position.id}" rows="1">
					<core:forEach var="item" items="${list}" varStatus="s">
					<div class="skills-content-left">
						<a class="images" href="<url:link href='${item.url}'/>" title="${item.title }" >
							<img src="<url:image src='${item.image.src}'/>" width="510" height="335" alt="${item.title }" />
						</a>
						<div class="title margin-bt10">
							<a href="<url:link href='${item.url}'/>" title="${item.title }">${item.title }</a>
						</div>
						<div class="text margin-bt25">
							<a href="<url:link href='${item.url}'/>" title="${item.title }">${item.digest }</a>
						</div>
						<div class="tags tags-skills tags-skills-ch">
							<content:detail key="${item.contentId}">
									<core:if test="${content.tagword!=null}">
										<util:split string="${content.tagword}" var="tagwords">
												<core:forEach var="word" items="${tagwords}" varStatus="s" begin="0" end="5">
													<a   href="<url:link href='${item.url}'/>"  >${word}</a>
												</core:forEach>
											</util:split>
										</core:if>
							</content:detail>
						</div>
					</div>
					</core:forEach>
					</position:content>
					</position:detail>

					<position:detail key="dapei|shounajiqiao|xiaotu">
					<position:content key="${position.id}" rows="2">
					<core:forEach var="item" items="${list}" varStatus="s">

					<div class="skills-content-left skills-content-right">
						<a class="images" href="<url:link href='${item.url}'/>" title="${item.title }">
							<img src="<url:image src='${item.image.src}'/>" width="310"
								height="152" alt="${item.title }" />
						</a>
						<div class="title ">
							<a href="<url:link href='${item.url}'/>" title="${item.title }">${item.title }</a>
						</div>
						<div class="text ">
							<a href="<url:link href='${item.url}'/>" title="${item.title }">${item.digest }</a>
						</div>
					</div>

					 </core:forEach>
					</position:content>
					</position:detail>


					<div class="clear"></div>
				</div>
			</div>
			<div class="collocation-right collocation-right-ch">
				<position:detail key="dapei|shouna|lepintuijian">
				<div class="bg3 collocation-right-head collocation-right-head-dapei">
					<a href="<url:link href='${position.url}'/>"  class="collocation-right-head-title collocation-right-head-title-dp white white-hover">${position.title }</a>
				</div>
				<div class="decoration_right_height decoration_right_height-dp"></div>
				<position:content key="${position.id}" rows="3">
				<div class="collocation-right-head-body-warp">
					<div class="collocation-right-head-body-list">
						<core:forEach var="item" items="${list}" varStatus="s">
						<div class="collocation-right-head-body">
							<div class="collocation-right-head-body-pic">
								<a title="${item.title }" class="collocation-right-head-body-pic-img" href="<url:link href='${item.url}'/>" >
									<img src="<url:image src='${item.image.src}'/>" width="140" height="118" alt="${item.title }"/>
								</a>
							</div>
							<div class="collocation-right-head-body-info"  >
								<a class="collocation-right-head-body-info-title" title="${item.title }">${item.title }</a>
								<span class="collocation-right-head-body-info-oldprice" >参考价：${item.shortTitle }元</span>
								<span class="collocation-right-head-body-info-newprice" >惊爆价：</span>
								<span class="collocation-right-head-body-info-price" >￥${item.digest }</span>
								<a href="<url:link href='${item.url}'/>"  class="bg3 collocation-right-head-body-info-btn" >去看看</a>
							</div>
							<div class="clear"></div>
						</div>
						</core:forEach>
					</div>
				</div>
				</position:content>
				</position:detail>
		</div>

			<div class="centern-big dp-box">
				<position:detail key="dapei|shounaxiaozhishi">
				<div class="title">
					<span class="icon"></span>
					<util:split string="${position.title }" separator="" var="strs">
					<span class="small-zhishi">
						<core:forEach var="str" items="${strs}" varStatus="ss">
							<core:if test="${ss.index < 2}">
								${str }
							</core:if>
						</core:forEach>
					</span>
					<core:forEach var="str" items="${strs}" varStatus="ss">
							<core:if test="${ss.index > 1}">
								${str }
							</core:if>
					</core:forEach>
					</util:split>
				</div>
				<div class="clear"></div>
				<position:content key="${position.id}" rows="6">
				<div class="dp-box-list">
					<ul>
						<core:forEach var="item" items="${list}" varStatus="s">
						<li class=" <core:if test="${s.index == 0|| s.index ==3}">first </core:if> <core:if test="${s.index < 3}"> border-bottom-line</core:if> " >
							<span class="icon png"></span>
							<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title }</a>
						</li>
						</core:forEach>
						<div class="clear"></div>
					</ul>
				</div>
				</position:content>
				</position:detail>
			</div>
		</div>
		<div class="clear"></div>


		<!-- 健康保养 -->
		<div class="centern-big collocation-centern">
			<div>

				<position:detail key="dapei|jiankangbaoyang">
				<div class="decoration_have_top">
					<div class="bg5 decoration_fengshui_head decoration_fengshui_head-ch fl">
						<a class="daily-left-head-titlebig" href="<url:link href='${position.url}'/>" >${position.title }</a>
						<a class="daily-left-head-titlesmall" href="<url:link href='${position.url}'/>" >${position.subTitle }</a>
					</div>
					<core:if test="${!empty position.url }">
					<a class="fr more bg5" href="<url:link href='${position.url}'/>" ></a>
					</core:if>
				</div>
				<div class="decoration_border fr"></div>
				<div class="clear"></div>
				</position:detail>
			</div>
			<div class="collocation-left collocation-right-ch">
				<div class="tags tags-skills tags-skills-1line">
					<position:detail key="dapei|jiankangbaoyangbiaoqian">
					<span>热门标签：</span>
					<position:content key="${position.id}" >
						<core:forEach var="item" items="${list}" varStatus="s">
							<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title }</a>
						</core:forEach>
					</position:content>
					</position:detail>
				</div>
				<div class="centern-big">
					<position:detail key="dapei|jiankangbaoyang">
					<position:content key="${position.id}" rows="3">
						<core:forEach var="item" items="${list}" varStatus="s">
							<div class="content-dapei <core:if test="${s.index==2}"> content-daipei-last </core:if>  ">
								<a class="images" href="<url:link href='${item.url}'/>" title="${item.title }">
									<img alt="${item.title }" src="<url:image src='${item.image.src}'/>" width="385" height="221" />
								</a>
								<div class="title">
									<a href="<url:link href='${item.url}'/>" title="${item.title }">${item.title }</a>
								</div>
								<div class="text">
									<a href="<url:link href='${item.url}'/>" title="${item.title }">${item.digest }</a>
								</div>
							</div>
						</core:forEach>
					</position:content>
					</position:detail>
				</div>
			</div>


			<div class="centern-big dp-box">
				<position:detail key="dapei|baoyangxiaozhishi">
				<div class="title">
					<span class="icon"></span>
					<util:split string="${position.title }" separator="" var="strs">
					<span class="small-zhishi">
						<core:forEach var="str" items="${strs}" varStatus="ss">
							<core:if test="${ss.index < 2}">
								${str }
							</core:if>
						</core:forEach>
					</span>
					<core:forEach var="str" items="${strs}" varStatus="ss">
							<core:if test="${ss.index > 1}">
								${str }
							</core:if>
					</core:forEach>
					</util:split>
				</div>
				<div class="clear"></div>
				<position:content key="${position.id}" rows="6">
				<div class="dp-box-list">
					<ul>
						<core:forEach var="item" items="${list}" varStatus="s">
						<li class=" <core:if test="${s.index == 0|| s.index ==3}">first </core:if> <core:if test="${s.index < 3}"> border-bottom-line</core:if> " >
							<span class="icon png"></span>
							<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title }</a>
						</li>
						</core:forEach>
						<div class="clear"></div>
					</ul>
				</div>
				</position:content>
				</position:detail>
			</div>
		</div>
		<div class="clear"></div>

		<%@ include file="/base/include/foot.tpl"%>
</body>
</html>

