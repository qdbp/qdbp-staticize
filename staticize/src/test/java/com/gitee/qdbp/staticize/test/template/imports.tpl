<core:import>java.util.Date</core:import>
<%--<core:import>com.gitee.qdbp.tools.utils.DateTools</core:import>--%>
<%--<core:import>com.gitee.qdbp.tools.utils.StringTools</core:import>--%>
<core:import>com.gitee.qdbp.tools.utils.NamingTools</core:import>
<core:import>com.alibaba.fastjson.JSON</core:import>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>core:import</title>
</head>
<body>
	${@DateTools.toNormativeString(@DateTools.parse('2018/8/20 15:25:35'))}

	${@NamingTools.toCamelString('user_name')}

	${phone}<core:if test="@StringTools.isPhone(phone)"> is phone.</core:if>

	<core:block>
		<core:set var="phoneNumber" value="phone" />
		phoneNumber: ${phoneNumber}
	</core:block>

	<core:if test="phoneNumber == null">
		phoneNumber not found!
	</core:if>

    <core:comment>phone2此变量不存在, 将当作字符串传递</core:comment>
	<core:block>
		<core:set var="phoneNumber" value="phone2" />
		phoneNumber: ${phoneNumber}
	</core:block>

	<core:if test="phoneNumber == null">
		phoneNumber not found!
	</core:if>

	<core:block>
		<core:set var="string" value="'This is a string!'" />
		string: ${string}
	</core:block>

	<core:block>
		<core:set var="json" value="@JSON.parse(\"{content:'Let\\\\'s go!'}\")" />
		json content: ${json.content}
	</core:block>

	<core:if test="json == null">
		json not found!
	</core:if>

	<core:block>
		<core:set var="json" value="${@JSON.parse(\"{content:'Let\\\\'s go!'}\")}">
		</core:set>
		json content: ${json.content}
	</core:block>

	<core:if test="${json == null}">
		json not found!
	</core:if>

	<core:block>
		<core:set var="json">
			${@JSON.parse("{content:'Let\\'s go!'}")}
		</core:set>
		json content: ${json.content}
	</core:block>

	<core:if test="${json == null}">
		json not found!
	</core:if>

	json content: ${@JSON.parse("{content:'Let\\'s go!'}").getString("content")}
</body>
</html>
