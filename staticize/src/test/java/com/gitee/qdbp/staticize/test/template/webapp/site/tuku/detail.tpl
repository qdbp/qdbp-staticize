<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="column" uri="http://www.jeshing.com/tags/column/"%>
<%@ taglib prefix="content" uri="http://www.jeshing.com/tags/content/"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>
<%@ taglib prefix="util" uri="http://www.jeshing.com/tags/util/"%>
<%@ taglib prefix="url" uri="http://www.jeshing.com/tags/url/"%>
<%@ taglib prefix="imgs" uri="http://www.jeshing.com/tags/images/"%>

<!DOCTYPE html>

<imgs:detail id="${URLDATA.id}" index="${URLDATA.index}">
<html>
<head>
	<%@ include file="/base/include/meta.tpl"%>
	<meta name="keywords" content="${detail.keyword}" />
	<meta name="description" content="${detail.digest}" />
	<title>${detail.title}</title>
	<%@ include file="/base/include/head.tpl"%>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/anli.css'/>">
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/anli.detail.css'/>">
	<script type="text/javascript" src="<url:link href='resources/site/js/anli.js'/>"></script>
	<script type="text/javascript" src="<url:link href='resources/site/js/anli.detail.js'/>"></script>

	<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/anli.ie.css'/>">
	<![endif]-->
    <core:if test="${param.preview!=null}">
    <script type="text/javascript" src="<url:link href='resources/site/js/preview.js'/>"></script>
    </core:if>
</head>
<body>
	<jsp:include page="/base/include/top.tpl" >
		<jsp:param name="column" value="anli" />
	</jsp:include>

	<%--页面主体--%>
	<%-- 案例图库 开始 --%>
	<div class="centern title-box title-box-noborder">
		<a class="link" href="#">案例图库</a>
		<div class="decoration-nav">
			<a href="<url:link/>">首页</a>
			<column:detail key="anli">
				<core:forEach var="parent" varStatus="p" items="${parents}">
				<core:if test="${parent.url != null}">
				<span> &gt; </span>
				<a href="<url:list column="anli"/>">${parent.title}</a>
				</core:if>
				</core:forEach>
				<span> &gt; </span>
				${root.shortTitle}详情
			</column:detail>
		</div>
		<a name="title" class="maodian"></a>
	</div>
	<div class="centern center-case">
		<div class="title">
		${detail.title}
		<core:if test="${detail.images.size()==0}">
			<span>(0</span><span> / 0)</span>
		</core:if>
		<core:if test="${detail.images.size()>0}">
			<span>(<core:if test="${URLDATA.index==0}">${URLDATA.index+1}</core:if><core:if test="${URLDATA.index>0}">${URLDATA.index}</core:if></span><span>/ ${detail.images.size()})</span>
		</core:if>
		</div>
		<div class="subtitle"><core:if test="${detail.subTitle!=null}">——${detail.subTitle}</core:if></div>

		<%-- 顶部大图 --%>
		<div class="marquee-list">
			<div class="marquee-wrap">
				<div class="marquee-item">
					<img src="<url:image src='${image.src}'/>" alt="${image.name}" /> <%-- width="910" height="520" --%>
				</div>
			</div>
			<url:page count="${detail.images.size()}" rows="1" prev="prev" next="next" first="first">
			<a class="prev preview <core:if test="${prev==null}">disabled</core:if>" <core:if test="${prev!=null}">href="${prev}#title"</core:if>></a>
			<a class="next preview <core:if test="${next==null}">disabled</core:if>" <core:if test="${next!=null}">href="${next}#title"</core:if>></a>

			<core:if test="${index==detail.images.size()}">
			<div class="next-wrap">
				<div class="bg"></div>
				<a class="close"></a>
				<div class="next-box">
					<div class="btn-box">
						<div class="left-img"><a href="<url:link href='${nextImages.url}'/>" class="img-link"></a><img src="<url:image src='${nextImages.image.src}'/>" alt="${nextImages.title}" width="140" height="140" /></div>
						<div class="right-btn">
							<span>${nextImages.title}</span>
							<a class="preview" href="<url:link href='${first}'/>">重新播放</a>
							<core:if test="${nextImages != null}"><a class="next-anli" href="<url:link href='${nextImages.url}'/>">下一案例</a></core:if>
						</div>
					</div>
					<p>猜你喜欢</p>
					<div class="img-box">
						<position:detail key="anli|xiangqing|cainixihuan">
							<position:content key="${position.id}" rows="3">
								<core:forEach var="item" items="${list}">
									<div class="image-item">
										<a href="${item.url }"><img src="<url:image src='${item.image.src}'/>" alt="${item.image.title}" width="140" height="140" /></a>
										<div class="text"><a href="${item.url}">${item.title}</a></div>
									</div>
								</core:forEach>
							</position:content>
						</position:detail>
					</div>
				</div>
			</div>
			</core:if>
			</url:page>
		</div>


		<%-- 底部缩略图 --%>
		<div class="centern best-seller">
			<a class="prev"></a>
			<div class="wrap">
				<div class="list">
					<core:forEach var="img" items="${detail.images}" varStatus="s">
						<div class="list-images">
							<img src="<url:image src='${img.src}'/>" width="150" height="85" alt="${img.name}"/>
							<a class="preview <core:if test="${index==s.index+1}">hover</core:if>" href="<url:link href="${URLDATA.column}/${URLDATA.id}.${s.index+1}.html#title"/>"></a>
						</div>
					</core:forEach>
				</div>
			</div>
			<a class="next"></a>
		</div>

		<div class="separator"></div>

		<div class="content">
			<div class="detail-wrap">
				<div class="huadong_pictrue_div">
					<div class="zx_right_top zx_right_top_m"><a href="#" title=""><img src="<url:link href='resources/site/images/anli/anli_img.png'/>" width="87" height="87" alt=""/></a></div>
						<div class="huadong_text">
							<div class="font zx_right_top_1_nr1">
								<div class="line"><a href="#" title="">元洲装饰</a></div>
							</div>
						</div>

			  	</div>
			  	<div class="huadong_pictrue_div">
					<div class="zx_right_top zx_right_top_m"><a href="#" title=""><img src="<url:link href='resources/site/images/anli/anli_img.png'/>" width="87" height="87" alt=""/></a></div>
						<div class="huadong_text">
							<div class="font zx_right_top_1_nr1">
								<div class="line"><a href="#" title="">元洲装饰</a></div>
							</div>
						</div>

			  	</div>
			  	<div class="huadong_pictrue_div">
					<div class="zx_right_top zx_right_top_m"><a href="#" title=""><img src="<url:link href='resources/site/images/anli/anli_img.png'/>" width="87" height="87" alt=""/></a></div>
						<div class="huadong_text">
							<div class="font zx_right_top_1_nr1">
								<div class="line"><a href="#" title="">元洲装饰</a></div>
							</div>
						</div>

			  	</div>
			<core:if test="${image.desc!=null}"><div class="text">${image.desc}</div></core:if>
			<div class="rel-tag">
				<div class="tag-list">
					<span class="orange">相关标签：</span>
					<core:forEach var="categorys" varStatus="c" items="${detail.categoryInfos}">
						<core:if test="${c.first==false}">|</core:if>
						<imgs:urltype type="${categorys.type}" name="${categorys.name}">
						<span class="word">${categorys.title}</span>
						</imgs:urltype>
					</core:forEach>
				</div>
				<div class="shoucang-wrap">
					<a href="#" class="shoucang" id="storeHref">收藏</a>
					<input type="hidden" value="${detail.id}" id="hidValue" name="hidValue" data-src="<url:link href="web/site/favorite/add.json"/>">
			        <input type="hidden" value="${detail.type.getKey()}" id="hidType">
					<div class="success-prompt" style="display:none;">收藏成功！请继续添加收藏！</div>
				</div>
			</div>
			</div>
			<br class="clear"/>
		</div>

	</div>

	<imgs:recommend contentId="${URLDATA.id}">
	<div class="centern title-box decoration decoration-rel">
		<a name="recommend" class="link">相关推荐<span>EDITOR'S SUJESTION</span></a>
		<core:if test="${count>2}"><a class="more more-change" src="<url:link href='web/site/imagealbum/recommend.json'/>"></a></core:if>
	</div>
	<%-- 内容 --%>
	<div class="centern">
		<core:forEach items="${list}" varStatus="s" var="item">
			<div class="content-warp <core:if test="${s.index%2==0}">content-warp-left</core:if>">
				<div class="content-head">${item.title }</div>
				<div class="content-subhead">${item.shortTitle }</div>
				<div class="content-body">
					<div class="content-img">
						<util:split string="${item.image.src}" var="images" separator=",">
								<core:forEach var="src" items="${images}">
									<img src="<url:image src='${src}'/>" width="140" height="140" alt="${item.image.title}"/>
								</core:forEach>
						</util:split>
					</div>
					<div class="content-tag">
						<core:forEach var="categorys" varStatus="s" items="${item.categoryInfos}">
							<core:if test="${s.index==0}">
								<a href="<url:link href='${item.url}'/>" title="${categorys.title}">${categorys.title}</a>
							</core:if>
							<core:if test="${s.index > 0}">
								 | <a href="<url:link href='${item.url}'/>" title="${categorys.title}">${categorys.title}</a>
							</core:if>
						</core:forEach>
					</div>
					<div class="content-detail-wrap">
						<div class="content-detail">
							<div class="bg"></div>
							<div class="content-text">${item.digest}</div>
							<div class="content-to-detail"><a href="<url:link href='${item.url}'/>" alt="详情">查看详情</a></div>
						</div>
					</div>
				</div>
			</div>
		</core:forEach>
	</div>
	</imgs:recommend>

	<%@ include file="/base/include/foot.tpl"%>
</body>
</html>
</imgs:detail>