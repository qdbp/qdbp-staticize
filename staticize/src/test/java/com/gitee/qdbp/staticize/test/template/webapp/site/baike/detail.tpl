<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="column" uri="http://www.jeshing.com/tags/column/"%>
<%@ taglib prefix="content" uri="http://www.jeshing.com/tags/content/"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>
<%@ taglib prefix="util" uri="http://www.jeshing.com/tags/util/"%>
<%@ taglib prefix="url" uri="http://www.jeshing.com/tags/url/"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="entry" uri="http://www.jeshing.com/tags/entry/" %>


<entry:detail id="${URLDATA.id}">
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/base/include/meta.tpl"%>
	<meta name="keywords" content="${detail.keyword}" />
	<meta name="description" content="${detail.digest}" />
	<title>${detail.title}</title>
	<%@ include file="/base/include/head.tpl"%>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/baike.citiaolist.css'/>">
	<script type="text/javascript" src="<url:link href='resources/site/js/baike.detail.js'/>"></script>
</head>
<body>

	<%@ include file="/base/include/top.tpl"%>
	<div class="centern title-box decoration-detail">
		<column:detail key="${detail.columnId}">
		<span class="title">${root.shortTitle}词条</span>
		<div class="decoration-nav">
			<a href="<url:link/>">首页</a>
			<core:forEach var="parent" varStatus="p" items="${parents}">
			<core:if test="${parent.url != null}">
			<span> &gt; </span>
			<a href="<url:link href='${parent.url}'/>">${parent.title}</a>
			</core:if>
			</core:forEach>
			<span> &gt; </span>
			${root.shortTitle}词条
		</div>
		</column:detail>
	</div>

	<div class="centern">
		<%--案例开始 --%>
		<div class="centern-left">


		<%-- 案例结束 --%>


			<%--地中海风格开始--%>
			<div class="center-width title-box title-box-noborder3">
				<span>${detail.title}</span>
			</div>
			<div class="center-width relative-product">
				<div class="box">
					<div class="wrap">
						<img src="<url:image src='${detail.image.src}'/>" width="260" height="200" alt="${img.title}"/>
					</div>
					<div class="wrap2">

					</div>
				</div>
				<div class="box2">
					<div class="warp3">

					</div>
					<div class="warp4">
						<div class="warp5">
							${detail.digest}
						</div>
					</div>
				</div>
				<br class="clear"/>
			</div>
		<%--地中海风格结束--%>

			<%--目录开始--%>
			<div class="center-width title-box title-box-noborder3">
				<span>目录</span>
			</div>
			<div class="word">
				<ul>
					<core:forEach var="chapter" varStatus="c" items="${detail.chapters}">
					<li>${c.index+1}、<a href="${detail.url}#chapter-${c.index+1}">${chapter.title}</a></li>
					</core:forEach>
				</ul>
			</div>

		<core:forEach var="chapter" varStatus="c" items="${detail.chapters}">
		<a name="chapter-${c.index+1}" class="maodian maodian-bk"></a>
		<div class="centern title-box decoration-detail2"></div>
			<div class="mulu">
				<div class="biaoti">
				<a href="<url:link href='${chapter.url}'/>">${c.index+1}、${chapter.title}</a>
				</div>
				<div class="neirong">
					${chapter.desc}
					<core:if test="${chapter.url!=null}">
						<a href="<url:link href='${chapter.url}'/>">查看全文</a>
					</core:if>
				</div>
				<div class="fenlei<core:if test="${c.last}">-last</core:if>" >
				<content:detail key="${chapter.contentId}">
					<core:if test="${content.tagword!=null}">
						<util:split string="${content.tagword}" var="tagwords" >
							<core:forEach var="word" items="${tagwords}" varStatus="s">
								<core:if test="${s.first == false}">|</core:if>
								<a href="<url:link href='${chapter.url}'/>">${word}</a>
							</core:forEach>
						</util:split>
					</core:if>
				</content:detail>
				</div>
			</div>
			</core:forEach>
		</div>
		<%--目录结束--%>
		<div class="centern-right zx_right_top_m">

		  <%--热门词条内容开始--%>
		  <position:detail key="${root.name}|xiangqing|remencitiao">
		  <position:content key="${position.id}" contentId="${detail.id}" rows="10" upgrade="true">
			 <div class="title-box title-box-noborder3">
				<span>${position.title}</span>
			</div>
			<div class="remen">
				<ul>
					<core:forEach var="item" items="${list}">
				   <li><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></li>
				   </core:forEach>
				</ul>
			</div>
			</position:content>
			</position:detail>

		   <%--热门词条内容开始结束--%>


		  <%--金品汇开始--%>
		  	<div class="decoration-must decoration-k">
		  	 <position:detail key="${root.name}|xiangqing|jinpinhui">
				<position:content key="${position.id}" contentId="${article.id}" rows="10" upgrade="true">
				<div class="title-box title-box-noborder3">
				</div>
			  	  	<div class="decoration-k-wrap-1">
	  	  				<core:forEach var="item" items="${list}">
			  	  			<div class="decoration-k-wrap-2">
			  				    <div class="images">
			  				    <a href="<url:link href='${item.url}'/>" title="${item.title}">
			  				    <img src="<url:image src='${item.image.src}'/>" height="300" width="300" alt="${item.image.title}"/>
			  				    </a>
			  				    </div>
			  				    <div class="text-align images-description">${item.title}</div>
				  		  	</div>
	  				    </core:forEach>
			  	  	</div>
			  	  	 </position:content>
		   			</position:detail>
			  <a class="prev"></a>
			  <a class="next"></a>
		   </div>

		   <%--金品汇结束--%>


		  </div>

	</div>

	<%@ include file="/base/include/foot.tpl"%>
</body>
</html>
</entry:detail>

