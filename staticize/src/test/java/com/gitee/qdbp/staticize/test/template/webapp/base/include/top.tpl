<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="column" uri="http://www.jeshing.com/tags/column/"%>
<%@ taglib prefix="url" uri="http://www.jeshing.com/tags/url/"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>

	<%--顶部 开始--%>
	<div class="header-info">
		<div class="centern-big">
			<div class="left">
				<span class="fs14 color-normal bold">南京</span>
				<a class="fs14 color666">[切换城市]</a>
				<span class="fs12 color666">您好<span id="top_userName"></span>，欢迎来到乐居中国！</span>
				<a class="fs12 color666" id="top_loginBtn">请登录</a>
				<a class="fs12 color666" id="top_userCenterBtn" style="display: none;">个人中心</a>
				<a class="fs12 color666" id="top_logoutBtn" style="display: none;">退出</a>
				<a class="fs12 color666" id="top_regBtn">免费注册</a>
			</div>
			<div class="right">
<!-- 				 <a class="left fs12 color666" title="在线客服">在线客服</a> -->
<!-- 				 <a class="left ml_5 fs12 color666" title="帮助中心">帮助中心</a> -->
				 <a class="left ml_5 bg4 sina-weibo-png"></a>
				 <a class="left ml_5 bg4 qq-weibo-png"></a>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="head-menu">
		<div class="centern-big head-nav" >
			<ul class="head-ul following" data-items=".head-li" data-block=".head-li-bg">
				<li class="head-li-icon"><a class="head-icon"  href="<url:link/>" title="乐居中国"></a></li>
				<position:detail key="shouye|toubudaohanglan">
					<position:content key="${position.id}">
						<core:forEach var="item" items="${list}" >
						<li class="head-li <core:if test="${param.column==item.name}">head-li-selected</core:if>" ><a href="<url:link href='${item.url}'/>" title="${item.title }" >${item.title}</a></li>
						</core:forEach>
					</position:content>
				</position:detail>
<%-- 				<li class="head-li ">优惠活动<a class="bg2 head-icon-hot">HOT</a></li> --%>
<!-- 				<li class="search-li"> -->
<!-- 					<span class="search-label"> -->
<!-- 						<font>文章</font><a class="bg1 search-dropdown"></a> -->
<!-- 					</span> -->
<!-- 					<input type="text" class="search-input"/> -->
<!-- 					<a class="search-btn">搜索</a> -->
<!-- 				</li> -->
				<li class="head-li-bg"></li>
			</ul>
			<div class="clear"></div>
		</div>
	</div>
	<%--顶部 结束--%>
