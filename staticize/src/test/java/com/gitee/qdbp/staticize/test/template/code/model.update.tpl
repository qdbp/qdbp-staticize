package #{java.vars.current.packages};

<core:each items="#{imports}" var="item">
${item}
</core:each>

/**
 * #{table.comment}更新类
 *
 * @author CodeGenerater
 * @version <fmt:date pattern="yyMMdd" />
 */
<core:if test="#{java.vars.operate.traces != null}">
<core:if test="#{java.vars.operate.traces.enable}">
@#{java.vars.operate.traces.annotation.capitalizeName}(target = "where")
</core:if>
<core:else>
@#{java.vars.operate.traces.annotation.capitalizeName}(enable = false)
</core:else>
</core:if>
<core:if test="#{java.vars.data.isolation != null}">
<core:if test="#{java.vars.data.isolation.enable}">
@#{java.vars.data.isolation.annotation.capitalizeName}(target = "where")
</core:if>
<core:else>
@#{java.vars.data.isolation.annotation.capitalizeName}(enable = false)
</core:else>
</core:if>
public class #{java.vars.current.capitalizeName} extends #{java.vars.model.bean.capitalizeName} {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;
    <core:each items="#{table.fields}" var="field">
    <core:if test="${!field.primaryKey && !field.businessKey && !field.logicallyDeleteField && !field.createTimeField}">
	<core:if test="${!field.updateTimeField}">
    /** ${field.comment}是否更新为空值 **/
    private Boolean ${field.normativeName}ToNull;
    </core:if>
    <core:if test="${field.updateTimeField || field.dataType.typedName == 'Date'}">
    /** ${field.comment}是否更新为数据库当前时间 **/
    private Boolean ${field.normativeName}ToCurrent;
    /** ${field.comment}的增加值(单位:秒) **/
    private Long ${field.normativeName}Add;
    </core:if>
    <core:elseif test="${field.dataType.typedName == 'Integer' || field.dataType.typedName == 'Long' || field.dataType.typedName == 'Double' || field.dataType.typedName == 'Float'}">
    /** ${field.comment}的增加值 **/
    private ${field.dataType.typedName} ${field.normativeName}Add;
    </core:elseif>
    </core:if>
    </core:each>

    /** 更新操作的条件 **/
    private #{java.vars.model.where.capitalizeName} where;
    <core:each items="#{table.fields}" var="field">
    <core:if test="${!field.primaryKey && !field.businessKey && !field.logicallyDeleteField && !field.createTimeField}">
    <core:if test="${!field.updateTimeField}">

    /** 判断${field.comment}是否更新为空值 **/
    public Boolean is${field.capitalizeName}ToNull() {
        return ${field.normativeName}ToNull;
    }

    <core:if test="${field.notNullField}">
    /**
     * 设置${field.comment}是否更新为空值
     *
     * @param ${field.normativeName}ToNull ${field.comment}是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    </core:if>
    <core:else>
    /** 设置${field.comment}是否更新为空值 **/
    </core:else>
    public void set${field.capitalizeName}ToNull(Boolean ${field.normativeName}ToNull) {
        this.${field.normativeName}ToNull = ${field.normativeName}ToNull;
    }
    </core:if>
    <core:if test="${field.updateTimeField || field.dataType.typedName == 'Date'}">

    /** 判断${field.comment}是否更新为数据库当前时间 **/
    public Boolean is${field.capitalizeName}ToCurrent() {
        return ${field.normativeName}ToCurrent;
    }

    /** 设置${field.comment}是否更新为数据库当前时间 **/
    public void set${field.capitalizeName}ToCurrent(Boolean ${field.normativeName}ToCurrent) {
        this.${field.normativeName}ToCurrent = ${field.normativeName}ToCurrent;
    }

    /** 获取${field.comment}的增加值(单位:秒) **/
    public Long get${field.capitalizeName}Add() {
        return ${field.normativeName}Add;
    }

    /** 设置${field.comment}的增加值(单位:秒) **/
    public void set${field.capitalizeName}Add(Long ${field.normativeName}Add) {
        this.${field.normativeName}Add = ${field.normativeName}Add;
    }
    </core:if>
    <core:elseif test="${field.dataType.typedName == 'Integer' || field.dataType.typedName == 'Long' || field.dataType.typedName == 'Double' || field.dataType.typedName == 'Float'}">

    /** 获取${field.comment}的增加值 **/
    public ${field.dataType.typedName} get${field.capitalizeName}Add() {
        return ${field.normativeName}Add;
    }

    /** 设置${field.comment}的增加值 **/
    public void set${field.capitalizeName}Add(${field.dataType.typedName} ${field.normativeName}Add) {
        this.${field.normativeName}Add = ${field.normativeName}Add;
    }
    </core:elseif>
    </core:if>
    </core:each>

    /** 获取更新操作的条件 **/
    public #{java.vars.model.where.capitalizeName} getWhere() {
        return where;
    }

    /** 获取更新操作的条件, force=是否强制返回非空条件 **/
    public #{java.vars.model.where.capitalizeName} getWhere(boolean force) {
        if (where == null && force) {
            where = new #{java.vars.model.where.capitalizeName}();
        }
        return where;
    }

    /** 设置更新操作的条件 **/
    public void setWhere(#{java.vars.model.where.capitalizeName} where) {
        this.where = where;
    }

    @Override
    public String toString() {
        return JsonTools.toLogString(this);
    }
}
