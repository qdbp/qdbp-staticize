package com.gitee.qdbp.staticize.test.utils;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.gitee.qdbp.staticize.parse.ExpItem;
import com.gitee.qdbp.staticize.parse.ExpItems;
import com.gitee.qdbp.staticize.parse.ExpParser;

@Test
public class ExpParserTest {

    @Test
    public void testParse1() {
        String source = "abc";
        Object result = ExpParser.parse(source);
        Assert.assertEquals(result.getClass(), String.class, "result class");
        Assert.assertEquals(result, source, "result string");
    }

    @Test
    public void testParse2() {
        String source = "abc\\${xxx}";
        String target = "abc${xxx}";
        Object result = ExpParser.parse(source);
        Assert.assertEquals(result.getClass(), String.class, "result class");
        Assert.assertEquals(result, target, "result string");
    }

    @Test
    public void testParse3() {
        String source = "abc\\#{xxx}";
        String target = "abc#{xxx}";
        Object result = ExpParser.parse(source);
        Assert.assertEquals(result.getClass(), String.class, "result class");
        Assert.assertEquals(result, target, "result string");
    }

    @Test
    public void testParse4() {
        String source = "#{xxx}";
        Object result = ExpParser.parse(source);
        Assert.assertEquals(result.getClass(), ExpItem.class);
        Assert.assertEquals(result.toString(), source, "result string");
    }

    @Test
    public void testParse5() {
        String source = "${xxx}";
        Object result = ExpParser.parse(source);
        Assert.assertEquals(result.getClass(), ExpItem.class);
        Assert.assertEquals(result.toString(), source, "result string");
        Assert.assertTrue(((ExpItem) result).isEvaluatable(), "result evaluatable");
    }

    @Test
    public void testParse6() {
        String source = "abc-#{xxx}-defg";
        Object result = ExpParser.parse(source);
        Assert.assertEquals(result.getClass(), ExpItems.class, "result class");
        Assert.assertEquals(result.toString(), source, "result class");
        Assert.assertEquals(((ExpItems) result).size(), 3, "result size");
    }

    @Test
    public void testParse7() {
        String source = "abc-#{xxx}${yyy}-defg";
        Object result = ExpParser.parse(source);
        Assert.assertEquals(result.getClass(), ExpItems.class, "result class");
        Assert.assertEquals(result.toString(), source, "result class");
        Assert.assertEquals(((ExpItems) result).size(), 4, "result size");
    }

    @Test
    public void testParse8() {
        String source = "abc-#{xxx} ${yyy}-defg";
        Object result = ExpParser.parse(source);
        Assert.assertEquals(result.getClass(), ExpItems.class, "result class");
        Assert.assertEquals(result.toString(), source, "result class");
        Assert.assertEquals(((ExpItems) result).size(), 5, "result size");
    }

    @Test
    public void testParse9() {
        String source = "#{xxx}${yyy}";
        Object result = ExpParser.parse(source);
        Assert.assertEquals(result.getClass(), ExpItems.class, "result class");
        Assert.assertEquals(result.toString(), source, "result class");
        Assert.assertEquals(((ExpItems) result).size(), 2, "result size");
    }

    @Test
    public void testParse10() {
        String source = "#{'{key}'}";
        Object result = ExpParser.parse(source);
        Assert.assertEquals(result.getClass(), ExpItem.class, "result class");
        Assert.assertEquals(result.toString(), source, "result class");
    }

    @Test
    public void testParse11() {
        String source = "${@JsonTools.parse(\"{nickname:'Let\\'s go!'}\")}";
        Object result = ExpParser.parse(source);
        Assert.assertEquals(result.getClass(), ExpItem.class, "result class");
        Assert.assertEquals(result.toString(), source, "result class");
    }
}
