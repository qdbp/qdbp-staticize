<core:import>java.util.Arrays</core:import>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>each tag</title>
</head>
<body>
	\#{number} -->  #{@Arrays.toString(number)}
	\#{ascii}  -->  #{@Arrays.toString(ascii)}
	--- inline
	<core:each var="i" status="s" items="#{number}" inline="true">
		${i}<core:if test="${!s.last}">, </core:if>
	</core:each>
	<core:set var="test.number" value="#{number}" scope="root" />
	--- {test.number}
	<core:each items="${test.number}" var="i" status="s">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- begin="6" rows="5"
	<core:each var="i" status="s" begin="6" rows="5">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number}
	<core:each items="#{number}" var="i" status="s">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number} begin="5" rows="5"
	<core:each items="#{number}" var="i" status="s" begin="5" rows="5">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number} begin="8" rows="5"
	<core:each items="#{number}" var="i" status="s" begin="8" rows="5">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number} begin="0" rows="0"
	<core:each items="#{number}" var="i" status="s" begin="0" rows="0">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number} begin="0" rows="1"
	<core:each items="#{number}" var="i" status="s" begin="0" rows="1">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number} begin="2" rows="3"
	<core:each items="#{number}" var="i" status="s" begin="2" rows="3">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number} begin="9" rows="1"
	<core:each items="#{number}" var="i" status="s" begin="9" rows="1">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number} begin="20" rows="1"
	<core:each items="#{number}" var="i" status="s" begin="20" rows="1">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number} step="3"
	<core:each items="#{number}" var="i" status="s" step="3">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number} begin="1" step="3"
	<core:each items="#{number}" var="i" status="s" begin="1" step="3">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number} begin="3" step="3"
	<core:each items="#{number}" var="i" status="s" begin="2" step="3">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- begin="6" end="5"
	<core:each var="i" status="s" begin="6" end="5">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- begin="-1" end="-5"
	<core:each var="i" status="s" begin="-1" end="-5">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- begin="-1" end="-10" step="-3"
	<core:each var="i" status="s" begin="-1" end="-10" step="-3">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- begin="-1" end="-9" step="-3"
	<core:each var="i" status="s" begin="-1" end="-9" step="-3">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number} step="-1"
	<core:each items="#{number}" var="i" status="s" step="-1">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number} step="-3"
	<core:each items="#{number}" var="i" status="s" step="-3">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number} begin="8" end="5"
	<core:each items="#{number}" var="i" status="s" begin="8" end="5">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {number} end="3" step="-1"
	<core:each items="#{number}" var="i" status="s" end="3" step="-1">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {ascii}
	<core:each items="#{ascii}" var="i" status="s">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {ascii} begin="0" rows="1"
	<core:each items="#{ascii}" var="i" status="s" begin="0" rows="1">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {ascii} begin="0" rows="2"
	<core:each items="#{ascii}" var="i" status="s" begin="0" rows="2">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {ascii} begin="1" rows="2"
	<core:each items="#{ascii}" var="i" status="s" begin="1" rows="2">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- {ascii} 20
	<core:each items="#{ascii}" var="i" status="s" begin="20">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- begin="6" rows="5"
	<core:each var="i" status="s" begin="6" rows="5">
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
	--- begin="3" rows="3" x begin="600" rows="4"
	<core:each var="i" status="s" begin="3" rows="3">
		<core:each var="i" status="s" begin="600" rows="4">
			index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
		</core:each>
		index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
	</core:each>
</body>
</html>
