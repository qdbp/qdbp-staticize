package com.gitee.qdbp.staticize.test.error;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.staticize.parse.TagParser;
import com.gitee.qdbp.staticize.publish.TemplateTools;
import com.gitee.qdbp.staticize.tags.base.Taglib;

@Test
public class IncompleteHtmlParseTest {

    @Test
    public void test1() {
        // JSP脚本中的任何标签都不解析, 因此遇到不完整的标签可以还原当作普通字符串处理
        testParseHtml("<test><%");
        testParseHtml("<test><%-");
        testParseHtml("<test>\n<%--");
    }

    @Test
    public void test2() {
        // XML注释中的任何标签都不解析, 因此遇到不完整的标签可以还原当作普通字符串处理
        testParseHtml("<test><!");
        testParseHtml("<test><!-");
        testParseHtml("<test><!--");
        testParseHtml("<test><!-- --");
        testParseHtml("<test>\n<!--");
    }

    @Test
    public void test3() {
        // CDATA中的标签需要解析, 因此只要CDATA打开了就会报错
        testParseHtml("<test><!");
        testParseHtml("<test><![CDATA");
        testParseHtml("<test><![CDATA["); // 开始标签还没有完全打开, 再遇到空白字符才能打开
        testErrorHtml("<test>\n<![CDATA[ ", "<![CDATA[");
        testErrorHtml("<test>\n<![CDATA[ ]]", "<![CDATA[");
    }

    @Test
    public void test4() {
        // core:if标签要遇到>才算完全打开
        testParseHtml("<test><core:");
        testParseHtml("<test><core:if");
        testParseHtml("<test><core:if ");
        testParseHtml("<test><core:if test=\"aaa\"");
        testParseHtml("<test><core:if test=\"a");
        testErrorHtml("<test><core:if test=\"aaa\">", "<core:if");
    }

    @Test
    public void test5() {
        testErrorHtml("<test></core:if>", "</core:if>");
        testErrorHtml("<test><core:if test=\"aaa\"></core:if", "<core:if");
    }

    @Test
    public void test6() {
        testParseHtml("<test>#{");
        testParseHtml("<test>#{aaa");
    }

    private void testParseHtml(String string) {
        TagParser.Options options = new TagParser.Options().setParseJspScript(true);
        String result = TemplateTools.render(string, options, null);
        System.out.println(string);
        System.out.println(result);
        System.out.println("=============================");
        Assert.assertEquals(result, string);
    }

    private void testErrorHtml(String string, String error) {
        try {
            TagParser.Options options = new TagParser.Options().setParseJspScript(true);
            String result = TemplateTools.render(string, Taglib.defaults(), options, null);
            System.out.println(string);
            System.out.println(result);
            System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
            Assert.fail("期望抛出异常, 不应该走到这里");
        } catch (ServiceException e) {
            System.out.println(string);
            System.out.println(e.getCause().getMessage());
            System.out.println("-----------------------------");
            Assert.assertTrue(e.getCause().getMessage().contains(error));
        }
    }
}
