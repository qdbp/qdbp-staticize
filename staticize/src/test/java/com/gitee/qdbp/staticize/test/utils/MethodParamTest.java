package com.gitee.qdbp.staticize.test.utils;

import java.util.Arrays;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.gitee.qdbp.tools.parse.MethodParamItems;
import com.gitee.qdbp.tools.parse.StringParser;
import com.gitee.qdbp.tools.utils.AssertTools;

@Test
public class MethodParamTest {

    @Test
    public void testParseMethodParams() {
        // ( user.name, 'xxx', 'xxx,yyy', [ 'aaa', 'bbb' ] )
        // ( user.name + ', hello!', 'Hello ' + user.name + ', welcome!' )
        // ( 'Let\'s go', "Let's go", '8,000.00', '8,000.00(元)' )
        // ( Math.round(user.height), Math.round( Math.abs(user.height) ) )
        // ( StringTools.removeChars('8,000.00(元)', ',') )
        // ( {'a':'1','b':'2'}, { a:1, b:'xxx,yyy', c:[ {c1:'3'}, {c2:['aaa', 'bbb']} ] } )

        String[] empty = new String[0];
        // @formatter:off
        testParseMethodParams(" () ", empty);
        testParseMethodParams(" ( ) ", empty);
        testParseMethodParams(" ( user.name , 'xxx' , 'xxx,yyy' ) ",
            "user.name", "'xxx'", "'xxx,yyy'");
        testParseMethodParams(" ( user.name , 'xxx' , 'xxx,yyy', [ 'aaa', 'bbb' ] ) ",
            "user.name", "'xxx'", "'xxx,yyy'", "[ 'aaa', 'bbb' ]");
        testParseMethodParams(" ( user.name + ', hello!', 'Hello ' + user.name + ', welcome!' ) ",
            "user.name + ', hello!'", "'Hello ' + user.name + ', welcome!'");
        testParseMethodParams(" ( 'Let\\'s go', \"Let's go\", '8,000.00', '8,000.00(元)' ) ",
            "'Let\\'s go'", "\"Let's go\"", "'8,000.00'", "'8,000.00(元)'");
        testParseMethodParams(" ( Math.round(user.height), Math.round( Math.abs(user.height) ) ) ",
            "Math.round(user.height)", "Math.round( Math.abs(user.height) )");
        testParseMethodParams(" ( StringTools.removeChars('8,000.00(元)', ',') ) ",
            "StringTools.removeChars('8,000.00(元)', ',')");
        testParseMethodParams(" ( Math.round(user.height), Math.round( Math.abs(user.height) ), removeChars('8,000.00(元)', ',') ) ",
            "Math.round(user.height)", "Math.round( Math.abs(user.height) )", "removeChars('8,000.00(元)', ',')");
        testParseMethodParams(" ( {'a':'1','b':'2'} , { a:1, b:'xxx,yyy', c:[ {c1:'3'}, {c2:['aaa', 'bbb']} ] } ) ",
            "{'a':'1','b':'2'}", "{ a:1, b:'xxx,yyy', c:[ {c1:'3'}, {c2:['aaa', 'bbb']} ] }");

        // 第1个字符不是左括号
        errorParseMethodParams(" {'a':'1','b':'2'} ");
        // 缺少右括号}
        errorParseMethodParams(" ( {'a':'1','b':'2'} , { a:1, b:'xxx,yyy', c:[ {c1:'3'}, {c2:['aaa', 'bbb']} ] ) ");
        // 缺少右括号)
        errorParseMethodParams(" ( {'a':'1','b':'2'} , { a:1, b:'xxx,yyy', c:[ {c1:'3'}, {c2:['aaa', 'bbb']} ] } ");
        System.out.println("------------------------------");
        // @formatter:on
    }

    private void testParseMethodParams(String source, String... expected) {
        MethodParamItems result = StringParser.parseMethodParams(source, 0);
        System.out.println("------------------------------");
        System.out.println(source);
        System.out.println("------------------------------");
        for (String item : result.getItems()) {
            System.out.println("\t" + item);
        }
        AssertTools.assertDeepEquals(result.getItems(), Arrays.asList(expected));
        Assert.assertEquals(result.getLeftBracket(), " (");
        Assert.assertEquals(result.getRightBracket(), ")");
        Assert.assertEquals(result.getEndIndex(), source.length() - 1);
    }

    private void errorParseMethodParams(String source) {
        MethodParamItems result = StringParser.parseMethodParams(source, 0);
        System.out.println("------------------------------");
        System.out.println(source);
        System.out.println("------------------------------");
        System.out.println("\tresult expected is null!");
        Assert.assertNull(result, source);
    }
}
