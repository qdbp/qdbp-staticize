<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>#{column.title}</title>
</head>
<body>
	<core:include src="../common/top.tpl" module="home" name="zhh"/>
	--------------
	<core:each items="#{number}" var="i" status="s">
		${i}
		<core:if test="${s.index < 2}"><core:include src="sub.tpl"/></core:if>
	</core:each>
	--------------
	<core:include src="../common/foot.tpl" title="#{column.title}" module="home"></core:include>
</body>
</html>
