package com.gitee.qdbp.staticize.test.tags.ext;

import java.io.IOException;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.NextStep;
import com.gitee.qdbp.staticize.test.tags.constant.ContextKeys;
import com.gitee.qdbp.staticize.test.tags.utils.TagConfig;
import com.gitee.qdbp.tools.utils.VerifyTools;

/**
 * URL链接标签, 将href转换为网站绝对路径
 *
 * @author zhaohuihua
 * @version 140504
 */
public class UrlLinkTag extends BaseTag {

    /** 变量名, 如果为空直接将链接地址输出到页面, 否则将图片链接保存到变量中 **/
    private String var;

    /**
     * 链接地址<br>
     * 如果未传href参数, 返回首页地址; 如果传了href参数, 但参数为空, 返回空字符串
     */
    private String href;

    /** 如果未传href参数, 返回首页地址; 如果传了href参数, 但参数为空, 返回空字符串 **/
    private boolean def = true;

    /**
     * 设置href的值
     * 
     * @param href 链接地址
     */
    public void setHref(String href) {
        this.href = href;
        this.def = false;
    }

    /**
     * 设置变量名
     *
     * @param var 变量名
     */
    public void setVar(String var) {
        this.var = var;
    }

    /**
     * 标签处理方法
     *
     * @return 下一步处理方式
     */
    @Override
    public NextStep doHandle() throws TagException, IOException {
        // 取网站地址
        String prefix = this.getPresetString(ContextKeys.SITE_URL);

        String url;
        if (VerifyTools.isBlank(href)) {
            if (prefix != null && !prefix.endsWith("/")) {
                prefix += "/";
            }
            // 如果没传href参数, 返回网站地址
            // 如果传了href参数, 但参数为空, 返回空字符串
            url = def ? prefix : "";
        } else {
            // 将href转换为网站绝对路径, true=追加版本号
            url = TagConfig.toSiteAbsoluteUrl(prefix, href, true);
        }

        // 没有指定var, 直接输出
        if (var == null) {
            // 将图片标签输出
            print(url);
            return NextStep.SKIP_BODY;
        }
        // 指定了var, 将href保存到值栈中
        else {
            this.addStackValue(var, url);
            return NextStep.EVAL_BODY;
        }
    }
}
