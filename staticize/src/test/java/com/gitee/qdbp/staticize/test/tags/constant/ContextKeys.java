package com.gitee.qdbp.staticize.test.tags.constant;

/**
 * ContextKey
 * 
 * @author zhaohuihua
 * @version 140728
 */
public interface ContextKeys {

    /** 分页信息(Map) **/
    String PAGING_INFO = "paging";

    /** 分页信息-当前页数(Integer) **/
    String PAGING_INDEX = "paging.index";

    /** 分页信息-总页数(Integer) **/
    String PAGING_COUNT = "paging.count";

    /** 站点对象(SiteInfo) **/
    String SITE_INFO = "site";

    /** 站点ID(String) **/
    String SITE_ID = "site.id";

    /** 站点英文标识(String) **/
    String SITE_NAME = "site.name";

    /** 站点网址(String) **/
    String SITE_URL = "site.url";

    /** 栏目对象(ColumnInfo) **/
    String COLUMN_INFO = "column";

    /** 栏目ID(String) **/
    String COLUMN_ID = "column.id";

    /** 栏目英文标识(String) **/
    String COLUMN_NAME = "column.name";

    /** URL相关数据(UrlData) **/
    String URL_DATA = "urldata";
}
