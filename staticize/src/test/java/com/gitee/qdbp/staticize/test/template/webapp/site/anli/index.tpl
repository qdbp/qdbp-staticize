<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="column" uri="http://www.jeshing.com/tags/column/"%>
<%@ taglib prefix="content" uri="http://www.jeshing.com/tags/content/"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>
<%@ taglib prefix="util" uri="http://www.jeshing.com/tags/util/"%>
<%@ taglib prefix="url" uri="http://www.jeshing.com/tags/url/"%>
<%@ taglib prefix="imgs" uri="http://www.jeshing.com/tags/images/"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/base/include/meta.tpl"%>
	<meta name="keywords" content="乐居中国" />
	<meta name="description" content="乐居中国" />
	<title>案例图库</title>
	<%@ include file="/base/include/head.tpl"%>
	<script type="text/javascript" src="<url:link href='resources/base/js/cyclic/zhh.cyclic.js'/>"></script>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/anli_index.css'/>" >
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/home.css'/>" >
	<script type="text/javascript" src="<url:link href='resources/site/js/newanli.js'/>" ></script>

</head>
<body>
    <jsp:include page="/base/include/top.tpl" >
		<jsp:param name="column" value="anli" />
	</jsp:include>

	<div class="clear"></div>
    <div class="centern-big center-dapei">
		<div class="daily-left daily-left-dapei">
			<div class="marquee-box marquee-box-dapei hover mq-box">
			<position:detail key="anli|lunbotu">
				<position:content key="${position.id}" rows="3">
				<div>
					<ul class="marquee-list marquee-lis-dapei mq-list  zhh-cyclic" data-box=".marquee-box" data-pointer-item=".mq-pointer">
						<div style="width: 100%;">
							<core:forEach var="item" items="${list}" varStatus="s">
							<li>
								<div class="marquee-item">
									<a href="<url:link href='${item.url}'/>" title="${item.title }" target="_blank">
										<img src="<url:image src='${item.image.src}'/>" width="845" height="420" alt="${item.title }">
									</a>
								</div>
							</li>
							</core:forEach>
						</div>
					</ul>
					<ul class="pointer pointer-ch">
						<core:forEach var="item" items="${list}" varStatus="s">
							<li class="mq-pointer"><a href="#${s.index}" title="${item.title}"></a></li>
						</core:forEach>
					</ul>
				</div>
				<div class="clear"></div>
				</position:content>
				</position:detail>
			</div>
		</div>

		<div class="daily-right daily-right-dapei">
			<position:detail key="anli|bianjituijian">
				<div class="daily-right-head daily-right-head-dapei"  >
					<span class="daily-right-head-icon daily-right-head-icon-dapei"  ></span>
					<span class="daily-right-head-title" >${position.title}</span>
				</div>
				<position:content key="${position.id}" rows="7">
					<div class="tuwen">
						<core:forEach var="item" items="${list}" begin="0" end="0">
							<div class="images"><img src="<url:image src="${item.image.src}"/>" width="121" height="91" alt="${item.image.title}"/></div>
							<div class="title"><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></div>
							<div class="text">${item.digest}</div>
						</core:forEach>
					</div>
					<div class="list">
						<core:forEach var="item" items="${list}" begin="1" end="6" varStatus="s">
							<core:if test="${s.index==1}">
								<div class="list-title"><span><a href="<url:link href='${item.url}'/>" class="lm-item">${item.shortTitle}</a></span><a href="<url:link href='${item.url}'/>">${item.title}</a></div>
							</core:if>
							<core:if test="${s.index>1}">
								<div class="line"></div>
								<div class="list-title"><span><a href="<url:link href='${item.url}'/>" class="lm-item">${item.shortTitle}</a></span><a href="<url:link href='${item.url}'/>">${item.title}</a></div>
							</core:if>
						</core:forEach>
					</div>
				</position:content>
			</position:detail>
		</div>
	</div>
    <div class="clear"></div>
	<div class="centern-big">
		<div class="tag-warp">
		<position:detail key="anli|tuijiananli">
			<div class="tag-img-warp-tip">
				<span class="tip-msg">${position.title}</span>
			</div>
			<div class="tag-img-warp">
				<position:content key="${position.id}" rows="7">
					<core:forEach var="item" items="${list}" varStatus="s">
						<div class="tag-img <core:if test="${s.first}">tag-img-first</core:if>" >
							<img src="<url:image src="${item.image.src}"/>" width="139" height="156" alt="${item.image.title}"><span>${item.title}</span>
							<a href="<url:link href='${item.url}'/>" ></a>
						</div>
					</core:forEach>
				</position:content>
			</div>
		</position:detail>
			<div class="tag-info-box">
				<imgs:category>
					<div class="tag-info-box">
						<div class="tag-split-line"></div>
						<core:forEach var="category" varStatus="s" items="${categoryInfos}">
							<core:if test="${s.first==false}">
								<div class="tag-split-line"></div>
							</core:if>
							<div class="tag-info-warp">
								<div class="tag-info-title">${category.title}</div>
								<div class="tag-info-content">
									<core:forEach var="child" varStatus="cc" items="${category.children}">
										<imgs:urltype name="${child.name}" type="${category.name}">
										<a <core:if test="${selected}">class="hover"</core:if> href="<url:list column="${URLDATA.column}" type='${type}'/>" title="${child.title}">
											${child.title}
										</a>
										</imgs:urltype>
									</core:forEach>
								</div>
								<a class="tag-info-opeartion tag-info-collapse">收起</a>
							</div>
						</core:forEach>
					</div>
				</imgs:category>
			</div>
		</div>
	</div>
	<div class="centern-big text-center">
		<div class="search-box"><a class="search-box-operation search-box-collapse"></a></div>
	</div>


	<div class="centern-big">
		<div class="type-wrap">
			<ul>
				<li>
					<a href="#" id="link-1" class="on" >最新发布</a>
				</li>
				<li>
					<a href="#"  id="link-2">最多收藏</a>
				</li>
			</ul>
		</div>
	</div>
	<!--最新发布-->
	<imgs:list rows="8">
		<div class="centern-big" id="newpublish">
			<core:forEach var ="img" varStatus="c" items="${images}">
				<div class="content-warp <core:if test="${(c.index+1)%2 > 0}">content-warp-left</core:if>">
					<div class="anli-wrap">
						<div class="content-head">${img.title}</div>
						<div class="content-subhead">${img.subTitle}</div>
					</div>
					<div class="content-body">
						<div class="content-img">
							<util:split var="src" string="${img.image.src}" separator=",">
								<core:forEach var="s" items="${src}">
								<img src="<url:image src='${s}'/>" width="170" height="182" alt="${img.image.title}"/>
								</core:forEach>
							</util:split>
						</div>
						<div class="content-detail-wrap">
							<div class="content-detail">
								<div class="bg"></div>
								<div class="content-text">${img.digest}</div>
								<div class="content-to-detail"><a href="<url:link href='${img.url}'/>" alt="查看详情" class="xiangqing">查看详情</a><a href="#" alt="收藏" class="shoucang" data-id="${img.id}" data-type="${img.type.getKey()}" data-src="<url:link href="web/site/favorite/add.json"/>">收藏</a></div>
							</div>
						</div>
					</div>
				</div>
			</core:forEach>
		</div>
	</imgs:list>
	<!--最多收藏-->
	<imgs:favorite rows="8">
		<div class="centern-big" id="moreshoucang" style="display:none;">
			<core:forEach var ="img" varStatus="c" items="${falist}">
				<div class="content-warp <core:if test="${(c.index+1)%2 > 0}">content-warp-left</core:if>">
					<div class="anli-wrap">
						<div class="content-head">${img.title}</div>
						<div class="content-subhead">${img.subTitle}</div>
					</div>
					<div class="content-body">
						<div class="content-img">
							<util:split var="src" string="${img.image.src}" separator=",">
								<core:forEach var="s" items="${src}">
								<img src="<url:image src='${s}'/>" width="170" height="182" alt="${img.image.title}"/>
								</core:forEach>
							</util:split>
						</div>
						<div class="content-detail-wrap">
							<div class="content-detail">
								<div class="bg"></div>
								<div class="content-text">${img.digest}</div>
								<div class="content-to-detail"><a href="<url:link href='${img.url}'/>" alt="查看详情" class="xiangqing">查看详情</a><a href="#" alt="收藏" class="shoucang" data-id="${img.id}" data-type="${img.type.getKey()}" data-src="<url:link href="web/site/favorite/add.json"/>">收藏</a></div>
							</div>
						</div>
					</div>
				</div>
			</core:forEach>
		</div>
	</imgs:favorite>

	<!--  底部 -->
	<%@ include file="/base/include/foot.tpl"%>
</body>
</html>

