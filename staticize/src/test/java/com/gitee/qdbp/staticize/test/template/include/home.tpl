
<%--顶部 开始--%>
<div class="header-box png">
	<div class="centern">
		<ul>
			<li class="logo">
				<a title="乐居中国" href="<url:link/>"><img alt="乐居中国" width="184" height="36" src="<url:link href='resources/site/images/common/logo.gif'/>" /></a>
			</li>
			<li class="local">
				<div class="icon"><div class="downdrop"><div class="text">南京站</div></div></div>
			</li>
			<li class="nav">
				<ul>
					<li><a href="<url:link/>" <core:if test="${param.column=='shouye'}">class="on"</core:if>>网站首页</a></li>
					<li><a href="<url:link href='zhuangxiu/'/>" <core:if test="${param.column=='zhuangxiu'}">class="on"</core:if>>装修宝典</a></li>
					<li><a href="<url:link href='dapei/'/>" <core:if test="${param.column=='dapei'}">class="on"</core:if>>家居搭配</a></li>
					<li><a href="<url:link href='anli/'/>" <core:if test="${param.column=='anli'}">class="on"</core:if>>案例参考</a></li>
				</ul>
			</li>
		</ul>
	</div>
</div>
<div class="header-margin"></div>
<%--顶部 结束--%>
