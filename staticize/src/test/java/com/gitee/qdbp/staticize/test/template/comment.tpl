<core:comment> 测试注释标签 </core:comment>
	--- {number}
	<core:each items="#{number}" var="i" status="s">
		index:${s.index} number:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:each>
<%--
	<core:each items="#{number}" var="i" status="s">
		index:${s.index} number:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:each>
--%>
	--- {number} begin="6" rows="5"
	<core:comment> 测试注释标签 </core:comment>
	<core:each items="#{number}" var="i" status="s" begin="6" rows="5">
		index:${s.index} number:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:each>
	<core:comment>
	<fmt:date />
	</core:comment>

<%-- 测试注释标签 --%>
