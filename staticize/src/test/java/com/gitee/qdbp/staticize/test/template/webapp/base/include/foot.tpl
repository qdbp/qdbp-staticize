<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>

<!--  底部 -->
<div class="footer_box">
	<div class="footer_main">
		<div class="footer_center">
			<div class="footer_center_left fl">
				<div class="footer_nav">
					<position:detail key="shouye|dibulianjie">
						<position:content key="${position.id}">
							<core:forEach var="item" items="${list}" varStatus="s">
								<core:if test="${s.index==0}">
									<a title="${item.url }" href="<url:link href='${item.url }'/>">${item.title}</a>
								</core:if>
								<core:if test="${s.index > 0 }">
									<span>|</span> <a  title="${item.url }" href="<url:link href='${item.url }'/>">${item.title}</a>
								</core:if>
							</core:forEach>
					     </position:content>
				     </position:detail>
				</div>
				<p>Copyright © lejujs.com All Rights Reserved</p>
				<p>苏ICP备13011498号-1</p>
			</div>
			<div class="footer_center_right fr">
				<div class="fr footer_weixin">
					<img  alt="乐居中国微信"   src="<url:link href='resources/base/images/common/weixin_img.jpg'/>"><span>关注乐居中国微信</span>
				</div>
				<div class="fr footer_qq">
					<span class="bg4"></span><font>乐居中国</font><a href="#" class="bg2"></a>
				</div>
				<div class="fr footer_weibo">
					<span class="bg4"></span><font>乐居中国</font><a href="#" class="bg2"></a>
				</div>
			</div>
		</div>
		<div class="footer_link">
			<position:detail key="shouye|zhuangxiuanli|youqinglianjie" >
				<position:content key="${position.id}" rows="9">
					<core:forEach var="item" items="${list}"  varStatus="s">
					<a href="<url:link href='${item.url}'/>">
						<img src="<url:image src='${item.image.src}'/>" alt="${item.title}" />
					</a>
					</core:forEach>
				</position:content>
			</position:detail>
		</div>
	</div>
</div>
<core:if test="${param.preview==null && article!=null}">
<%-- 评论样式列表 --%>
<div class="pinglun-template hide">
	<%--评论框开始 --%>
	<div class="plun-wrap">
	<div class="plun">
		<div class="title">文章评论</div>
		<div class="box-wrap">
			<form action="<url:link href="web/site/comment/saveComment.json"/>" name="comment-form" method="post">
				<input type="hidden" name="contentId" value="{contentId}"/>
				<input type="hidden" name="commentType" value="{commentType}"/>
				<div class="publish">
					<textarea name="commentContent" maxlength="300" data-show='.show-number' rows="" cols="" required="true" minlength="5" class="valid"></textarea>
				</div>
				<div class="btn">
					<p>您还可以输入<span class="show-number">300</span>个字</p>

					<p class="right-btn">
						<core:if test="${!isLogin}">
							<a class="login">登录</a>
						</core:if>
						<core:if test="${isLogin}">
							<a class="comment">发表评论</a>
						</core:if>
					</p>
				</div>
			</form>
		</div>
	 </div>
	 </div>
	 <%-- 评论框结束 --%>

	 <%-- 评论分页开始 --%>
	 <div class="plun-head-wrap">
		<div class="plun-head" data-query-url="<url:link href="web/site/comment/listComment.json"/>">
			<a href="#" class="all">全部评论 {count}</a>
			<div class="plun-right">
				<div class="zb"><span>{index}</span>/{totalPage}</div>
				<a class="prev">上一页</a>
				<a class="next">下一页</a>
			</div>
		</div>
	 </div>
	 <%-- 评论分页结束 --%>

	<%-- 子评论发布框开始 --%>
	<div class="myhf-wrap">
	 <div class="myhf">
	 	<form action="<url:link href="web/site/comment/saveComment.json"/>" method="post">
	 		<input type="hidden" name="contentId" value="{contentId}"/>
			<input type="hidden" name="commentType" value="{commentType}"/>
			<input name="commentContent" value="//@{replyUserName}"/>
			<div class="myhf-btn">
				<core:if test="${isLogin}">
					<a class="cancel">取消</a>
					<a class="comment">发表评论</a>
				</core:if>
				<core:if test="${!isLogin}">
					<a class="login-btn">登录</a>
				</core:if>
			</div>
		</form>
	 </div>
	 </div>
	 <%-- 子评论发布框结束 --%>

	<%-- 评论内容开始 --%>
	<div class="pinlun-content-wrap">
		<div class="pinlun-content">
			<div class="list-wrap">
				<div class="images">
					<img config-src="<url:image src='{photoUrl}'/>" alt="{nickname}" width="55" height="55"/>
				</div>
				<input type="hidden" name="commentId" value="{commentId}">
				<input type="hidden" name="userId" value="{userId}">
				<div class="title">{nickname}</div>
				<div class="time">{createTime}</div>
				<div class="text">{commentContent}</div>
				<div class="text huifu"><a><span class="hf">回复<span>{replyNum}</span></span></a></div>
			</div>
			<br class="clear"/>
		</div>
	</div>
	<%-- 评论内容结束 --%>
</div>
</core:if>
<script>
	(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,"script","//www.google-analytics.com/analytics.js","ga");

	ga("create", "UA-51090725-1", "lejujs.com");
	ga("require", "displayfeatures");
	ga("set", "contentGroup7", "Group Name");
	ga("send", "event", "button", "click", "nav buttons", 4);

	ga("send", "pageview");
</script>
