package com.gitee.qdbp.staticize.test.tags.utils;

import com.gitee.qdbp.staticize.utils.TagUtils;
import com.gitee.qdbp.tools.utils.VerifyTools;

/**
 * URL相关数据<br>
 * 如 http://xxx/site-web/design/readme/details.new.12345678.5.2.html<br>
 * 网站URL前缀(prefix) = http://xxx/site-web/<br>
 * site-web = 工程名<br>
 * design = 模块名称(module)<br>
 * readme = 频道名称(channel)<br>
 * details = 模板类型(不是模板路径), index|list|details<br>
 * new = 风格(style)<br>
 * 12345678 = 详情页ID(id)<br>
 * 5 = 当前页数(index), 列表页和详情页有分页<br>
 * 2 = 子分页数(subindex), 有些页面有两种分页, 如装修公司案例和评论都有分页<br>
 * 通过有没有ID和当前页数可以判断是index|list|detail的哪一种<br>
 * http://xxx/site-web/design/, 这是一个指向design模块的index页面<br>
 * http://xxx/site-web/design/1.html, 这是一个指向design模块的list页面<br>
 * http://xxx/site-web/design/12345678.html, 这是一个指向design模块的detail页面<br>
 * 
 * @author zhaohuihua
 * @version 140619
 */
public class UrlData {

    /** 网站URL前缀 **/
    private String prefix;

    /** 模块名称 **/
    private String module;

    /** 频道名称 **/
    private String channel;

    /** 子频道名称 **/
    private String subchannel;

    /** 风格 **/
    private String style;

    /** 类型 **/
    private String type;

    /** 详情页ID **/
    private String id;

    /** 当前页数 **/
    private int index;

    /** 子分页数 **/
    private int subindex;

    /** 模板文件路径 **/
    private String template;

    /**
     * 获取URL全路径
     * 
     * @return 返回URL全路径
     */
    public String getUrl() {
        return createUrl(this.index);
    }

    /**
     * 获取分页URL
     * 
     * @param index 指定页数
     * @return 返回分页URL
     */
    public String getPageUrl(int index) {
        return createUrl(index);
    }

    /**
     * 获取分页URL
     * 
     * @param offset 与当前页的相对偏移量
     * @return 返回分页URL
     */
    public String getToPageUrl(int offset) {
        return createUrl(this.index + offset);
    }

    /**
     * 创建URL
     * 
     * @param index 当前页数
     * @return URL
     */
    private String createUrl(int index) {
        // URL路径
        StringBuilder path = new StringBuilder();
        append(path, prefix, TagUtils.SLASH);
        append(path, module, TagUtils.SLASH);
        append(path, channel, TagUtils.SLASH);
        append(path, subchannel, TagUtils.SLASH);

        // URL文件名
        StringBuilder name = new StringBuilder();
        if (!VerifyTools.isBlank(style) || !VerifyTools.isBlank(type)) {
            // 详情页
            if (!VerifyTools.isBlank(id)) {
                name.append("d").append(TagUtils.DOT);
            }
            // 列表页
            else if (index > 0) {
                name.append("l").append(TagUtils.DOT);
            }
        }
        append(name, style, TagUtils.DOT);
        append(name, type, TagUtils.DOT);
        append(name, id, TagUtils.DOT);
        append(name, index, TagUtils.DOT);
        append(name, subindex, TagUtils.DOT);

        if (name.length() > 0) {
            path.append(name).append("html");
        }
        return path.toString();
    }

    private void append(StringBuilder buffer, String value, String suffix) {
        if (!VerifyTools.isBlank(value)) {
            buffer.append(value).append(suffix);
        }
    }

    private void append(StringBuilder buffer, int value, String suffix) {
        if (value > 0) {
            buffer.append(value).append(suffix);
        }
    }

    /**
     * 获取网站URL前缀
     * 
     * @return 返回网站URL前缀
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * 设置网站URL前缀
     * 
     * @param prefix 要设置的网站URL前缀
     */
    void setPrefix(String prefix) {
        if (prefix != null && !prefix.endsWith(TagUtils.SLASH)) {
            prefix += TagUtils.SLASH;
        }
        this.prefix = prefix;
    }

    /**
     * 获取模块名称
     * 
     * @return 返回模块名称
     */
    public String getModule() {
        return module;
    }

    /**
     * 设置模块名称
     * 
     * @param module 要设置的模块名称
     */
    void setModule(String module) {
        this.module = module;
    }

    /**
     * 获取频道名称
     * 
     * @return 返回频道名称
     */
    public String getChannel() {
        return channel;
    }

    /**
     * 设置频道名称
     * 
     * @param channel 要设置的频道名称
     */
    void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * 获取子频道名称
     * 
     * @return 返回子频道名称
     */
    public String getSubchannel() {
        return subchannel;
    }

    /**
     * 设置子频道名称
     * 
     * @param subchannel 要设置的子频道名称
     */
    void setSubchannel(String subchannel) {
        this.subchannel = subchannel;
    }

    /**
     * 获取类型
     * 
     * @return 返回类型
     */
    public String getType() {
        return type;
    }

    /**
     * 设置类型
     * 
     * @param type 要设置的类型
     */
    void setType(String type) {
        this.type = type;
    }

    /**
     * 获取风格
     * 
     * @return 返回风格
     */
    public String getStyle() {
        return style;
    }

    /**
     * 设置风格
     * 
     * @param style 要设置的风格
     */
    void setStyle(String style) {
        this.style = style;
    }

    /**
     * 获取详情页ID
     * 
     * @return 返回详情页ID
     */
    public String getId() {
        return id;
    }

    /**
     * 设置详情页ID
     * 
     * @param id 要设置的详情页ID
     */
    void setId(String id) {
        this.id = id;
    }

    /**
     * 获取当前页数
     * 
     * @return 返回当前页数
     */
    public int getIndex() {
        return index;
    }

    /**
     * 设置当前页数
     * 
     * @param index 要设置的当前页数
     */
    void setIndex(int index) {
        this.index = index;
    }

    /**
     * 获取子分页数
     * 
     * @return 返回子分页数
     */
    public int getSubindex() {
        return subindex;
    }

    /**
     * 设置子分页数
     * 
     * @param subindex 要设置的子分页数
     */
    void setSubindex(int subindex) {
        this.subindex = subindex;
    }

    /**
     * 获取模板文件路径
     * 
     * @return 返回模板文件路径
     */
    public String getTemplate() {
        return template;
    }

    /**
     * 设置模板文件路径
     * 
     * @param template 要设置的模板文件路径
     */
    void setTemplate(String template) {
        this.template = template;
    }
}
