<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>#{column.title}</title>
</head>
<body>

	<core:each items="#{number}" var="i" status="s">
		${i}    <core:if test="s.first">first</core:if><core:else>not first</core:else>
	</core:each>
	--------------
	<core:each items="#{number}" var="i" status="s">
		${i}    <core:if test="i > 300 && i < 600">大于300且小于600</core:if>
	</core:each>
	--------------
	<core:each items="#{number}" var="i" status="s">
		${i}    <core:if test="${i < 300}">小于300</core:if>
				<core:elseif test="${i < 600}">小于600</core:elseif>
				<core:else>其他</core:else>
	</core:each>
	--------------
	<core:each items="#{number}" var="i" status="s">
		${i}    <core:if test="${i < 300}">小于300</core:if>
				<core:elseif test="${i < 600}">小于600</core:elseif>
				<core:else>其他</core:else>
		${i}
		<core:if test="${i < 400}">小于400</core:if>
		<core:elseif test="${i < 800}">小于800</core:elseif>
		<core:else>其他</core:else>
		<core:if test="${!s.last}">----</core:if>
	</core:each>
	--------------
		text a
		<core:if test="${xxx==null}">
			text b
		</core:if>
		<core:else>
		text c
		</core:else><fmt:date var="d">当前时间: ${d}</fmt:date>
		----
		text a
		<core:if test="${xxx!=null}">
			text b
		</core:if>
		<core:else>
		text c
		</core:else><fmt:date var="d">当前时间: ${d}</fmt:date>
		----
		text a
		<core:if test="${xxx==null}">
			text b
		</core:if>
		text c
		----
		text a
		<core:if test="${xxx==null}">text b
		</core:if>text c
		----
		text a
		<core:if test="${xxx==null}"><fmt:date var="d">当前时间: ${d}</fmt:date>
		</core:if><fmt:date var="d">当前时间: ${d}</fmt:date>
	--------------
	<core:block>
		text a
		<core:if test="${xxx==null}">
			text b
		</core:if>
		<core:else>
		text c
		</core:else><fmt:date var="d">当前时间: ${d}</fmt:date>
	</core:block>
		----
	<core:block>
		text a
		<core:if test="${xxx!=null}">
			text b
		</core:if>
		<core:else>
		text c
		</core:else><fmt:date var="d">当前时间: ${d}</fmt:date>
	</core:block>
		----
	<core:block>
		text a
		<core:if test="${xxx==null}">
			text b
		</core:if>
		text c
	</core:block>
		----
	<core:block>
		text a
		<core:if test="${xxx==null}">text b
		</core:if>text c
	</core:block>
		----
	<core:block>
		text a
		<core:if test="${xxx==null}"><fmt:date var="d">当前时间: ${d}</fmt:date>
		</core:if><fmt:date var="d">当前时间: ${d}</fmt:date>
	</core:block>
	--------------

</body>
</html>
