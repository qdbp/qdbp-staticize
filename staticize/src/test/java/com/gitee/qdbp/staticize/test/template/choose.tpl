<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>#{column.title}</title>
</head>
<body>

	<core:each items="number" var="i" status="s">
		${i}    <core:choose><core:when test="s.first">first</core:when><core:otherwise>not first</core:otherwise></core:choose>
	</core:each>
	--------------
	<core:each items="number" var="i" status="s">
		${i}    <core:choose><core:when test="i > 300 && i < 600">大于300且小于600</core:when></core:choose>
	</core:each>
	--------------
	<core:each items="number" var="i" status="s">
		${i}    <core:choose>
					<core:when test="i < 300">小于300</core:when>
					<core:when test="i < 600">小于600</core:when>
					<core:otherwise>其他</core:otherwise>
				</core:choose>
	</core:each>
	--------------
	<core:each items="number" var="i" status="s">
		${i}    <core:choose>
					<core:when test="i < 300">小于300</core:when>
					<core:when test="i < 600">小于600</core:when>
					<core:otherwise>其他</core:otherwise>
				</core:choose>
		${i}
		<core:choose>
			<core:when test="i < 400">小于400</core:when>
			<core:when test="i < 800">小于800</core:when>
			<core:otherwise>其他</core:otherwise>
		</core:choose>
		<core:if test="!s.last">----</core:if>
	</core:each>
	--------------
		text a
		<core:choose>
		<core:when test="xxx==null">
			text b
		</core:when>
		<core:otherwise>
		text c
		</core:otherwise>
		</core:choose><fmt:date var="d">当前时间: ${d}</fmt:date>
		----
		text a
		<core:choose>
		<core:when test="xxx!=null">
			text b
		</core:when>
		<core:otherwise>
		text c
		</core:otherwise>
		</core:choose><fmt:date var="d">当前时间: ${d}</fmt:date>
		----
		text a
		<core:choose>
		<core:when test="xxx==null">
			text b
		</core:when>
		</core:choose>
		text c
		----
		text a
		<core:choose>
		<core:when test="xxx==null">text b
		</core:when>
		</core:choose>text c
		----
		text a
		<core:choose>
		<core:when test="xxx==null"><fmt:date var="d">当前时间: ${d}</fmt:date>
		</core:when>
		</core:choose><fmt:date var="d">当前时间: ${d}</fmt:date>
	--------------
	<core:block>
		text a
		<core:choose>
		<core:when test="xxx==null">
			text b
		</core:when>
		<core:otherwise>
		text c
		</core:otherwise>
		</core:choose><fmt:date var="d">当前时间: ${d}</fmt:date>
	</core:block>
		----
	<core:block>
		text a
		<core:choose>
		<core:when test="xxx!=null">
			text b
		</core:when>
		<core:otherwise>
		text c
		</core:otherwise>
		</core:choose><fmt:date var="d">当前时间: ${d}</fmt:date>
	</core:block>
		----
	<core:block>
		text a
		<core:choose>
		<core:when test="xxx==null">
			text b
		</core:when>
		</core:choose>
		text c
	</core:block>
		----
	<core:block>
		text a
		<core:choose>
		<core:when test="xxx==null">text b
		</core:when>
		</core:choose>text c
	</core:block>
		----
	<core:block>
		text a
		<core:choose>
		<core:when test="xxx==null"><fmt:date var="d">当前时间: ${d}</fmt:date>
		</core:when>
		</core:choose><fmt:date var="d">当前时间: ${d}</fmt:date>
	</core:block>
	--------------

</body>
</html>
