<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="column" uri="http://www.jeshing.com/tags/column/"%>
<%@ taglib prefix="content" uri="http://www.jeshing.com/tags/content/"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>
<%@ taglib prefix="util" uri="http://www.jeshing.com/tags/util/"%>
<%@ taglib prefix="url" uri="http://www.jeshing.com/tags/url/"%>
<%@ taglib prefix="imgs" uri="http://www.jeshing.com/tags/images/"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/base/include/meta.tpl"%>
	<meta name="keywords" content="网站地图" />
	<meta name="description" content="网站地图" />
	<title>网站地图 - 乐居中国</title>
<%@ include file="/base/include/head.tpl"%>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/siteinfo.aboutus.css'/>">
</head>
<body>

	<%@ include file="/base/include/top.tpl"%>

	<%--页面主体--%>
	<div class="banner sitemap_banner png"></div>

	<div class="centern">
		<%@ include file="_banner.tpl"%>
		<br class="clear" />
	</div>

	<div class="centern">
		<div class="about_left">
			<%@ include file="_menu.tpl"%>
		</div>

		<div class="about_right">
			<div class="ab_title">
				<img src="<url:link href='resources/site/images/siteinfo/mtu.png'/>"/> 网站地图<span>Site
					map</span>
			</div>
			<div class="line"></div>
			<div class="ab_content sm_cont">
				<div class="sm_title">家庭专题</div>
				<div class="sm_content">
					<a href="<url:link href='zhuanti/danshenguizu/'/>">单身贵族</a>
					<a href="<url:link href='zhuanti/errenshijie/'/>">二人世界</a>
					<a href="<url:link href='zhuanti/sankouzhijia/'/>">三口之家</a>
					<a href="<url:link href='zhuanti/tianlunzhile/'/>">天伦之乐</a>
				</div>
			</div>

			<div class="ab_content sm_cont">
				<div class="sm_title">装修宝典</div>
				<div class="sm_content">
					<a href="<url:link href='zhuangxiubidu/list.html'/>">装修必读</a>
					<a href="<url:link href='zhuangxiu/zhuangxiujiqiao/list.html'/>">装修技巧</a>
					<a href="<url:link href='zhuangxiu/jingtiaoxixuan/list.html'/>">精挑细选</a>
					<a href="<url:link href='zhuangxiu/jiazhuanggaizaowang/list.html'/>">家装改造王</a>
					<a href="<url:link href='zhuangxiu/zhuangxiufengshui/list.html'/>">装修风水</a>
				</div>
			</div>

			<div class="ab_content sm_cont">
				<div class="sm_title">家居搭配</div>
				<div class="sm_content">
					<a href="<url:link href='dapei/zhuangshijingcui/list.html'/>">装饰精粹</a>
					<a href="<url:link href='dapei/dapeijiqiao/list.html'/>">搭配技巧</a>
					<a href="<url:link href='dapei/shounajiqiao/list.html'/>">收纳技巧</a>
					<a href="<url:link href='dapei/jiankangbaoyang/list.html'/>">健康保养</a>
				</div>
			</div>

			<div class="ab_content sm_cont">
				<div class="sm_title">案例参考</div>
				<div class="sm_content">
					<a href="<url:link href='anli/'/>">案例参考</a>
					<a href="<url:link href='anli/list.html'/>">案例分类</a>
				</div>
			</div>

			<div class="ab_content sm_cont">
				<div class="sm_title">乐居百科</div>
				<div class="sm_content sm_baike">
					<a href="<url:link href='baike/'/>">乐居百科</a>
					<a href="<url:link href='baike/baikesheji/list.html'/>">乐居百科-设计</a>
					<a href="<url:link href='baike/baikezhuangxiu/list.html'/>">乐居百科-装修</a>
					<a href="<url:link href='baike/baikechanpin/list.html'/>">乐居百科-产品</a>
					<a href="<url:link href='baike/baikeshenghuo/list.html'/>">乐居百科-生活</a>
				</div>
			</div>

			<div class="ab_content sm_cont">
				<div class="sm_title">乐品</div>
				<div class="sm_content sm_baike">
					<a href="<url:link href='lepin/'/>">乐品</a>
					<a href="<url:link href='lepin/ruanzhuang/list.html'/>">乐品-软装</a>
					<a href="<url:link href='lepin/jiaju/list.html'/>">乐品-家居</a>
					<a href="<url:link href='lepin/jiancai/list.html'/>">乐品-建材</a>
					<a href="<url:link href='lepin/jiadian/list.html'/>">乐品-家电</a>
				</div>
			</div>

			<div class="ab_content sm_cont">
				<div class="sm_title">装修计算器</div>
				<div class="sm_content sm_baike">
					<a href="<url:link href='jisuanqi/qiangzhuan.html'/>">墙砖计算器</a>
					<a href="<url:link href='jisuanqi/dizhuan.html'/>">地砖计算器</a>
					<a href="<url:link href='jisuanqi/diban.html'/>">地板计算器</a>
					<a href="<url:link href='jisuanqi/bizhi.html'/>">壁纸计算器</a>
					<a href="<url:link href='jisuanqi/tuliao.html'/>">涂料计算器</a>
					<a href="<url:link href='jisuanqi/chuanglian.html'/>">窗帘计算器</a>
				</div>
			</div>

			<div class="ab_content sm_cont">
				<div class="sm_title">关于我们</div>
				<div class="sm_content sm_baike">
					<a href="<url:link href='siteinfo/aboutus.html'/>">关于我们</a>
					<a href="<url:link href='siteinfo/contactus.html'/>">联系我们</a>
					<a href="<url:link href='siteinfo/joinus.html'/>">加入我们</a>
					<a href="<url:link href='siteinfo/copyright.html'/>">版权声明</a>
					<a href="<url:link href='siteinfo/sitemap.html'/>">网站地图</a>
				</div>
			</div>

		</div>

	</div>
	<br class="clear"/>
	<%@ include file="/base/include/foot.tpl"%>
</body>
</html>

