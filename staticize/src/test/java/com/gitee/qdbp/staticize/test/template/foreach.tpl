<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>each tag</title>
</head>
<body>
	--- {number}
	<core:forEach items="#{number}" var="i" varStatus="s">
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
	--- {number} begin="6" rows="5"
	<core:forEach items="#{number}" var="i" varStatus="s" begin="5" end="9">
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
	--- {number} begin="8" rows="5"
	<core:forEach items="#{number}" var="i" varStatus="s" begin="7" end="11">
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
	--- {number} begin="1" rows="1"
	<core:forEach items="#{number}" var="i" varStatus="s" begin="0" end="0">
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
	--- {number} begin="2" rows="3"
	<core:forEach items="#{number}" var="i" varStatus="s" begin="1" end="3">
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
	--- {number} begin="10" rows="1"
	<core:forEach items="#{number}" var="i" varStatus="s" begin="9" end="9">
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
	--- {number} begin="20" rows="1"
	<core:forEach items="#{number}" var="i" varStatus="s" begin="19" end="19">
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
	--- {number} step="3"
	<core:forEach items="#{number}" var="i" varStatus="s" step="3">
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
	--- {number} begin="2" step="3"
	<core:forEach items="#{number}" var="i" varStatus="s" begin="1" step="3">
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
	--- {ascii}
	<core:forEach items="#{ascii}" var="i" varStatus="s">
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
	--- {ascii} 1-1
	<core:forEach items="#{ascii}" var="i" varStatus="s" begin="0" end="0">
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
	--- {ascii} 2-3
	<core:forEach items="#{ascii}" var="i" varStatus="s" begin="1" end="2">
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
	--- {ascii} 20
	<core:forEach items="#{ascii}" var="i" varStatus="s" begin="19" end="19">
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
	--- begin="6" rows="5"
	<core:forEach var="i" varStatus="s" begin="6" end="10">
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
	--- begin="3" rows="3" x begin="600" rows="4"
	<core:forEach var="i" varStatus="s" begin="3" end="5">
		<core:forEach var="i" varStatus="s" begin="600" end="603">
			index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
		</core:forEach>
		index:${s.index} count:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	</core:forEach>
</body>
</html>
