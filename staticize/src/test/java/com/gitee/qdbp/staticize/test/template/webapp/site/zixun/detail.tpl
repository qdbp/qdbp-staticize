<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="column" uri="http://www.jeshing.com/tags/column/"%>
<%@ taglib prefix="content" uri="http://www.jeshing.com/tags/content/"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>
<%@ taglib prefix="util" uri="http://www.jeshing.com/tags/util/"%>
<%@ taglib prefix="url" uri="http://www.jeshing.com/tags/url/"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<content:article id="${URLDATA.id}" page="${URLDATA.index}">
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/base/include/meta.tpl"%>
		<meta name="keywords" content="${article.keyword}" />
		<meta name="description" content="${article.digest}" />
		<title>${article.title}</title>
	<%@ include file="/base/include/head.tpl"%>

	<!-- add by wangzheng at 20140808 for 分享 begin -->
	<script type="text/javascript" charset="utf-8" src="http://static.bshare.cn/b/buttonLite.js#style=-1&amp;uuid=&amp;pophcol=2&amp;lang=zh"></script>
	<script type="text/javascript" charset="utf-8" src="http://static.bshare.cn/b/bshareC0.js"></script>
	<!-- add by wangzheng at 20140808 for 分享  end -->

	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/zixun.detail.css'/>">
	<%--
	<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/zixun.detail.ie.css'/>">
	<![endif]-->
	--%>
    <script type="text/javascript" src="<url:link href='resources/site/js/page.js'/>"></script>
    <script type="text/javascript" src="<url:link href='resources/site/js/pinglun.js'/>"></script>
	<script type="text/javascript" src="<url:link href='resources/site/js/zixun.detail.js'/>"></script>
	<core:if test="${param.preview!=null}">
    <script type="text/javascript" src="<url:link href='resources/site/js/preview.js'/>"></script>
	</core:if>


</head>
<body>

	<%@ include file="/base/include/top.tpl"%>

	<div class="centern title-box decoration-detail">
	<column:detail key="${article.columnId}">
		<a class="link highlight">${root.shortTitle}文章</a>
		<div class="decoration-nav">
			<a href="<url:link/>">首页</a>
			<core:forEach var="parent" varStatus="p" items="${parents}">
			<span> &gt; </span>
			<a href="<url:link href='${parent.url}'/>">${parent.title}</a>
			</core:forEach>
			<span> &gt; </span>
			${root.shortTitle}文章
		</div>
	</column:detail>
	</div>


	<div class="centern">
		<%--内容开始--%>
		<div class="centern-left">
			<div class="decoration-content">
				<div class="title text-align text-align-new">${article.title}</div>
				<div class="subtitle text-align text-align-new">
					<span>
						<fmt:formatDate value="${article.issueTime}" type="both" pattern="yyyy年MM月dd日 HH:mm:ss" />
					</span>
					<span>
						来自: <b>${article.source.title}</b>
						<core:if test="${article.editor!=null}"><span class="bianji">编辑: <b>${article.editor}</b></span></core:if>
					</span>
					<a class="pinlunNum" data-id="${URLDATA.id }" data-url="<url:link href="web/site/comment/countComment.json"/>"><span class="plcount">0</span>条评论</a>

				</div>
			</div>

		<div class="center-width articale">
			${article.content}
		</div>

		<div class="center-width">
			<%-- 分页 --%>
			<core:if test="${count>1}">
				<div class="pages">
					<util:paging count="${count}" index="${URLDATA.index}" rows="1"/>
				</div>
			</core:if>
		</div>
		<%--内容结束--%>

		<div class="title-ch whole taglist-whole detail-whole">
		    <div class="fenxiang">
					<div class="pinglun">
						<a href="#"><span>我想说两句</span></a>

					</div>
					<div class="add">
						<a href="#"><img src="<url:link href='resources/site/images/wenda/add.png'/>"/></a>
						<span>分享</span>
						<div class="triangle">
							<a href="#"><img
								src="<url:link href='resources/site/images/wenda/daosanjiao.png'/>"/></a>
						</div>
					</div>
					<div class="bshare-custom layer png hide">
						<a title="分享到新浪微博" class="bshare-sinaminiblog">新浪微博</a>
						<a title="分享到QQ空间" class="bshare-qzone">QQ空间</a>
						<a title="分享到腾讯微博" class="bshare-qqmb">腾讯微博</a>
						<a title="分享到豆瓣" class="bshare-douban">豆瓣</a>
						<a title="分享到人人网" class="bshare-renren">人人网</a>
					</div>
					<div class="pinglun shoucang">
						<a href="#" id="storeHref"><span>收藏</span></a>
						<input type="hidden" value="${article.id}" id="hidValue" name="hidValue" data-src="<url:link href="web/site/favorite/add.json"/>">
					    <input type="hidden" value="${article.type.getKey()}" id="hidType">
					</div>


	     	</div>
	     </div>

		<div class="pinglun-box" data-id="${URLDATA.id }" ></div>

		<%--相关商品开始--%>
		<div class="center-width title-box title-box-noborder">
			<a class="link" href="#">相关商品<span>RELATED PRODUCTS</span></a>
		</div>
		<div class="center-width relative-product">
			<div class="box">
				<div class="wrap">
					<position:detail key="${root.name}|xiangqing|xiangguanshangpin">
						<position:content key="${position.id}" contentId="${article.id}">
							<core:forEach var="item" items="${list}">
								<div class="item">
									<img src="<url:image src='${item.image.src}'/>" width="605" height="160" alt="${item.image.title}"/>
									<a href="<url:link href='${item.url}'/>" target="_blank" title="${item.image.title}" ></a>
								</div>
							</core:forEach>
						</position:content>
					</position:detail>
				</div>
			</div>
			<a class="prev"></a>
			<a class="next"></a>
		</div>
		<%--相关商品结束--%>

		<%-- 相关推荐 开始 --%>
		<div class="center-width title-box title-box-noborder">
			<a class="link" href="#">相关推荐<span>EDITOR'S SUJESTION</span></a>
		</div>
		<div class="center-width">
			<content:bytagword contentType="${article.type.key}"  rows="2" contentId="${article.id}">
				<core:forEach var="item" items="${list}" varStatus="s">
					<div class="content-box <core:if test="${s.index==0}">to-left</core:if> con-ch">
						<div class="images">
							<a href="<url:link href='${item.url}'/>" title="${item.image.title}"><img src="<url:image src='${item.image.src}'/>" alt="${item.image.title}" height="150" width="160"/></a>
							<div class="clear"></div>
						</div>
						<div class="content">
							<div class="title">
								<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a>
							</div>
							<div class="text">
								<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.digest}</a>
							</div>
						</div>
					</div>
				</core:forEach>
			</content:bytagword>
			<br class="clear" />
		</div>

		<core:if test="${article.tagword!=null}">
		<%-- 相关推荐 结束 --%>
		<div class="center-width tags-box tags-margin">
			<span>热门标签：</span>
			<util:split string="${article.tagword }" var="tagwords">
				<core:forEach var="word" items="${tagwords}" varStatus="s">
					<a <core:if test="${s.first}">class="first"</core:if> href="<url:list column="zixun" type="${word}" style="tagword"/>" title="${word}">${word}</a>
				</core:forEach>
			</util:split>
		</div>
		</core:if>

		</div>
		<div class="centern-right zx_right_top_m">
		   <div class="huadong_pictrue_div">
			   	<position:detail key="${root.name}|xiangqing|youceguanggao">
					<position:content key="${position.id}" rows="1">
						<core:forEach var="item" items="${list}">
							<div class="zx_right_top zx_right_top_m"><a href="<url:link href='${item.url}'/>" title="${item.image.title}"><img src="<url:image src='${item.image.src}'/>" width="300" height="235" alt="${item.image.title}"/></a></div>
							<div class="huadong_text">
								<div class="font zx_right_top_1_nr1">
									<div class="line"><a href="<url:link href='${item.url}'/>" title="${item.shortTitle}">${item.shortTitle}</a></div>
									<div class="line"><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></div>
								</div>
							</div>
						</core:forEach>
					</position:content>
				</position:detail>
		  </div>

		  <%--装修必读开始--%>
		  <div class="decoration-must">
		  <position:detail key="${root.name}|xiangqing|zhuangxiubidu">
			  <div class="title"><a href="<url:link href='${position.url}'/>">${position.title}</a></div>
			  <position:content key="${position.id}" rows="5">
				   <div class="content">
				   <ul>
				   <core:forEach var="item" items="${list}">
					   <li><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></li>
				   </core:forEach>
				   </ul>
				   </div>
			   </position:content>
		   </position:detail>
		   </div>
		   <%--装修必读结束--%>


		  <%--金品汇开始--%>
		  <div class="decoration-must decoration-k">
	  		  <position:detail key="${root.name}|xiangqing|jinpinhui">
				<position:content key="${position.id}" contentId="${article.id}" rows="10" upgrade="true">
				<div class="title"></div>
			  	  	<div class="decoration-k-wrap-1">
		  		   		<core:forEach var="item" items="${list}">
		  	  				<div class="decoration-k-wrap-2">
			  				   <div class="images">
			  				   <img src="<url:image src='${item.image.src}'/>" height="300" width="300" alt="${item.image.title}"/>
			  				   <a href="<url:link href='${item.url}'/>" title="${item.title}"></a>
			  				   </div>
			  				   <div class="text-align images-description">${item.title}</div>
			  		  		</div>
		  		  		</core:forEach>
			  	  	</div>
				 </position:content>
			  </position:detail>
			  <a class="prev"></a>
			  <a class="next"></a>
		   </div>
		   <%--金品汇结束--%>


		  </div>
		<br class="clear"/>
	</div>

	<input type="hidden" name="pageUrl" value="zixun/${URLDATA.id }.html"/>
	<%@ include file="/base/include/foot.tpl"%>
</body>
</html>
</content:article>

