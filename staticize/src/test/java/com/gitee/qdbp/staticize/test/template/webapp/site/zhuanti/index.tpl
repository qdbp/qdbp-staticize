<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="column" uri="http://www.jeshing.com/tags/column/"%>
<%@ taglib prefix="content" uri="http://www.jeshing.com/tags/content/"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>
<%@ taglib prefix="util" uri="http://www.jeshing.com/tags/util/"%>
<%@ taglib prefix="url" uri="http://www.jeshing.com/tags/url/"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="/base/include/meta.tpl"%>
	<column:detail key="zhuanti">
		<meta name="keywords" content="${column.seo.key}" />
		<meta name="description" content="${column.seo.desc}" />
		<title>${column.seo.title}</title>
	</column:detail>
	<%@ include file="/base/include/head.tpl"%>
	<script type="text/javascript" src="<url:link href='resources/thirdparty/jquery/jquery.easing.1.3.js'/>"></script>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/zhuanti.css'/>">
		<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/dapei.css'/>">
	<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/zhuangxiu.ie.css'/>">
	<![endif]-->
	<script type="text/javascript" src="<url:link href='resources/site/js/zhuanti.js'/>"></script>


</head>
<body>

	<jsp:include page="/base/include/top.tpl" >
		<jsp:param name="column" value="zhuanti" />
	</jsp:include>

	<%-- banner开始 --%>
	<position:detail key="zhuanti|daohanglanguanggao">
	<position:content key="${position.id}">
		<core:forEach var="item" items="${list}" >
		<core:if test="${item.name==URLDATA.type}">
		<div class="marquee-box" style="background-image:url(<url:image src='${item.image.src}'/>);">
			<div class="centern">
			<div class="text text-gai">${item.digest}</div>
			<div class="clear"></div>
			</div>
		</div>
		</core:if>
		</core:forEach>
	</position:content>
	</position:detail>
	<%-- banner结束 --%>

	<div class="centern centern_7_4">
		<position:detail key="zhuanti|zicaidan">
		<position:content key="${position.id}"  rows="4">
		<core:forEach var="item" items="${list}" >
		<div class="centern_7_4_1 hover-on <core:if test='${item.name == URLDATA.type}'>on</core:if>">
			<div class="magnify">
				<div class="triangle-up"></div>
				<div class="triangle-white"></div>
				<img src="<url:image src='${item.image.src}'/>" alt="${item.image.title}" width="240" height="200"/>
				<div class="centern_7_4_1_1">${item.title}</div>
				<div class="centern_7_4_1_2">${item.shortTitle}</div>
				<a href="<url:link href='${item.url}'/>" class="dssh-content-bg" title="${item.shortTitle}"></a>
			</div>
		</div>
		</core:forEach>
		</position:content>
		</position:detail>

		<div class="clear"></div>
	</div>

	<%-- 浮动层开始 --%>
	<div class="point-float">
		<div class="items">
			<ul>
				<li><a href="<url:index column='${URLDATA.column}' type='${URLDATA.type}' />#shejiyuansu">设计元素</a></li>
				<li><a href="<url:index column='${URLDATA.column}' type='${URLDATA.type}' />#zhuangxiujiqiao">装修技巧</a></li>
				<li><a href="<url:index column='${URLDATA.column}' type='${URLDATA.type}' />#dapeijiqiao">搭配技巧</a></li>
				<li><a href="<url:index column='${URLDATA.column}' type='${URLDATA.type}' />#anlituijian">案例推荐</a></li>
				<li><a href="<url:index column='${URLDATA.column}' type='${URLDATA.type}' />#danpintuijian">单品推荐</a></li>
			</ul>
		</div>
	</div>
	<%-- 浮动层结束 --%>

	<a name="shejiyuansu" class="maodian"></a>
	<%-- 设计元素开始 --%>
	<position:detail key="${URLDATA.type}|shejiyuansu">
		<div class="centern title-box title-box-noborder">
			${position.title}<span>${position.subTitle}</span>
		</div>
		<div class="centern design-all">
		<position:content key="${position.id}"  rows="3">
			<core:forEach var="item" items="${list}" >
					<div class="design-content">
						<div class="title"><a href="<url:link href='${item.url}'/>"  title="${item.title}">${item.title}</a></div>
						<div class="text"><p>${item.digest}</p></div>
					</div>

			</core:forEach>
		</position:content>
		</div>
	</position:detail>
	<%-- 设计元素結束 --%>

	<%-- 装修技巧开始 --%>
	<position:detail key="${URLDATA.type}|zhuangxiujiqiao">
	<a name="zhuangxiujiqiao" class="maodian"></a>
	<div class="centern title-box title-box-big">
		<div class="centern title-box title-box-col">
			${position.title}<span>${position.subTitle}</span>
		</div>
	</div>

	<div class="marquee-skill">
		<div class="centern skiils">
			<div class="content-box  first-change">
				<div class="title">${position.shortTitle}</div>
				<div class="text text-a">${position.digest}</div>
				<div class="go"><a href="<url:link href='${position.url}'/>"></a></div>
				<div class="more"><a href="<url:link href='${position.url}'/>">更多</a></div>
			</div>

			<position:content key="${position.id}"  rows="4">
			<core:forEach var="item" items="${list}" >
			<div class="content-box to-top to-top-change">
				<div class="images">
					<a href="<url:link href='${item.url}'/>" title="${item.image.title}"><img src="<url:image src='${item.image.src}'/>" alt="${item.image.title}" width="240" height="200"/></a>
					<div class="clear"></div>
				</div>
				<div class="content">
					<div class="title"><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></div>
					<div class="text">${item.digest}</div>
				</div>
			</div>
			</core:forEach>
			</position:content>
			<br class="clear" />
		</div>
	</div>
	</position:detail>
	<%-- 装修技巧 结束 --%>

	<%-- 搭配技巧开始 --%>
	<position:detail key="${URLDATA.type}|dapeijiqiao">
	<a name="dapeijiqiao" class="maodian"></a>
	<div class="centern title-box title-box-big-green">
		<div class="centern title-box title-box-col title-box-col-green">
			${position.title}<span>${position.subTitle}</span>
		</div>
	</div>

	<div class="marquee-skill marquee-dapei">
		<div class="centern skiils">
			<div class="content-box  first-change">
				<div class="title">${position.shortTitle}</div>
				<div class="text text-a">${position.digest}</div>
				<div class="go"><a href="<url:link href='${position.url}'/>"></a></div>
				<div class="more"><a href="<url:link href='${position.url}'/>">更多</a></div>
			</div>

			<position:content key="${position.id}"  rows="4">
			<core:forEach var="item" items="${list}" >
			<div class="content-box to-top to-top-change">
				<div class="images">
					<a href="<url:link href='${item.url}'/>" title="${item.image.title}"><img src="<url:image src='${item.image.src}'/>" alt="${item.image.title}" width="240" height="200"/></a>
					<div class="clear"></div>
				</div>
				<div class="content">
					<div class="title"><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></div>
					<div class="text">${item.digest}</div>
				</div>
			</div>
			</core:forEach>
			</position:content>
			<br class="clear" />
		</div>
	</div>
	</position:detail>
	<%-- 搭配技巧 结束 --%>

	<%-- 案例推荐开始 --%>
	<position:detail key="${URLDATA.type}|anlituijian">
	<a name="anlituijian" class="maodian"></a>
	<div class="centern title-box title-box-big">
		<div class="centern title-box title-box-col">
			${position.title}<span>${position.subTitle}</span>
		</div>
	</div>

	<div class="marquee-skill marquee-caserecommend">
		<div class="centern">
		<position:content key="${position.id}"  rows="8">
		<core:forEach var="item" items="${list}" >
			<div class="case-content">
				<div class="images"><a href="<url:link href='${item.url}'/>" title="${item.image.title}"><img src="<url:image src='${item.image.src}'/>" alt="${item.image.title}" width="210" height="160"/></div>
				<div class="title"><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></div>
				<div class="description">${item.shortTitle}</div>
				<div class="type">${item.digest}</div>
			</div>
		</core:forEach>
		</position:content>
		</div>
	</div>
	</position:detail>
	<%-- 案例推荐结束 --%>

	<%-- 单品推荐开始 --%>
	<position:detail key="${URLDATA.type}|danpintuijian">
	<a name="danpintuijian" class="maodian"></a>
	<div class="centern title-box title-box-big-green">
		<div class="centern title-box title-box-col title-box-col-green">
			${position.title}<span>${position.subTitle}</span>
		</div>
	</div>
	<div class="marquee-skill marquee-item">
		<div class="centern">
		<position:content key="${position.id}"  rows="8">
			<core:forEach var="item" items="${list}" >
			<div class="case-content-dp">
				<div class="images"><a href="<url:link href='${item.url}'/>" title="${item.image.title}"><img src="<url:image src='${item.image.src}'/>" alt="${item.image.title}" width="230" height="230"/></a></div>
				<div class="title"><a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a></div>
			</div>
			</core:forEach>
		</position:content>
		</div>
	</div>
	</position:detail>
	<%-- 单品推荐结束 --%>

	<%@ include file="/base/include/foot.tpl"%>
</body>
</html>

