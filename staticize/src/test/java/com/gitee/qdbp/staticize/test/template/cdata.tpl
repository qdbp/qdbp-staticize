<core:comment> 测试注释标签 </core:comment>
	--- {number}
	<core:each items="#{number}" var="i" status="s">
	<![CDATA[
		index:${s.index} number:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	]]>
	</core:each>
	--- {number} begin="6" rows="5"
	<core:comment> 测试注释标签 </core:comment>
	<core:each items="#{number}" var="i" status="s" begin="6" rows="5">
	<![CDATA[
		index:${s.index} number:${s.count} begin:${s.begin} rows:${s.rows} first:${s.first} last:${s.last} value:${i}
	]]>
	</core:each>
	<![CDATA[
	<core:comment>
	<fmt:date var="@DateTools.parse('2020-01-01')"/>
	</core:comment>
	]]>

a &lt; b
<![CDATA[a &lt; b]]>
<![CDATA[number.size = #{number.length}]]>
<core:if test="number.length &gt; 0">
number.size = #{number.length}
</core:if>

<![CDATA[<%-- 测试CDATA中的注释标签 --%>]]>
