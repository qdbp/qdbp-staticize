
<%--顶部 开始--%>
	<div class="header-info">
		<div class="centern-big">
			<div class="left">
				<span class="fs14 color-normal bold">南京</span>
				<a class="fs14 color666">[切换城市]</a>
				<span class="fs12 color666">您好，欢迎来到乐居中国！</span>
				<a class="fs12 color666">请登录</a>
				<a class="fs12 color666">免费注册</a>
			</div>
			<div class="right">
				 <a class="left fs12 color666">在线客服</a>
				 <a class="left ml_5 fs12 color666">帮助中心</a>
				 <a class="left ml_5 bg4 sina-weibo-png"></a>
				 <a class="left ml_5 bg4 qq-weibo-png"></a>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="head-menu">
		<div class="centern-big head-nav" >
			<ul class="head-ul">
				<li class="head-li-icon"><a class="head-icon"></a></li>
				<position:detail key="shouye|toubudaohanglan">
					<position:content var="list" key="${position.id}">
						<core:each var="item" items="${list}" >
						<li class="head-li <core:if test="${param.column==item.name}">head-li-selected</core:if>" ><a href="<url:link href='${item.url}'/>" >${item.title}</a></li>
						</core:each>
					</position:content>
				</position:detail>
				<li class="search-li">
					<span class="search-label">
						<font>文章</font><a class="bg1 search-dropdown"></a>
					</span>
					<input type="text" class="search-input"/>
					<a class="search-btn">搜索</a>
				</li>
			</ul>
			<div class="clear"></div>
		</div>
	</div>
<%--顶部 结束--%>
