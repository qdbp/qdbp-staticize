package com.gitee.qdbp.staticize.test.publish;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.testng.annotations.Test;
import com.gitee.qdbp.tools.utils.DateTools;
import com.gitee.qdbp.tools.utils.JsonTools;

/**
 * 发布测试类
 *
 * @author zhaohuihua
 * @version 140605
 */
@Test
public class TemplatePublishTest {

    @Test
    public void testCascadePublish() throws Exception {
        BasePublishTools.testPublish("../template/", "cascade", null);
    }

    private Map<String, Object> getStackFunctionsTestData() {
        Map<String, Object> user1 = JsonTools.parseAsMap(
            "{name:'coral',height:170,weight:60,addresses:[{type:'home',telphone:'0551-555555'},{type:'office',telphone:'0551-666666'}]}");
        user1.put("birthday", DateTools.parse("2000-01-01"));
        Map<String, Object> user2 = JsonTools.parseAsMap("{}");
        Map<String, Object> user3 =
                JsonTools.parseAsMap("{name:'evan',addresses:[{type:'',telphone:''},{type:'',telphone:''}]}");
        Map<String, Object> user4 = JsonTools.parseAsMap("{name:'kelly',addresses:[]}");

        Map<String, Object> map = new HashMap<>();
        map.put("user1", user1);
        map.put("user2", user2);
        map.put("user3", user3);
        map.put("user4", user4);
        return map;
    }

    @Test
    public void testStackFunctionsPublish() throws Exception {
        Map<String, Object> map = getStackFunctionsTestData();
        BasePublishTools.testPublish("../template/", "stack.functions", map);
    }

    @Test
    public void testStackFunctionsPublish2() throws Exception {
        Map<String, Object> map = getStackFunctionsTestData();
        BasePublishTools.testPublish("../template/", "stack.functions2", map);
    }

    @Test
    public void testCommentTagPublish() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("number", new Integer[] { 111, 222, 333, 444, 555, 666, 777, 888, 999, 1000 });

        BasePublishTools.testPublish("../template/", "comment", map);
    }

    @Test
    public void testCdataTagPublish() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("number", new Integer[] { 111, 222, 333, 444, 555, 666, 777, 888, 999, 1000 });

        BasePublishTools.testPublish("../template/", "cdata", map);
    }

    @Test
    public void testDateFormatPublish() throws Exception {
        Map<String, Object> map = new HashMap<>();

        long ms = System.currentTimeMillis();
        map.put("yesterday", new Date(ms - 24 * 60 * 60 * 1000));

        BasePublishTools.testPublish("../template/", "date.format", map);
    }

    @Test
    public void testEachTagPublish() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("number", new Integer[] { 111, 222, 333, 444, 555, 666, 777, 888, 999, 1000 });
        map.put("ascii", new String[] { "ascii" });

        BasePublishTools.testPublish("../template/", "each", map);
    }

    @Test
    public void testEach2TagPublish() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("number", new Integer[] { 111, 222, 333, 444, 555, 666, 777, 888, 999, 1000 });
        map.put("ascii", new String[] { "ascii" });

        BasePublishTools.testPublish("../template/", "each2", map);
    }

    @Test
    public void testIfTagPublish() throws Exception {
        Map<String, Object> map = new HashMap<>();
        Map<String, String> column = new HashMap<>();
        column.put("id", "100088");
        column.put("title", "资讯频道");
        map.put("column", column);
        map.put("number", new Integer[] { 111, 222, 333, 444, 555, 666, 777, 888, 999, 1000 });

        BasePublishTools.testPublish("../template/", "if", map);
    }

    @Test
    public void testIf2TagPublish() throws Exception {
        Map<String, Object> map = new HashMap<>();
        Map<String, String> column = new HashMap<>();
        column.put("id", "100088");
        column.put("title", "资讯频道");
        map.put("column", column);
        map.put("number", new Integer[] { 111, 222, 333, 444, 555, 666, 777, 888, 999, 1000 });

        BasePublishTools.testPublish("../template/", "if2", map);
    }

    @Test
    public void testChooseTagPublish() throws Exception {
        Map<String, Object> map = new HashMap<>();
        Map<String, String> column = new HashMap<>();
        column.put("id", "100088");
        column.put("title", "资讯频道");
        map.put("column", column);
        map.put("number", new Integer[] { 111, 222, 333, 444, 555, 666, 777, 888, 999, 1000 });

        BasePublishTools.testPublish("../template/", "choose", map);
    }

    @Test
    public void testIncludeTagPublish() throws Exception {
        Map<String, Object> map = new HashMap<>();
        Map<String, String> column = new HashMap<>();
        column.put("id", "100088");
        column.put("title", "资讯频道");
        map.put("column", column);
        map.put("user", "zhaohuihua");
        map.put("number", new Integer[] { 111, 222, 333, 444, 555, 666, 777, 888, 999, 1000 });

        BasePublishTools.testPublish("../template/", "include/include", map);
    }

    @Test
    public void testImportPublish() throws Exception {
        Map<String, Object> map = new HashMap<>();

        long ms = System.currentTimeMillis();
        map.put("phone", "13900008888");
        map.put("yesterday", new Date(ms - 24 * 60 * 60 * 1000));

        // imports.tpl
        BasePublishTools.testPublish("../template/", "imports", map);
    }
}
