//package com.gitee.qdbp.staticize.test.tags.ext;
//
//import java.io.IOException;
//import com.gitee.qdbp.staticize.exception.TagException;
//import com.gitee.qdbp.staticize.tags.base.BaseTag;
//import com.gitee.qdbp.staticize.tags.base.NextStep;
//
///**
// * 分页URL标签
// *
// * @author zhaohuihua
// * @version 140630
// */
//public class UrlPageTag extends BaseTag {
//
//    /**
//     * 一共多少行
//     */
//    private int count = 0;
//
//    /**
//     * 每页显示多少行
//     */
//    private int rows = 0;
//
//    /**
//     * 当前页数
//     */
//    private int page;
//
//    /**
//     * 保存上一页的URL的变量名
//     */
//    private String prev;
//
//    /**
//     * 保存下一页的URL的变量名
//     */
//    private String next;
//
//    /**
//     * 保存第一页的URL的变量名
//     */
//    private String first;
//
//    /**
//     * 保存最后一页的URL的变量名
//     */
//    private String last;
//
//    /**
//     * 设置每页显示多少行
//     *
//     * @param rows 每页显示多少行
//     */
//    public void setRows(int rows) {
//        this.rows = rows;
//    }
//
//    /**
//     * 设置一共多少行
//     *
//     * @param count 一共多少行
//     */
//    public void setCount(int count) {
//        this.count = count;
//    }
//
//    /**
//     * 设置当前页数
//     *
//     * @param page 当前页数。
//     */
//    public void setPage(int page) {
//        this.page = page;
//    }
//
//    /**
//     * 设置保存上一页的URL的变量名
//     * 
//     * @param prev 保存上一页的URL的变量名
//     */
//    public void setPrev(String prev) {
//        this.prev = prev;
//    }
//
//    /**
//     * 设置保存下一页的URL的变量名
//     * 
//     * @param next 保存下一页的URL的变量名
//     */
//    public void setNext(String next) {
//        this.next = next;
//    }
//
//    /**
//     * 设置保存第一页的URL的变量名
//     * 
//     * @param first 保存第一页的URL的变量名
//     */
//    public void setFirst(String first) {
//        this.first = first;
//    }
//
//    /**
//     * 设置保存最后一页的URL的变量名
//     * 
//     * @param last 保存最后一页的URL的变量名
//     */
//    public void setLast(String last) {
//        this.last = last;
//    }
//
//    /**
//     * 标签处理方法
//     *
//     * @return 下一步处理方式
//     */
//    @Override
//    public NextStep doHandle() throws TagException, IOException {
//
//        // 根据总行数计算总页数
//        int pages = 0;
//        if (count != 0) {
//            int row = TagUtil.getPageRows(rows);
//            pages = count % row == 0 ? count / row : count / row + 1;
//        }
//
//        int index = this.page <= 0 ? 1 : this.page;
//
//        this.addStackValue("pages", pages);
//        this.addStackValue("index", index);
//
//        if (prev != null) {
//            String prevUrl = null;
//            if (index > 1) {
//                String href = UrlUtil.getInstance().getToPageUrl(request, -1);
//                prevUrl = TagUtil.toSiteAbsoluteUrl(href);
//            }
//            this.addStackValue(prev, prevUrl);
//        }
//
//        if (next != null) {
//            String nextUrl = null;
//            if (index < pages) {
//                String href = UrlUtil.getInstance().getToPageUrl(request, 1);
//                nextUrl = TagUtil.toSiteAbsoluteUrl(href);
//            }
//            this.addStackValue(next, nextUrl);
//        }
//
//        if (first != null) {
//            String firstUrl = null;
//            if (count > 0) {
//                String href = UrlUtil.getInstance().getToPageUrl(request, 1 - index);
//                firstUrl = TagUtil.toSiteAbsoluteUrl(href);
//            }
//            this.addStackValue(first, firstUrl);
//        }
//
//        if (last != null) {
//            String lastUrl = null;
//            if (count > 0 && index > 1) {
//                String href = UrlUtil.getInstance().getToPageUrl(request, pages - index);
//                lastUrl = TagUtil.toSiteAbsoluteUrl(href);
//            }
//            this.addStackValue(last, lastUrl);
//        }
//        return NextStep.EVAL_BODY;
//    }
//
//}
