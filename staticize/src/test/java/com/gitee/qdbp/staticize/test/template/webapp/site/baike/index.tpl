<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="column" uri="http://www.jeshing.com/tags/column/"%>
<%@ taglib prefix="content" uri="http://www.jeshing.com/tags/content/"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>
<%@ taglib prefix="util" uri="http://www.jeshing.com/tags/util/"%>
<%@ taglib prefix="url" uri="http://www.jeshing.com/tags/url/"%>

<!DOCTYPE html>
<html>
<head>
<%@ include file="/base/include/meta.tpl"%>
<column:detail key="baike">
	<meta name="keywords" content="${column.seo.key}" />
	<meta name="description" content="${column.seo.desc}" />
	<title>${column.seo.title}</title>
</column:detail>
<%@ include file="/base/include/head.tpl"%>
<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/home.css'/>">
<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/decoration_collection.css'/>">
<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/baike.index.css'/>">
<script type="text/javascript" src="<url:link href='resources/base/js/cyclic/zhh.cyclic.js'/>"></script>
	<script type="text/javascript" src="<url:link href='resources/site/js/baike.js'/>"></script>
</head>
<body>

	<jsp:include page="/base/include/top.tpl">
		<jsp:param name="column" value="baike" />
	</jsp:include>

	<!--  百科详情 -->
	<div class="centern-big baike_mt">
		<div class="baike_top_box">
			<position:detail key="baike|zicaidan">
				<ul class="baike_top_nav" >
					<position:content key="${position.id}" rows="4">
						<core:forEach var="item" items="${list}" varStatus="s">
							<li <core:if test="${s.last }">class="baike_lipr"</core:if>><a class="baike_icon <core:if test="${s.index==0 }">checked</core:if> " href="<url:link href='${item.url}'/>" title="${item.title }">${item.title }</a></li>
						</core:forEach>
					</position:content>
				</ul>
			</position:detail>
			<div class="baike_top_list_box">
				<position:detail key="baike|sheji|remenbiaoqian">
				<div class="baike_top_list">
					<span class="baike_top_list_lable fl">${position.title }</span>
					<div class="baike_top_list_cnt fl">
					<position:content key="${position.id }" rows="100">
						<core:forEach var="item" items="${list}">
						<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a>
						</core:forEach>
					</position:content>
					</div>
				</div>
				</position:detail>
				<position:detail key="baike|sheji|zuixinbiaoqian">
				<div class="baike_top_list baike_top_list_border">
					<span class="baike_top_list_lable fl">${position.title }</span>
					<div class="baike_top_list_cnt fl">
					<position:content key="${position.id }" rows="100">
						<core:forEach var="item" items="${list}">
						<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a>
						</core:forEach>
					</position:content>
					</div>
				</div>
				<div class="baike_more">
					<a href="<url:list column="baike" type="baikesheji"/>">查看更多<span class="baike_icon"></span></a>
				</div>
				</position:detail>
			</div>
			<div class="baike_top_list_box hide">
				<position:detail key="baike|zhuangxiu|remenbiaoqian">
				<div class="baike_top_list">
					<span class="baike_top_list_lable fl">${position.title }</span>
					<div class="baike_top_list_cnt fl">
					<position:content key="${position.id }" rows="100">
						<core:forEach var="item" items="${list}">
						<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a>
						</core:forEach>
					</position:content>
					</div>
				</div>
				</position:detail>
				<position:detail key="baike|zhuangxiu|zuixinbiaoqian">
				<div class="baike_top_list baike_top_list_border">
					<span class="baike_top_list_lable fl">${position.title }</span>
					<div class="baike_top_list_cnt fl">
					<position:content key="${position.id }" rows="100">
						<core:forEach var="item" items="${list}">
						<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a>
						</core:forEach>
					</position:content>
					</div>
				</div>
				<div class="baike_more">
					<a  href="<url:list column="baike" type="baikezhuangxiu"/>">查看更多<span class="baike_icon"></span></a>
				</div>
				</position:detail>
			</div>
			<div class="baike_top_list_box hide">
				<position:detail key="baike|chanpin|remenbiaoqian">
				<div class="baike_top_list">
					<span class="baike_top_list_lable fl">${position.title }</span>
					<div class="baike_top_list_cnt fl">
					<position:content key="${position.id }" rows="100">
						<core:forEach var="item" items="${list}">
						<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a>
						</core:forEach>
					</position:content>
					</div>
				</div>
				</position:detail>
				<position:detail key="baike|chanpin|zuixinbiaoqian">
				<div class="baike_top_list baike_top_list_border">
					<span class="baike_top_list_lable fl">${position.title }</span>
					<div class="baike_top_list_cnt fl">
					<position:content key="${position.id }" rows="100">
						<core:forEach var="item" items="${list}">
						<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a>
						</core:forEach>
					</position:content>
					</div>
				</div>
				<div class="baike_more">
					<a  href="<url:list column="baike" type="baikechanpin"/>" >查看更多<span class="baike_icon"></span></a>
				</div>
				</position:detail>
			</div>
			<div class="baike_top_list_box hide">
				<position:detail key="baike|shenghuo|remenbiaoqian">
				<div class="baike_top_list">
					<span class="baike_top_list_lable fl">${position.title }</span>
					<div class="baike_top_list_cnt fl">
					<position:content key="${position.id }" rows="100">
						<core:forEach var="item" items="${list}">
						<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a>
						</core:forEach>
					</position:content>
					</div>
				</div>
				</position:detail>
				<position:detail key="baike|shenghuo|zuixinbiaoqian">
				<div class="baike_top_list baike_top_list_border">
					<span class="baike_top_list_lable fl">${position.title }</span>
					<div class="baike_top_list_cnt fl">
					<position:content key="${position.id }" rows="100">
						<core:forEach var="item" items="${list}">
						<a href="<url:link href='${item.url}'/>" title="${item.title}">${item.title}</a>
						</core:forEach>
					</position:content>
					</div>
				</div>
				<div class="baike_more">
					<a  href="<url:list column="baike" type="baikeshenghuo"/>" >查看更多<span class="baike_icon"></span></a>
				</div>
				</position:detail>
			</div>

		</div>

		<!--  百科设计 -->
		<position:detail key="baike|sheji">
		<div class="centern-big case-centern">
			<div>
				<div class="decoration_have_top">
					<div class="baike_icon baike_design_head fl">
						<a class="daily-left-head-titlebig" href="<url:list column="baike" type="baikesheji"/>">${position.title} </a>
						<a class="daily-left-head-titlesmall" href="<url:list column="baike" type="baikesheji"/>" >${position.subTitle}</a>
					</div>
					<a class="fr more bg5" href="<url:list column="baike" type="baikesheji"/>"></a>
				</div>
				<div class="clear"></div>
			</div>
			<div class="baike_design_box">
				<div class="fl baike_design_left">
					<div class="baike_design_left_t"></div>
					<div class="baike_design_left_banner">
						<a href="<url:list column="baike" type="baikesheji"/>"><img src="../resources/site/images/baike/baike_banner1.jpg" /></a>
					</div>
					<div class="baike_design_left_b"></div>
				</div>
				<div class="fl baike_design_right ">
					<div class="baike_design_right_main ">
						<position:content key="${position.id}" rows="100">
						<div class="baike_design_right_nav">
							<ul class="nav_cascade">
								<core:forEach var="item" items="${list}" varStatus="s">
									<li><a <core:if test="${s.index == 0}">class="check"</core:if> href="#">${item.title }<span <core:if test="${s.index == 0}">class="share"</core:if>></span></a></li>
								</core:forEach>
							</ul>
						</div>
						<core:forEach var="item" items="${list}" varStatus="s">
						<position:content key="${item.positionId}" rows="100">
						<div class="baike_design_right_box  mq-box" >
							<a class="fl baike_design_right_box_on baike_icon mq-prev"></a>
							<a class="fr baike_design_right_box_next baike_icon mq-next"></a>
							<div class="baike_design_right_box_warp mq-list">
							<core:forEach var="itemchild" items="${list}" varStatus="schild" >
								<div class="fl baike_design_right_box_list ">
									<div class="baike_design_right_box_list_inner">
										<a href="<url:link href='${itemchild.url}'/>" >
											<img src="<url:image src='${itemchild.image.src}'/>" alt="${itemchild.image.title}" width="374" height="210"/>
										</a>
										<a class="baike_design_right_box_list_title" href="#">${itemchild.title }</a>
										<a class="baike_design_right_box_list_sub" href="#">${itemchild.digest }</a>
									</div>
								</div>
							</core:forEach>
							</div>
						</div>
						</position:content>
						</core:forEach>
						</position:content>
					</div>
				</div>
			</div>
		</div>
		</position:detail>

		<!--  百科装修 -->
		<position:detail key="baike|zhuangxiu">
		<div class="centern-big case-centern">
			<div>
				<div class="decoration_have_top">
					<div class="baike_icon baike_decoration_head fl">
						<a class="daily-left-head-titlebig" href="<url:list column="baike" type="baikezhuangxiu"/>">${position.title} </a>
						<a class="daily-left-head-titlesmall" href="<url:list column="baike" type="baikezhuangxiu"/>">${position.subTitle}</a>
					</div>
					<a class="fr more bg5" href="<url:list column="baike" type="baikezhuangxiu"/>"></a>
				</div>

				<div class="clear"></div>
			</div>
			<div class="baike_design_box">
				<div class="fl baike_design_left">
					<div class="baike_design_left_t"></div>
					<div class="baike_design_left_banner">
						<a  href="<url:list column="baike" type="baikezhuangxiu"/>"><img src="../resources/site/images/baike/baike_banner2.jpg" /></a>
					</div>
					<div class="baike_design_left_b"></div>
				</div>

				<div class="fl baike_design_right">
					<div class="baike_design_right_main">
						<position:content key="${position.id}" rows="100">
						<div class="baike_design_right_nav">
							<ul class="nav_cascade">
								<core:forEach var="item" items="${list}" varStatus="s">
									<li><a <core:if test="${s.index == 0}">class="check"</core:if> href="#">${item.title }<span <core:if test="${s.index == 0}">class="share"</core:if>></span></a></li>
								</core:forEach>
							</ul>
						</div>
						<core:forEach var="item" items="${list}" varStatus="s">
						<position:content key="${item.positionId}" rows="100">
						<div class="baike_design_right_box  mq-box" >
							<a class="fl baike_design_right_box_on baike_icon mq-prev"></a>
							<a class="fr baike_design_right_box_next baike_icon mq-next"></a>
							<div class="baike_design_right_box_warp mq-list">
							<core:forEach var="itemchild" items="${list}" varStatus="schild" >
								<div class="fl baike_design_right_box_list ">
									<div class="baike_design_right_box_list_inner">
									<a href="<url:link href='${itemchild.url}'/>" >
										<img src="<url:image src='${itemchild.image.src}'/>" alt="${itemchild.image.title}" width="374" height="210"/>
									</a>
									<a class="baike_design_right_box_list_title" href="#">${itemchild.title }</a>
									<a class="baike_design_right_box_list_sub" href="#">${itemchild.digest }</a>
									</div>
								</div>
							</core:forEach>
							</div>
						</div>
						</position:content>
						</core:forEach>
						</position:content>

					</div>
				</div>

			</div>
		</div>
		</position:detail>

		<!--  百科产品 -->
		<position:detail key="baike|chanpin">
		<div class="centern-big case-centern">
			<div>
				<div class="decoration_have_top">
					<div class="baike_icon baike_products_head fl">
						<a class="daily-left-head-titlebig"  href="<url:list column="baike" type="baikechanpin"/>">${position.title} </a>
						<a class="daily-left-head-titlesmall"  href="<url:list column="baike" type="baikechanpin"/>" >${position.subTitle}</a>
					</div>
					<a class="fr more bg5" href="<url:list column="baike" type="baikechanpin"/>"></a>
				</div>
				<div class="clear"></div>
			</div>
			<div class="baike_design_box">
				<div class="fl baike_design_left">
					<div class="baike_design_left_t"></div>
					<div class="baike_design_left_banner">
						<a href="<url:list column="baike" type="baikechanpin"/>"><img src="../resources/site/images/baike/baike_banner3.jpg" /></a>
					</div>
					<div class="baike_design_left_b"></div>
				</div>
				<div class="fl baike_design_right">
					<div class="baike_design_right_main">
						<position:content key="${position.id}" rows="100">
						<div class="baike_products_right_nav">
							<ul class="nav_cascade">
								<core:forEach var="item" items="${list}" varStatus="s">
									<li><a <core:if test="${s.index == 0}">class="check"</core:if> href="#">${item.title }<span <core:if test="${s.index == 0}">class="share"</core:if>></span></a></li>
								</core:forEach>
							</ul>
						</div>
						<core:forEach var="item" items="${list}" varStatus="s">
						<position:content key="${item.positionId}" rows="100">
						<div class="baike_design_right_box  mq-box" >
							<a class="fl baike_design_right_box_on baike_icon mq-prev"></a>
							<a class="fr baike_design_right_box_next baike_icon mq-next"></a>
							<div class="baike_design_right_box_warp mq-list">
							<core:forEach var="itemchild" items="${list}" varStatus="schild" >
								<div class="fl baike_design_right_box_list ">
									<div class="baike_design_right_box_list_inner">
									<a href="<url:link href='${itemchild.url}'/>" >
										<img src="<url:image src='${itemchild.image.src}'/>" alt="${itemchild.image.title}" width="374" height="210"/>
									</a>
									<a class="baike_design_right_box_list_title" href="#">${itemchild.title }</a>
									<a class="baike_design_right_box_list_sub" href="#">${itemchild.digest }</a>
									</div>
								</div>
							</core:forEach>
							</div>
						</div>
						</position:content>
						</core:forEach>
						</position:content>
					</div>
				</div>
			</div>

		</div>
		</position:detail>

		<!--  百科生活 -->
		<position:detail key="baike|shenghuo">
		<div class="centern-big case-centern">
			<div>
				<div class="decoration_have_top">
					<div class="baike_icon baike_life_head fl">
						<a class="daily-left-head-titlebig"  href="<url:list column="baike" type="baikeshenghuo"/>">${position.title} </a>
						<a class="daily-left-head-titlesmall"  href="<url:list column="baike" type="baikeshenghuo"/>">${position.subTitle}</a>
					</div>
					<a class="fr more bg5" href="<url:list column="baike" type="baikeshenghuo"/>"></a>
				</div>

				<div class="clear"></div>
			</div>
			<div class="baike_design_box">
				<div class="fl baike_design_left">
					<div class="baike_design_left_t"></div>
					<div class="baike_design_left_banner">
						<a href="<url:list column="baike" type="baikeshenghuo"/>"><img src="../resources/site/images/baike/baike_banner4.jpg" /></a>
					</div>
					<div class="baike_design_left_b"></div>
				</div>
				<div class="fl baike_design_right">
					<div class="baike_design_right_main">

						<position:content key="${position.id}" rows="100">
						<div class="baike_design_right_nav">
							<ul class="nav_cascade">
								<core:forEach var="item" items="${list}" varStatus="s">
									<li><a <core:if test="${s.index == 0}">class="check"</core:if> href="#">${item.title }<span <core:if test="${s.index == 0}">class="share"</core:if>></span></a></li>
								</core:forEach>
							</ul>
						</div>
						<core:forEach var="item" items="${list}" varStatus="s">
						<position:content key="${item.positionId}" rows="100">
						<div class="baike_design_right_box  mq-box" >
							<a class="fl baike_design_right_box_on baike_icon mq-prev"></a>
							<a class="fr baike_design_right_box_next baike_icon mq-next"></a>
							<div class="baike_design_right_box_warp mq-list">
							<core:forEach var="itemchild" items="${list}" varStatus="schild" >
								<div class="fl baike_design_right_box_list ">
									<div class="baike_design_right_box_list_inner">
									<a href="<url:link href='${itemchild.url}'/>" >
										<img src="<url:image src='${itemchild.image.src}'/>" alt="${itemchild.image.title}" width="374" height="210"/>
									</a>
									<a class="baike_design_right_box_list_title" href="#">${itemchild.title }</a>
									<a class="baike_design_right_box_list_sub" href="#">${itemchild.digest }</a>
									</div>
								</div>
							</core:forEach>
							</div>
						</div>
						</position:content>
						</core:forEach>
						</position:content>

					</div>
				</div>
			</div>

		</div>
		</position:detail>

	</div>
	<div class="clear"></div>

	<%@ include file="/base/include/foot.tpl"%>
</body>
</html>

