package com.gitee.qdbp.staticize.test.publish;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import com.gitee.qdbp.staticize.common.IMetaData;
import com.gitee.qdbp.staticize.io.FileInputCreator;
import com.gitee.qdbp.staticize.io.IReaderCreator;
import com.gitee.qdbp.staticize.parse.TagParser;
import com.gitee.qdbp.staticize.publish.FilePublisher;
import com.gitee.qdbp.tools.files.PathTools;

/**
 * 发布测试类
 * 
 * @author zhaohuihua
 * @version 140605
 */
public class ErrorIncludeTagPublisher {

    public static void main(String[] args) throws Exception {

        URL path = PathTools.findClassResource(ErrorIncludeTagPublisher.class, "../template/");
        String folder = PathTools.toUriPath(path);

        String in = "error/include.if.tpl";
        String out = "error/include.if.html";

        System.out.println(folder);

        IReaderCreator input = new FileInputCreator(folder);
        TagParser parser = new TagParser(input);
        IMetaData metadata = parser.parse(in);

        Map<String, Object> data = new HashMap<>();

        Map<String, String> column = new HashMap<>();
        column.put("id", "100088");
        column.put("title", "资讯频道");
        data.put("column", column);
        data.put("user", "zhaohuihua");
        data.put("number", new Integer[] { 111, 222, 333, 444, 555, 666, 777, 888, 999, 1000 });

        FilePublisher publisher = new FilePublisher(metadata, folder);
        publisher.publish(data, out);
    }
}
