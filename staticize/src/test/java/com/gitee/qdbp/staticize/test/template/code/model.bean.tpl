package #{java.vars.current.packages};

<core:each items="#{imports}" var="item">
${item}
</core:each>

/**
 * #{table.comment}实体类
 *
 * @author CodeGenerater
 * @version <fmt:date pattern="yyMMdd" />
 */
<core:if test="#{java.vars.operate.traces != null}">
#{java.vars.operate.traces.text}
</core:if>
<core:if test="#{java.vars.data.isolation  != null}">
#{java.vars.data.isolation.text}
</core:if>
public class #{java.vars.current.capitalizeName} implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 表名 **/
    public static final String TABLE = "#{table.originalName}";
    <core:each items="#{table.fields}" var="field">
    /** ${field.comment} **/
    <core:if test="${field.dataType.stringType && field.length != null}">
    @Size(max = ${field.length})
    </core:if>
    private ${field.dataType.typedName} ${field.normativeName};
    </core:each>
    <core:each items="#{table.fields}" var="field">

    /** 获取${field.comment} **/
    public ${field.dataType.typedName} get${field.capitalizeName}() {
        return ${field.normativeName};
    }
    <core:if test="${field.forceGetter != null}">

    /** 获取${field.comment}, force=是否强制返回非空对象 **/
    public ${field.dataType.typedName} get${field.capitalizeName}(boolean force) {
        if (${field.normativeName} == null && force) {
            ${field.normativeName} = ${field.forceGetter.newInstanceString};
        }
        return ${field.normativeName};
    }
    </core:if>

    /** 设置${field.comment} **/
    public void set${field.capitalizeName}(${field.dataType.typedName} ${field.normativeName}) {
        this.${field.normativeName} = ${field.normativeName};
    }
    </core:each>

    /**
     * 将当前对象转换为子类对象
     *
     * @param clazz 目标类型
     * @return 目标对象
     */
    public <T extends #{java.vars.model.bean.capitalizeName}> T to(Class<T> clazz) {
        T instance;
        try {
            instance = clazz.newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to create " + clazz.getSimpleName() + " instance.", e);
        }

        <core:each items="#{table.fields}" var="field">
        instance.set${field.capitalizeName}(this.get${field.capitalizeName}()); // ${field.comment}
        </core:each>
        return instance;
    }

    @Override
    public String toString() {
        return JsonTools.toLogString(this);
    }
}
