<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<meta name="keywords" content="${column.seoKey}" />
	<meta name="description" content="${column.seoDes}" />
	<title>${SEOTITLE}</title>
		<script type="text/javascript" src="resources/site/js/zixun.js"></script>
	<link rel="stylesheet" type="text/css" href="resources/site/css/zixun.css" />
</head>

<body>

	<div id="zixun">
		<div class="main">
			<!--part_a 开始-->
			<div class="part_a">
				<div class="part_left">
					<!--滑动图片 start-->
					<div class="left">
						<div class="slide_1">
							<position:content key="zixun|shouyelunbotu" rows="6">
							<p class="img marquee">
							<core:each  var="item" items="${list}">
								<a href="${item.tcUrl}" title="${item.tcTitle}"><url:image src="${item.tcImageUrl}" height="290" width="440" /></a>
							</core:each>
							</p>
							<p class="link_bg"></p>
							<div class="link">
								<p class="link_left imgdesc">
									<core:each var="item" items="${list}">
										<a href="${item.tcUrl}" title="${item.tcTitle}">${item.tcTitle}</a>
									</core:each>
								</p>
								<p class="link_right pointer">
									<core:each
										var="item"
										items="${list}"
										status="s"
										>
										<a title="${item.tcTitle}"<core:if test='${s.index==0}'> class="active"</core:if>></a>
									</core:each>
								</p>
								<br class="clear"/>
							</div>
							</position:content>
						</div>
					</div>
					<!--滑动图片 end-->

					<!--文章列表 start-->
					<position:content key="zixun|shouyetuijian" rows="8">
						<div class="right">
							<div class="elite">
								<core:each var="content" items="${list}" begin="A" rows="1">
									<p class="title ellipsis"><a href="${content.tcUrl}" title="${content.tcTitle}" >${content.tcTitle}</a></p>
									<p class="summary">${content.tcDesc}</p>
								</core:each>
							</div>
							<div class="list_wrap">
								<ul>
									<core:each var="content" items="${list}" begin="1">
										<li><a href="${content.tcUrl}" title="${content.tcTitle}">${content.tcTitle}</a></li>
									</core:each>
									<br class="clear"/>
								</ul>
							</div>
						</div>
					</position:content>
					<!--文章列表 end-->
					<br class="clear"/>
				</div>

				<div class="part_right">
					<!--热点聚焦 end-->
					<column:details key="zixun|redianjujiao">
					<div class="hot_focus">
						<p class="headline"><a href="<url:list name='zixun|redianjujiao' page='1'/>" title="${column.colTitle}">${column.colTitle}</a></p>
						<content:list rows="3">
							<core:each var="content" items="${contents}" status="s">
							 <core:choose>
								<core:when test="${s.index==0}">
									<div class="focus_block focus_block_first">
										<p class="title ellipsis"><a href="<url:detail name='zixun|redianjujiao' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></p>
										<p class="summary">${content.contentDigest}</p>
									</div>
								</core:when>
								<core:otherwise>
									 <div class="focus_block">
										<p class="title ellipsis"><a href="<url:detail name='zixun|redianjujiao' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></p>
										<p class="summary">${content.contentDigest}</p>
									</div>
								</core:otherwise>
								</core:choose>
							</core:each>
						</content:list>
					</div>
					</column:details>
					<!--热点聚焦 end-->
				</div>
				<br class="clear"/>
			</div>
			<!--part_a 结束-->

			<!--part_b 开始-->
			<div class="part_b">
				<div class="part_left tab-box" target=".list_wrap">
					<!--有四个模块-->
					<!--装修攻略、百变空间、明星家居 开始-->
					<div class="headline">
						<ul>
							<column:details key="zixun|zhuangxiugonglue">
								<li><a href="<url:list name='zixun|zhuangxiugonglue' page='1'/>" title="装修攻略" class="tab-item active">${column.colTitle}</a></li>
							</column:details>
							<column:details key="zixun|baibiankongjian">
								<li><a href="<url:list name='zixun|baibiankongjian' style='bigimg' page='1'/>" title="百变空间" class="tab-item">${column.colTitle}</a></li>
							</column:details>
							<column:details key="zixun|chaoliuqushi">
								<li><a href="<url:list name='zixun|chaoliuqushi' style='bigimg' page='1'/>" title="潮流趋势" class="tab-item">${column.colTitle}</a></li>
							</column:details>
							<column:details key="zixun|jiajufengshui">
								<li><a href="<url:list name='zixun|jiajufengshui' page='1'/>" title="家居风水" class="tab-item">${column.colTitle}</a></li>
							</column:details>
							<br class="clear"/>
						</ul>
					</div>

					<!--装修攻略 开始-->
					<div class="list_wrap">
					<column:details key="zixun|zhuangxiugonglue">
						<content:list rows="11">
						<div class="image_text">
							<ul>
								<core:each var="content" items="${contents}" status="s" begin="1" rows="2">
									<li<core:if test="${s.index==0}"> class="first"</core:if>>
										<p class="img"><a href="<url:detail name='zixun|zhuangxiugonglue' id='${content.contentId}'/>" title="${content.title}"><url:image src="${content.mainPictureUrl}" height="160" width="230" /></a></p>
										<p class="title ellipsis"><a href="<url:detail name='zixun|zhuangxiugonglue' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></p>
									</li>
								</core:each>
								<br class="clear"/>
							</ul>
						</div>
						<div class="text_list">
							<ul>
								<core:each var="content" items="${contents}" status="s" begin="3">
									<li>
										<p class="title ellipsis <core:if test='${s.index==0}'>first</core:if>"><a href="<url:detail name='zixun|zhuangxiugonglue' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></p>
									</li>
								</core:each>
								<br class="clear"/>
							</ul>
						</div>
						</content:list>
					</column:details>
					</div>
					<!-- 百变空间 -->
					<div class="list_wrap hide">
					<column:details key="zixun|baibiankongjian">
						<content:list rows="11">
						<div class="image_text">
							<ul>
								<core:each var="content" items="${contents}" status="s" begin="1" rows="2">
									<li<core:if test="${s.index==0}"> class="first"</core:if>>
										<p class="img"><a href="<url:detail name='zixun|baibiankongjian' id='${content.contentId}'/>" title="${content.title}"><url:image src="${content.mainPictureUrl}" height="160" width="230" /></a></p>
										<p class="title ellipsis"><a href="<url:detail name='zixun|baibiankongjian' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></p>
									</li>
								</core:each>
								<br class="clear"/>
							</ul>
						</div>
						<div class="text_list">
							<ul>
								<core:each var="content" items="${contents}" status="s" begin="3">
									<li>
										<p class="title ellipsis <core:if test='${s.index==0}'>first</core:if>"><a href="<url:detail name='zixun|baibiankongjian' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></p>
									</li>
								</core:each>
								<br class="clear"/>
							</ul>
						</div>
						</content:list>
					</column:details>
					</div>
					<!-- 潮流趋势 -->
					<div class="list_wrap hide">
					<column:details key="zixun|chaoliuqushi">
						<content:list rows="11">
						<div class="image_text">
							<ul>
								<core:each var="content" items="${contents}" status="s" begin="1" rows="2">
									<li<core:if test="${s.index==0}"> class="first"</core:if>>
										<p class="img"><a href="<url:detail name='zixun|chaoliuqushi' id='${content.contentId}'/>" title="${content.title}"><url:image src="${content.mainPictureUrl}" height="160" width="230" /></a></p>
										<p class="title ellipsis"><a href="<url:detail name='zixun|chaoliuqushi' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></p>
									</li>
								</core:each>
								<br class="clear"/>
							</ul>
						</div>
						<div class="text_list">
							<ul>
								<core:each var="content" items="${contents}" status="s" begin="3">
									<li>
										<p class="title ellipsis <core:if test='${s.index==0}'>first</core:if>"><a href="<url:detail name='zixun|chaoliuqushi' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></p>
									</li>
								</core:each>
								<br class="clear"/>
							</ul>
						</div>
						</content:list>
					</column:details>
					</div>
					<!-- 家居风水 -->
					<div class="list_wrap hide">
					<column:details key="zixun|jiajufengshui">
						<content:list rows="11">
						<div class="image_text">
							<ul>
								<core:each var="content" items="${contents}" status="s" begin="1" rows="2">
									<li<core:if test="${s.index==0}"> class="first"</core:if>>
										<p class="img"><a href="<url:detail name='zixun|jiajufengshui' id='${content.contentId}'/>" title="${content.title}"><url:image src="${content.mainPictureUrl}" height="160" width="230" /></a></p>
										<p class="title ellipsis"><a href="<url:detail name='zixun|jiajufengshui' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></p>
									</li>
								</core:each>
								<br class="clear"/>
							</ul>
						</div>
						<div class="text_list">
							<ul>
								<core:each var="content" items="${contents}" status="s" begin="3">
									<li>
										<p class="title ellipsis <core:if test='${s.index==0}'>first</core:if>"><a href="<url:detail name='zixun|jiajufengshui' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></p>
									</li>
								</core:each>
								<br class="clear"/>
							</ul>
						</div>
						</content:list>
					</column:details>
					</div>
					<!--装修攻略、百变空间、明星家居 结束-->
				</div>

				<div class="part_right">
					<!--高端访谈 开始-->
					<div class="interview">
						<column:details key="zixun|gaoduanfangtan">
						<p class="headline"><a href="<url:list name='zixun|gaoduanfangtan' style='bigimg'/>" title="${column.colTitle}">${column.colTitle}</a></p>
						<content:list rows="2">
						<core:each var="content" items="${contents}">
						<div class="block">
							<div class="left"><a href="<url:detail name='zixun|gaoduanfangtan' id='${content.contentId}'/>" title="${content.title}"><url:image src="${content.mainPictureUrl}" height="70" width="90" /></a></div>
							<div class="right">
								<p class="title">${content.title}</p>
								<p class="link"><a href="<url:detail name='zixun|gaoduanfangtan' id='${content.contentId}'/>" title="查看详情">【查看详情】</a></p>
							</div>
							<br class="clear"/>
						</div>
						</core:each>
						</content:list>
						</column:details>
					</div>
					<!--高端访谈 结束-->

					<!--对话设计师 开始-->
					<div class="interview designer_talk">
						<column:details key="zixun|duihuashejishi">
							<p class="headline"><a href="<url:list name='zixun|duihuashejishi' style='bigimg'/>" title="${column.colTitle}">${column.colTitle}</a></p>
							<content:list rows="1">
							<core:each var="content" items="${contents}">
								<div class="block">
									<div class="left"><a href="<url:detail name='zixun|duihuashejishi' id='${content.contentId}'/>" title="${content.title}"><url:image src="${content.mainPictureUrl}" height="70" width="90" /></a></div>
									<div class="right">
										<p class="title">${content.title}</p>
										<p class="link"><a href="<url:detail name='zixun|duihuashejishi' id='${content.contentId}'/>" title="查看详情">【查看详情】</a></p>
									</div>
									<br class="clear"/>
								</div>
							</core:each>
							</content:list>
							<div class="list_wrap">
								<ul>
								<content:list begin="2" rows="2">
									<core:each var="content" items="${contents}">
										<li><a href="<url:detail name='zixun|duihuashejishi' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></li>
									</core:each>
								</content:list>
									<br class="clear"/>
								</ul>
							</div>
						</column:details>
					</div>
					<!--对话设计师 结束-->
				</div>
				<br class="clear"/>
			</div>
			<!--part_b 结束-->

			<!--part_c 开始-->
			<div class="part_c">
				<!--part_c 左侧 开始-->
				<div class="part_left">
					<!--户型解析 开始-->
					<div class="parsing">
					<column:details key="zixun|huxingjiexi">
						<div class="headline">
							<div class="left"><a href="<url:list name='zixun|huxingjiexi' style='bigimg' page='1'/>" title="${column.colTitle}">${column.colTitle}</a></div>
							<div class="right">

								<a href="<url:list name='zixun|huxingjiexi' page='1'/>" title="更多">
									<p class="img"><img src="resources/site/images/zixun/icon/headline_more.png" border="0" alt=">" /></p>
									<p class="more">更多</p>
									<br class="clear"/>
								</a>
							</div>
							<br class="clear"/>
						</div>

						<content:list rows="7">
						<div class="img">
							<core:each var="content" items="${contents}" status="s" begin="1" rows="1">
								<div class="<core:choose><core:when test='${s.index==0}'>img_left</core:when><core:otherwise>img_right</core:otherwise></core:choose>">
									<p class="title_img"><a href="<url:detail name='zixun|huxingjiexi' id='${content.contentId}'/>" title="${content.title}"><url:image src="${content.mainPictureUrl}" height="120" width="175" /></a></p>
									<p class="title ellipsis"><a href="<url:detail name='zixun|huxingjiexi' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></p>
								</div>
							</core:each>
							<br class="clear"/>
						</div>
						<div class="list_wrap">
							<ul>
								<core:each var="content" items="${contents}" begin="2">
									<li><a href="<url:detail name='zixun|huxingjiexi' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></li>
								</core:each>
								<br class="clear"/>
							</ul>
						</div>
						</content:list>
					</column:details>
					</div>
					<!--户型解析 结束-->

					<!--探店报告 开始-->
					<div class="report">
					<column:details key="zixun|tandianbaogao">
						<div class="headline">
							<div class="left"><a href="<url:list name='zixun|tandianbaogao' style='bigimg'/>" title="${column.colTitle}">${column.colTitle}</a></div>
							<div class="right">
								<a href="<url:list name='zixun|tandianbaogao' page='1'/>" title="更多">
									<p class="img"><img src="resources/site/images/zixun/icon/headline_more.png" border="0" alt=">" /></p>
									<p class="more">更多</p>
									<br class="clear"/>
								</a>
							</div>
							<br class="clear"/>
						</div>
						<content:list rows="3">
							<core:each var="content" items="${contents}">
								<div class="block">
									<div class="left"><a href="<url:detail name='zixun|tandianbaogao' id='${content.contentId}'/>" title="${content.title}"><url:image src="${content.mainPictureUrl}" height="90" width="140" /></a></div>
									<div class="right">
										<p class="title ellipsis"><a href="<url:detail name='zixun|tandianbaogao' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></p>
										<p class="summary">${content.contentDigest}</p>
									</div>
									<br class="clear"/>
								</div>
							</core:each>
						</content:list>
					</column:details>
					</div>
					<!--探店报告 结束-->
				</div>
				<!--part_c 左侧 结束-->

				<!--part_c 右侧 开始-->
				<div class="part_right">
					<!--家居健康 开始-->
					<div class="home_health">
					<column:details key="zixun|jiajujiankang">
						<content:list rows="8">
						<p class="headline"><a href="<url:list name='zixun|jiajujiankang' page='1'/>" title="${column.colTitle}">${column.colTitle}</a></p>
						<core:each var="content" items="${contents}" begin="1" rows="1">
							<div class="top_articel">
								<p class="img"><a href="<url:detail name='zixun|jiajujiankang' id='${content.contentId}'/>" title="${content.title}"><url:image src="${content.mainPictureUrl}" height="140" width="210" /></a></p>
								<p class="link_bg"></p>
								<p class="link ellipsis"><a href="<url:detail name='zixun|jiajujiankang' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></p>
							</div>
						</core:each>
						<div class="list_wrap">
							<ul>
								<core:each var="content" items="${contents}" begin="1">
								<li class="ellipsis"><a href="<url:detail name='zixun|jiajujiankang' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></li>
								</core:each>
								<br class="clear"/>
							</ul>
						</div>
						</content:list>
					</column:details>
					</div>
					<!--家居健康 结束-->
				</div>
				<!--part_c 右侧 结束-->
				<br class="clear"/>
			</div>
			<!--part_c 结束-->

			<!--part_d 精挑细选 开始-->
			<div class="selective">
				<column:details key="zixun|jingtiaoxixuan">
					<div class="headline">
						<div class="left"><a href="<url:list name='zixun|jingtiaoxixuan' style='bigimg'/>" title="${column.colTitle}">${column.colTitle}</a></div>
						<div class="right">
							<a href="<url:list name='zixun|jingtiaoxixuan' page='1'/>" title="更多">
								<p class="img"><img src="resources/site/images/zixun/icon/headline_more.png" border="0" alt=">" /></p>
								<p class="more">更多</p>
								<br class="clear"/>
							</a>
						</div>
						<br class="clear"/>
					</div>
					<div class="list_wrap">
						<div class="prev"><p class="img"><img src="resources/site/images/zixun/icon/slide_prev.png" alt="上一组"/></p></div>
						<div class="next"><p class="img"><img src="resources/site/images/zixun/icon/slide_next.png" alt="下一组"/></p></div>
						<ul class="imglist">
							<content:list rows="10">
								<core:each var="content" items="${contents}" status="s">
									<li>
										<p class="title_img"><a href="<url:detail name="zixun|jingtiaoxixuan" id='${content.contentId}'/>" title="${content.title}"><url:image src="${content.mainPictureUrl}" height="152" width="230" /></a></p>
										<p class="title"><a href="<url:detail name="zixun|jingtiaoxixuan" id='${content.contentId}'/>" title="${content.title}">${content.title}</a></p>
									</li>
								</core:each>
							</content:list>
						</ul>
					</div>
				</column:details>
			</div>
			<!--part_d 精挑细选 结束-->

			<!--part_f 开始-->
			<div class="part_f">
				<div class="part_left">
					<!--品牌动态 开始-->
					<div class="brand">
					 <column:details key="zixun|pinpaidongtai" >
						<div class="headline">
							<div class="left"><a href="<url:list name='zixun|pinpaidongtai' page='1'/>" title="${column.colTitle}">${column.colTitle}</a></div>
							<div class="right">
								<a href="<url:list name='zixun|pinpaidongtai' page='1'/>" title="更多">
									<p class="img"><img src="resources/site/images/zixun/icon/headline_more.png" border="0" alt=">" /></p>
									<p class="more">更多</p>
									<br class="clear"/>
								</a>
							</div>
							<br class="clear"/>
						</div>
						<content:list rows="3">
						<core:each var="content" items="${contents}" status="s">
							<core:choose>
								<core:when test="${s.index==0}">
									<div class="block block_first">
										<div class="block_left">
											<a href="<url:detail name='zixun|pinpaidongtai' id='${content.contentId}'/>" title="${content.title}"><url:image src="${content.mainPictureUrl}" height="90" width="140" /></a>
										</div>
										<div class="block_right">
											<div class="title">
												<div class="left ellipsis"><a href="<url:detail name='zixun|pinpaidongtai' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></div>
												<%-- <div class="right">
													<span class="share">分享</span>
													<span class="line">|</span>
													<span class="comment">评论（9999）</span>
													<br class="clear"/>
												</div>--%>
												<br class="clear"/>
											</div>
											<div class="summary">${content.contentDigest}</div>
										</div>
										<br class="clear"/>
									</div>
								</core:when>
								<core:otherwise>
									<div class="block">
										<div class="block_left">
											<a href="<url:detail name='zixun|pinpaidongtai' id='${content.contentId}'/>" title="${content.title}"><url:image src="${content.mainPictureUrl}" height="90" width="140" /></a>
										</div>
										<div class="block_right">
											<div class="title">
												<div class="left"><a href="<url:detail name='zixun|pinpaidongtai' id='${content.contentId}'/>" title="${content.title}">${content.title}</a></div>
												<%--<div class="right">
													<span class="share">分享</span>
													<span class="line">|</span>
													<span class="comment">评论（9999）</span>
													<br class="clear"/>
												</div>--%>
												<br class="clear"/>
											</div>
											<div class="summary">${content.contentDigest}</div>
										</div>
										<br class="clear"/>
									</div>
								</core:otherwise>
							</core:choose>
						</core:each>
					  </content:list>
					 </column:details>
					</div>
					<!--品牌动态 结束-->
				</div>
				<div class="part_right">
					<!--装修计算器 开始-->
					<div class="calculator">
						<p class="headline">装修计算器</p>
						<div class="list_wrap">
							<div class="block">
								<div class="left block_li block_1">
									<p class="img"></p>
									<p class="name">墙砖计算器</p>
								</div>
								<div class="right block_li block_2">
									<p class="img"></p>
									<p class="name">地砖计算器</p>
								</div>
								<br class="clear"/>
							</div>
							<div class="block">
								<div class="left block_li block_3">
									<p class="img"></p>
									<p class="name">地板计算器</p>
								</div>
								<div class="right block_li block_4">
									<p class="img"></p>
									<p class="name">壁纸计算器</p>
								</div>
								<br class="clear"/>
							</div>
							<div class="block">
								<div class="left block_li block_5">
									<p class="img"></p>
									<p class="name">涂料计算器</p>
								</div>
								<div class="right block_li block_6">
									<p class="img"></p>
									<p class="name">窗帘计算器</p>
								</div>
								<br class="clear"/>
							</div>
						</div>
					</div>
					<!--装修计算器 结束-->
				</div>
				<br class="clear"/>
			</div>
			<!--part_f 结束-->
		</div>
	</div>

	</body>
</html>
