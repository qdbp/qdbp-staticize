<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="column" uri="http://www.jeshing.com/tags/column/"%>
<%@ taglib prefix="content" uri="http://www.jeshing.com/tags/content/"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>
<%@ taglib prefix="util" uri="http://www.jeshing.com/tags/util/"%>
<%@ taglib prefix="url" uri="http://www.jeshing.com/tags/url/"%>
<%@ taglib prefix="imgs" uri="http://www.jeshing.com/tags/images/"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/base/include/meta.tpl"%>
	<meta name="keywords" content="加入我们" />
	<meta name="description" content="加入我们" />
	<title>加入我们 - 乐居中国</title>
	<%@ include file="/base/include/head.tpl"%>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/siteinfo.aboutus.css'/>">
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/lepin.css'/>">
    <script type="text/javascript" src="<url:link href='resources/thirdparty/jquery/jquery.pin.min.js'/>"></script>
	<script type="text/javascript" src="<url:link href='resources/site/js/siteinfo.aboutus.js'/>"></script>
	<script type="text/javascript" src="<url:link href='resources/site/js/zhaopin.js'/>"></script>
</head>
<body>

	<%@ include file="/base/include/top.tpl"%>

	<%--页面主体--%>
	<div class="banner join_banner png"></div>

	<div class="centern">
		<%@ include file="_banner.tpl"%>
		<br class="clear" />
	</div>

	<div class="centern">
		<div class="about_left">
			<%@ include file="_menu.tpl"%>
		</div>

		<div class="about_right">
			<div class="ab_title">
				<img src="<url:link href='resources/site/images/siteinfo/mtu.png'/>"/> 加入我们<span>Join us</span>
			</div>
			<div class="join_yyzx" >
				<div class="join_yunyin">运营中心</div>
				<a href="#"></a>
			</div>
			<%-- 运营中心内容 start --%>
			<div class="join_content_box join_cont_title">
				<div class="join_content join_cont_title catagory_title"  >
					<a href="#"  ><span class="spa">•</span>互联网SNS专员</a>
					<a href="#"  ><span class="spa">•</span>互联网活动策划专员</a>
					<a href="#"  ><span class="spa">•</span>互联网sem专员</a>
					<a href="#"  ><span class="spa">•</span>互联网高级编辑</a>
					<a href="#"  ><span class="spa">•</span>网站运营专员</a>
					<a href="#"  ><span class="spa">•</span>互联网社区专员</a>
					<a href="#"  ><span class="spa">•</span>网络推广专员</a>
				</div>




				<div class="join_contmixi join_contmixi-ch  hulianwang"    style="display: none;" >
					<div class="join_cont_title">互联网SNS专员</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						<p>1、策划、组织并跟进微博/微信/BBSS营销活动，并评估活动效果；</p>
						<p>2、监控和收集微博/微信/BBS的日常效果数据，并及时作出调整优化；</p>
	                    <p>3、结合业务需要和SNS发展规划，完成微博/微信/BBS的日常运营；</p>
	                    <p>4、研究学习微博/微信/BBS等SNS营销运营的方向、最新思路，作好信息搜集并转化内部应用；</p>
	                    <p>5、独立操作平台建设以及维护。</p>
					</div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						<p>1、熟悉互联网行业，两年以上SNS运营推广经验，具备大型网站经验者优先；</p>
						<p>2、有微博，BBS或SNS运营的工作经验</p>
						<p>3、有一定的美术功底，能熟练使用PS等做图软件</p>
						<p>4、有一定的文字功底，善于编辑原创博文</p>
						<p>5、能熟练使用PPT，EXCEL，WORD等办公软件</p>
						<p>6、沟通能力强，能充分了解客户需求</p>
						<p>7、工作踏实，为人正直，有服务精神</p>
						<p>8、有互联网相关资源者优先，有家居行业从业经验者优先。</p>
					</div>
				</div>



				<div class="join_contmixi join_contmixi-ch hulianwang catagory_content"    style="display: none;" >
					<div class="join_cont_title">互联网活动策划专员</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						<p>1、负责跟进策划客户服务，及时跟进重大客户的市场服务；</p>
						<p>2、负责策划网站线上、线下活动的策划、组织和实施。</p>
	                    <p>3、负责网站家居、节庆日、当前焦点专题的策划。</p>
	                    <p>4、负责策划、组织、执行网络的宣传及推广活动，熟悉病毒营销，善于利用各种网络资源提高公司网站访问量和知名度，使营销推广效果最大化，打造市场影响力，提升品牌知名度。</p>

					</div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						<p>1、本科以上学历，房地产、中文、经济、市场营销类等相关专业。</p>
						<p>2、熟悉家居行业及市场，熟悉家居项目营销和销售流程，能独立完成营销各阶段的策划报告。</p>
						<p>3、优秀的文案功底，有较强的创造性思维能力、创意概念及良好的沟通能力；有较强的团队意识和敬业精神，能承受较强的工作压力。</p>
						<p>4、有综合运用包括策划、软文宣传、公关活动等在内的各种营销方式进行市场宣传、品牌推广的能力；</p>
						<p>5、熟练操作办公软件；</p>
						<p>6、有互联网相关资源者优先。</p>

					</div>
				</div>




				<div class="join_contmixi join_contmixi-ch hulianwang catagory_content"   style="display: none;">
					<div class="join_cont_title">互联网SEM专员</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						<p>1、制定SEM账户管理的计划和策略并独立执行：</p>
	                    <p>1）负责百度凤巢、Google AdWords，搜狗等搜索引擎关键词投放 ；</p>
	                    <p>2) 制定搜索优化策略，最大化提高搜索营销的转化率；</p>
	                    <p>3) 对搜索推广结果进行数据分析，判断和整理；</p>
	                    <p>4) 定期准备搜索推广数据报告；</p>
	                    <p>2、负责所有搜索营销图片、文字类型广告的创意工作；有效构建和开拓网络营销资源和推广联盟体系，负责和各种相关资源合作方、网站联络，洽谈合作，签订协议等，并做好广告效果监测。</p>

					</div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						<p>1、熟悉互联网行业，两年以上网站推广、网络营销经验，具备大型网站经验者优先；</p>
	                    <p>2、精通各大搜索引擎（Baidu、Google、Yahoo、soso、有道、搜狗）的搜索排名技术；</p>
	                    <p>3、对网络营销有深刻理解和实操经验，熟悉各种营销方式，如竞价排名、网络广告投放、电子邮件营销、BBS营销、web2.0营销等；</p>
	                    <p>4、熟悉搜索引擎，熟练掌握软文、交换链接、邮件推广、SNS推广、论坛推广及其他的推广方式来提高网站的知名度；</p>
	                    <p>5、具有团队合作意识，能够独立工作，并能承受一定的工作压力；</p>
	                    <p>6、有互联网相关资源者优先。</p>



					</div>
				</div>



				<div class="join_contmixi join_contmixi-ch hulianwang catagory_content"   style="display: none;">
					<div class="join_cont_title">互联网高级编辑</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						<p>1、负责网站频道、栏目等内容策划及后续页面改版优化；</p>
	                    <p>2、负责网站内容采编及内容更新维护；</p>
	                    <p>3、负责资讯整合，策划制作专题专栏；</p>
	                    <p>4、关注行业发展和竞争对手动态，对网站发展提出建设性意见。</p>

					</div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						<p>1、三年以上媒体从业经验，具有家居、装修相关媒体从业经验者优先；</p>
	                    <p>2、能够织和策划与频道相关互动主题活动，有设计师资源或互动工作经验者优先；</p>
	                    <p>3、能够熟练使用各种办公软件，具备一定美工设计基础，对网页代码熟悉者优先；</p>
	                    <p>4、熟悉网络应用，熟悉网络媒体传播特点；</p>
	                    <p>5、工作细致耐心，责任心强，吃苦耐劳；</p>
	                    <p>6、具有良好的语言沟通能力和团队合作精神。</p>


					</div>
				</div>



				<div class="join_contmixi join_contmixi-ch  hulianwang catagory_content"   style="display: none;">
					<div class="join_cont_title">网站运营专员</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						<p>1. 通过网站运营提升网站价值和粘性，提高用户、商户活跃度，提高交易量，促进网站各项收入提升；</p>
	                    <p>2. 线上、线下活动的策划及运营，提升用户粘度；</p>
	                    <p>3. 根据公司平台模式制定规范业务流程，制定平台运营规则;</p>
	                    <p>4. 对运营数据进行监控，出具运营报表并分析各类运营数据，根据分析结果提出优化改进方案；</p>
	                    <p>5. 掌握市场趋势、行业现状、市场需求和消费者行为规律、趋势；</p>
	                    <p>6. 制定并执行品类规划、产品开发运营计划，追踪、分析、总结各项计划的执行效果，用市场化的手段，充分发挥平台的杠杆作用；</p>
	                    <p>7. 与产品、技术等团队协作，通过产品开发和运营落地，持续完善、拓展和提高平台用户体验。</p>

					</div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						<p>1. 大专及以上学历，计算机相关专业，三年以上互联网运营相关工作经验，对互联网、移动互联网新产品有丰富的设计和管理经验，精通互联网产品和服务，有独到的见解和认识；</p>
	                    <p>2. 对互联网产品有很强的敏锐度、有很好的产品分析、总结归纳能力；</p>
	                    <p>3. 对互联网发展及其趋势有较深入的认知，精通网站运营，具备创造性思维及逻辑思维能力；</p>
	                    <p>4. 对产品和数据运营敏感，有较强的文字功底，能独立编写网站市场推广活动方案策划书，有丰富的市场策划、营销推广、广告媒体工作经验；</p>
	                    <p>5. 具有较好的产品、用户体验分析的能力与方法，对市场发展方向和动态有较强的分析能力，了解网站用户的服务需求，能够根据需求与市场变化迅速做出回应；</p>
	                    <p>6. 有较强的计划与执行能力，良好的沟通协作能力，擅于发现和解决问题，责任心强，踏实肯干、诚实敬业，能够主动承受工作压力；</p>
	                    <p>7. 有SNS、O2O、微信、电商等运营经验者优先考虑。</p>


					</div>
				</div>


				<div class="join_contmixi join_contmixi-ch hulianwang catagory_content" style="display: none;">
					<div class="join_cont_title">互联网社区专员</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						<p>1、策划、组织并跟进微博/微信/BBSS营销活动，并评估活动效果；</p>
	                    <p>2、监控和收集微博/微信/BBS的日常效果数据，并及时作出调整优化；</p>
	                    <p>3、结合业务需要和SNS发展规划，完成微博/微信/BBS的日常运营；</p>
	                    <p>4、研究学习微博/微信/BBS等SNS营销运营的方向、最新思路，作好信息搜集并转化内部应用；</p>
	                    <p>5、独立操作平台建设以及维护。</p>

					</div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						<p>1、熟悉互联网行业，两年以上SNS运营推广经验，具备大型网站经验者优先；</p>
	                    <p>2、有微博，BBS或SNS运营的工作经验</p>
	                    <p>3、有一定的美术功底，能熟练使用PS等做图软件</p>
	                    <p>4、有一定的文字功底，善于编辑原创博文</p>
	                    <p>5、能熟练使用PPT，EXCEL，WORD等办公软件</p>
	                    <p>6、沟通能力强，能充分了解客户需求</p>
	                    <p>7、工作踏实，为人正直，有服务精神</p>
	                    <p>8、有互联网相关资源者优先，有家居行业从业经验者优先。</p>


					</div>
				</div>


				<div class="join_contmixi join_contmixi-ch hulianwang catagory_content"  style="display: none;">
					<div class="join_cont_title">网络推广专员</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						<p>1.根据市场整体计划，参与制定推广方案，并对推广效果进行监测及优化；</p>
	                    <p>2.定期回顾广告投放效果并调整投放策略，直接对其效果负责；</p>
	                    <p>3.定期和广告代理、搜索引擎媒体进行沟通并进行管理；</p>
	                    <p>4.根据搜索引擎效果数据，进行账户及广告的优化、拓展网站推广内容，并与运营部门紧密协同；</p>
	                    <p>4.1在关键词，广告创意，账户结构等各个环节的优化提出持续的策略调整并实施，提高转化率</p>
	                    <p>4.2对相关的广告版本及广告页面进行测试及优化，提高转化率</p>
	                    <p>5.关注竞争对手、行业动态，及时调整投放策略，保持竞争优势；</p>
	                    <p>6.维护现有渠道和合作伙伴，拓展流量渠道和拓展网站的整体推广与对外合作；</p>
	                    <p>7.与其他部门沟通协调，获得跨部门的支持和合作。</p>


					</div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						<p>1. 电子商务、市场营销相关专业，3年以上网络推广运作经验。</p>
	                    <p>2. 熟练搜索引擎对于网站流量、品牌口碑、用户体验等的覆盖原理和策略；</p>
	                    <p>3. 具有较强的策划文案能力，能针对提高网站流量和转化率，进行合理的内容和版块规划；</p>
	                    <p>4. 熟悉搜索引擎推广模式和基本搜索引擎优化、交换友情链接等技术；熟悉百度竞价、Google AdWords、Bing Ads后台操作；</p>
	                    <p>5. 具备较强的数据处理能力和分析能力，能够对推广相关例如SEM广告投放的各种情况进行准确分析和研究；</p>
	                    <p>6. 熟练各种网站分析和统计工具；</p>
	                    <p>7. 独立完成推广方案和流程的制定，确保推广目标KPI完成；</p>
	                    <p>8. 熟练使用办公系列软件，具有一定的组织管理能力、判断能力、沟通协调、表达能力。</p>



					</div>
				</div>
				<br class="clear"/>
			</div>
			<%-- 运营中心内容 end --%>






			<div class="join_yyzx">
				<div class="join_yunyin">研发中心</div>
				<a href="#"></a>
			</div>
			<%-- 研发中心内容 start --%>
			<div class="join_content_box">
				<div class="join_content catagory_title1">
					<a href="#" id="zhiliang"><span class="spa">•</span>软件质量工程师</a>
					<a href="#" id="jiagou"><span class="spa">•</span>核心架构师</a>
					<a href="#" id="gaoji"><span class="spa">•</span>高级软件工程师</a>
					<a href="#" id="sousuo"><span class="spa">•</span>搜索架构师</a>
					<a href="#" id="yingyong"><span class="spa">•</span>JAVA应用架构师</a>
					<a href="#"id="shuju"><span class="spa">•</span>高级数据库管理员</a>
					<a href="#" id="yinqing"><span class="spa">•</span>搜索引擎高级开发工程师</a>
               </div>

				<div class="join_contmixi join_contmixi-ch  "  style="display: none;">
					<div class="join_cont_title">软件质量工程师</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						<p>1、负责项目开发团队的日常跟踪，协助项目经理完成团队成员的日常沟通、协助和能力培养等工作。</p>
	                    <p>2、协助项目经理完成相应项目的需求分析、计划定制和开发资源的调配工作。</p>
	                    <p>3、负责协助项目经理跟踪需求分析、评估、反馈、开发、测试和发布等定制开发全过程。</p>
	                    <p>4、协助沉淀项目的共性需求，为产品的改进提供建设性建议。</p>
	                    <p>5、定期向上级主管汇报工作情况、遇到的问题和解决思路等。</p>
	                    <p>6、熟悉使用JIRA工具、SVN版本管理工具</p>
	                    <p>7、熟悉LR性能测试工具，安卓自动化测试工具</p>


					</div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>职位描述：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						    <p>1.编写测试计划，搭建测试环境</p>
	                        <p>2.按需求编写、变更、更新测试用例</p>
	                        <p>3.按照测试用例进行测试，编写缺陷报告，保证系统质量测试</p>
	                        <p>4.反馈需求中存在的问题</p>
	                        <p>5.编写测试报告、缺陷分析报告</p>
	                        <p>6.测试用例评审及更新。</p>

					</div>
				</div>




	           	<div class="join_contmixi join_contmixi-ch "  style="display: none;">
					<div class="join_cont_title">核心架构师</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						             <p>1.负责公司平台产品的架构设计规划；</p>
	                                 <p>2.规划平台产品发展方向，支撑公司业务系统的快速发展；</p>
	                                 <p>3.负责平台产品的性能评测、容量评估，线上问题跟踪和处理；</p>
	                                 <p>4.能从可测性、容灾和系统稳定性等方面推动平台产品的发展。</p>
	                                 <p>5.主持和参与系统逻辑模型和物理模型设计；</p>
	                                 <p>6.负责设计或开发系统核心代码；</p>
	                                 <p>7.负责关键技术研究及技术预研；</p>
	                                 <p>8.通过开发工具或开发方法的改进，提高开发效率，并对开发人员进行技术培训；</p>
	                                 <p>9.负责新技术的研究与技术积累、关键技术的验证。</p>

	                </div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						     <p>1. 计算机或相关专业本科及以上学历，硕士优先；</p>
	                         <p>2. 8年以上工作经验，有大型网站开发经验。熟悉J2EE技术平台; 熟悉Spring，Struts和Hibernate等主流的开发框架；</p>
	                         <p>3. 负责公司产品业务架构、应用架构、数据架构及系统架构规划及设计工作；</p>
	                         <p>4. 督导产品架构的完善与研发实施；</p>
	                         <p>5. 根据行业发展方向，提供产品系统优化的策略方案；</p>
	                         <p>6. 解决系统的技术难关，指导系统开发人员技术攻关能力；</p>
	                         <p>7. 搭建系统架构体系及系统优化策略；</p>
	                         <p>8. 有服务器性能调优经验的优先；</p>
	                         <p>9. 对技术有激情，有强烈的责任心及良好的团队合作精神。</p>
	              </div>
				</div>


	           	<div class="join_contmixi join_contmixi-ch "  style="display: none;">
					<div class="join_cont_title">高级软件工程师</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						             <p>1、负责关键技术攻坚，带领团队完成相应项目开发、支持和技术性文档编写等工作。</p>
	                                 <p>2、负责项目开发团队的日常管理，协助项目经理完成团队成员的日常沟通、协助和能力培养等工作。</p>
	                                 <p>3、协助项目经理完成相应项目的需求分析、计划定制和开发资源的调配工作。</p>
	                                 <p>4、负责带领团队完成相应行业的需求分析、评估、反馈、开发、测试和发布等定制开发全过程。</p>
	                                 <p>5、协助沉淀项目的共性需求，为产品的改进提供建设性建议。 </p>
	                                 <p>6、对系统的重用、扩展、安全、性能、伸缩性、简洁等做系统级的把握。 </p>
	                                 <p>7、定期向上级主管汇报工作情况、遇到的问题和解决思路等。</p>

	                </div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						     <p>1、 具备五年以上Java软件开发经验,至少参与过2个大型互联网项目的研发 。</p>
	                         <p>2、 精通Java编程技术、设计模式、组件开发技术和面向对象思想 。 </p>
	                         <p>3、 精通主流WEB服务器，有一定的开发使用经验 。</p>
	                         <p>4、 精通oracle、sql server等大型关系数据库，有一定的数据库设计及数据库优化的经验 。</p>
	                         <p>5、 具有丰富的J2EE架构设计经验，精通java编程、设计模式和组件技术，精通正则表达式，熟练使用常用开源软件。</p>
	                         <p>6、 具有面向对象分析、设计、开发能力（OOA、OOD、OOP），掌握UML建模技能，能够使用常用的建模工具。 </p>
	                         <p>7、 具备一定的功能需求分析、系统设计的经验，能独立开展工作。</p>
	                         <p>8、 具备良好的团队协作能力和沟通能力，责任心强，工作耐心细致。</p>
	                         <p>9、 具备规范性开发、项目管理经验，参与过敏捷团队开发或建设者优先。 </p>
	                         <p>10、有大型系统架构设计或重构经验者优先，有SOA开发经验者优先。</p>

	              </div>
				</div>


	            <div class="join_contmixi join_contmixi-ch "  style="display: none;">
					<div class="join_cont_title">搜索架构师</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						             <p>1. 配合首席架构师，完成公司平台数据库的整体规划、部署方案、性能优化、安全等全面规划和管理；</p>
	                                 <p>2. 制定数据库设计标准，控制数据库设计的过程，统一变更和应用规范； </p>
	                                 <p>3. 对生产环境数据库进行全面管理和优化，解决数据库性能问题； </p>
	                                 <p>4. 建立分布式数据库系统，建立云数据存储和计算系统，保障数据库应用的扩展性和可伸缩性。</p>

	                </div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						    <p>1、5年以上大中型项目的数据分析、数据架构规划经验，丰富的数据库设计和管理经验；</p>
	                        <p>2、熟练掌握数据库设计理论，有大型电商平台，互联网平台的项目经验；</p>
	                        <p>3、精通MySQL数据库的开发、设计，熟悉MySQL集群和分布式部署、数据同步方案，有过大中型项目系统发、部署以及维护经验；</p>
	                        <p>4、熟练使用Cneter OS等Linux系统，精通配置管理Mysql数据库；</p>
	                        <p>5、精通MySQL Master/Slaves/Cluster应用运行体系及高可用解决方案；</p>
	                        <p>6、精通/熟悉MySQL数据库的运行机制和体系构架；</p>
	                        <p>7、精通/熟悉MySQL数据库的原理、精通数据库核心参数的设置和调整，经验丰富者优先；</p>
	                        <p>8、主动沟通，积极聆听，良好的团队合作精神和认真负责的工作态度； </p>
	                        <p>9、善于总结，将工作上的内容提取归纳成知识点和方法,与他人形成知识共享。</p>



	              </div>
				</div>




	        	<div class="join_contmixi join_contmixi-ch "  style="display: none;">
					<div class="join_cont_title">JAVA应用架构师</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						             <p>1、负责互联网网站、信息化系统和电子商务平台的系统规划、架构设计工作；</p>
	                                 <p>2、业务需求系统分析，提出技术研究及可行性报告及系统概要设计；</p>
	                                 <p>3、结合需求设计高扩展性、高性能、安全、稳定、可靠的技术系统 ；</p>
	                                 <p>4、指导研发工程师的产品开发和技术研究工作，解决各类技术疑难问题，形成良好的研发氛围，提升团队整体技术水平。</p>


	                </div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						     <p>1、5年以上软件行业相关工作经验，3年以上技术管理经验，2年以上架构设计经验；</p>
	                         <p>2、熟悉缓存技术，网站优化，服务器优化，集群技术处理、网站负载均衡、系统性能调优等软件编程高级技术；</p>
	                         <p>3、良好的逻辑思维能力，熟悉业务抽象和数据模型设计，具有很强的分析问题和解决问题的能力，对解决具有挑战性问题充满激情；</p>
	                         <p>4、知识面广，思路开阔，创新能力强，对新技术持有敏感性并愿意致力于新技术的探索和研究；</p>
	                         <p>5、具备大型电商系统架构、大型门户网站系统架构或大型(BS架构)系统架构经验者优先。</p>




	              </div>
				</div>





	 			<div class="join_contmixi join_contmixi-ch  "  style="display: none;">
					<div class="join_cont_title">高级数据库管理员（DBA）</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						            <p>1、安装搭建数据库服务器及群集，以及应用程序工具构建；</p>
	                                <p>2、熟悉数据库系统的存储结构预测未来的存储需求，制订数据库的存储方案；</p>
	                                <p>3、根据需求创建数据库存储结构和数据库对象；</p>
	                                <p>4、管理、监控数据库的存取访问，确保数据库的安全及稳定；</p>
	                                <p>5、海量数据存储架构搭建及维护。</p>



	                </div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						          <p>1、5年以上DBA经验，有海量数据库集成群构架经验者优先；</p>
	                              <p>2、精通MySQL Master/Slaves/Cluster应用运行体系及高可用解决方案；</p>
	                              <p>3、精通/熟悉MySQL数据库的运行机制和体系构架；</p>
	                              <p>4、精通/熟悉MySQL数据库的原理、精通数据库核心参数的设置和调整，经验丰富者优先；</p>
	                              <p>5、具备大型网站数据库高并发量设计经验、熟悉数据中心的设计、容量/性能管理和调优者优先；</p>
	                              <p>6、熟悉Linux以及Shell等脚本语言；</p>
	                              <p>7、有CMDBA或MySQL Cluster DBA Certification认证或同等技术水平者优先；</p>
	                              <p>8、具有良好的英文读写能力。</p>





	              </div>
				</div>





				<div class="join_contmixi join_contmixi-ch  "   style="display: none;">
					<div class="join_cont_title">搜索引擎高级开发工程师</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						          <p>1 、负责搜索引擎内核技术研发，核心算法开发，把握整体技术框架和发展方向；</p>
	                              <p>2 、利用数据挖掘和数理统计的理论方法解决搜索、推荐等实际问题；</p>
	                              <p>3 、不断改进搜索引擎算法及策略，实现技术与商业目标之间的完美结合，提升搜索引擎和互联网的商业价值；</p>
	                              <p>4、 负责个性化推荐系统的分析设计，对用户历史行为分析运用，提高转化率；</p>
	                              <p>5、 负责搜索产品服务架构和数据存储架构的设计与升级；</p>
	                              <p>6、 负责进行引擎/分词/爬虫的研究，提升搜索质量、性能、效率；</p>
	                              <p>7、 对搜索日志进行分析和挖掘，提高现有的搜索模块的精确度。</p>


	                </div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						         <p>1、8年以上工作经验，5年以上大中型互联网垂直搜索引擎系统开发经验，2年以上搜索引擎架构以及数据存储架构设计经验；</p>
	                             <p>2、C/C++或JAVA编程语言，至少精通一门编程语言；</p>
	                             <p>3、精通搜索引擎原理及相关知识，精通全文检索（分布式检索和实时索引）、网络爬虫和个性化推荐原理，同时精通倒排索引、中文分词、排序和自然语言处理等相关技术；有熟练使用并深入理解Lucene/Solr等搜索引擎工具者优先考虑；</p>
	                             <p>4、精通 Mysql 、Oracle、SQL Server 主流数据库之一，精通 SQL；</p>
	                             <p>5、熟练掌握并深入理解一种或两种NoSql，比如MongoDb、HBase、MemcacheDb；</p>
	                             <p>7、有优秀的沟通、理解能力，逻辑思维能力强，并且能够独立思考以及解决问题；</p>
	                             <p>8、工作态度踏实、认真、积极主动，能承受一定工作压力并注意团队合作；</p>
	                             <p>9、 有负责大中型互联网搜索引擎系统开发项目经验者优先考虑。</p>





	              </div>
				</div>
				<br class="clear"/>
			</div>
			<%-- 研发中心内容 end --%>



			<div class="join_yyzx">
				<div class="join_yunyin">营销中心</div>
				<a href="#"></a>
			</div>
			<%-- 营销中心内容 start --%>
			<div class="join_content_box">
				<div class="join_content">
					<a href="#">网站客服</a><a href="#">家具导购</a>
				</div>
				<br class="clear"/>
			</div>
			<%-- 营销中心内容 end --%>

			<div class="join_yyzx">
				<div class="join_yunyin">产品中心</div>
				<a href="#"></a>
			</div>
			<%-- 产品中心内容 start --%>
			<div class="join_content_box">
				<div class="join_content catagory_title2">
					<a href="#"  id="tiyan"><span class="spa">•</span>用户体验分析师</a>
				</div>

				<div class="join_contmixi join_contmixi-ch chanpin" id="fenxi" style="display: none;">
					<div class="join_cont_title">用户体验分析师</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						        <p>1、围绕网站平台整体及各产品线开展用户体验相关研究，目标是提升整体及各产品的用户体验水平，从而提高用户粘性；</p>
	                            <p>2、平台整体层面的用户体验研究将涉及全站流程优化、全站用户体验一致性等相关研究；</p>
	                            <p>3、产品线研究将围绕产品生命周期展开，主要包括产品需求调研、产品原型测试、产品上线后的测试、产品满意度及痛点调研等；</p>
	                            <p>4、具体负责调研项目的需求接洽、研究设计、数据收集和分析、报告撰写、报告陈述和推进落地等工作；</p>
	                            <p>5、合作关系：主要与产品经理、UI设计师紧密合作，配合完成产品开发、设计和优化相关的用户研究工作，同时也需要与网站运营等进行沟通。</p>



	                </div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						         <p>1、统计学、人机交互、心理学、社会学、市场营销等相关专业；</p>
	                             <p>2、热爱用户体验研究，3年以上互联网行业用户体验研究工作(设计)经验；</p>
	                             <p>3、熟悉网页交互设计制作，掌握用户研究、可用性研究方法，熟练使用统计分析软件；</p>
	                             <p>4、具有较强的分析能力和逻辑性，肯于钻研和深入思考；</p>
	                             <p>5、具有良好的沟通能力，跨团队协作能力；</p>
	                             <p>6、踏实勤奋，积极上进，并能承受较高强度的工作压力。</p>






	              </div>
				</div>
				<br class="clear"/>
			</div>
			<%-- 产品中心内容 end --%>



			<div class="join_yyzx">
					<div class="join_yunyin">商务中心</div>
					<a href="#"></a>
		   </div>
		   <%-- 商务中心内容 start --%>
		   <div class="join_content_box">
			   <div class="join_content catagory_title3" >
						<a href="#" id="kouzhan"><span class="spa">•</span>商务拓展经理</a>
			   </div>


				<div class="join_contmixi join_contmixi-ch shangwu" id="shangwuzhongxin" style="display: none;">
					<div class="join_cont_title">商务拓展经理</div>
					<div class="join_cont_gangwei"><span>></span>岗位职责：</div>
					<div class="join_cont_neirong">
						        <p>1. 基于公司业务发展需求，制定业务合作渠道拓展计划；</p>
	                           <p> 2. 负责新业务商务模式确定及内部协调；</p>
	                            <p>3. 拓展新渠道，维护加强已建立合作关系的渠道；</p>
	                           <p> 4. 收集并分析行业市场信息；</p>
	                            <p>5. 对BD合作效果负责，对负责的营销渠道的成果负责。</p>



	                </div>
					<div class="clear"></div>
					<div class="join_cont_gangwei join_cont_gangwei-ch"><span>></span>任职资格：</div>
					<div class="clear"></div>
					<div class="join_cont_neirong">
						       <p>1.二年以上互联网BD经历，渠道拓展、网盟等相关经历优先；</p>
	                           <p>2. 善于沟通，把握双方需求，策划双赢的合作方案；</p>
	                           <p>3. 熟悉各类媒体，建立和目标客户一致的营销渠道；</p>
	                           <p>4. 熟悉互联网线上线下推广工作的组织策划、营销方式与监督执行，能够独立撰写和执行推广方案；</p>
	                           <p>5. 对数据有一定敏感度，能通过数据发现问题，并配合运营给出优化方案；</p>
	                           <p>6. 有良好的团队合作精神、执行能力、谈判能力、表达能力、逻辑思维能力；</p>
	                           <p>7. 有较高职业素养，能承担压力；</p>
	                           <p>8. 有家居建材线上经验者优先。</p>






	              </div>
              </div>
				<br class="clear"/>
			</div>
		    <%-- 商务中心内容 end --%>

		</div>
	</div>
	<br class="clear"/>

	<%@ include file="/base/include/foot.tpl"%>
</body>
</html>