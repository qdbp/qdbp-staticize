<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="column" uri="http://www.jeshing.com/tags/column/"%>
<%@ taglib prefix="content" uri="http://www.jeshing.com/tags/content/"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>
<%@ taglib prefix="util" uri="http://www.jeshing.com/tags/util/"%>
<%@ taglib prefix="url" uri="http://www.jeshing.com/tags/url/"%>
<%@ taglib prefix="imgs" uri="http://www.jeshing.com/tags/images/"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/base/include/meta.tpl"%>
	<meta name="keywords" content="版权声明" />
	<meta name="description" content="版权声明" />
	<title>版权声明 - 乐居中国</title>
<%@ include file="/base/include/head.tpl"%>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/siteinfo.aboutus.css'/>">
</head>
<body>

	<%@ include file="/base/include/top.tpl"%>

	<%--页面主体--%>
	<div class="banquan_banner"></div>

	<div class="centern">
		<%@ include file="_banner.tpl"%>
		<br class="clear" />
	</div>

	<div class="centern">
		<div class="about_left">
			<%@ include file="_menu.tpl"%>
		</div>

		<div class="about_right">
		<div class="ab_title">
			<img src="<url:link href='resources/site/images/siteinfo/mtu.png'/>"/>
		版权声明<span>Copyright statement</span>
		</div>
		<div class="line"></div>
		<div class="ab_content ab_content-ch">
		   <div class="ab_content-ch-p">
				<p>一、用户保证对其上传的图片或素材享有合法的所有权或使用权。</p>
				<p>（一） "我图网"上的图片或素材均由用户上传</p>
				<p>（二） 用户在"我图网"上销售的是免版税类图片的使用权。</p>
				<p>（三） 若相关图片或素材侵犯了第三人的知识产权，一切法律后果均由该用户承担。</p>
		  </div>
		  <div class="ab_content-ch-p">
			<p>二、除用户上传的图片或素材外，"我图网"享有"我图网"的相关知识产权所有权。</p>
			<p>（一） "我图网"相关知识产权包括但不限于："我图网"商标、我图论坛、我图博客、我图威客、我图设计、我图广告等创造性、独创性智力成果。</p>
			<p>（二） 任何单位和个人未经"我图网"书面同意，不得使用"我图网"相关知识产权，否则"我图网"将依法追究其法律责任。</p>
		 </div>
		 <div class="ab_content-ch-p">
			<p>三、"我图网"仅负有法定的信息管理义务。</p>
			<p>（一） "我图网"作为图片或素材的交易、共享、存储平台，管理信息的能力有限，仅负有法定的删除、屏蔽、断开连接等义务。</p>
			<p>（二） "我图网"仅对公众人物、知名明星等侵权信息明显度较高的肖像类图片或素材进行审查。</p>
		</div>
		 <div class="ab_content-ch-p">
			<p>四、投诉、举报及相关处理。</p>
			<p>（一） 任何单位和个人向"我图网"投诉、举报时，应提交身份证明、权属证明、具体链接URL、被侵权基本情况说明等相关材料。</p>
			<p>（二） 接到上述材料后，"我图网"进行必要的审查；若情况属实，"我图网"将采取删除、屏蔽、断开连接等措施，对同一用户的重复侵权行为进行“黄名单”或“黑名单”管理。</p>
		</div>
		</div>


		</div>

	</div>

	<br class="clear"/>
	<%@ include file="/base/include/foot.tpl"%>
</body>
</html>

