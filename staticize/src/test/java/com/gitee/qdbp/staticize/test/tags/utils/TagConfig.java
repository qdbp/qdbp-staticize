package com.gitee.qdbp.staticize.test.tags.utils;

import java.util.regex.Pattern;
import com.gitee.qdbp.staticize.utils.TagUtils;
import com.gitee.qdbp.tools.utils.Config;
import com.gitee.qdbp.tools.utils.VerifyTools;

/**
 * 加载tag.txt配置文件
 *
 * @author zhaohuihua
 * @version 140724
 */
public class TagConfig extends Config {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    private static final String PATH = "classpath:/web/tag.txt";

    /** HTTP开头的URL是绝对路径 **/
    private static final Pattern HTTP = Pattern.compile("^https?://");

    /** 静态文件版本号 **/
    private static final String KEY_VERSION = "site.static.file.version";

    /** 静态文件类型 **/
    private static final String KEY_TYPES = "site.static.file.types";

    /** 静态文件类型正则表达式 **/
    private static Pattern PTN_TYPES;

    private static final TagConfig instance = new TagConfig();

    private TagConfig() {
        super(PATH);
    }

    public static TagConfig getInstance() {
        return instance;
    }

    /**
     * 转换为网站绝对URL
     *
     * @author zhaohuihua
     * @param prefix 网站URL前缀
     * @param href 链接地址
     * @return 网站绝对URL
     */
    public static String toSiteAbsoluteUrl(String prefix, String href) {
        return toSiteAbsoluteUrl(prefix, href, false);
    }

    /**
     * 转换为网站绝对URL
     *
     * @author zhaohuihua
     * @param prefix 网站URL前缀
     * @param href 链接地址
     * @param addVersion 是否追加版本号
     * @return 网站绝对URL
     */
    public static String toSiteAbsoluteUrl(String prefix, String href, boolean addVersion) {
        if (PTN_TYPES == null) {
            String types = TagConfig.getInstance().getString(KEY_TYPES);
            PTN_TYPES = Pattern.compile("\\.(" + types + ")$");
        }

        if (VerifyTools.isBlank(href)) {
            return "";
        }
        // HTTP开头的URL是绝对路径
        else if (HTTP.matcher(href).find()) {
            return href; // 绝对路径直接返回
        }

        // 处理网站URL前缀
        if (prefix == null) {
            prefix = "";
        } else if (!prefix.endsWith(TagUtils.SLASH)) {
            prefix += TagUtils.SLASH;
        }

        // 如果href以/开头, 去掉/
        if (href.startsWith(TagUtils.SLASH)) {
            href = href.substring(1);
        }
        href = prefix + href;

        // 如果是静态文件, 加上版本号
        if (addVersion && PTN_TYPES.matcher(href).find()) {
            String version = TagConfig.getInstance().getString(KEY_VERSION);
            if (!VerifyTools.isBlank(version)) {
                href += "?" + version;
            }
        }
        return href;
    }
}
