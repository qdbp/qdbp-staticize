package com.gitee.qdbp.staticize.test.publish;

import java.net.URL;
import java.util.Map;
import com.gitee.qdbp.staticize.common.IMetaData;
import com.gitee.qdbp.staticize.io.FileInputCreator;
import com.gitee.qdbp.staticize.io.IReaderCreator;
import com.gitee.qdbp.staticize.parse.TagParser;
import com.gitee.qdbp.staticize.publish.FilePublisher;
import com.gitee.qdbp.staticize.tags.base.Taglib;
import com.gitee.qdbp.tools.files.PathTools;
import com.gitee.qdbp.tools.utils.AssertTools;

/**
 * 基础发布测试类
 *
 * @author zhaohuihua
 * @version 20200916
 */
abstract class BasePublishTools {

    public static void testPublish(String resFolder, String resName, Map<String, Object> data) throws Exception {
        URL path = PathTools.findClassResource(BasePublishTools.class, resFolder);
        String folder = PathTools.toUriPath(path);

        String in = resName + ".tpl";
        String out = resName + ".html";
        String txt = resName + ".txt";

        System.out.println(folder);

        IReaderCreator input = new FileInputCreator(folder);
        TagParser parser = new TagParser(Taglib.defaults(), input);
        for (int i = 10; i < 500; i += 7) {
            TagParser.Options options = new TagParser.Options();
            options.setBufferSize(i);
            IMetaData metadata = parser.parse(in, options);

            FilePublisher publisher = new FilePublisher(metadata, folder);
            publisher.publish(data, out);

            AssertTools.assertTextFileEquals(PathTools.concat(folder, out), PathTools.concat(folder, txt));
        }
    }
}
