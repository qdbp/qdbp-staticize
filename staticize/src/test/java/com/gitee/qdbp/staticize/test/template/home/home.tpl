<!DOCTYPE html>
<html>
<head>
	<core:include src="../include/meta.tpl"/>
	<column:detail key="shouye">
		<meta name="keywords" content="${column.seo.key}" />
		<meta name="description" content="${column.seo.desc}" />
		<title>${column.seo.title}</title>
	</column:detail>
	<core:include src="../include/head.tpl"/>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/home.css'/>">
 	<script type="text/javascript" src="<url:link href='resources/base/js/cyclic/zhh.cyclic.js'/>"></script>
 	<script type="text/javascript" src="<url:link href='resources/site/js/home.js'/>"></script>
</head>
<body>

	<core:include src="../include/top.tpl" column="home" />

	<div class="marquee-box hover">
		<position:detail key="shouye|shouyelunbotu">
			<position:content var="list" key="${position.id}" rows="3">
				<div>
					<ul class="marquee-list zhh-cyclic" data-box=".marquee-box" data-pointer-item=".mq-pointer">
						<div style="width: 100%;">
							<core:each var="item" items="${list}" status="s">
							<li>
								<div class="marquee-item">
									<a href="<url:link href='${item.url}'/>" title="${item.title}" target="_blank"><img src="<url:image src='${item.image.src}'/>" alt="${item.title}" width="1351" height="420"/></a>
								</div>
							</li>
							</core:each>
						</div>
					</ul>
					<ul class="pointer">
						<core:each var="item" items="${list}" status="s">
							<li class="mq-pointer"><a href="#${s.index}" title="${item.title}"></a></li>
						</core:each>
					</ul>
				</div>
			</position:content>
		</position:detail>
		<div class="clear"></div>
	</div>
	<div class="centern-big">
		<position:detail key="shouye|shuliangtongjiwei">
				<position:content var="list" key="${position.id}" rows="4">
					<core:each var="item" items="${list}"  status="s">
						<div class="number-info <core:if test="${s.last}"> margin0-r </core:if> ">
							<util:split string="${item.digest}" separator="" var="strs">
								<core:each var="str" items="${strs}">
									<a>${str }</a>
								</core:each>
							</util:split>
							<a class="span_a" href="<url:link href='${item.url}'/>">${item.title}</a>
						</div>
					</core:each>
				</position:content>
		</position:detail>
	</div>
	<div class="clear"></div>
	<position:detail key="zhuangxiu|qingsongjiubu" >
	<div class="centern-big step-bottom clear_box">
		 <span class="step-nine-info step-nine-font fl"  href="<url:link href='${position.url}'/>">
			<a class="bg4 step-nine-icon fl"></a>
			<a class="step_nine_f fl" href="<url:link href='${position.url}'/>" >${position.title }</a>
		 </span>
		 <span class="step-nine-info fl">
			${position.shortTitle }
		 </span>
	</div>
	<div class="centern-big step-nine-content step_box">
		<position:content var="list" key="${position.id}" rows="9">
				<core:each var="item" items="${list}" status="s">
					<a href="<url:link href='${item.url}'/>" <core:if test="${s.last}"> class="margin0-r" </core:if> style="background:url(<url:image src='${item.image.src}'/>) no-repeat">${item.title}</a>
				</core:each>
		</position:content>
	</div>
	</position:detail>
	<div class="centern-big" style="margin-top: 30px;">
		<div class="daily-left">
			<position:detail key="shouye|meiridaodu" >
			<div class="bg3 daily-left-head" >
				<a class="daily-left-head-titlebig" href="<url:link href='${position.url}'/>">${position.title }</a>
				<a class="daily-left-head-titlesmall" href="<url:link href='${position.url}'/>">${position.subTitle }</a>
				<span class="daily-left-head-line" ></span>
			</div>
			<position:content var="list" key="${position.id}" rows="4">
				<div class="daily-imginfo" >
					<core:each var="item" items="${list}" status="s">
						<div class="daily-imginfo-warp" >
							<a class="daily-img"  href="<url:link href='${item.url}'/>"  style="background:url(<url:image src='${item.image.src}'/>) no-repeat"></a>
							<a class="daily-info" href="<url:link href='${item.url}'/>" >${item.title}</a>
						</div>
					</core:each>
				</div>
			</position:content>
			</position:detail>
			<div class="daily-txtinfo-warp" >

				<position:detail key="shouye|meiridaodutuijian" >
					<position:content var="list" key="${position.id}" rows="1">
						<core:each var="item" items="${list}" status="s">
							<a  class="daily-txtinfo-title" href="<url:link href='${item.url}'/>">${item.title }</a>
							<a  class="daily-txtinfo-content" href="<url:link href='${item.url}'/>">${item.digest }</a>
						</core:each>
					</position:content>
				</position:detail>

				<position:detail key="shouye|meiridaodutuijian|zhuangxiu" >
					<div  class="daily-txtinfo-split" ></div>
					<div class="daily-txtinfo-item-warp"><a class="bg2 orange-icon"  href="<url:link href='${item.url}'/>" >${position.title }</a></div>
					<div class="daily-txtinfo-item-ul">
						<ul>
							<position:content var="list" key="${position.id}" rows="3">
								<core:each var="item" items="${list}" status="s">
									<li><span></span><a href="<url:link href='${item.url}'/>"  class="<core:if test="${s.index==0}"> orange</core:if> orange-hover"   >${item.title }</a></li>
								</core:each>
							</position:content>
						</ul>
					</div>
					<div class="clear"></div>
				</position:detail>
				<position:detail key="shouye|meiridaodutuijian|dapei" >
					<div  class="daily-txtinfo-split" ></div>
					<div class="daily-txtinfo-item-warp"><a class="bg2 green-icon" href="<url:link href='${item.url}'/>" >${position.title }</a></div>
					<div class="daily-txtinfo-item-ul">
						<ul>
							<position:content var="list" key="${position.id}" rows="3">
								<core:each var="item" items="${list}" status="s">
									<li><span></span><a href="<url:link href='${item.url}'/>"  class="<core:if test="${s.index==0}"> green </core:if> green-hover"  >${item.title }</a></li>
								</core:each>
							</position:content>
						</ul>
					</div>
					<div class="clear"></div>
				</position:detail>
				<position:detail key="shouye|meiridaodutuijian|anli" >
					<div  class="daily-txtinfo-split" ></div>
					<div class="daily-txtinfo-item-warp"><a class="bg2 blue-icon" href="<url:link href='${item.url}'/>" >${position.title }</a></div>
					<div class="daily-txtinfo-item-ul">
						<ul>
							<position:content var="list" key="${position.id}" rows="3">
								<core:each var="item" items="${list}" status="s">
									<li><span></span><a href="<url:link href='${item.url}'/>"  class="<core:if test="${s.index==0}"> blue </core:if> blue-hover"  >${item.title }</a></li>
								</core:each>
							</position:content>
						</ul>
					</div>
					<div class="clear"></div>
				</position:detail>
				<position:detail key="shouye|meiridaodutuijian|anli" >
					<div  class="daily-txtinfo-split" ></div>
					<div class="daily-txtinfo-item-warp"><a class="bg2 pink-icon" href="<url:link href='${item.url}'/>" >${position.title }</a></div>
					<div class="daily-txtinfo-item-ul">
						<ul>
							<position:content var="list" key="${position.id}" rows="3">
								<core:each var="item" items="${list}" status="s">
									<li><span></span><a href="<url:link href='${item.url}'/>"   class="<core:if test="${s.index==0}">pink</core:if> pink-hover"   >${item.title }</a></li>
								</core:each>
							</position:content>
						</ul>
					</div>
					<div class="clear"></div>
				</position:detail>
			</div>
			<div class="clear"></div>

			<position:detail key="shouye|tuijianhuodong|zuo" >
			<div class="daily-recommend-head-warp">
				<span class="bg1 daily-recommend-head-icon"></span>
				<a href="<url:link href='${item.url}'/>"  class="daily-recommend-head-txt">${position.title }</a>
			</div>
			<div class="daily-recommend-warp ">
						<position:content var="list" key="${position.id}" rows="2">
							<core:each var="item" items="${list}" status="s">
								<div class="daily-recommend-pic-warp  <core:if test="${s.index==0}"> margin20-r </core:if>">
									<div class="daily-recommend-pic-info">
										<a class="img_a"  href="<url:link href='${item.url}'/>" style="background:url(<url:image src='${item.image.src}'/>)"></a>
										<a class="span_a" href="<url:link href='${item.url}'/>"  >${item.title }</a>
									</div>
								</div>
							</core:each>
						</position:content>
			</div>
			</position:detail>
		</div>
		<div class="daily-right"  >
			<position:detail key="shouye|lejudaoxiang" >
			<div class="daily-right-head"  >
				<span class="daily-right-head-icon"  ></span>
				<span class="daily-right-head-title" >${position.title }</span>
			</div>
			<div class="daily-right-body-top"  ></div>
			<div class="daily-right-body"  >
				<position:content var="list" key="${position.id}" rows="5">
					<core:each var="pitem" items="${list}" status="s">
							<div class="daily-right-body-head">
								<a class="bg4 daily-right-body-icon-${s.index+1 }" href="<url:link href='${pitem.url}'/>"></a>
								<a  class="daily-right-body-title" href="<url:link href='${pitem.url}'/>">${pitem.title }</a>
								<span  class="daily-right-body-titlesub">${pitem.shortTitle }</span>
							</div>

								<position:content var="list" key="${pitem.positionId}" rows="6">
									<core:each var="item" items="${list}" status="ss">
										<div class="daily-right-body-content">
											<a class="bg1 a-icon" href="<url:link href='${item.url}'/>"></a>
											<a class="a-txt" href="<url:link href='${item.url}'/>" >${item.title }</a>
											<core:if test="${item.digest=='new' }">
												<span class="bg2 daily-right-new">NEW</span>
											</core:if>
											<core:if test="${item.digest=='hot' }">
												<span class="bg2 daily-right-hot">HOT</span>
											</core:if>
<!-- 											<span class="bg2 daily-right-new">NEW</span> -->
<!-- 											<span class="bg2 daily-right-hot">HOT</span> -->
										</div>
									</core:each>
								</position:content>

							<div class="clear"></div>
					</core:each>
				</position:content>
			</div>
			<div class="daily-right-body-bottom"  ></div>
			</position:detail>
		</div>
	</div>
	<div class="clear"></div>
	<!--  装修宝典 -->
	 <div class="centern-big decoration-centern"  >
		<div class="decoration-left mq-box">
			<position:detail key="shouye|zhuangxiubaodian|lunbotu" >
			<div class="bg3 decoration-left-head" >
				<a class="decoration-left-head-titlebig" href="<url:link href='${position.url}'/>">${position.title }</a>
				<a class="decoration-left-head-titlesmall" href="<url:link href='${position.url}'/>">${position.subTitle }</a>
				<span class="decoration-left-head-line"></span>
			</div>
			<position:content var="list" key="${position.id}" rows="9">
			<div class="decoration-left-pic-warp mq-list"  data-pointer-curr="circle-icon-selected"  >
					<core:each var="item" items="${list}" status="s">
						<core:if test="${s.index==0||s.index==3||s.index==6}">
							<div class="decoration-left-pic-warp-block">
						</core:if>
						<div class="decoration-left-pic">
							<core:if test="${s.index==0||s.index==3||s.index==6||s.index==2||s.index==5||s.index==8}">
								<core:if test="${s.index==0||s.index==3||s.index==6}"><a class="bg1 icon-share icon-blue-up1-share" ></a></core:if>
								<core:if test="${s.index==2||s.index==5||s.index==8}"><a class="bg1 icon-share icon-green-up3-share" ></a></core:if>
								<a class="img-a"  href="<url:link href='${item.url}'/>"   style="background:url(<url:image src='${item.image.src}'/>)"></a>
								<a href="<url:link href='${item.url}'/>"
								<core:if test="${s.index==0||s.index==3||s.index==6}">class="txt-a bluebg"</core:if>
								<core:if test="${s.index==2||s.index==5||s.index==8}">class="txt-a greenbg"</core:if>
								>${item.title }</a>
							</core:if>
							<core:if test="${s.index==1||s.index==4||s.index==7}">
								<a class="bg1 icon-share icon-pink-down2-share" ></a>
								<a href="<url:link href='${item.url}'/>" class="txt-a pinkbg"	>${item.title }</a>
								<a class="img-a"  href="<url:link href='${item.url}'/>"   style="background:url(<url:image src='${item.image.src}'/>)"></a>
							</core:if>
						</div>
						<core:if test="${s.index==2||s.index==5||s.index==8||s.last}">
							</div>
						</core:if>
					</core:each>
			</div>
			<div class="decoration-circle " >
				<core:each var="x" begin="1" rows="${list.size()%3==0?list.size()/3:(list.size()/3)+1}">
					<a class="bg1 circle-icon mq-pointer"></a>
<!-- 					circle-icon-selected -->
				</core:each>
			</div>
			</position:content>
			</position:detail>

			<position:detail key="shouye|zhuangxiubaodian|tuijian|zuo" >
				<position:content var="list" key="${position.id}" rows="4">
				<div class="decoration-left-middle-warp">
					<core:each var="item" items="${list}" status="s">
						<core:if test="${s.index==0}">
							<div class="decoration-left-middle-left">
								<a class="decoration-left-middle-left-img"  href="<url:link href='${item.url}'/>" style="background:url(<url:image src='${item.image.src}'/>)"></a>
							</div>
						</core:if>
					</core:each>
					<div class="decoration-left-middle-right">
						<core:each var="item" items="${list}" status="s">
							<core:if test="${s.index==0}">
								<a href="<url:link href='${item.url}'/>" class="decoration-left-middle-right-title">${item.title}</a>
							</core:if>
							<core:if test="${s.index!=0}">
								<div class="decoration-left-middle-right-content">
									<a href="<url:link href='${item.url}'/>" class="bg1 decoration-left-middle-right-content-icon-a"></a>
									<a href="<url:link href='${item.url}'/>" class="decoration-left-middle-right-content-txt-a">${item.title}</a>
								</div>
							</core:if>
						</core:each>
					</div>
				</div>
				</position:content>
			</position:detail>
			<position:detail key="shouye|zhuangxiubaodian|tuijian|you" >
				<position:content var="list" key="${position.id}" rows="4">
				<div class="decoration-left-middle-warp">
					<core:each var="item" items="${list}" status="s">
						<core:if test="${s.index==0}">
							<div class="decoration-left-middle-left">
								<a class="decoration-left-middle-left-img"  href="<url:link href='${item.url}'/>" style="background:url(<url:image src='${item.image.src}'/>)"></a>
							</div>
						</core:if>
					</core:each>
					<div class="decoration-left-middle-right">
						<core:each var="item" items="${list}" status="s">
							<core:if test="${s.index==0}">
								<a href="<url:link href='${item.url}'/>" class="decoration-left-middle-right-title">${item.title}</a>
							</core:if>
							<core:if test="${s.index!=0}">
								<div class="decoration-left-middle-right-content">
									<a href="<url:link href='${item.url}'/>" class="bg1 decoration-left-middle-right-content-icon-a"></a>
									<a href="<url:link href='${item.url}'/>" class="decoration-left-middle-right-content-txt-a">${item.title}</a>
								</div>
							</core:if>
						</core:each>
					</div>
				</div>
				</position:content>
			</position:detail>
			<div class="clear"></div>

			<position:detail key="shouye|zhuangxiujisuanqi" >
			<div class="decoration-warp">
				<position:content var="list" key="${position.id}" rows="6">
						<core:each var="item" items="${list}" status="s">
							<div class="decoration-icon-warp">
								<a class="decoration-icon" href="<url:link href='${item.url}'/>" style="background:url(<url:image src='${item.image.src}'/>)">
									<span class="decoration-icon-txt" >${item.title}</span>
								</a>
							</div>
						</core:each>
				</position:content>
			</div>
			</position:detail>
		</div>
		<div class="decoration-right" >
			<position:detail key="shouye|remenzhuangxiugongsi" >
			<div class="bg3 decoration-right-head" >
				<span class="decoration-right-head-title" >${position.title}</span>
			</div>
			<position:content var="list" key="${position.id}" rows="8">
            <div class="decoration_right_height"></div>
            <div class="decoration-right-head-list" >
				<div class="decoration-right-head-body">
					<core:each var="item" items="${list}" status="s" >
						<a class="decoration-right-head-bodyimg"  href="<url:link href='${item.url}'/>" style="background:url(<url:image src='${item.image.src}'/>)"></a>
					</core:each>
				</div>
			</div>
			</position:content>
			</position:detail>
		</div>
	</div>
	<div class="clear"></div>
	<!-- 家居搭配 -->
	<div class="centern-big collocation-centern">
		<div class="collocation-left mq-box"  >
			<position:detail key="shouye|jiajudapei|lunbotu" >
			<div class="bg3 collocation-left-head" >
				<a class="decoration-left-head-titlebig" href="<url:link href='${position.url}'/>">${position.title }</a>
				<a class="decoration-left-head-titlesmall" href="<url:link href='${position.url}'/>">${position.subTitle }</a>
				<span class="collocation-left-head-line"  ></span>
			</div>
			<position:content var="list" key="${position.id}" rows="9">
			<div class="decoration-left-pic-warp mq-list"  data-pointer-curr="circle-icon-selected">
					<core:each var="item" items="${list}" status="s">
					<core:if test="${s.index==0||s.index==3||s.index==6}">
							<div class="decoration-left-pic-warp-block">
					</core:if>
					<div class="decoration-left-pic">
						<core:if test="${s.index==0||s.index==3||s.index==6||s.index==2||s.index==5||s.index==8}">
							<core:if test="${s.index==0||s.index==3||s.index==6}"><a class="bg1 icon-share icon-pink-up1-share" ></a></core:if>
							<core:if test="${s.index==2||s.index==5||s.index==8}"><a class="bg1 icon-share icon-blue-up3-share" ></a></core:if>
							<a class="img-a"  href="<url:link href='${item.url}'/>"   style="background:url(<url:image src='${item.image.src}'/>)"></a>
							<a href="<url:link href='${item.url}'/>"
							<core:if test="${s.index==0||s.index==3||s.index==6}">class="txt-a pinkbg"</core:if>
							<core:if test="${s.index==2||s.index==5||s.index==8}">class="txt-a bluebg"</core:if>
							>${item.title }</a>
						</core:if>
						<core:if test="${s.index==1||s.index==4||s.index==7}">
							<a class="bg1 icon-share icon-green-down2-share" ></a>
							<a href="<url:link href='${item.url}'/>" class="txt-a greenbg"	>${item.title }</a>
							<a class="img-a"  href="<url:link href='${item.url}'/>"   style="background:url(<url:image src='${item.image.src}'/>)"></a>
						</core:if>
					</div>
					<core:if test="${s.index==2||s.index==5||s.index==8||s.last}">
							</div>
					</core:if>
					</core:each>
			</div>
			<div class="decoration-circle" >
				<core:each var="x" begin="1" rows="${list.size()%3==0?list.size()/3:(list.size()/3)+1}">
					<a class="bg1 circle-icon mq-pointer"></a>
<!-- 					circle-icon-selected -->
				</core:each>
			</div>
			</position:content>
			</position:detail>

			<position:detail key="shouye|jiajudapei|tuijian|zuo" >
				<position:content var="list" key="${position.id}" rows="4">
				<div class="decoration-left-middle-warp">
					<core:each var="item" items="${list}" status="s">
						<core:if test="${s.index==0}">
							<div class="decoration-left-middle-left">
								<a class="decoration-left-middle-left-img"  href="<url:link href='${item.url}'/>" style="background:url(<url:image src='${item.image.src}'/>)"></a>
							</div>
						</core:if>
					</core:each>
					<div class="decoration-left-middle-right">
						<core:each var="item" items="${list}" status="s">
							<core:if test="${s.index==0}">
								<a href="<url:link href='${item.url}'/>" class="decoration-left-middle-right-title">${item.title}</a>
							</core:if>
							<core:if test="${s.index!=0}">
								<div class="decoration-left-middle-right-content">
									<a href="<url:link href='${item.url}'/>" class="bg1 decoration-left-middle-right-content-icon-a"></a>
									<a href="<url:link href='${item.url}'/>" class="decoration-left-middle-right-content-txt-a">${item.title}</a>
								</div>
							</core:if>
						</core:each>
					</div>
				</div>
				</position:content>
			</position:detail>
			<position:detail key="shouye|jiajudapei|tuijian|you" >
				<position:content var="list" key="${position.id}" rows="4">
				<div class="decoration-left-middle-warp">
					<core:each var="item" items="${list}" status="s">
						<core:if test="${s.index==0}">
							<div class="decoration-left-middle-left">
								<a class="decoration-left-middle-left-img"  href="<url:link href='${item.url}'/>" style="background:url(<url:image src='${item.image.src}'/>)"></a>
							</div>
						</core:if>
					</core:each>
					<div class="decoration-left-middle-right">
						<core:each var="item" items="${list}" status="s">
							<core:if test="${s.index==0}">
								<a href="<url:link href='${item.url}'/>" class="decoration-left-middle-right-title">${item.title}</a>
							</core:if>
							<core:if test="${s.index!=0}">
								<div class="decoration-left-middle-right-content">
									<a href="<url:link href='${item.url}'/>" class="bg1 decoration-left-middle-right-content-icon-a"></a>
									<a href="<url:link href='${item.url}'/>" class="decoration-left-middle-right-content-txt-a">${item.title}</a>
								</div>
							</core:if>
						</core:each>
					</div>
				</div>
				</position:content>
			</position:detail>

			<div class="clear"></div>
			<div class="collocation-warp" >

				<position:detail key="shouye|lejuaijia" >
				<div class="collocation-left-picwarp"  >
					<span class="bg1 collocation-left-picicon" ></span>
					<span class="collocation-left-pictitle" >${position.title }</span>
				</div>
				<div class="collocation-left-zt"  >
					<position:content var="list" key="${position.id}" rows="4">
						<core:each var="item" items="${list}" status="s">
						<div class="collocation-left-zt-warp">
							<a class="collocation-left-zt-bg"  href="<url:link href='${item.url}'/>" style="background: url(<url:image src='${item.image.src}'/>) no-repeat;"></a>
							<a class="collocation-left-zt-txt" href="<url:link href='${item.url}'/>" >${item.title}</a>
							<span class="bg1 collocation-left-zt-share" ></span>
						</div>
						</core:each>
					</position:content>
				</div>
				</position:detail>
			</div>
		</div>
		<div class="collocation-right" >

			<position:detail key="shouye|lepinyougou" >
			<div class="bg3 collocation-right-head" >
				<span class="collocation-right-head-title" >${position.title }</span>
			</div>
			<div class="collocation-right-head-body-top"></div>
			<position:content var="list" key="${position.id}" rows="4">
			<div class="collocation-right-head-body-warp" >
				<div class="collocation-right-head-body-list">
					<core:each var="item" items="${list}"  status="s">
						<div class="collocation-right-head-body">
							<div  class="collocation-right-head-body-pic" >
								<a class="collocation-right-head-body-pic-img" href="<url:link href='${item.url}'/>" style="background: url(<url:image src='${item.image.src}'/>) no-repeat;"></a>
							</div>
							<div class="collocation-right-head-body-info"  >
								<a class="collocation-right-head-body-info-title" >${item.title }</a>
								<span class="collocation-right-head-body-info-oldprice" >参考价：${item.shortTitle }元</span>
								<span class="collocation-right-head-body-info-newprice" >惊爆价：</span>
								<span class="collocation-right-head-body-info-price" >￥${item.digest }</span>
								<a href="<url:link href='${item.url}'/>"  class="bg3 collocation-right-head-body-info-btn" >去看看</a>
							</div>
							<div class="clear"></div>
						</div>
					</core:each>
				</div>
			</div>
			</position:content>
			</position:detail>
		</div>
	</div>
	<div class="clear"></div>

	<!--  装修案例 -->
	<div class="centern-big case-centern">
		<div class="case-left">
			<position:detail key="shouye|zhuangxiuanli" >
				<div class="bg3 case-left-head" >
					<a class="decoration-left-head-titlebig" href="<url:link href='${position.url}'/>">${position.title }</a>
					<a class="decoration-left-head-titlesmall" href="<url:link href='${position.url}'/>">${position.subTitle }</a>
					<span class="case-left-head-line" ></span>
				</div>
				<position:content var="list" key="${position.id}" rows="6">
					<div class="case-left-nav" >
						<core:each var="item" items="${list}" status="s">
							<a class="case-left-nav-item" name="anli-${item.positionId}" data-position-id="${item.positionId}">${item.title }</a>
						</core:each>
					</div>
					<div class="case-image-warp-parent">
					<core:each var="itemp" items="${list}" status="s">
						<div name="anlicontent-${itemp.positionId}">
							<div class="decoration-case-box">
								<position:content var="list" key="${itemp.positionId}" rows="15">
									<core:each var="item" items="${list}" status="s">
										<core:if test="${s.index < 3}">
										<div class="decoration-case-img-${s.index+1 } ">
											<a href="<url:link href='${item.url}'/>"  >
												<img src="<url:image src='${item.image.src}'/>" alt="${item.title}" width="275" height="295"/>
												<span 	class="decoration-case-img-bg1"></span>
												<span   class="decoration-case-title1">${item.title }</span>
												<span   class="decoration-case-author1">${item.shortTitle }</span>
											</a>
										</div>
										</core:if>
										<core:if test="${s.index > 2}">
										<div class="decoration-case-img-${s.index+1 } ">
											<a href="<url:link href='${item.url}'/>"  >
												<img src="<url:image src='${item.image.src}'/>" alt="${item.title}" width="132" height="142"/>
												<span 	class="decoration-case-img-bg"></span>
												<span   class="decoration-case-title">${item.title }</span>
												<span   class="decoration-case-author">${item.shortTitle }</span>
											</a>
										</div>
										</core:if>
									</core:each>
						</position:content>
									</div>
								</div>
					</core:each>
					</div>
				</position:content>
			</position:detail>
		</div>
		<div class="case-right">
			<position:detail key="shouye|renqishejishi" >
				<div class="bg3 case-right-head-body-top" >
					<span class="case-right-head-body-toptxt" >${position.title }</span>
				</div>
				<position:content var="list" key="${position.id}" rows="6">
					<core:each var="item" items="${list}"  status="s">
						<div class="case-right-item-warp" >
		<!-- 					<a class="bg2 case-right-item-icon" >特约</a> -->
							<a class="case-right-item-pic"  href="<url:link href='${item.url}'/>" style="background:url(<url:image src='${item.image.src}'/>)"></a>
							<a href="<url:link href='${item.url}'/>" class="case-right-item-name" >${item.title }</a>
							<a href="<url:link href='${item.url}'/>" class="case-right-item-txt" >${item.shortTitle }</a>
						</div>
					</core:each>
				</position:content>
	 		</position:detail>
		</div>
	</div>

	<core:include src="../include/foot.tpl"/>

</body>
</html>

