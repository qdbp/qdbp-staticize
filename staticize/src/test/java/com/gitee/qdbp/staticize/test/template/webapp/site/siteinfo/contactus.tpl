<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="column" uri="http://www.jeshing.com/tags/column/"%>
<%@ taglib prefix="content" uri="http://www.jeshing.com/tags/content/"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>
<%@ taglib prefix="util" uri="http://www.jeshing.com/tags/util/"%>
<%@ taglib prefix="url" uri="http://www.jeshing.com/tags/url/"%>
<%@ taglib prefix="imgs" uri="http://www.jeshing.com/tags/images/"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/base/include/meta.tpl"%>
	<meta name="keywords" content="联系我们" />
	<meta name="description" content="联系我们" />
	<title>联系我们 - 乐居中国</title>
<%@ include file="/base/include/head.tpl"%>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/siteinfo.aboutus.css'/>">
</head>
<body>

	<%@ include file="/base/include/top.tpl"%>

	<%--页面主体--%>
	<div class="banner contact_banner"></div>

	<div class="centern">
		<%@ include file="_banner.tpl"%>
		<br class="clear" />
	</div>

	<div class="centern">
		<div class="about_left">
			<%@ include file="_menu.tpl"%>
		</div>

		<div class="contact_right">
			<div class="about_right">
				<div class="ab_title">
					<img src="<url:link href='resources/site/images/siteinfo/mtu.png'/>"/> 联系我们<span>Contact
						us </span>
				</div>
				<div class="line"></div>
				<div class="ab_content sm_cont">
					<div class="sm_title">总部（南京）</div>
					<div class="sm_content">
						<div class="contact_content">
							<img src="<url:link href='resources/site/images/siteinfo/address.png'/>">地址：南京市汉中路89号金鹰国际商城29楼</img>
						</div>
						<div class="contact_content">
							<img src="<url:link href='resources/site/images/siteinfo/postcode.png'/>">邮编：210029</img>
						</div>
						<div class="contact_content">
							<img src="<url:link href='resources/site/images/siteinfo/tel.png'/>">电话：025-51806018</img>
						</div>

					</div>

				</div>
				<div class="ab_content sm_cont">
					<div class="sm_title">北京分公司（北京）</div>
					<div class="sm_content">
						<div class="contact_content">
							<img src="<url:link href='resources/site/images/siteinfo/address.png'/>">地址：南京市汉中路89号金鹰国际商城29楼</img>
						</div>
						<div class="contact_content">
							<img src="<url:link href='resources/site/images/siteinfo/postcode.png'/>">邮编：210029</img>
						</div>
						<div class="contact_content">
							<img src="<url:link href='resources/site/images/siteinfo/tel.png'/>">电话：025-51806018</img>
						</div>

					</div>

				</div>
				<div class="ab_content sm_cont">
					<div class="sm_title">武汉分公司（北京）</div>
					<div class="sm_content sm_baike">
						<div class="contact_content">
							<img src="<url:link href='resources/site/images/siteinfo/address.png'/>">地址：南京市汉中路89号金鹰国际商城29楼</img>
						</div>
						<div class="contact_content">
							<img src="<url:link href='resources/site/images/siteinfo/postcode.png'/>">邮编：210029</img>
						</div>
						<div class="contact_content">
							<img src="<url:link href='resources/site/images/siteinfo/tel.png'/>">电话：025-51806018</img>
						</div>

					</div>

				</div>


			</div>
			<div class="contact_right_bottom">
				<div class="contact_email">
					<div class="cont_img">
						<img src="<url:link href='resources/site/images/siteinfo/emails.png'/>"/>
					</div>
					<div class="cont_nrong">
						<span>如果您是媒体、网站、渠道， 想洽谈资源置换、市场合作。</span>
					</div>
					<div class="cont_nrong conts">
						请联系：<span>marketing@lejujs.com</span>
					</div>
				</div>

				<div class="contact_email contact_emails">
					<div class="cont_img cont_imgs">
						<img src="<url:link href='resources/site/images/siteinfo/email.png'/>"/>
					</div>
					<div class="cont_nrong">
						<span>如果您想在乐居中国投放广告或获得精准业务</span>
					</div>
					<div class="cont_nrong conts">
						请联系：<span>ad@lejujs.com</span>
					</div>
				</div>
		</div>
	</div>
	</div>
	<br class="clear"/>

	<%@ include file="/base/include/foot.tpl"%>
</body>
</html>

