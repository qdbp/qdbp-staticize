<!--  底部 -->
<div class="footer_box">
	<div class="footer_main">
		<div class="footer_center">
			<div class="footer_center_left fl">
				<div class="footer_nav">
					<position:detail key="shouye|dibulianjie">
						<position:content key="${position.id}">
							<core:each var="item" items="${list}" status="s">
								<core:if test="${s.index==0}">
									<a href="<url:link href='${item.url}'/>">${item.title}</a>
								</core:if>
								<core:if test="${s.index > 0}">
									<span>|</span> <a href="<url:link href='${item.url}'/>">${item.title}</a>
								</core:if>
							</core:each>
					     </position:content>
				     </position:detail>
				</div>
				<p>Copyright © lejujs.com All Rights Reserved</p>
				<p>苏ICP备13011498号-1</p>
			</div>
			<div class="footer_center_right fr">
				<div class="fr footer_weixin">
					<img src="<url:link href='resources/base/images/common/weixin_img.jpg'/>"><span>关注乐居中国微信</span>
				</div>
				<div class="fr footer_qq">
					<span class="bg4"></span><font>乐居中国</font><a href="#" class="bg2"></a>
				</div>
				<div class="fr footer_weibo">
					<span class="bg4"></span><font>乐居中国</font><a href="#" class="bg2"></a>
				</div>
			</div>
		</div>
		<div class="footer_link">
			<position:detail key="shouye|zhuangxiuanli|youqinglianjie" >
				<position:content key="${position.id}" rows="9">
					<core:each var="item" items="${list}"  status="s">
					<a href="<url:link href='${item.url}'/>">
						<img src="<url:image src='${item.image.src}'/>" alt="${item.title}" />
					</a>
					</core:each>
				</position:content>
			</position:detail>
		</div>
	</div>
</div>

<script>
	(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,"script","//www.google-analytics.com/analytics.js","ga");

	ga("create", "UA-51090725-1", "lejujs.com");
	ga("require", "displayfeatures");
	ga("set", "contentGroup7", "Group Name");
	ga("send", "event", "button", "click", "nav buttons", 4);

	ga("send", "pageview");
</script>
