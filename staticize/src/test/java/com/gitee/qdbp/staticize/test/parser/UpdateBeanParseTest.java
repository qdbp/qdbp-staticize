package com.gitee.qdbp.staticize.test.parser;

import java.net.URL;
import com.gitee.qdbp.able.debug.ConsoleDebugger;
import com.gitee.qdbp.staticize.io.FileInputCreator;
import com.gitee.qdbp.staticize.io.IReaderCreator;
import com.gitee.qdbp.staticize.parse.TagParser;
import com.gitee.qdbp.staticize.tags.base.Taglib;
import com.gitee.qdbp.tools.files.PathTools;

public class UpdateBeanParseTest {

    public static void main(String[] args) throws Exception {
        URL path = PathTools.findClassResource(UpdateBeanParseTest.class, "../template/code/");
        String folder = PathTools.toUriPath(path);

        String in = "model.update.tpl";
        System.out.println(folder);
        IReaderCreator input = new FileInputCreator(folder);
        TagParser parser = new TagParser(Taglib.defaults(), input);
        TagParser.Options options = new TagParser.Options();
        options.setDebugger(new ConsoleDebugger());
        try {
            parser.parse(in, options);
        } catch (Exception e) {
            System.out.println("模板解析失败: " + in);
            e.printStackTrace();
        }
    }
}
