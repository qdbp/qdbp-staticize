typeis(user1.addresses[0].telphone,'string') = ${@typeis(user1.addresses[0].telphone,'string')}
typeis(user1.addresses[0].telphone,'undefined') = ${@typeis(user1.addresses[0].telphone,'undefined')}
typeis(user1.addresses[0].telphone,'null') = ${@typeis(user1.addresses[0].telphone,'null')}
typeis(user2.addresses[0].telphone,'string') = ${@typeis(user2.addresses[0].telphone,'string')}
typeis(user2.addresses[0].telphone,'undefined') = ${@typeis(user2.addresses[0].telphone,'undefined')}
typeis(user2.addresses[0].telphone,'null') = ${@typeis(user2.addresses[0].telphone,'null')}
typeis(user3.addresses[0].telphone,'string') = ${@typeis(user3.addresses[0].telphone,'string')}
typeis(user3.addresses[0].telphone,'undefined') = ${@typeis(user3.addresses[0].telphone,'undefined')}
typeis(user3.addresses[0].telphone,'null') = ${@typeis(user3.addresses[0].telphone,'null')}

typeis(user1.height,'int') = ${@typeis(user1.height,'int')}
typeis(user1.height,'integer') = ${@typeis(user1.height,'integer')}

typeis(user1.birthday,'date') = ${@typeis(user1.birthday,'date')}
typeis(user1.birthday,'Date') = ${@typeis(user1.birthday,'Date')}

isBlank(user1.addresses[0].telphone) = ${@isBlank(user1.addresses[0].telphone)}
isBlank(user1.addresses[0]) = ${@isBlank(user1.addresses[0])}
isBlank(user1.addresses) = ${@isBlank(user1.addresses)}

isBlank(user2.addresses[0].telphone) = ${@isBlank(user2.addresses[0].telphone)}
isBlank(user2.addresses[0]) = ${@isBlank(user2.addresses[0])}
isBlank(user2.addresses) = ${@isBlank(user2.addresses)}

isBlank(user3.addresses[0].telphone) = ${@isBlank(user3.addresses[0].telphone)}
isBlank(user4.addresses) = ${@isBlank(user4.addresses)}

isNotBlank(user1.addresses[0].telphone) = ${@isNotBlank(user1.addresses[0].telphone)}
isNotBlank(user1.addresses[0]) = ${@isNotBlank(user1.addresses[0])}
isNotBlank(user1.addresses) = ${@isNotBlank(user1.addresses)}

isNotBlank(user2.addresses[0].telphone) = ${@isNotBlank(user2.addresses[0].telphone)}
isNotBlank(user2.addresses[0]) = ${@isNotBlank(user2.addresses[0])}
isNotBlank(user2.addresses) = ${@isNotBlank(user2.addresses)}

isNotBlank(user3.addresses[0].telphone) = ${@isNotBlank(user3.addresses[0].telphone)}
isNotBlank(user4.addresses) = ${@isNotBlank(user4.addresses)}

length(user1.addresses[0].telphone) = ${@length(user1.addresses[0].telphone)}
length(user1.addresses[0]) = ${@length(user1.addresses[0])}
length(user1.addresses) = ${@length(user1.addresses)}

length(user2.addresses[0].telphone) = ${@length(user2.addresses[0].telphone)}
length(user2.addresses[0]) = ${@length(user2.addresses[0])}
length(user2.addresses) = ${@length(user2.addresses)}

length(user3.addresses[0].telphone) = ${@length(user3.addresses[0].telphone)}
length(user4.addresses) = ${@length(user4.addresses)}

<core:if test="@contains(user1.addresses[0].telphone)">
Contains 'user1.addresses[0].telphone'
</core:if>
<core:if test="@contains(user1.addresses[0])">
Contains 'user1.addresses[0]'
</core:if>
<core:if test="@contains(user1.addresses)">
Contains 'user1.addresses'
</core:if>

<core:if test="@contains(user2.addresses[0].telphone)">
Contains 'user1.addresses[0].telphone'
</core:if>
<core:else>
Not contains 'user2.addresses[0].telphone'
</core:else>
<core:if test="@contains(user2.addresses[0])">
Contains 'user1.addresses[0]'
</core:if>
<core:else>
Not contains 'user2.addresses[0]'
</core:else>
<core:if test="@contains(user2.addresses)">
Contains 'user1.addresses'
</core:if>
<core:else>
Not contains 'user2.addresses'
</core:else>

<core:if test="@contains(user1.addresses) and user1.addresses.size() > 0">
user1.addresses:
<core:each var="item" items="user1.addresses">
${item.type}: ${item.telphone}
</core:each>
</core:if>

<core:if test="@contains(user2.addresses) and user2.addresses.size() > 0">
user2.addresses:
<core:each var="item" items="user2.addresses">
${item.type}: ${item.telphone}
</core:each>
</core:if>

<core:if test="!@contains(user1.addresses) or user1.addresses.size() == 0">
user1.addresses.size: 0
</core:if>
<core:else>
user1.addresses.size: ${user1.addresses.size()}
</core:else>

<core:if test="!@contains(user2.addresses) or user2.addresses.size() == 0">
user2.addresses.size: 0
</core:if>
<core:else>
user2.addresses.size: ${user2.addresses.size()}
</core:else>
