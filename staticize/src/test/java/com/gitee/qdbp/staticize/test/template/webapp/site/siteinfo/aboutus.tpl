<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="column" uri="http://www.jeshing.com/tags/column/"%>
<%@ taglib prefix="content" uri="http://www.jeshing.com/tags/content/"%>
<%@ taglib prefix="position" uri="http://www.jeshing.com/tags/position/"%>
<%@ taglib prefix="util" uri="http://www.jeshing.com/tags/util/"%>
<%@ taglib prefix="url" uri="http://www.jeshing.com/tags/url/"%>
<%@ taglib prefix="imgs" uri="http://www.jeshing.com/tags/images/"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/base/include/meta.tpl"%>
	<meta name="keywords" content="关于我们" />
	<meta name="description" content="关于我们" />
	<title>关于我们 - 乐居中国</title>
<%@ include file="/base/include/head.tpl"%>
	<link rel="stylesheet" type="text/css" href="<url:link href='resources/site/css/siteinfo.aboutus.css'/>">
</head>
<body>

	<%@ include file="/base/include/top.tpl"%>

	<%--页面主体--%>
	<div class="banner"></div>

	<div class="centern">
		<%@ include file="_banner.tpl"%>
		<br class="clear" />
	</div>

	<div class="centern">
		<div class="about_left">
			<%@ include file="_menu.tpl"%>
		</div>

		<div class="about_right">
		<div class="ab_title">
			<img src="<url:link href='resources/site/images/siteinfo/mtu.png'/>"/>
		关于我们<span>About us</span>
		</div>
		<div class="line"></div>
		<div class="ab_content ab_content-ch">
		 <div class="ab_content-ch-p">
			<p>2010年7月，太平洋家居网PChouse正式上线。凭借强大的资讯优势，依托太平洋网站群的整合资源厚积薄发，以提升读者消费品质为目标，旨在建立一个能囊括全面丰富家居资讯的专业网络平台。</p>
 		</div>
 		 <div class="ab_content-ch-p">
			<p>截至目前，PChouse 已完成了第一阶段的建设，重点推出了评测、搭配、导购、论坛、快问、图库、行业等栏目，并将接继推出指导产品导购的"产品库"频道，期望能为家居消费者提供最专业、全面、先进和优化的家居构筑方案。</p>
		</div>
 		 <div class="ab_content-ch-p">
			<p>PChouse有资深的媒体背景、优秀的团队组织，坚持原创风格，专注于家居、家装的专业解读，资讯、导购、导用、互动为出发点，谋求行业技术角度和用户适用体验的高度结合，以评测点评、搭配导购、设计新知、特色产品、行业新闻及互动活动等用户感兴趣的内容为主导，力求为广大装修装饰用户和家居生活爱好者全面营造集建材、设计、家电、家具、家品于一体的家居氛围。</p>

		</div>
 		 <div class="ab_content-ch-p">
			<p>PConline凭借完善的设施、强大的技术力量与训练有素的信息采集队伍，充分利用太平洋电脑市场丰富的资源优势，迅速、及时地提供专业产品评测、权威市场报价、最新业界动态以及全面的企业信息，并建立了国内权威的IT产品媒体评测室以及最全面的IT企业资料库、产品库以及人才供应库，在IT企业与终端用户之间构建起一座互动共赢的大型信息交互平台。</p>
		</div>
 		 <div class="ab_content-ch-p">
			<p>Alexa权威认证PConline是唯一曾进入世界100强的IT专业门户网站，访问量突破两千万，同时根据权威市场调研机构AC尼尔森分析报告，证实PConline遥遥领先于其他竞争对手，是名副其实的中国IT第一站。</p>
		</div>
 		 <div class="ab_content-ch-p">
			<p>近年来的飞速发展，PConline协同下设的近50个专业频道，内容丰富全面、权威专业，涉及IT行业的方方面面，已成为国内IT市场的晴雨表和IT厂商推广产品、展示形象的重要渠道。</p>
		</div>


		</div>

	</div>
	<br class="clear"/>

	<%@ include file="/base/include/foot.tpl"%>
</body>
</html>

