package com.gitee.qdbp.staticize.tags.core;

/**
 * SetVariable标签处理类<br>
 * &lt;core:each var="userBean" items="${userList}"&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;core:set var="temp.userName" value="${userBean.displayName}" /&gt;<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;${temp.userName}<br>
 * &lt;/core:each&gt;<br>
 * scope=parent/root/preset; 默认为parent;<br>
 * 如果指定scope=preset, 则可通过#{temp.userName}访问<br>
 * 如果指定了value, 则不允许有标签体<br>
 * -- &lt;core:set var="tempName" value="${userBean.displayName}" /&gt;<br>
 * -- ${tempName}<br>
 * 如果没有指定value, 则标签体内容就是value, 此时标签体内容必须是一个表达式<br>
 * -- &lt;core:set var="tempName"&gt;${userBean.displayName}&lt;/core:set&gt;<br>
 * 
 * @author zhaohuihua
 * @version 20200528
 */
public class SetVariableTag extends SetVariableBase {

    /** 保存变量值的变量名 **/
    public String getVar() {
        return super.getVar();
    }

    /** 保存变量值的变量名 **/
    public void setVar(String var) {
        super.setVar(var);
    }

    /** 获取变量值的表达式 **/
    public Object getValue() {
        return super.getValue();
    }

    /** 获取变量值的表达式 **/
    public void setValue(Object value) {
        super.setValue(value);
    }

    /** 保存变量值的范围(parent/root/preset) **/
    public String getScope() {
        return super.getScope();
    }

    /** 保存变量值的范围(parent/root/preset) **/
    public void setScope(String scope) {
        super.setScope(scope);
    }
}
