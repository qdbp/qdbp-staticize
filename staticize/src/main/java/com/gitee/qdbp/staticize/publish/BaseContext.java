package com.gitee.qdbp.staticize.publish;

import java.io.IOException;
import java.util.Map;
import com.gitee.qdbp.able.beans.KeyString;
import com.gitee.qdbp.able.model.reusable.ExpressionContext;
import com.gitee.qdbp.able.model.reusable.ExpressionMap;
import com.gitee.qdbp.staticize.common.IContext;
import com.gitee.qdbp.staticize.common.IWriter;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.Taglib;

/**
 * 基础上下文处理类
 *
 * @author zhaohuihua
 * @version 140523
 */
public abstract class BaseContext extends ExpressionContext implements IContext, Taglib.Aware {

    /** 标签库 **/
    private Taglib taglib;
    /** 值栈环境变量, 通过${xxx}取值, 用于实现子标签的变量在子标签关闭后不覆盖父标签的变量 **/
    private final ExpressionMap stack = new ExpressionMap();
    /** 存储接口 **/
    private IWriter writer;

    /** 内部构造函数 **/
    protected BaseContext(IWriter writer) {
        this.writer = writer;
    }

    @Override
    public Taglib getTaglib() {
        return this.taglib;
    }

    @Override
    public void setTaglib(Taglib taglib) {
        this.taglib = taglib;
        // 获取全局引用静态类名
        addImportClasses(taglib.getGlobalImports());
        // 解析值栈函数
        // 如果方法的参数是字符串, 而表达式没有写引号, 则需要自动追加引号
        // 如将@xxx(user.name)替换为ThisStack.xxx('user.name')
        setStackWrapperConstructor(taglib.getStackWrapperConstructor());
        addStackFunctions(taglib.getStackFunctions());
    }

    /**
     * 获取值栈环境变量容器, 通过${xxx}取值
     *
     * @return 值栈环境变量容器
     */
    @Override
    public Map<String, Object> stack() {
        return stack;
    }

    /**
     * 根据表达式从环境变量中获取值
     *
     * @param prefix 表达式前缀类型
     * @param expression 表达式
     * @return 环境变量中的对象
     */
    protected abstract Object doGetValue(String prefix, String expression) throws TagException;

    /**
     * 根据表达式从环境变量中获取值
     *
     * @param expression 表达式
     * @return 环境变量中的对象
     */
    @Override
    public final Object getValue(String expression) throws TagException {
        if (expression == null || expression.trim().length() == 0) {
            throw new TagException("The expression cannot be empty.");
        }

        KeyString result = parseExpression(expression);

        String prefix = result.getKey();
        String content = result.getValue();
        content = replaceCustomizedFunctions(content);

        return this.doGetValue(prefix, content);
    }

    private KeyString parseExpression(String expression) {
        String v = expression.trim();
        if (v.length() < 4) {
            // ${x} 最短也要有4个字符
            throw new TagException("An error expression.");
        }
        char p = v.charAt(0);
        if ((p == '$' || p == '#') && v.charAt(1) == '{' && v.charAt(v.length() - 1) == '}') {
            return new KeyString(String.valueOf(p), v.substring(2, v.length() - 1).trim());
        } else {
            throw new TagException("An error expression.");
        }
    }

    @Override
    public void write(Object content) throws IOException {
        writer.write(content);
    }

    public IWriter getWriter() {
        return this.writer;
    }

    @Override
    public IWriter resetWriter(IWriter writer) {
        IWriter origin = this.writer;
        this.writer = writer;
        return origin;
    }
}
