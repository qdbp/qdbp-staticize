package com.gitee.qdbp.staticize.tags.base;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gitee.qdbp.able.beans.KeyString;
import com.gitee.qdbp.able.model.reusable.ExpressionContext;
import com.gitee.qdbp.able.model.reusable.StackWrapper;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.tools.files.PathTools;
import com.gitee.qdbp.tools.utils.Config;
import com.gitee.qdbp.tools.utils.StringTools;

/**
 * 标签库
 *
 * @author zhaohuihua
 * @version 140520
 */
public class Taglib {

    /** 日志对象 **/
    private static final Logger log = LoggerFactory.getLogger(Taglib.class);

    /** 默认配置文件路径 **/
    private static final String DEF_PATH = "classpath:settings/tags/taglib.txt";
    /** JSTL风格配置文件路径 **/
    private static final String JSTL_PATH = "classpath:settings/tags/jstl.txt";

    /** 全局默认实例 **/
    private static Taglib DEF_INSTANCE;
    /** 全局JSTL风格实例 **/
    private static Taglib JSTL_INSTANCE;

    /** 获取全局默认实例 **/
    public static Taglib defaults() {
        if (DEF_INSTANCE == null) {
            DEF_INSTANCE = DefInstance.INSTANCE;
        }
        return DEF_INSTANCE;
    }

    /** 获取JSTL风格实例 **/
    public static Taglib jstl() {
        if (JSTL_INSTANCE == null) {
            JSTL_INSTANCE = JstlInstance.INSTANCE;
        }
        return JSTL_INSTANCE;
    }

    /**
     * 静态内部类单例模式, 同时解决延迟加载和并发问题(缺点是无法传参)<br>
     * 加载外部类时, 不会加载内部类, 也就不会创建实例对象;<br>
     * 只有DEF_INSTANCE==null调用DefInstance.INSTANCE时才会加载静态内部类;<br>
     * 加载类是线程安全的, 虚拟机保证只会装载一次内部类, 不会出现并发问题<br>
     *
     * @author zhaohuihua
     * @version 20200129
     */
    private static class DefInstance {

        public static final Taglib INSTANCE = new Taglib(DEF_PATH);
    }

    private static class JstlInstance {

        public static final Taglib INSTANCE = new Taglib(JSTL_PATH);
    }

    public interface Aware {

        void setTaglib(Taglib taglib);
    }

    public interface Creator {

        Taglib create();
    }

    /** 标签库容器 **/
    private final Map<String, Class<? extends ITag>> taglib = new HashMap<>();
    /** 全局导入的静态类 **/
    private final Set<String> globalImports = new HashSet<>();
    /** 值栈函数 **/
    private final Set<Method> stackFunctions = new HashSet<>();
    /** 值栈包装类的类型 **/
    private Class<? extends StackWrapper> stackWrapperClass;
    /** 值栈包装类的构造函数 **/
    private Constructor<? extends StackWrapper> stackWrapperConstructor;

    /** 构造函数 **/
    public Taglib() {
        this(DEF_PATH);
    }

    /** 构造函数 **/
    public Taglib(String path) {
        // 读取配置项
        loadConfig(path);
    }

    /** 构造函数 **/
    public Taglib(List<KeyString> items) {
        loadConfig(items);
    }

    /**
     * 加载配置文件
     * 
     * @param path 配置文件路径
     */
    protected void loadConfig(String path) {
        URL url = PathTools.findResource(path, Taglib.class);
        Config config = new Config(url);
        // 生成标签与处理类的映射关系
        loadConfig(config.entries());
        String msg = "Success to load taglib, {} tags, {} global imports and {} stack functions were found in {}";
        log.info(msg, taglib.size(), globalImports.size(), stackFunctions.size(), url.toString());
    }

    /**
     * 加载配置信息
     * 
     * @param items 配置项, key=tagName, value=className
     */
    protected void loadConfig(List<KeyString> items) {
        for (KeyString item : items) {
            String key = item.getKey();
            if ("@StackWrapper".equals(key)) {
                // 指定了值栈本身的包装类
                // @StackWrapper=com.gitee.qdbp.staticize.tags.base.StackWrapper
                loadStackWrapperClass(key, item.getValue());
            } else if (key.charAt(0) == '@') {
                loadGlobalImports(key, item.getValue());
            } else {
                setTag(key, item.getValue());
            }
        }
        if (this.stackWrapperClass != null) {
            loadGlobalFunctions(stackWrapperClass);
        } else {
            loadGlobalFunctions(StackWrapper.class);
        }
    }

    protected void loadGlobalImports(String key, String value) {
        String newkey = key.substring(1);
        String className = StringTools.removePrefixAt(value, '.');
        if (!newkey.equals(className)) {
            String fmt = "Global import config format error, key[%s] not equals class name[%s]";
            throw new IllegalArgumentException(String.format(fmt, key, className));
        }
        this.globalImports.add(value);
    }

    protected void loadStackWrapperClass(String key, String value) {
        try {
            stackWrapperClass = ExpressionContext.parseStackWrapperClass(value);
            if (stackWrapperClass != null) {
                stackWrapperConstructor = ExpressionContext.parseStackWrapperConstructor(stackWrapperClass);
            }
        } catch (IllegalArgumentException e) {
            log.warn("Key='{}', {}", key, e.getMessage());
        }
    }

    /** 从StackWrapper对象上获取有返回值的方法, 作为全局函数 **/
    protected void loadGlobalFunctions(Class<? extends StackWrapper> clazz) {
        Set<Method> functions = ExpressionContext.parseStackFunctions(clazz);
        if (!functions.isEmpty()) {
            this.stackFunctions.addAll(functions);
        }
    }

    /**
     * 设置标签处理类
     *
     * @param tagName 标签名
     * @param className 标签处理类
     * @return 标签处理类
     */
    @SuppressWarnings("unchecked")
    public Class<? extends ITag> setTag(String tagName, String className) {
        Class<?> clazz;
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            log.error("Tag class not found: " + className);
            return null;
        }

        if (clazz.isAssignableFrom(ITag.class)) {
            log.error("The class is not assignable from ITag: " + className);
            return null;
        }

        return setTag(tagName, (Class<ITag>) clazz);
    }

    /**
     * 设置标签处理类
     *
     * @param name 标签名
     * @param clazz 标签处理类
     * @return 标签处理类
     */
    public Class<? extends ITag> setTag(String name, Class<? extends ITag> clazz) {
        return taglib.put(name, clazz);
    }

    /**
     * 获取标签处理类
     *
     * @param name 标签名
     * @return 标签处理类
     */
    public Class<? extends ITag> getTag(String name) {
        return taglib.get(name);
    }

    /**
     * 判断标签是否存在
     *
     * @param name 标签名
     * @return 标签是否存在
     */
    public boolean containsTag(String name) {
        return taglib.containsKey(name);
    }

    /** 获取全局静态类 **/
    public List<String> getGlobalImports() {
        return new ArrayList<>(globalImports);
    }

    /** 增加全局静态类 **/
    public void addGlobalImports(String... globalImports) {
        if (globalImports != null) {
            this.globalImports.addAll(Arrays.asList(globalImports));
        }
    }

    /** 值栈函数 **/
    public Set<Method> getStackFunctions() {
        return stackFunctions;
    }

    /** 值栈包装类的构造函数 **/
    public Constructor<? extends StackWrapper> getStackWrapperConstructor() {
        return stackWrapperConstructor;
    }

    private TagFactory defaultTagFactory;
    private final Map<Class<? extends ITag>, TagFactory> tagFactories = new HashMap<>();

    /** 获取默认的标签实例化工厂类 **/
    public TagFactory getDefaultTagFactory() {
        return defaultTagFactory;
    }

    /** 设置默认的标签实例化工厂类 **/
    public void setDefaultTagFactory(TagFactory defaultTagFactory) {
        this.defaultTagFactory = defaultTagFactory;
    }

    /** 获取指定标签的实例化工厂类 **/
    public TagFactory getTagFactory(Class<? extends ITag> clazz) {
        return tagFactories.get(clazz);
    }

    public <T extends ITag> void putTagFactory(Class<T> clazz, TagFactory factory) {
        tagFactories.put(clazz, factory);
    }

    public <T extends ITag> T newTagInstance(Class<T> clazz) {
        TagFactory factory;
        if (tagFactories.containsKey(clazz)) {
            factory = getTagFactory(clazz);
        } else {
            factory = defaultTagFactory;
        }

        if (factory != null) {
            try {
                return factory.newTagInstance(clazz);
            } catch (Exception e) {
                String fmt = "Create tag instance error: %s for %s.newTagInstance()";
                String msg = String.format(fmt, clazz.getName(), factory.getClass().getName());
                throw new TagException(msg, e);
            }
        } else {
            try {
                return clazz.newInstance();
            } catch (Exception e) {
                // 基本上走不到这里, 除非处理类的构造函数报错
                throw new TagException("Create tag instance error: " + clazz.getName(), e);
            }
        }
    }
}
