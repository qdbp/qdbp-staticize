package com.gitee.qdbp.staticize.tags.base;

import java.io.IOException;
import com.gitee.qdbp.staticize.common.IContext;
import com.gitee.qdbp.staticize.common.IWriter;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.tools.utils.VerifyTools;

/**
 * 带子标签内容缓存的标签处理类<br>
 * 某些标签需要根据子标签内容决定处理方式, 这就需要缓存所有子标签的输出
 * 
 * @author zhaohuihua
 * @version 20200906
 */
public abstract class CachingTag<T extends IWriter> extends BaseTag {

    private final T caching;
    private IWriter origin;
    private boolean enabled;

    public CachingTag(T caching) {
        this.caching = caching;
        this.enabled = true;
    }

    /**
     * 设置基础数据
     *
     * @param parent 父标签
     * @param context 环境变量
     */
    @Override
    public void doSetBaseData(ITag parent, IContext context) {
        super.doSetBaseData(parent, context);
        if (enabled) {
            this.origin = resetWriter(caching);
        }
    }

    /** 是否使用缓存 **/
    protected boolean doIsEnabled() {
        return enabled;
    }

    /** 是否使用缓存 **/
    protected void doSetEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * 向缓存处理类写入内容
     *
     * @param content 内容
     */
    @Override
    protected void print(Object content) throws IOException {
        if (!enabled) {
            super.print(content);
        } else {
            if (VerifyTools.isNotBlank(content)) {
                caching.write(content);
            }
        }
    }

    /**
     * 向目标Writer写入内容
     *
     * @param content 内容
     */
    protected void printToOrigin(Object content) throws IOException {
        if (VerifyTools.isNotBlank(content)) {
            origin.write(content);
        }
    }

    @Override
    public final void doEnded(NextStep next) throws TagException, IOException {
        if (!enabled) {
            return;
        }
        if (next != NextStep.SKIP_BODY) {
            doEnded(caching, origin);
        }
        if (next != NextStep.LOOP_BODY) {
            this.resetWriter(this.origin);
        }
    }

    /**
     * 标签结束的处理, 一般是将缓冲区内容写入原IWriter(origin)
     * 
     * @param caching 缓冲Writer
     * @param origin 原Writer
     * @throws TagException 标签处理异常
     * @throws IOException 输出处理异常
     */
    protected abstract void doEnded(T caching, IWriter origin) throws TagException, IOException;
}
