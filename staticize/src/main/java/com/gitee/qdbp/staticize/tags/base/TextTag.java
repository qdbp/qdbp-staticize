package com.gitee.qdbp.staticize.tags.base;

import java.io.IOException;
import com.gitee.qdbp.staticize.annotation.TryEvalString;

/**
 * 文本标签处理类
 * 
 * @author zhaohuihua
 * @version 140520
 */
public class TextTag extends BaseTag {

    private String value;

    @TryEvalString(enabled = false)
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public NextStep doHandle() throws IOException {
        if (value != null) {
            print(value);
        }
        return NextStep.EVAL_BODY;
    }
}
