package com.gitee.qdbp.staticize.common;

import java.io.Closeable;
import java.io.IOException;

/**
 * 存储接口
 *
 * @author zhaohuihua
 * @version 20200815
 */
public interface IWriter extends Closeable {

    /** 写入内容 **/
    void write(Object value) throws IOException;

    /** 清空缓冲区, 将缓冲区中的内容写入输出流 **/
    void flush() throws IOException;

    /** 关闭存储类, 释放资源 **/
    @Override
    void close() throws IOException;
}
