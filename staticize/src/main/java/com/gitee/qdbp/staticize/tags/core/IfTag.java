package com.gitee.qdbp.staticize.tags.core;

import com.gitee.qdbp.staticize.annotation.AttrRequired;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.ITag;
import com.gitee.qdbp.staticize.tags.base.NextStep;

/**
 * if 标签处理类
 * 
 * @author zhaohuihua
 * @version 140521
 */
public class IfTag extends BaseTag {

    /** 判断并列的IF/ELSEIF标签有没有被执行过 **/
    static final String IF_TAG_EVAL_DONE = "IF_TAG_EVAL_DONE";

    /** 测试值 **/
    protected Boolean test;

    /**
     * 设置测试值
     * 
     * @param test 测试值
     */
    @AttrRequired
    public void setTest(Boolean test) {
        this.test = test != null && test;
    }

    /** {@inheritDoc} **/
    public NextStep doHandle() throws TagException {
        ITag parent = this.parent();
        parent.stack().put(IF_TAG_EVAL_DONE, test);

        if (test != null && test) {
            return NextStep.EVAL_BODY;
        } else {
            return NextStep.SKIP_BODY;
        }
    }
}
