package com.gitee.qdbp.staticize.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import com.gitee.qdbp.staticize.tags.base.ITag;

/**
 * 紧邻于其他标签的标记<br>
 * 用于类似else和elseif的标签, if与else之间不允许有除空白字符以外的内容, 而且标签之间的空白字符将被舍弃
 *
 * @author zhaohuihua
 * @version 20200926
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AdjoinAfter {

    /** 允许紧邻于哪些标签 **/
    Class<? extends ITag>[] value();
}
