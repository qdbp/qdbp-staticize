package com.gitee.qdbp.staticize.parse;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import com.gitee.qdbp.staticize.common.IMetaData;
import com.gitee.qdbp.staticize.common.NodeMetaData;
import com.gitee.qdbp.staticize.tags.base.ITag;
import com.gitee.qdbp.staticize.tags.base.Taglib;

class RootMetaDataImpl extends NodeMetaDataImpl implements IMetaData {

    private Taglib taglib;

    RootMetaDataImpl(Class<? extends ITag> clazz, String name, String tpl, String path, int row, int column) {
        super(clazz, name, tpl, path, row, column);
    }

    RootMetaDataImpl(String tplId, String realPath, IMetaData root, NodeMetaData node, List<NodeMetaData> commonNodes,
            Map<String, Object> attrs) {
        super(root.getTagClass(), root.getName(), tplId, realPath, node.getRow(), node.getColumn());
        ((NodeMetaDataImpl) node).resetTplIdAndPath(tplId, realPath);
        this.taglib = root.getTaglib();
        if (commonNodes != null) {
            for (NodeMetaData item : commonNodes) {
                this.appendChild((NodeMetaDataImpl) item);
            }
        }
        this.appendChild((NodeMetaDataImpl) node);
        if (attrs != null) {
            for (Entry<String, Object> item : attrs.entrySet()) {
                this.addAttribute(item.getKey(), item.getValue());
            }
        }
    }

    public void setTaglib(Taglib taglib) {
        this.taglib = taglib;
    }

    @Override
    public Taglib getTaglib() {
        return taglib;
    }

    /** 将指定节点包装为根节点 **/
    @Override
    public IMetaData wrapAsRoot(String tplId, String realPath, NodeMetaData node, List<NodeMetaData> commonNodes,
            Map<String, Object> attrs) {
        return new RootMetaDataImpl(tplId, realPath, this, node, commonNodes, attrs);
    }

    @Override
    public String toString() {
        return this.path;
    }
}
