package com.gitee.qdbp.staticize.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * String类型的属性值要不要尝试作为表达式解析<br>
 * 设置在Setter方法上面
 *
 * @author zhaohuihua
 * @version 20210404
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TryEvalString {

    boolean enabled() default true;
}
