package com.gitee.qdbp.staticize.tags.core;

import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.NextStep;

/**
 * 注释标签<br>
 * &lt;core:comment&gt;注释内容&lt;/core:comment&gt;
 * 
 * @author zhaohuihua
 * @version 170213
 */
public class CommentTag extends BaseTag {

    public NextStep doHandle() throws TagException {
        return NextStep.SKIP_BODY;
    }
}
