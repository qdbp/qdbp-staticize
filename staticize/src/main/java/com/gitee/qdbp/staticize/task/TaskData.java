package com.gitee.qdbp.staticize.task;

import java.util.Map;

/**
 * 任务数据
 * 
 * @author zhaohuihua
 * @version 140722
 */
class TaskData {

    /** 任务名称 **/
    private final String name;

    /** 预置数据 **/
    private Map<String, Object> preset;

    /** 模板路径(相对路径) **/
    private String template;

    /** 输出路径(相对路径) **/
    private String output;

    /**
     * 任务数据构造函数
     * 
     * @param name 任务名称
     */
    public TaskData(String name) {
        this.name = name;
    }

    /**
     * 获取任务名称
     * 
     * @return 任务名称
     */
    public String getName() {
        return name;
    }

    /**
     * 取得预置数据
     * 
     * @return 返回预置数据
     */
    public Map<String, Object> getPreset() {
        return preset;
    }

    /**
     * 设置预置数据
     * 
     * @param preset 要设置的预置数据
     */
    public void setPreset(Map<String, Object> preset) {
        this.preset = preset;
    }

    /**
     * 取得模板路径(是相对路径)
     * 
     * @return 返回模板路径
     */
    public String getTemplate() {
        return template;
    }

    /**
     * 设置模板路径(相对路径)
     * 
     * @param template 要设置的模板路径
     */
    public void setTemplate(String template) {
        this.template = template;
    }

    /**
     * 取得输出路径(相对路径)
     * 
     * @return 返回输出路径(相对路径)
     */
    public String getOutput() {
        return output;
    }

    /**
     * 设置输出路径(相对路径)
     * 
     * @param output 要设置的输出路径(相对路径)
     */
    public void setOutput(String output) {
        this.output = output;
    }
}
