package com.gitee.qdbp.staticize.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.gitee.qdbp.able.beans.Copyable;
import com.gitee.qdbp.staticize.utils.OgnlUtils;
import com.gitee.qdbp.tools.utils.ConvertTools;
import com.gitee.qdbp.tools.utils.JsonTools;
import com.gitee.qdbp.tools.utils.ReflectTools;
import com.gitee.qdbp.tools.utils.StringTools;
import com.gitee.qdbp.tools.utils.VerifyTools;

/**
 * 层叠形的Map, 关键字可以包含多级, 如site.name<br>
 * 可用于Ognl取值<br>
 * a.b.c, a.b.c.d 不能共存, 后设置的覆盖先设置的
 *
 * <pre>
 * CascadeMap map = new CascadeMap();
 * map.put("author", "xxx"); // 该设置无效, 会被后面的覆盖掉
 * map.put("author.code", 100);
 * map.put("author.name", "zhaohuihua");
 *
 * map.put("code.folder.biz", "service");
 * map.put("code.folder.page", "views");
 * // map.put("code.folder", "java"); // 该设置会覆盖code.folder.biz和code.folder.page
 *
 * OgnlTools.getValue(map, "author"); = map.get("author"); -- Map({code=100, name=zhaohuihua})
 * OgnlTools.getValue(map, "author.code"); = map.get("author.code");  -- 100
 * OgnlTools.getValue(map, "author.name"); = map.get("author.name"); -- zhaohuihua
 *
 * OgnlTools.getValue(map, "code.folder"); = map.get("code.folder"); -- Map({service=service, page=views})
 * OgnlTools.getValue(map, "code.folder.biz"); = map.get("code.folder.biz"); -- service
 * OgnlTools.getValue(map, "code.folder.page"); = map.get("code.folder.biz"); -- views
 * OgnlTools.getValue(map, "code.folder.xxx"); -- null
 * map.get("code.folder.xxx"); -- null
 * </pre>
 *
 * @author zhaohuihua
 * @version 140729
 */
public class CascadeMap implements Map<String, Object>, Cloneable, Copyable, Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    private final Map<String, Object> map = new HashMap<>();

    public CascadeMap() {
    }

    public CascadeMap(Map<? extends String, ?> map) {
        putAll(map);
    }

    /**
     * 两个CascadeMap相加
     *
     * @author zhaohuihua
     * @param map 另一个CascadeMap
     */
    @Override
    public void putAll(Map<? extends String, ?> map) {
        if (map != null) {
            union(this.map, map);
        }
    }

    /**
     * 将两个Map组合在一起
     *
     * @author zhaohuihua
     * @param src 第一个Map
     * @param other 另一个Map
     */
    @SuppressWarnings("unchecked")
    private static void union(Map<String, Object> src, Map<? extends String, ?> other) {
        for (Entry<? extends String, ?> o : other.entrySet()) {
            Object sv = src.get(o.getKey());
            Object ov = o.getValue();
            if (sv == null) {
                if (ov instanceof Map) {
                    Map<String, Object> map = new HashMap<>();
                    src.put(o.getKey(), map);
                    union(map, (Map<? extends String, ?>) ov);
                } else {
                    src.put(o.getKey(), ov);
                }
            } else if (sv instanceof Map && ov instanceof Map) {
                union((Map<String, Object>) sv, (Map<? extends String, ?>) ov);
            } else {
                src.put(o.getKey(), ov);
            }
        }
    }

    /**
     * 向容器中增加值 (值不会转换成map)
     *
     * @author zhaohuihua
     * @param key 关键字, 不允许包含.点号
     * @param value 变量值
     * @return 原变量值
     */
    public Object set(String key, Object value) {
        if (key.indexOf('.') >= 0) {
            throw new IllegalArgumentException("key can't support dot.");
        }
        return map.put(key, value);
    }

    /**
     * 向容器中增加值 (值将会转换为map)
     *
     * @author zhaohuihua
     * @param key 关键字, 可以包含多级, 如site.name
     * @param value 变量值
     * @return 原变量值
     */
    @Override
    @SuppressWarnings("unchecked")
    public Object put(String key, Object value) {
        if (key == null) {
            throw new NullPointerException("key is null");
        }

        String[] array = StringTools.split(key, '.');
        int length = array.length;
        Map<String, Object> curr = map;
        for (int i = 0; i < length; i++) {
            String string = array[i];

            if (i == length - 1) {
                return curr.put(string, valueToObject(value));
            }

            Object object = curr.get(string);
            if (object == null) {
                Map<String, Object> temp = new HashMap<>();
                curr.put(string, temp);
                curr = temp;
                continue;
            }

            if (object instanceof Map) {
                curr = (Map<String, Object>) object;
            } else {
                // String fmt = "%s type is %s, can't append node.";
                // String type = object.getClass().getSimpleName();
                // String msg = String.format(fmt, buffer, type);
                // throw new IllegalArgumentException(msg);
                // 覆盖之前的设置
                Map<String, Object> temp = new HashMap<>();
                curr.put(string, temp);
                curr = temp;
                continue;
            }
        }
        return null;
    }

    private Object valueToObject(Object value) {
        if (value == null || ReflectTools.isPrimitive(value.getClass(), false)) {
            return value;
        } else if (VerifyTools.isCollection(value)) {
            return valueToList(ConvertTools.parseList(value));
        } else {
            return JsonTools.beanToMap(value);
        }
    }

    private List<Object> valueToList(List<Object> values) {
        List<Object> result = new ArrayList<>();
        for (Object object : values) {
            result.add(valueToObject(object));
        }
        return result;
    }

    /**
     * KEY是否存在对应的值
     *
     * @author zhaohuihua
     * @param key 关键字, 可以包含多级, 如site.name
     * @return 是否存在
     */
    @Override
    public boolean containsKey(Object key) {
        if (!(key instanceof String)) {
            throw new IllegalArgumentException("key must be string type.");
        }
        String[] array = StringTools.split((String) key, '.');
        int length = array.length;
        Map<?, ?> curr = map;
        for (int i = 0; i < length; i++) {
            String string = array[i];

            if (i == length - 1) {
                return curr.containsKey(string);
            }

            Object object = curr.get(string);
            if (object instanceof Map) {
                curr = (Map<?, ?>) object;
            } else {
                // 不是最后一级, 下一级却不是Map, 直接返回false
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    public String getString(Object key) {
        Object value = get(key);
        if (value instanceof String) {
            return (String) value;
        } else {
            return null;
        }
    }

    /**
     * 获取对象并转换类型
     *
     * @author zhaohuihua
     * @param key 关键字, 可以包含多级, 如site.name
     * @param clazz 目标类型
     * @return 变量值, 如果取值错误返回null
     */
    public <T> T get(String key, Class<T> clazz) {
        return JsonTools.convert(get(key), clazz);
    }

    @Override
    public Object get(Object key) {
        if (!(key instanceof String)) {
            throw new IllegalArgumentException("key must be string type.");
        }
        String string = (String) key;
        if (string.indexOf('.') > 0) {
            return OgnlUtils.getValue(map, string);
        } else {
            return map.get(string);
        }
    }

    /**
     * 删除KEY对应的值
     *
     * @author zhaohuihua
     * @param key 关键字, 可以包含多级, 如site.name
     * @return 原变量值
     */
    @Override
    public Object remove(Object key) {
        if (!(key instanceof String)) {
            throw new IllegalArgumentException("key must be string type.");
        }
        String[] array = StringTools.split((String) key, '.');
        int length = array.length;
        Map<?, ?> curr = map;
        for (int i = 0; i < length; i++) {
            String string = array[i];

            if (i == length - 1) {
                return curr.remove(string);
            }

            Object object = curr.get(string);
            if (object instanceof Map) {
                curr = (Map<?, ?>) object;
            } else {
                // 不是最后一级, 下一级却不是Map, 直接返回
                return null;
            }
        }
        return null;
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public Set<String> keySet() {
        return map.keySet();
    }

    @Override
    public Collection<Object> values() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        return map.entrySet();
    }

    private static String DEFAULT_PLACEHOLDER_TYPE = "{}";
    private static final Map<String, Pattern> PLACEHOLDERS = new HashMap<>();
    static {
        String NAME_CHAR = "\\s*([-.\\w]+)\\s*";
        addPattern("{}", Pattern.compile("\\{" + NAME_CHAR + "\\}"));
        addPattern("{{}}", Pattern.compile("\\{\\{" + NAME_CHAR + "\\}\\}"));
        addPattern("${}", Pattern.compile("\\$\\{" + NAME_CHAR + "\\}"));
        addPattern("#{}", Pattern.compile("#\\{" + NAME_CHAR + "\\}"));
        addPattern("<>", Pattern.compile("<" + NAME_CHAR + ">"));
        addPattern("<%=%>", Pattern.compile("<%=" + NAME_CHAR + "%>"));
    }

    private String defaultPlaceholderType = DEFAULT_PLACEHOLDER_TYPE;

    /**
     * 增加占位符类型
     * 
     * @param type 占位符类型
     * @param pattern 占位符匹配规则
     */
    public static void addPattern(String type, Pattern pattern) {
        PLACEHOLDERS.put(type, pattern);
    }

    /** 设置全局默认占位符类型 **/
    public static void setGlobalDefaultPlaceholderType(String type) {
        if (PLACEHOLDERS.containsKey(type)) {
            DEFAULT_PLACEHOLDER_TYPE = type;
        } else {
            throw new IllegalArgumentException("Placeholder type not found.");
        }
    }

    /** 设置默认占位符类型 **/
    public void setDefaultPlaceholderType(String type) {
        if (PLACEHOLDERS.containsKey(type)) {
            defaultPlaceholderType = type;
        } else {
            throw new IllegalArgumentException("Placeholder type not found.");
        }
    }

    /**
     * 格式化, 实现参数替换
     * 
     * @param string 原字符串
     * @param maps 备选参数映射表
     * @return 格式化后的字符串
     * @see #format(String, String, boolean, CascadeMap...)
     */
    public String format(String string, CascadeMap... maps) {
        return format(defaultPlaceholderType, string, false, maps);
    }

    /**
     * 格式化, 实现参数替换
     * 
     * @param string 原字符串
     * @param remainUnknown 是否保留不能识别的表达式
     * @param maps 备选参数映射表
     * @return 格式化后的字符串
     * @see #format(String, String, boolean, CascadeMap...)
     */
    public String format(String string, boolean remainUnknown, CascadeMap... maps) {
        return format(defaultPlaceholderType, string, remainUnknown, maps);
    }

    /**
     * 格式化, 实现参数替换
     * 
     * @param type 占位符类型
     * @param string 原字符串
     * @param maps 备选参数映射表
     * @return 格式化后的字符串
     * @see #format(String, String, boolean, CascadeMap...)
     * @see #addPattern(String, Pattern) 增加占位符类型
     */
    public String format(String type, String string, CascadeMap... maps) {
        return format(type, string, false, maps);
    }

    /**
     * 格式化, 实现参数替换
     *
     * <pre>
     * String ptn = "尊敬的{nickName}:感谢参与!我们将于{config.deliver.time}天内寄送奖品,请注意查收!收件地址:{home.address}.";
     * UserInfo user = queryUserInfo(); // { nickName:string, home:{telphone:string,address:string}, office:{}, ... }
     * CascadeMap map = new CascadeMap();
     * map.putAll(JsonTools.beanToMap(user));
     * map.put("config.deliver.time", 7);
     * String message = map.format(ptn);
     * </pre>
     *
     * @author zhaohuihua
     * @param type 占位符类型
     * @param string 原字符串
     * @param remainUnknown 是否保留不能识别的表达式
     * @param maps 备选参数映射表
     * @return 参数替换后的字符串
     * @see #addPattern(String, Pattern) 增加占位符类型
     */
    public String format(String type, String string, boolean remainUnknown, CascadeMap... maps) {
        if (type == null) {
            throw new NullPointerException("type is null.");
        }
        if (string == null) {
            return null;
        }
        Pattern ptn = PLACEHOLDERS.get(type);
        if (ptn == null) {
            ptn = Pattern.compile(type);
        }
        Matcher matcher = ptn.matcher(string);
        StringBuilder buffer = new StringBuilder();
        int start = 0;
        while (matcher.find()) {
            buffer.append(string, start, matcher.start());
            if (matcher.groupCount() < 1) {
                throw new IllegalArgumentException("Pattern must be use catch group: " + ptn);
            }
            String key = matcher.group(1);
            Object value = this.get(key);
            if (value == null) {
                // 从备选参数映射表中查找
                for (CascadeMap m : maps) {
                    value = m.get(key);
                    if (value != null) {
                        break;
                    }
                }
            }
            if (value != null) {
                buffer.append(value);
            } else if (remainUnknown) { // 是否保留不能识别的表达式
                buffer.append(matcher.group(0));
            }
            start = matcher.end();
        }
        buffer.append(string.substring(start));
        return buffer.toString();
    }

    @Override
    public CascadeMap copy() {
        return ConvertTools.deepCopyObject(this);
    }

    @Override
    @SuppressWarnings("all")
    public CascadeMap clone() {
        return this.copy();
    }

    public String toString() {
        return map.toString();
    }
}
