package com.gitee.qdbp.staticize.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 属性必填标记注解<br>
 * 设置在Setter方法上面
 *
 * @author zhaohuihua
 * @version 20200929
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AttrRequired {
}
