package com.gitee.qdbp.staticize.tags.core;

import java.io.IOException;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.NextStep;

/**
 * 代码块标签, 什么也不干, 仅用于作用域隔离
 * 
 * @author zhaohuihua
 * @version 20200930
 */
public class BlockTag extends BaseTag {

    @Override
    public NextStep doHandle() throws IOException {
        return NextStep.EVAL_BODY;
    }
}
