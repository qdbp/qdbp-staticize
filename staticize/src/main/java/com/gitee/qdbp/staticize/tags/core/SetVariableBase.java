package com.gitee.qdbp.staticize.tags.core;

import com.gitee.qdbp.staticize.common.NodeMetaData;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.ITag;
import com.gitee.qdbp.staticize.tags.base.NextStep;
import com.gitee.qdbp.staticize.utils.TagUtils;
import com.gitee.qdbp.tools.utils.StringTools;

/**
 * 设置变量基础标签类
 * 
 * @author zhaohuihua
 * @version 20200528
 */
public class SetVariableBase extends BaseTag {

    /** 保存变量值的变量名 **/
    private String var;
    /** 获取变量值的表达式 **/
    private Object value;
    /** 有没有变量值 **/
    private boolean hasValue;
    /** 保存变量值的范围(parent/root/preset) **/
    private String scope;

    @Override
    public void doValidate(NodeMetaData metadata) throws TagException {
        if (hasValue) {
            // 有设置value, 则标签体中只能出现空白文本
            for (NodeMetaData item : metadata.getChildren()) {
                if (!TagUtils.isTextTag(item.getTagClass())) {
                    throw new TagException("Attribute 'value' and tag body cannot both be set.");
                }
                String text = item.getAttributeValue("value", String.class);
                if (text != null && !StringTools.isAsciiWhitespace(text)) {
                    throw new TagException("Attribute 'value' and tag body cannot both be set.");
                }
            }
        } else {
            // 标签体中只能出现空白文本标签和表达式标签
            NodeMetaData exp = null;
            for (NodeMetaData item : metadata.getChildren()) {
                Class<? extends ITag> tagType = item.getTagClass();
                if (TagUtils.isTextTag(tagType)) {
                    String text = item.getAttributeValue("value", String.class);
                    if (text != null && !StringTools.isAsciiWhitespace(text)) {
                        throw new TagException("The tag body must be an expression.");
                    }
                } else if (TagUtils.isExpressionTag(tagType)) {
                    if (exp == null) {
                        exp = item;
                    } else {
                        // 只能出现一个表达式标签
                        throw new TagException("The tag body must be an expression.");
                    }
                } else {
                    // 不能出现其他标签
                    throw new TagException("The tag body must be an expression.");
                }
            }
            if (exp == null) {
                throw new TagException("Attribute value and tag body cannot both be empty.");
            }
            // 清空标签体
            metadata.getChildren().clear();
            // 从标签体中获取唯一的一个表达式作为value
            Object value = exp.getAttributeValue("value", Object.class);
            this.value = this.doEvalValue("value", value);
            this.hasValue = true;
        }
    }

    /** {@inheritDoc} **/
    public NextStep doHandle() throws TagException {
        String scope = this.scope == null || "this".equals(this.scope) ? "parent" : this.scope;
        this.addVariableValue(this.var, this.value, scope);
        return NextStep.SKIP_BODY;
    }

    /** 保存变量值的变量名 **/
    protected String getVar() {
        return var;
    }

    /** 保存变量值的变量名 **/
    protected void setVar(String var) {
        this.var = var;
    }

    /** 获取变量值的表达式 **/
    protected Object getValue() {
        return value;
    }

    /** 获取变量值的表达式 **/
    protected void setValue(Object value) {
        this.value = value;
        this.hasValue = true;
    }

    /** 保存变量值的范围(parent/root/preset) **/
    protected String getScope() {
        return scope;
    }

    /** 保存变量值的范围(parent/root/preset) **/
    protected void setScope(String scope) {
        this.scope = scope;
    }

}
