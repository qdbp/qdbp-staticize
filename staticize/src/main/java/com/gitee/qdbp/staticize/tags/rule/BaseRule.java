package com.gitee.qdbp.staticize.tags.rule;

import com.gitee.qdbp.staticize.common.IContext;
import com.gitee.qdbp.staticize.common.NodeMetaData;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.ITag;

/**
 * 规则标签
 * 
 * @author zhaohuihua
 * @version 140728
 */
public abstract class BaseRule extends BaseTag {

    private IContext context;

    /**
     * 设置基础数据
     *
     * @param parent 父标签
     * @param context 上下文对象
     */
    @Override
    public void doSetBaseData(ITag parent, IContext context) {
        super.doSetBaseData(parent, context);
        this.context = context;
    }

    @Override
    public void doValidate(NodeMetaData metadata) throws TagException {
    }

    protected final void addPresetValue(String key, Object value) {
        this.context.preset().put(key, value);
    }
}
