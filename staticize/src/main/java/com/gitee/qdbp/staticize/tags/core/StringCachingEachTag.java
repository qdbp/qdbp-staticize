package com.gitee.qdbp.staticize.tags.core;

import java.io.IOException;
import com.gitee.qdbp.staticize.common.IWriter;
import com.gitee.qdbp.staticize.common.StringCachingWriter;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.BaseEachTag;
import com.gitee.qdbp.tools.utils.StringTools;

/**
 * 输出字符串的Each标签
 *
 * @author zhaohuihua
 * @version 20210427
 */
public class StringCachingEachTag extends BaseEachTag<StringCachingWriter> {

    public StringCachingEachTag() {
        super(new StringCachingWriter());
    }

    @Override
    protected final void doEnded(StringCachingWriter caching, IWriter origin) throws TagException, IOException {
        doEnded(caching.getContent(), origin);
        caching.clear();
    }

    protected void doEnded(String content, IWriter writer) throws TagException, IOException {
        if (content.length() == 0) {
            return;
        }

        // 循环标签不需要输出前置空白, 因为迭代内容已经有换行符了

        if (this.isInline() && !this.es.isFirst()) {
            writer.write(StringTools.trimLeft(content));
        } else {
            writer.write(content);
        }
    }
}
