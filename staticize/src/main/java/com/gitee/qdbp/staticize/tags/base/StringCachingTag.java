package com.gitee.qdbp.staticize.tags.base;

import java.io.IOException;
import com.gitee.qdbp.staticize.common.IWriter;
import com.gitee.qdbp.staticize.common.StringCachingWriter;
import com.gitee.qdbp.staticize.exception.TagException;

/**
 * 缓存String内容的标签
 *
 * @author zhaohuihua
 * @version 20200906
 */
public abstract class StringCachingTag extends CachingTag<StringCachingWriter> {

    public StringCachingTag() {
        super(new StringCachingWriter());
    }

    @Override
    protected final void doEnded(StringCachingWriter caching, IWriter origin) throws TagException, IOException {
        doEnded(caching.getContent(), origin);
        caching.clear();
    }

    /**
     * 标签结束的处理, 一般是将缓冲区内容写入原IWriter
     * 
     * @param content 缓冲区内容
     * @param writer 原Writer
     * @throws TagException 标签处理异常
     * @throws IOException 输出处理异常
     */
    protected abstract void doEnded(String content, IWriter writer) throws TagException, IOException;
    
}
