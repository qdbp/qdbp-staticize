package com.gitee.qdbp.staticize.common;

import java.util.List;
import com.gitee.qdbp.staticize.parse.AttrData;
import com.gitee.qdbp.staticize.tags.base.ITag;

/**
 * 解析后的标签元数据树(ReadOnly)
 *
 * @author zhaohuihua
 * @version 140520
 */
interface BaseMetaData {

    /**
     * 获取模板ID
     *
     * @return 返回模板ID
     */
    String getTplId();

    /**
     * 获取模板文件路径
     *
     * @return 返回模板文件路径
     */
    String getRealPath();

    /**
     * 获取标签所在的行数
     *
     * @return 返回标签所在的行数
     */
    int getRow();

    /**
     * 获取标签所在的列数
     *
     * @return 返回标签所在的列数
     */
    int getColumn();

    /**
     * 获取标签名称
     *
     * @return 返回标签名称
     */
    String getName();

    /**
     * 获取标签处理类
     *
     * @return 返回标签处理类
     */
    Class<? extends ITag> getTagClass();

    /**
     * 获取标签的所有属性
     *
     * @return 属性列表
     */
    List<AttrData> getAttributes();

    /**
     * 取得标签的指定属性
     *
     * @param attrName 属性名称
     * @return 属性对象
     */
    AttrData getAttribute(String attrName);

    /**
     * 判断标签是否存在指定的属性
     *
     * @param attrName 属性名称
     * @return 是否存在
     */
    boolean hasAttribute(String attrName);

    /**
     * 取得标签的指定属性值
     *
     * @param attrName 属性名称
     * @param clazz 属性值类型
     * @return 属性对象
     */
    <T> T getAttributeValue(String attrName, Class<T> clazz);

    /**
     * 获取子节点
     *
     * @return 返回子节点
     */
    List<NodeMetaData> getChildren();
}
