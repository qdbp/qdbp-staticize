package com.gitee.qdbp.staticize.io;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Date;
import com.gitee.qdbp.staticize.common.IReader;
import com.gitee.qdbp.tools.files.FileTools;
import com.gitee.qdbp.tools.files.PathTools;
import com.gitee.qdbp.tools.utils.VerifyTools;

/**
 * 输入流构建接口实现类
 * 
 * @author zhaohuihua
 * @version 170624
 */
public class UrlReaderCreator implements IReaderCreator {

    /** 基准路径 */
    private final URL base;

    /** 构造函数 **/
    public UrlReaderCreator() {
        this.base = null;
    }

    /**
     * 构造函数
     * 
     * @param base 基准路径
     */
    public UrlReaderCreator(URL base) {
        this.base = base;
    }

    /** {@inheritDoc} **/
    @Override
    public IReader create(String path) throws IOException {
        VerifyTools.requireNotBlank(path, "path");

        URL url;
        if (base == null) {
            url = PathTools.findResource(path);
        } else {
            url = PathTools.findRelativeResource(base, path);
        }

        Charset charset = Charset.forName(FileTools.getEncoding(url.openStream()));
        Reader reader = new InputStreamReader(url.openStream(), charset);
        return new SimpleReader(path, reader);
    }

    /** {@inheritDoc} **/
    @Override
    public String getRelativePath(String path, String src) {
        // 处理导入模板的路径
        if (!src.startsWith("/") && !src.startsWith("\\")) {
            // 相对路径, 加上当前模板的上级路径
            src = PathTools.concat(PathTools.removeFileName(path), src);
        }
        return src;
    }

    /** {@inheritDoc} **/
    @Override
    public Date getUpdateTime(String path) {
        VerifyTools.requireNotBlank(path, "path");
        URL url = PathTools.findRelativeResource(base, path);
        return PathTools.getLastModified(url);
    }
}
