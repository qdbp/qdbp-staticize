package com.gitee.qdbp.staticize.parse;

import java.util.ArrayList;
import java.util.List;
import com.gitee.qdbp.staticize.annotation.AdjoinAfter;
import com.gitee.qdbp.staticize.annotation.ChooseContainer;
import com.gitee.qdbp.staticize.annotation.OutputWrapper;
import com.gitee.qdbp.staticize.common.NodeMetaData;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.parse.AttrSetter.MethodInfo;
import com.gitee.qdbp.staticize.tags.base.BaseEachTag;
import com.gitee.qdbp.staticize.tags.base.ExpressionTag;
import com.gitee.qdbp.staticize.tags.base.ITag;
import com.gitee.qdbp.staticize.tags.base.TextTag;
import com.gitee.qdbp.staticize.utils.TagUtils;
import com.gitee.qdbp.tools.utils.ConvertTools;
import com.gitee.qdbp.tools.utils.StringTools;
import com.gitee.qdbp.tools.utils.VerifyTools;

/**
 * 标签容器
 *
 * @author zhaohuihua
 * @version 140520
 */
class TagBox {

    /** 根节点的名称 **/
    private static final String ROOT_NAME = "{root}";
    /** 文本节点的名称 **/
    private static final String TEXT_NAME = "{text}";
    /** 文本节点的属性名 **/
    private static final String TEXT_ATTR = "value";
    /** 前置空白的属性名 **/
    private static final String LEADING_BLANK = "leadingBlank";
    /** 前置空白的临时属性名 **/
    private static final String LEADING_TEMP = "{leadingBlank}";

    /** 解析器 **/
    private final TagParse parser;
    /** 当前标签的名称 **/
    protected String name;
    /** 当前开始标签 **/
    protected String openTag;
    /** 当前结束标签 **/
    protected String closeTag;
    /** 当前标签开始位置所在的行数 **/
    protected int row;
    /** 当前标签开始位置所在的列数 **/
    protected int column;
    /**
     * 当前标签是否处于打开状态<br>
     * 只有当标签刚创建成功, 正在准备取属性时opened=false<br>
     * 标签打开时, curr=新标签, opened=true<br>
     * 标签关闭时, curr=parent, opened=true
     */
    protected boolean opened;
    /** 根节点 **/
    protected final RootMetaDataImpl root;
    /** 正在处理的节点 **/
    protected NodeMetaDataImpl curr;
    /** 正在处理的文本(文本缓存) **/
    protected StringBuilder text;
    /**
     * 最后一个标签结束时遗留的行尾空白文本<br>
     * 如果下一标签的开始与上一标签的结束在同一行, 则将上一标签的trailingBlank作为下一标签的leadingBlank
     */
    // 例如这一段内容
    //     text a
    //     <core:if test="${xxx}">
    //         text b
    //     </core:if>text c
    // lastTrailingBlank记录的是</core:if>所在的行+前面的换行和缩进
    // 处理后, 相当于这样:
    //     text a<core:if test="${xxx}">
    //         text b</core:if>
    //     text c
    protected RowAndText lastTrailingBlank;

    /**
     * 构造函数
     *
     * @param parser 标签解析器
     */
    public TagBox(TagParse parser) {
        this.parser = parser;
        this.name = ROOT_NAME;
        this.openTag = "<" + ROOT_NAME + ">";
        this.closeTag = "</" + ROOT_NAME + ">";
        this.row = parser.row;
        this.column = parser.column;
        RootMetaDataImpl root = new RootMetaDataImpl(TextTag.class, name, parser.tpl, parser.path, row, column);
        root.setTaglib(parser.taglib);
        this.root = root;
        this.curr = root;
        this.text = new StringBuilder();
    }

    /** 处理已经缓存的文本 **/
    public void flush() {
        newTextTag();
    }

    /**
     * 创建新标签<br>
     * 在curr下增加新标签, 将curr设为新标签, 并设opened=false<br>
     * 创建新标签之前先处理已经缓存的文本(text), 即在当前标签下增加文本标签
     *
     * @author zhaohuihua
     * @param name 标签名称
     * @param openTag 开始标签
     * @param closeTag 结束标签
     * @param tagRow 所在行数
     * @param tagColumn 所在列数
     * @throws TagException 标签未打开时出现新标签
     */
    public void newTag(String name, String openTag, String closeTag, int tagRow, int tagColumn) throws TagException {
        newTag(name, openTag, closeTag, tagRow, tagColumn, parser.taglib.getTag(name));
    }

    /**
     * 创建新标签<br>
     * 在curr下增加新标签, 将curr设为新标签, 并设opened=false<br>
     * 创建新标签之前先处理已经缓存的文本(text), 即在当前标签下增加文本标签
     *
     * @author zhaohuihua
     * @param name 标签名称
     * @param openTag 开始标签
     * @param closeTag 结束标签
     * @param tagRow 所在行数
     * @param tagColumn 所在列数
     * @param tagClass 标签处理类 (如果为空则根据name从taglib获取)
     * @throws TagException 标签未打开时出现新标签
     */
    public void newTag(String name, String openTag, String closeTag, int tagRow, int tagColumn,
            Class<? extends ITag> tagClass) throws TagException {
        // 处理已经缓存的文本
        newTextTag();

        if (!this.opened) {
            // 实际上走不到这里, <core:if <core:test>, 这种情况取if标签的属性时就会报错
            String fmt = "%s:%s, the tag %s has not been opened, unexpected a new tag %s.";
            throw new TagException(String.format(fmt, row, column, this.openTag, openTag));
        }

        // 记录下当前标签开始的位置
        this.row = tagRow >= 0 ? tagRow : parser.row;
        if (tagColumn >= 0) {
            this.column = tagColumn;
        } else {
            // 创建新标签
            if (openTag.endsWith(">")) {
                // <core:test> 减掉最后一个>, 因为执行到此时实际位于<core:test, 最后这个>是手工拼上去的
                this.column = parser.column - (openTag.length() - 1);
            } else {
                this.column = parser.column - openTag.length();
            }
        }
        NodeMetaDataImpl data =
                new NodeMetaDataImpl(tagClass, name, openTag, closeTag, parser.tpl, parser.path, row, column);
        // 处理上一标签遗留的行尾空白文本
        // 例如这一段内容, 这里指的是fmt:date标签, 此标签位于其他标签结束的同一行
        //     text a
        //     <core:if test="${xxx}">
        //         text b
        //     </core:if><fmt:date var="d">当前时间: ${d}</fmt:date>
        // 如果不处理将会输出
        //     text a
        //         text b当前时间: 2020-08-08
        // 处理之后输出
        //     text a
        //         text b
        //     当前时间: 2020-08-08
        handleLastTrailingBlank(data, LEADING_TEMP);
        // 将新标签绑定到标签树上
        curr.appendChild(data);

        // 输出日志
        parser.debug("{}({})\t{}", row, column, StringTools.trimRight(openTag, '>'));

        // 当前处理标签设置为新标签
        this.curr = data;
        this.name = name;
        this.openTag = openTag;
        this.closeTag = closeTag;
        this.opened = false;
    }

    /** 创建文本标签 */
    private void newTextTag() {
        if (text.length() == 0) {
            return;
        }

        // 删除紧跟在import标签后的换行符, 防止文件开头的import导致多出一个空行
        char c = this.text.charAt(0);
        boolean startsWithNewLine = c == '\r' || c == '\n';
        if (startsWithNewLine && checkIsAllNonOutputTag(root.children())) {
            if (c == '\r' && this.text.length() >= 2 && this.text.charAt(1) == '\n') {
                this.text.delete(0, 2);
            } else {
                this.text.delete(0, 1);
            }
        }
        if (text.length() == 0) {
            return;
        }
        // 输出文本信息
        parser.debug(text.toString());

        if (lastTrailingBlank != null) {
            // 如果当前文本开始于换行符, 说明当前文本与上一标签不在同一行
            if (!startsWithNewLine && this.row == lastTrailingBlank.row) {
                // 当前文本开始于上一标签结束的同一行
                // 将上一标签的trailingBlank作为当前文本的前置内容
                this.text.insert(0, lastTrailingBlank.text);
            }
            lastTrailingBlank = null;
        }
        NodeMetaDataImpl data = new NodeMetaDataImpl(TextTag.class, TEXT_NAME, parser.tpl, parser.path, row, column);
        data.addAttribute(TEXT_ATTR, text.toString());
        // 将新标签绑定到标签树上
        curr.appendChild(data);
        // 清空文本缓存
        text.setLength(0);

        // 更新行数列数作为后续标签的开始位置
        this.row = parser.row;
        this.column = parser.column;
    }

    /** 检查是否全部都是没有输出的节点 (ImportTag/CommentTag) **/
    private boolean checkIsAllNonOutputTag(List<NodeMetaDataImpl> nodes) {
        if (nodes == null || nodes.isEmpty()) {
            return false;
        }
        for (NodeMetaDataImpl item : nodes) {
            if (TagUtils.isTextTag(item.getTagClass())) {
                String text = item.getAttributeValue(TEXT_ATTR, String.class);
                if (text == null) {
                    continue;
                }
            }
            if (TagUtils.isImportTag(item.getTagClass()) || TagUtils.isCommentTag(item.getTagClass())) {
                continue;
            }
            return false;
        }
        return true;
    }

    /**
     * 创建表达式标签<br>
     * 创建新标签之前先处理已经缓存的文本(text), 即在当前标签下增加文本标签
     *
     * @author zhaohuihua
     * @param text 标签名称
     * @throws TagException 标签未打开时出现新标签
     */
    public void newExpression(String text) throws TagException {

        // 处理已经缓存的文本
        newTextTag();

        if (!this.opened) {
            // 实际上走不到这里, <core:if <core:test>, 这种情况取if标签的属性时就会报错
            String fmt = "%s:%s, the tag %s has not been opened, unexpected the expression %s .";
            throw new TagException(String.format(fmt, row, column, this.openTag, text));
        }

        // 创建新标签
        // +1说明: 例如${list}, 当前字符是}, column=7, 而text.length=7
        // 相减就等于0了, 而column是从1开始的, 所以要+1
        int column = parser.column - text.length() + 1;
        String name = "{expression}";
        NodeMetaDataImpl data =
                new NodeMetaDataImpl(ExpressionTag.class, text, name, name, parser.tpl, parser.path, row, column);
        // 设置属性
        data.addAttribute(TEXT_ATTR, new ExpItem(text));
        // 处理上一标签遗留的行尾空白文本
        handleLastTrailingBlank(data, LEADING_BLANK);
        // 将新标签绑定到标签树上
        curr.appendChild(data);

        // 输出日志
        parser.debug("{}({})\t{}", parser.row, column, text);

        // 更新行数列数作为后续标签的开始位置
        this.row = parser.row;
        this.column = parser.column;
    }

    private void handleLastTrailingBlank(NodeMetaDataImpl data, String attrKey) {
        if (lastTrailingBlank != null) {
            if (data.row == lastTrailingBlank.row) {
                // 当前标签开始于上一标签结束的同一行
                // 将上一标签的trailingBlank作为当前文本的leadingBlank
                data.addAttribute(attrKey, lastTrailingBlank.text);
                // 前置空白截取于已输出的文本, 不能重复输出
                // parser.log(lastTrailingBlank.text);
            }
            lastTrailingBlank = null;
        }
    }

    /**
     * 处理缓存文本
     *
     * @param text 文本信息
     */
    public void addText(String text) {
        if (text != null && text.length() > 0) {
            this.text.append(text);
        }
    }

    /**
     * 给当前标签增加属性
     *
     * @param attributes 属性对象
     */
    public void addAttributes(List<AttrData> attributes) throws TagException {
        // 校验必选字段
        try {
            AttrSetter.instance.validAttrRequireds(curr.clazz, attributes);
        } catch (TagException e) {
            e.prependMessage(String.format("%s:%s, %s, ", row, column, openTag));
            throw e;
        }

        // 遍历属性表, 如果不是表达式就转换为标签字段对应的数据类型
        for (AttrData item : attributes) {
            String field = item.getKey();
            Object value = item.getValue();

            // 转换为驼峰命名
            String camelName;
            try {
                camelName = TagUtils.toCamelName(field);
            } catch (IllegalArgumentException e) {
                // <core:if -test="${i}"></core:if>
                String fmt = "%s:%s, attribute '%s' of tag '%s', %s";
                String txt = e.getMessage();
                String msg = String.format(fmt, row, column, field, openTag, txt);
                throw new TagException(msg, e);
            }

            // value只有ExpItem/ExpItems/String三种情况
            Object result;
            // 包含有表达式
            if (value instanceof ExpItem || value instanceof ExpItems) {
                // 解析时不转换数据类型, 等到发布时根据Context取值
                // 将属性值设置到当前标签中
                result = item;
                curr.addAttribute(item);
            } else { // 是普通字符串, 转换数据类型
                try {
                    // 将属性值转换为标签字段对应的数据类型
                    // -- 只在value是boolean或数字时转换boolean
                    // -- 只在value是number时转换数字
                    // 在新版本中, 属性变量不要求一定用${}包裹, attr="${name}"可以简写为attr="name"
                    // -- 如果attr是String, 而context中不存在name, 则将name本身当作字符串赋给attr
                    // -- 如果attr是Object, 而context中不存在name, 则将null赋给attr
                    result = tryConvertPrimitiveValue(curr.clazz, camelName, (String) value);
                    // 将属性值设置到当前标签中
                    curr.addAttribute(field, result);
                } catch (TagException e) {
                    // <core:each var="i" begin="A"></core:each>
                    parser.debug("\t{}=\"{}\"", field, value);
                    e.prependMessage(String.format("%s:%s, %s, ", row, column, openTag));
                    throw e;
                }
            }

            // 输出日志
            String type = result == null ? null : result.getClass().getSimpleName();
            parser.debug("\t{}=\"{}\"\t({})", field, value, type);
        }
    }

    /**
     * 尝试转换基本类型, 例如"true"转换为true, 数字型字符串转换为数字
     * 
     * @param tagType 标签类型
     * @param name 字段名
     * @param value 字段值
     * @return 转换后的值
     */
    private Object tryConvertPrimitiveValue(Class<? extends ITag> tagType, String name, String value) {
        MethodInfo methodInfo = AttrSetter.instance.getMethodInfo(tagType, name);
        if (value == null) {
            return null;
        }
        if (methodInfo == null) {
            return value;
        }
        Class<?> type = methodInfo.getParamType();
        if (type == String.class || type.isAssignableFrom(value.getClass())) {
            return value;
        }
        String string = value.trim();
        if (type == boolean.class || type == Boolean.class) {
            if ("true".equalsIgnoreCase(string)) {
                return true;
            } else if ("false".equalsIgnoreCase(string)) {
                return false;
            } else if (StringTools.isDigit(string)) {
                return StringTools.isPositive(string, false);
            }
        } else if (StringTools.isNumber(string)) {
            String number = StringTools.removeChars(string, ',', ' ');
            if (type == int.class || type == Integer.class) {
                Integer result = ConvertTools.toInteger(number, null);
                return result == null ? number : result;
            } else if (type == long.class || type == Long.class) {
                Long result = ConvertTools.toLong(number, null);
                return result == null ? number : result;
            } else if (type == float.class || type == Float.class) {
                Float result = ConvertTools.toFloat(number, null);
                return result == null ? number : result;
            } else if (type == double.class || type == Double.class) {
                Double result = ConvertTools.toDouble(number, null);
                return result == null ? number : result;
            }
        }
        return string;
    }

    /** 当前标签打开 **/
    public void open() {
        // 输出日志
        if (this.curr == this.root) {
            // 根节点
            parser.debug("{}({})\t{}", row, column, openTag);
        } else {
            parser.debug("{}\t>", parser.row);
        }
        this.opened = true;
    }

    /** 当前标签关闭 **/
    public void close() {
        if (!opened) {
            // 没有打开直接结束, 没有标签体
            parser.debug("{}-{}\t/>", curr.row, parser.row);
        } else {
            // 处理缓存的文本
            newTextTag();
            // 输出日志
            parser.debug("{}-{}\t</{}>", curr.row, parser.row, name);
        }
        // 标签结束处理
        doCloseHandle();

        // 重设当前处理数据
        curr = curr.getParent();
        this.name = curr == null ? null : curr.name;
        this.openTag = curr == null ? null : curr.openTag;
        this.closeTag = curr == null ? null : curr.closeTag;
        this.opened = true;

        // 更新行数列数作为后续文本标签的开始位置
        this.row = parser.row;
        this.column = parser.column;
    }

    /** 清除最后一个空行, 避免最终生成的文件中因标签导致很多空行 */
    // 场景1: 开始标签和结束标签独占一行, 需要清除<core:if>和</core:if>前面的一个空行
    //     text a
    //     <core:if test="${xxx}">
    //         text b
    //     </core:if>
    // 清除之后变成
    //     text a<core:if test="${xxx}">
    //         text b</core:if>
    // 场景2: 开始标签和结束标签在同一行, 或没有标签体, 或开始标签的同一行存在其他子内容
    //        需要清除结束标签前的空行
    //        需要清除开始标签前的空行, 并保存在leadingBlank字段
    //        在输出内容前先输出leadingBlank, 如果没有输出内容则leadingBlank被丢弃
    // 场景2.1: 开始标签和结束标签在同一行
    //     text a
    //     <core:if test="${xxx}">text b</core:if>
    // 场景2.2: 没有标签体
    //     text a
    //     <user:info id="100001" />
    // 场景2.3: 开始标签的同一行存在其他子内容
    //     text a
    //     <core:if test="${xxx}">text b
    //     </core:if>
    private void doCloseHandle() {
        if (curr == root) {
            return;
        }

        boolean leadingBlankResolved = false;
        // 如果存在紧邻标记, 判断上一个标签是否正确
        AdjoinAfter adjoin = curr.getTagClass().getAnnotation(AdjoinAfter.class);
        if (adjoin != null) {
            NodeMetaData prev = findAndCheckPrevNode(curr, adjoin.value());
            if (prev != null) {
                leadingBlankResolved = true;
                curr.delAttribute(LEADING_TEMP);
                // 有紧邻标记的标签, 从上一标签继承leadingBlank
                String leadingBlank = getLeadingBlank(prev);
                if (leadingBlank != null) {
                    prependLeadingBlank(curr, leadingBlank);
                }
            }
        }

        // 父级是否存在ChooseContainer注解
        ChooseContainer chooseContainer = curr.getTagClass().getAnnotation(ChooseContainer.class);
        if (chooseContainer != null) {
            // 检查ChooseContainer下不允许出现其他节点
            checkChooseContainerChildren(curr, chooseContainer.children());
        }

        // 什么情况下需要清除开始标签前面的空行?
        // 1. 开始标签独占一行: 清除即可
        // 2. 没有标签体: 清除并保存在leadingBlank属性中
        // 3. 开始标签的同一行存在其他子内容: 清除并将空行添加为前置子元素
        // 如果存在临时前置空行, 说明当前标签位于上一标签结束行的同一行, 就不需要清除前置空行了
        AttrData tempLeading = curr.delAttribute(LEADING_TEMP);
        if (tempLeading != null) {
            String leadingBlank = (String) tempLeading.getValue();
            // ChooseContainer下面的节点只会有一个显示, 因此都继承其前置空白
            if (chooseContainer != null) {
                replaceCildrenLeadingBlank(curr, leadingBlank);
            } else if (leadingBlank != null) {
                prependLeadingBlank(curr, leadingBlank);
            }
        } else {
            // 从当前节点的兄弟节点中查找最后的文本节点
            NodeMetaData prevTextSibling = findPrevTextSibling(curr);
            if (prevTextSibling != null) {
                // 清除最后的空行, 如果有需要则保存
                String leadingBlank = removeLastBlankLine(prevTextSibling);
                // ChooseContainer下面的节点只会有一个显示, 因此都继承其前置空白
                if (chooseContainer != null) {
                    replaceCildrenLeadingBlank(curr, leadingBlank);
                } else if (leadingBlank != null && !leadingBlankResolved) {
                    prependLeadingBlank(curr, leadingBlank);
                }
            }
        }

        // 什么情况下需要清除结束标签前面的空行?
        // 结束标签占用的这一行对当前标签来说, 是没有意义的
        // 只要开始标签和结束标签不在同一行, 就需要清除结束标签前面的空行
        if (this.row > curr.row) {
            // 从当前节点的子节点中查找最后的文本节点
            NodeMetaData lastTextChild = findLastTextNode(curr.getChildren());
            if (lastTextChild != null) {
                // 将清除掉的内容记录下来, 如果结束标签同一行后面有内容, 作为nextTag的leadingBlank
                String lastIndent = removeLastBlankLine(lastTextChild);
                if (lastIndent != null) {
                    this.lastTrailingBlank = new RowAndText(this.row, lastIndent);
                }
            }
        }
    }

    /**
     * 在标签前面添加leadingBlank
     * 
     * @param node 标签
     * @param leadingBlank 前置空白
     */
    private void prependLeadingBlank(NodeMetaDataImpl node, String leadingBlank) {
        if (leadingBlank == null || leadingBlank.length() == 0) {
            return;
        }
        if (BaseEachTag.class.isAssignableFrom(node.getTagClass())) {
            return; // 循环标签不需要设置前置空白, 因为迭代内容已经有换行符了
        }
        List<NodeMetaData> children = node.getChildren();
        if (children.isEmpty() || isOutputWrapper(node.getTagClass())) {
            node.addAttribute(LEADING_BLANK, leadingBlank);
        } else if (existTagInSameRow(children, node.row)) {
            NodeMetaDataImpl data = new LeadingBlank(TextTag.class, parser.tpl, parser.path, node.row, 0);
            data.addAttribute(TEXT_ATTR, leadingBlank);
            node.prependChild(data);
        }
    }

    private boolean isOutputWrapper(Class<? extends ITag> tagClass) {
        return tagClass.getAnnotation(OutputWrapper.class) != null;
    }

    private void replaceCildrenLeadingBlank(NodeMetaDataImpl node, String leadingBlank) {
        for (NodeMetaDataImpl child : node.children()) {
            // 删除自己的leadingBlank
            removeLeadingBlank(child);
            // 从父节点继承leadingBlank
            if (leadingBlank != null) {
                prependLeadingBlank(child, leadingBlank);
            }
        }
    }

    private static class LeadingBlank extends NodeMetaDataImpl {

        LeadingBlank(Class<? extends ITag> clazz, String tpl, String path, int row, int column) {
            super(clazz, TEXT_NAME, tpl, path, row, column);
        }
    }

    /** 获取标签的前置空白 **/
    private String getLeadingBlank(NodeMetaData node) {
        if (node.hasAttribute(LEADING_BLANK)) {
            return node.getAttributeValue(LEADING_BLANK, String.class);
        } else {
            List<NodeMetaData> children = node.getChildren();
            if (children == null || children.isEmpty()) {
                return null;
            }
            NodeMetaData first = children.get(0);
            if (first instanceof LeadingBlank) {
                return first.getAttributeValue(TEXT_ATTR, String.class);
            } else {
                return null;
            }
        }
    }

    /** 删除标签的前置空白 **/
    private String removeLeadingBlank(NodeMetaDataImpl node) {
        if (node.hasAttribute(LEADING_BLANK)) {
            AttrData attr = node.delAttribute(LEADING_BLANK);
            return attr == null ? null : (String) attr.getValue();
        } else {
            List<NodeMetaDataImpl> children = node.children();
            if (children == null || children.isEmpty()) {
                return null;
            }
            if (children.get(0) instanceof LeadingBlank) {
                NodeMetaDataImpl first = children.remove(0);
                return first.getAttributeValue(TEXT_ATTR, String.class);
            } else {
                return null;
            }
        }
    }

    /**
     * 判断是否存在指定行号的节点
     * 
     * @param nodes 节点列表
     * @param row 行号
     * @return 是否存在
     */
    private boolean existTagInSameRow(List<NodeMetaData> nodes, int row) {
        for (NodeMetaData item : nodes) {
            int beginRow = item.getRow();
            if (TagUtils.isTextTag(item.getTagClass())) {
                String text = item.getAttributeValue(TEXT_ATTR, String.class);
                if (text == null) {
                    continue;
                }
                if (text.startsWith("\r") || text.startsWith("\n")) {
                    beginRow++;
                }
            }
            return row == beginRow;
        }
        return false;
    }

    /**
     * 查找指定节点的上一个非空文本节点
     * 
     * @param node 指定节点
     * @return 文本节点
     */
    private NodeMetaData findPrevTextSibling(NodeMetaDataImpl node) {
        List<NodeMetaData> siblings = getPrevSiblings(node);
        return findLastTextNode(siblings);
    }

    /**
     * 查找最后的一个有内容的文本节点(遇到非文本节点则返回null)
     * 
     * @param nodes 节点列表
     * @return 文本节点
     */
    private NodeMetaData findLastTextNode(List<NodeMetaData> nodes) {
        if (nodes == null || nodes.isEmpty()) {
            return null;
        }
        for (int i = nodes.size() - 1; i >= 0; i--) {
            NodeMetaData item = nodes.get(i);
            if (!TagUtils.isTextTag(item.getTagClass())) {
                return null; // 遇到不是文本的标签, 结束
            }
            String text = item.getAttributeValue(TEXT_ATTR, String.class);
            if (text == null) {
                continue;
            }
            return item;
        }
        return null;
    }

    private static List<NodeMetaData> getPrevSiblings(NodeMetaDataImpl node) {
        if (node.getParent() == null) {
            return null;
        }
        List<NodeMetaData> siblings = node.getParent().getChildren();
        if (siblings == null || siblings.isEmpty()) {
            return null;
        }
        List<NodeMetaData> previous = new ArrayList<>();
        for (NodeMetaData item : siblings) {
            if (item == node) {
                break;
            } else {
                previous.add(item);
            }
        }
        return previous;
    }

    /**
     * 查找上一节点, 并检查是不是指定类型的节点
     * 
     * @param targetTypes 指定的类型
     * @return 上一节点
     */
    private NodeMetaData findAndCheckPrevNode(NodeMetaDataImpl node, Class<? extends ITag>[] targetTypes) {
        List<NodeMetaData> siblings = getPrevSiblings(node);
        if (siblings == null || siblings.isEmpty()) {
            return null;
        }
        int maxSibIndex = siblings.size() - 1;
        // 查找指定类型的前置标签
        NodeMetaData prevNode = null;
        int prevIndex = -1;
        for (int i = maxSibIndex; i >= 0; i--) {
            NodeMetaData item = siblings.get(i);
            if (VerifyTools.isExists(item.getTagClass(), targetTypes)) {
                prevNode = item;
                prevIndex = i;
                break;
            }
        }
        if (prevNode == null) { // 紧邻标记的标签未找到前置标签
            if (VerifyTools.isExists(node.getTagClass(), targetTypes)) {
                // 如果可以紧邻自己, 则无需报错, 如 <when></when><when></when><otherwise></otherwise>
                return null;
            } else {
                String fmt = "%s:%s, %s, no matching front tag found.";
                throw new TagException(String.format(fmt, node.row, node.column, node.openTag));
            }
        }
        // 判断前置标签与当前标签之间有没有其他内容 (空白字符除外)
        boolean existOthers = false;
        for (int i = prevIndex + 1; i <= maxSibIndex; i++) {
            NodeMetaData item = siblings.get(i);
            if (TagUtils.isTextTag(item.getTagClass())) {
                String text = item.getAttributeValue(TEXT_ATTR, String.class);
                if (text == null || StringTools.isAsciiWhitespace(text)) {
                    continue;
                }
            }
            existOthers = true;
        }
        if (existOthers) {
            String fmt = "Non blank content is not allowed between %s(%s:%s) and %s(%s:%s).";
            throw new TagException(String.format(fmt, prevNode.getOpenTag(), prevNode.getRow(), prevNode.getColumn(),
                node.openTag, node.row, node.column));
        }
        return prevNode;
    }

    /** 检查ChooseContainer下不允许出现其他节点 **/
    private void checkChooseContainerChildren(NodeMetaDataImpl node, Class<? extends ITag>[] childrenTypes) {
        for (NodeMetaData child : node.getChildren()) {
            if (TagUtils.isTextTag(child.getTagClass())) {
                String text = child.getAttributeValue(TEXT_ATTR, String.class);
                if (text == null || StringTools.isAsciiWhitespace(text)) {
                    continue;
                }
                String fmt = "Non blank text is not allowed under %s(%s:%s).";
                throw new TagException(String.format(fmt, node.openTag, node.row, node.column));
            }
            if (VerifyTools.isNotExists(child.getTagClass(), childrenTypes)) {
                String fmt = "Tag %s(%s:%s) is not allowed under %s(%s:%s).";
                throw new TagException(String.format(fmt, child.getOpenTag(), child.getRow(), child.getColumn(),
                    node.openTag, node.row, node.column));
            }
        }
    }

    /** 清除最后一个空行, 返回被清除的内容 **/
    private static String removeLastBlankLine(NodeMetaData textTag) {
        AttrData attr = textTag.getAttribute(TEXT_ATTR);
        Object value = attr.getValue();
        if (!(value instanceof String)) {
            return null;
        }
        String text = (String) value;
        // 清除最后一个换行符及之后的内容
        // 换行符: \n 或 \r\n 或 \r
        int nIdx = Math.max(text.lastIndexOf('\n'), text.lastIndexOf('\r'));
        if (nIdx < 0) {
            return null; // 没有换行符
        }
        String lastIndent = text.substring(nIdx);
        if (!StringTools.isAsciiWhitespace(lastIndent)) {
            return null; // 换行符之后的字符中存在非空白字符
        }
        if (nIdx > 0 && text.charAt(nIdx) == '\n' && text.charAt(nIdx - 1) == '\r') {
            nIdx--;
        }
        String newtext = text.substring(0, nIdx);
        attr.setValue(newtext.length() == 0 ? null : newtext);
        return lastIndent;
    }

    private static class RowAndText {

        private final int row;
        private final String text;

        public RowAndText(int row, String text) {
            this.row = row;
            this.text = text;
        }
    }

    public static NodeMetaDataImpl newTextTag(String text, String tpl, String path, int row, int column) {
        NodeMetaDataImpl data = new NodeMetaDataImpl(TextTag.class, TEXT_NAME, tpl, path, row, column);
        data.addAttribute(TEXT_ATTR, text);
        return data;
    }
}
