package com.gitee.qdbp.staticize.tags.base;

import java.io.IOException;
import com.gitee.qdbp.staticize.annotation.AttrRequired;
import com.gitee.qdbp.staticize.annotation.DynamicAttrSupport;
import com.gitee.qdbp.staticize.exception.TagException;

/**
 * Include标签, 支持动态参数
 * 
 * @author zhaohuihua
 * @version 140722
 */
@DynamicAttrSupport("param")
public class IncludeTag extends BaseTag {

    private String src;

    public IncludeTag() {
        super();
    }

    public String getSrc() {
        return src;
    }

    @AttrRequired
    public void setSrc(String src) {
        this.src = src;
    }

    /** {@inheritDoc} **/
    @Override
    public NextStep doHandle() throws TagException, IOException {
        return NextStep.EVAL_BODY;
    }
}
