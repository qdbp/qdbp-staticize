package com.gitee.qdbp.staticize.tags.base;

import java.io.IOException;

/**
 * 表达式标签处理类
 * 
 * @author zhaohuihua
 * @version 20200816
 */
public class ExpressionTag extends BaseTag {

    private Object value;

    public void setValue(Object value) {
        this.value = value;
    }

    public NextStep doHandle() throws IOException {
        if (value != null) {
            print(value);
        }
        return NextStep.EVAL_BODY;
    }
}
