package com.gitee.qdbp.staticize.exception;

import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.able.result.IResultMessage;
import com.gitee.qdbp.tools.utils.StringTools;

/**
 * 标签异常类
 * 
 * @author zhaohuihua
 * @version 140519
 */
public class TagException extends ServiceException {

    /** 版本序列号 **/
    private static final long serialVersionUID = 7269605602712999273L;

    /** 构造函数 **/
    public TagException() {
        super();
    }

    /**
     * 构造函数
     * 
     * @param message 异常信息
     */
    public TagException(String message) {
        super(TagErrorCode.TAG_FORMAT_ERROR);
        super.setMessage(message);
    }

    /**
     * 构造函数
     * 
     * @param message 异常信息
     * @param cause 引发原因
     */
    public TagException(String message, Throwable cause) {
        super(TagErrorCode.TAG_FORMAT_ERROR, cause);
        super.setMessage(message);
    }

    private enum TagErrorCode implements IResultMessage {

        TAG_FORMAT_ERROR;

        @Override
        public String getCode() {
            return this.name();
        }

        @Override
        public String getMessage() {
            return StringTools.replace(this.name(), "_", " ", ".", "");
        }
    }
}
