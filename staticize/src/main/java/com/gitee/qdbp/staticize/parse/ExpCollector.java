package com.gitee.qdbp.staticize.parse;

import com.gitee.qdbp.tools.utils.CharTools;

/**
 * 表达式收集器
 *
 * @author zhaohuihua
 * @version 20200927
 */
class ExpCollector {

    /** 表达式内容 **/
    private final StringBuilder buffer = new StringBuilder();
    /** 最后一个字符 **/
    private char lastChar = 0;
    /** 引号, 用来记录是单引号还是双引号 **/
    private char quotation = 0;

    /**
     * 有没有打开字符串模式, 也就是说正在取字符串取到一半<br>
     * 字符串模式下可接受结束字符, 如下例, 字符串中的}不会导致表达式结束<br>
     * ${JsonTools.parse("{name:'Let\'s go!'}")}
     *
     * @return 是否为字符串模式
     */
    public boolean isStringStatus() {
        return quotation != 0;
    }

    /** 获取表达式内容 **/
    public String getContent() {
        return buffer.toString();
    }

    /**
     * 解析表达式内容, 每次传过来一个字符
     *
     * @param c 属性字符
     */
    public void parse(char c) {
        if (CharTools.isQuotes(c)) {
            if (quotation != 0) {
                if (lastChar != '\\' && c == quotation) {
                    quotation = 0;
                }
            } else {
                if (lastChar != '\\') {
                    quotation = c;
                }
            }
        }
        buffer.append(c);
        lastChar = c;
    }
}
