package com.gitee.qdbp.staticize.publish;

import java.io.IOException;
import java.util.Map;
import com.gitee.qdbp.staticize.common.IContext;
import com.gitee.qdbp.staticize.common.IMetaData;
import com.gitee.qdbp.staticize.common.IWriter;
import com.gitee.qdbp.staticize.exception.TagException;

/**
 * 根据标签数据发布一个页面, 输出到OutputStream<br>
 * 不支持多线程<br>
 *
 * @author zhaohuihua
 * @version 140522
 */
public class OutputPublisher extends BasePublisher {

    /**
     * 构造函数
     *
     * @param root 根节点
     */
    public OutputPublisher(IMetaData root) {
        super(root);
    }

    /**
     * 根据标签数据发布一个页面
     *
     * @param writer 页面输出流
     * @throws TagException 标签错误
     * @throws IOException 写文件失败
     */
    public void publish(IWriter writer) throws TagException, IOException {
        this.publish(null, writer);
    }

    /**
     * 根据标签数据发布一个页面
     *
     * @param preset 预置数据
     * @param writer 页面输出流
     * @throws TagException 标签错误
     * @throws IOException 写文件失败
     */
    public void publish(Map<String, Object> preset, IWriter writer) throws TagException, IOException {
        IContext context = new SimpleContext(writer);
        if (preset != null) {
            context.preset().putAll(preset);
        }

        publish(context);
        writer.flush();
    }

}
