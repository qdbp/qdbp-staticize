package com.gitee.qdbp.staticize.io;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import com.gitee.qdbp.staticize.common.IReader;

/**
 * SimpleReader
 *
 * @author zhaohuihua
 * @version 20200817
 */
public class SimpleReader implements IReader {

    private final String realPath;
    private final Reader reader;

    public SimpleReader(String realPath, String content) {
        this(realPath, new StringReader(content));
    }

    public SimpleReader(String realPath, Reader reader) {
        this.realPath = realPath;
        this.reader = reader;
    }

    @Override
    public int read(char[] cbuf) throws IOException {
        return reader.read(cbuf);
    }

    @Override
    public int read(char[] cbuf, int offset, int length) throws IOException {
        return reader.read(cbuf, offset, length);
    }

    @Override
    public String getRealPath() {
        return realPath;
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }

}
