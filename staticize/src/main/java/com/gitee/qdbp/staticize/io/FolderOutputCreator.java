package com.gitee.qdbp.staticize.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import com.gitee.qdbp.staticize.common.IWriter;
import com.gitee.qdbp.tools.files.PathTools;

/**
 * 固定输出到某一根文件夹下的文件输出流接口实现类<br>
 * 初始时已知根文件夹, 创建输出流时传入相对路径<br>
 * 
 * @author zhaohuihua
 * @version 180317
 */
public class FolderOutputCreator implements IWriterCreator {

    private final String folder;

    public FolderOutputCreator(String folder) {
        if (folder == null) {
            throw new NullPointerException("folder is required");
        }
        this.folder = folder;
    }

    /**
     * 根据文件路径创建输出流
     * 
     * @param path 文件相对路径
     * @return 输出接口
     */
    public IWriter create(String path) throws IOException {
        if (path == null) {
            throw new NullPointerException("path is required");
        }

        // 检查文件夹, 如果不存在则创建
        File file = new File(PathTools.concat(folder, path));
        File dir = file.getParentFile();
        if (!dir.exists()) {
            dir.mkdirs();
        }

        return new StreamWriter(new FileOutputStream(file));
    }
}
