package com.gitee.qdbp.staticize.parse;

import com.gitee.qdbp.able.debug.Debugger;
import com.gitee.qdbp.staticize.common.IMetaData;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.exception.TemplateException;
import com.gitee.qdbp.staticize.io.IReaderCreator;
import com.gitee.qdbp.staticize.tags.base.Taglib;

/**
 * 标签解析器, 解析模板, 生成标签元数据树, 不支持多线程<br>
 * 关于转义符, 需要转义时在表达式前加斜杠\, 如home-\${root.name}-list, \#{today}<br>
 * 文本中的转义符是在解析时由TagParser在遇到$或#时判断前一个字符进行特殊处理的<br>
 * 否则类似表达式的文本中间的标签无法解析到: <br>
 * \#{item&lt;core:if test="expression"&gt;xxx&lt;/core:if&gt;}<br>
 * 属性中的转义符是在AttrCollector中调用ExpParser.parse()时处理的, <br>
 * &lt;channel:query name="\${item}-${i}"&gt;&lt;/channel:query&gt;<br>
 *
 * @author zhaohuihua
 * @version 140519
 */
public class TagParser {

    /** 标签库 **/
    private final Taglib taglib;
    /** 模板读取接口 **/
    private final IReaderCreator input;

    /**
     * 标签解析器构造函数
     *
     * @param input 模板读取接口, 由于include标签, 一定要知道模板路径<br>
     *            但模板有可能来自文件, 也有可能来自数据库, 所以需要模板读取接口
     */
    public TagParser(IReaderCreator input) {
        this(Taglib.defaults(), input);
    }

    /**
     * 标签解析器构造函数
     *
     * @param taglib 标签库
     * @param input 模板读取接口, 由于include标签, 一定要知道模板路径<br>
     *            但模板有可能来自文件, 也有可能来自数据库, 所以需要模板读取接口
     */
    public TagParser(Taglib taglib, IReaderCreator input) {
        this.taglib = taglib;
        this.input = input;
    }

    /**
     * 标签解析器构造函数
     *
     * @param taglib 标签库
     * @param input 模板读取接口, 由于include标签, 一定要知道模板路径<br>
     *            但模板有可能来自文件, 也有可能来自数据库, 所以需要模板读取接口
     * @param clearXmlComment 是否清除XML注释
     * @deprecated use {@link Options#setClearXmlComment(boolean)}
     */
    @Deprecated
    public TagParser(Taglib taglib, IReaderCreator input, boolean clearXmlComment) {
        this(taglib, input);
    }

    /**
     * 解析模板标签
     *
     * @param tpl 模板ID(相对路径)
     * @return 标签元数据
     * @throws TagException 解析失败
     * @throws TemplateException 模板不存在/模板读取失败
     */
    public IMetaData parse(String tpl) throws TagException, TemplateException {
        return parse(tpl, new Options());
    }

    /**
     * 解析模板标签
     *
     * @param tpl 模板ID(相对路径)
     * @param options 标签解析的选项
     * @return 标签元数据
     * @throws TagException 解析失败
     * @throws TemplateException 模板不存在/模板读取失败
     */
    public IMetaData parse(String tpl, Options options) throws TagException, TemplateException {
        return new TagParse(taglib, input, options, tpl).parse();
    }

    /**
     * 标签解析选项
     *
     * @author zhaohuihua
     * @version 20210529
     */
    public static class Options {

        /** 是否清除XML注释 **/
        private boolean clearXmlComment = false;
        /** 是否解析JSP脚本 **/
        private boolean parseJspScript = true;
        /** 缓存大小 (读取文件时一次读取多少) **/
        private int bufferSize = 1024 * 4;
        /** 调试信息收集接口 **/
        private Debugger debugger;

        /** 缓存大小 (读取文件时一次读取多少) **/
        public int getBufferSize() {
            return bufferSize;
        }

        /** 缓存大小 (读取文件时一次读取多少) **/
        public void setBufferSize(int bufferSize) {
            this.bufferSize = bufferSize;
        }

        /** 是否清除XML注释 **/
        public boolean isClearXmlComment() {
            return clearXmlComment;
        }

        /** 是否清除XML注释 **/
        public Options setClearXmlComment(boolean clearXmlComment) {
            this.clearXmlComment = clearXmlComment;
            return this;
        }

        /** 是否解析JSP脚本 **/
        public boolean isParseJspScript() {
            return parseJspScript;
        }

        /** 是否解析JSP脚本 **/
        public Options setParseJspScript(boolean parseJspScript) {
            this.parseJspScript = parseJspScript;
            return this;
        }

        /** 调试信息收集接口 **/
        public Debugger getDebugger() {
            return debugger;
        }

        /** 调试信息收集接口 **/
        public Options setDebugger(Debugger debugger) {
            this.debugger = debugger;
            return this;
        }
    }
}
