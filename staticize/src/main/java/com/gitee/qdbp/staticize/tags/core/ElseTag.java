package com.gitee.qdbp.staticize.tags.core;

import java.util.Map;
import com.gitee.qdbp.staticize.annotation.AdjoinAfter;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.ITag;
import com.gitee.qdbp.staticize.tags.base.NextStep;

/**
 * else 标签处理类
 * 
 * @author zhaohuihua
 * @version 140521
 */
@AdjoinAfter({ IfTag.class, ElseIfTag.class })
public class ElseTag extends BaseTag {

    /** {@inheritDoc} **/
    public NextStep doHandle() throws TagException {
        ITag parent = this.parent();
        Map<String, Object> pstack = parent.stack();
        Boolean done = (Boolean) pstack.get(IfTag.IF_TAG_EVAL_DONE);
        if (done == null) {
            throw new TagException("No matching 'if' tag was found.");
        }

        pstack.remove(IfTag.IF_TAG_EVAL_DONE);
        if (!done) {
            // 没有执行过
            return NextStep.EVAL_BODY;
        } else {
            return NextStep.SKIP_BODY;
        }
    }
}
