package com.gitee.qdbp.staticize.utils;

import com.gitee.qdbp.staticize.parse.ExpItem;
import com.gitee.qdbp.staticize.parse.ExpParser;
import com.gitee.qdbp.staticize.tags.base.ExpressionTag;
import com.gitee.qdbp.staticize.tags.base.ITag;
import com.gitee.qdbp.staticize.tags.base.ImportTag;
import com.gitee.qdbp.staticize.tags.base.IncludeTag;
import com.gitee.qdbp.staticize.tags.base.TextTag;
import com.gitee.qdbp.staticize.tags.core.CommentTag;
import com.gitee.qdbp.tools.utils.CharTools;
import com.gitee.qdbp.tools.utils.StringTools;

/**
 * 标签解析工具类
 *
 * @author zhaohuihua
 * @version 140522
 */
public class TagUtils {

    /** 斜杠/ **/
    public static final String SLASH = "/";

    /** 点号. **/
    public static final String DOT = ".";

    /** 构造函数 **/
    private TagUtils() {
    }

    /** 判断是不是文本标签 **/
    public static boolean isTextTag(Class<? extends ITag> tag) {
        return tag == TextTag.class || TextTag.class.isAssignableFrom(tag);
    }

    /** 判断是不是表达式标签 **/
    public static boolean isExpressionTag(Class<? extends ITag> tag) {
        return tag == ExpressionTag.class || ExpressionTag.class.isAssignableFrom(tag);
    }

    /** 判断是不是Import标签 **/
    public static boolean isImportTag(Class<? extends ITag> tag) {
        return tag == ImportTag.class || ImportTag.class.isAssignableFrom(tag);
    }

    /** 判断是不是Comment标签 **/
    public static boolean isCommentTag(Class<? extends ITag> tag) {
        return tag == CommentTag.class || CommentTag.class.isAssignableFrom(tag);
    }

    /** 判断是不是Include标签 **/
    public static boolean isIncludeTag(Class<? extends ITag> tag) {
        return tag == IncludeTag.class || IncludeTag.class.isAssignableFrom(tag);
    }

    /**
     * 转换为驼峰命名<br>
     * 如: to_camel_name = toCamelName, say-i-love-u = sayILoveU
     *
     * @param name 名称
     * @return 驼峰式名称
     * @throws IllegalArgumentException 名称不符合命名规范
     */
    public static String toCamelName(String name) throws IllegalArgumentException {
        StringBuilder buffer = new StringBuilder();
        boolean upper = false;
        char[] array = name.toCharArray();
        for (char c : array) {
            if (c == '_' || c == '-') {
                if (buffer.length() == 0) {
                    throw new IllegalArgumentException("第一个字符不能是分隔符");
                }
                if (upper) {
                    throw new IllegalArgumentException("不能含有连续的分隔符");
                }
                upper = true;
            } else {
                if (upper && c >= 'a' && c <= 'z') {
                    buffer.append((char) (c + 'A' - 'a'));
                } else {
                    buffer.append(c);
                }
                upper = false;
            }
        }
        if (upper) {
            throw new IllegalArgumentException("最后一个字符不能是分隔符");
        }
        return buffer.toString();
    }

    /**
     * 生成Attribute, 如name="xxx", 如果value==null则返回空字符串
     *
     * @param name 名称
     * @param value 值
     * @return Attribute
     */
    public static String createAttribute(String name, String value) {
        StringBuilder buffer = new StringBuilder();
        if (value != null) {
            buffer.append(" ").append(name).append("=\"").append(value).append("\"");
        }
        return buffer.toString();
    }

    /**
     * 判断是不是花括号包围的字符串
     * 
     * @param string 字符串
     * @return 是不是花括号包围的字符串
     */
    public static boolean isWrappedByCurlyBracket(String string) {
        if (string == null) {
            return false;
        }
        string = string.trim();
        if (string.length() < 2) {
            return false;
        }
        if (string.charAt(0) != '{' || string.charAt(string.length() - 1) != '}') {
            return false;
        }
        return ExpParser.parse('$' + string) instanceof ExpItem;
    }

    /**
     * 截取子字符串, 将会解析转义字符, 如 &amp;lt;替换为&lt;
     * 
     * @param src 原字符串
     * @param start 开始位置
     * @param end 结束位置
     * @return 子字符串
     */
    public static String substring(char[] src, int start, int end) {
        return substring(src, start, end, true);
    }

    /**
     * 截取子字符串
     * 
     * @param src 原字符串
     * @param start 开始位置
     * @param end 结束位置
     * @param parseEscape 是否解析转义字符, 如 &amp;lt;替换为&lt;
     * @return 子字符串
     */
    public static String substring(char[] src, int start, int end, boolean parseEscape) {
        char[] dest = CharTools.subarray(src, start, end);
        if (parseEscape) {
            return parseEscape(new String(dest));
        } else {
            return new String(dest);
        }
    }

    public static String parseEscape(String src) {
        // TODO 怎么解析所有的&开头;结尾的转义字符
        return StringTools.replace(src, "&lt;", "<", "&gt;", ">", "&amp;", "&", "&apos;", "'", "&quot;", "\"");
    }
}
