package com.gitee.qdbp.staticize.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 动态参数支持注解
 *
 * @author zhaohuihua
 * @version 20200929
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DynamicAttrSupport {

    /** 存放动态参数的容器名称 **/
    String value();
}
