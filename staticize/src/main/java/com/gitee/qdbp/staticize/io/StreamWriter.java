package com.gitee.qdbp.staticize.io;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import com.gitee.qdbp.staticize.common.IWriter;

/**
 * 输出流的存储实现类
 *
 * @author zhaohuihua
 * @version 20200815
 */
public class StreamWriter implements IWriter, AutoCloseable {

    private static final Charset UTF8 = StandardCharsets.UTF_8;

    /** 输出流 **/
    private final OutputStream stream;
    /** 编码字符集 **/
    private final Charset charset;

    /** 构造函数 **/
    public StreamWriter(OutputStream stream) {
        this(stream, UTF8);
    }

    /** 构造函数 **/
    public StreamWriter(OutputStream stream, Charset charset) {
        this.stream = stream;
        this.charset = charset;
    }

    @Override
    public void write(Object value) throws IOException {
        if (value != null) {
            stream.write(value.toString().getBytes(charset));
        }
    }

    @Override
    public void flush() throws IOException {
        this.stream.flush();
    }

    @Override
    public void close() throws IOException {
        this.stream.close();
    }

    /** 编码字符集 **/
    public Charset getCharset() {
        return charset;
    }

}
