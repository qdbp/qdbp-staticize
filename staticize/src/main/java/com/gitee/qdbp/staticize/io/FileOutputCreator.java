package com.gitee.qdbp.staticize.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import com.gitee.qdbp.staticize.common.IWriter;

/**
 * 文件输出流接口实现类
 * 
 * @author zhaohuihua
 * @version 140730
 */
public class FileOutputCreator implements IWriterCreator {

    /**
     * 根据文件路径创建输出流
     * 
     * @param path 文件路径
     * @return 输出流
     */
    public IWriter create(String path) throws IOException {
        if (path == null) {
            throw new NullPointerException("path is required");
        }

        // 检查文件夹, 如果不存在则创建
        File file = new File(path);
        File dir = file.getParentFile();
        if (!dir.exists()) {
            dir.mkdirs();
        }

        return new StreamWriter(new FileOutputStream(file));
    }
}
