package com.gitee.qdbp.staticize.tags.base;

/**
 * 迭代状态
 *
 * @author zhaohuihua
 * @version 140604
 */
public interface EachStatus {

    /** 开始行数(默认值0) **/
    Integer getBegin();

    /** 结束行数(可以为空) **/
    Integer getEnd();

    /** 迭代步长(默认值1) **/
    Integer getStep();

    /** 当前迭代对象在集合中的索引(从0开始) **/
    Integer getIndex();

    /** 迭代次数计数(从1开始) **/
    Integer getCount();

    /** 迭代次数限制 **/
    Integer getLimit();

    /** 是否为第一次迭代 **/
    boolean isFirst();

    /** 是否为最后一次迭代 **/
    boolean isLast();

    /** 当前迭代对象 **/
    Object getItem();

    /** 判断能否迭代, 如果不能迭代则一次都不会触发 **/
    boolean iterable();

    /** 是否存在下一迭代对象 **/
    boolean hasNext();

    /** 执行迭代 **/
    Object next();
}
