package com.gitee.qdbp.staticize.parse;

import com.gitee.qdbp.able.beans.KeyValue;

/**
 * 标签属性数据
 *
 * @author zhaohuihua
 * @version 160407
 */
public class AttrData extends KeyValue<Object> {

    /** serialVersionUID **/
    private static final long serialVersionUID = 1L;

    /** 构造函数 **/
    public AttrData() {
    }

    /**
     * 构造函数
     *
     * @param key KEY
     * @param value VALUE
     */
    public AttrData(String key, Object value) {
        super(key, value);
    }
}
