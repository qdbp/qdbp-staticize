package com.gitee.qdbp.staticize.io;

import java.io.IOException;
import java.util.Date;
import com.gitee.qdbp.able.exception.ResourceNotFoundException;
import com.gitee.qdbp.staticize.common.IReader;

/**
 * 读取接口的构建接口<br>
 * 有可能从文件读取, 也有可能从数据库读取
 * 
 * @author zhaohuihua
 * @version 140730
 */
public interface IReaderCreator {

    /**
     * 根据资源路径创建读取接口
     * 
     * @param tpl 模板ID(文件相对路径)
     * @return 读取接口
     * @throws IOException 创建输入流失败
     * @throws ResourceNotFoundException 资源不存在
     */
    IReader create(String tpl) throws IOException, ResourceNotFoundException;

    /**
     * 获取资源的更新时间
     * 
     * @param tpl 模板ID(文件相对路径)
     * @return 更新时间
     * @throws IOException 获取时间失败
     * @throws ResourceNotFoundException 资源不存在
     */
    Date getUpdateTime(String tpl) throws IOException, ResourceNotFoundException;

    /**
     * 获取模板的相对路径
     * 
     * @param tpl 原模板(相对路径)
     * @param newTpl 新模板ID(相对路径)
     * @return 模板路径
     */
    String getRelativePath(String tpl, String newTpl);
}
