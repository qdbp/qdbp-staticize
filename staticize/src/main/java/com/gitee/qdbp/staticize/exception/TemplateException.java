package com.gitee.qdbp.staticize.exception;

/**
 * 模板异常类
 * 
 * @author zhaohuihua
 * @version 140519
 */
public class TemplateException extends Exception {

    /** 版本序列号 **/
    private static final long serialVersionUID = 8679127267772236439L;

    /** 构造函数 **/
    public TemplateException() {
        super();
    }

    /**
     * 构造函数
     * 
     * @param message 异常信息
     */
    public TemplateException(String message) {
        super(message);
    }

    /**
     * 构造函数
     * 
     * @param cause 引发原因
     */
    public TemplateException(Throwable cause) {
        super(cause);
    }

    /**
     * 构造函数
     * 
     * @param message 异常信息
     * @param cause 引发原因
     */
    public TemplateException(String message, Throwable cause) {
        super(message, cause);
    }
}
