package com.gitee.qdbp.staticize.tags.core;

/**
 * 循环迭代处理类<br>
 * <br>
 * status=迭代状态变量名<br>
 * &nbsp;&nbsp;index: 从0开始的当前迭代索引<br>
 * &nbsp;&nbsp;count: 从1开始的迭代次数计数<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;如 begin=20, end=29, step=3, 则index=[20,23,26,29], count=[1,2,3,4]<br>
 * &nbsp;&nbsp;begin: 开始行数<br>
 * &nbsp;&nbsp;rows: 迭代行数<br>
 * &nbsp;&nbsp;end: 结束行数(begin+rows-1=end)<br>
 * &nbsp;&nbsp;step: 迭代步长<br>
 * &nbsp;&nbsp;limit: 迭代次数<br>
 * &nbsp;&nbsp;first: 是否为第一次迭代<br>
 * &nbsp;&nbsp;last: 是否为最后一次迭代<br>
 * <br>
 * items=进行迭代的集合<br>
 * 如果没有指定items, 则begin必须, rows/end/limit必填其一<br>
 * 
 * <pre>
 * 从6开始遍历5次, 6,7,8,9,10
 * &lt;core:each var="i" status="s" begin="6" limit="5"&gt;
 *     ${s.count}-${i}
 * &lt;/core:each&gt;
 *
 * 从6开始
 * &lt;core:each items="${list}" var="i" begin="6"&gt;
 *     ...
 * &lt;/core:each&gt;
 *
 * 只显示第1条
 * &lt;core:each items="${list}" var="i" begin="1" rows="1"&gt;
 *     ...
 * &lt;/core:each&gt;
 * </pre>
 * 
 * @author zhaohuihua
 * @version 140521
 */
public class EachTag extends StringCachingEachTag {

    /** 设置迭代对象的变量名 **/
    public void setVar(String var) {
        super.setVar(var);
    }

    /** 设置保存迭代状态的变量名 **/
    public void setStatus(String status) {
        super.setStatus(status);
    }

    /** 设置保存迭代状态的变量名(兼容c:forEach) **/
    public void setVarStatus(String status) {
        super.setStatus(status);
    }

    /** 设置开始行数(从0开始) **/
    public void setBegin(Integer begin) {
        super.setBegin(begin);
    }

    /** 设置结束行数(begin+rows-1=end) **/
    public void setEnd(Integer end) {
        super.setEnd(end); // 可以为空, 可以小于0
    }

    /** 设置限制行数(可为空, 默认为不限制) **/
    public void setRows(Integer rows) {
        super.setRows(rows);
    }

    /** 设置迭代步长 **/
    public void setStep(Integer step) {
        super.setStep(step); // 可以为空, 可以小于0
    }

    /** 设置迭代次数限制 **/
    public void setLimit(Integer limit) {
        super.setLimit(limit);
    }

    /** 设置是否输出在同一行(非首次迭代时删除第一个文本节点前面的换行) **/
    public void setInline(Boolean inline) {
        super.setInline(inline);
    }

    /** 要进行迭代的集合 **/
    public void setItems(Object items) {
        super.setItems(items);
    }
}
