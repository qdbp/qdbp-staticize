package com.gitee.qdbp.staticize.task;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gitee.qdbp.staticize.common.IMetaData;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.exception.TemplateException;
import com.gitee.qdbp.staticize.publish.OutputPublisher;

/**
 * 任务线程
 * 
 * @author zhaohuihua
 * @version 140722
 */
class TaskThread extends Thread {

    /** 日志对象 **/
    private static final Logger log = LoggerFactory.getLogger(TaskThread.class);

    private final StaticizeTask pool;

    private final List<TaskData> failed;

    public TaskThread(StaticizeTask pool) {
        this.pool = pool;
        this.failed = new ArrayList<>();
    }

    /** {@inheritDoc} **/
    @Override
    public void run() {

        while (!pool.cancel) {
            TaskData data;
            while ((data = pool.pop()) != null) {
                publish(data, false);
            }

            if (pool.done) {
                break;
            }
            try {
                if (!pool.cancel) {
                    synchronized (this) {
                        log.info("线程休眠开始");
                        this.wait();
                        log.info("线程休眠结束");
                    }
                }
            } catch (InterruptedException | IllegalMonitorStateException e) {
                log.error("线程休眠失败", e);
            }
        }

        if (!pool.cancel) {
            // 重试失败数据
            for (TaskData data : failed) {
                publish(data, true);
            }
        }

        pool.stop(this);

        log.info("线程结束!");
    }

    private void publish(TaskData data, boolean retry) {
        String desc = "发布" + data.getName() + (retry ? "(重试), " : ", ");
        log.debug(desc + "开始...");

        String template = data.getTemplate();
        IMetaData tags;
        try {
            tags = pool.cache.get(template);
        } catch (TemplateException | TagException e) {
            if (!retry) {
                pool.failed++;
                failed.add(data);
            }
            log.error(desc + "模板解析失败: " + template, e);
            return;
        }

        try {
            Map<String, Object> preset = data.getPreset();
            String path = data.getOutput();
            OutputPublisher publisher = new OutputPublisher(tags);
            publisher.publish(preset, pool.output.create(path));
            log.info(desc + "执行成功: template=" + template + ", output=" + path);
        } catch (Exception e) {
            if (!retry) {
                pool.failed++;
                failed.add(data);
            }
            log.error(desc + "执行失败: " + data.getOutput(), e);
            return;
        }

        if (retry) {
            pool.failed--; // 重试成功
        }
        pool.finished++;
    }
}
