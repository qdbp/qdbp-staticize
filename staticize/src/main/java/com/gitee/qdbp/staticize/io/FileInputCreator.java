package com.gitee.qdbp.staticize.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Date;
import com.gitee.qdbp.staticize.common.IReader;
import com.gitee.qdbp.tools.files.FileTools;
import com.gitee.qdbp.tools.files.PathTools;
import com.gitee.qdbp.tools.utils.VerifyTools;

/**
 * 文件读取接口实现类
 * 
 * @author zhaohuihua
 * @version 140730
 */
public class FileInputCreator implements IReaderCreator {

    /** 文件根路径 **/
    private final String folder;

    private final Class<?>[] classes;

    /**
     * 文件读取接口构造函数
     * 
     * @param folder 根路径
     */
    public FileInputCreator(String folder, Class<?>... classes) {
        VerifyTools.requireNotBlank(folder, "folder");
        this.folder = folder;
        this.classes = classes;
    }

    /** {@inheritDoc} **/
    @Override
    public IReader create(String path) throws IOException {
        VerifyTools.requireNotBlank(path, "path");

        String fullpath = PathTools.concat(folder, path);
        URL url = PathTools.findResource(fullpath, classes);
        Charset charset = Charset.forName(FileTools.getEncoding(url.openStream()));
        Reader reader = new InputStreamReader(url.openStream(), charset);
        return new SimpleReader(path, reader);
    }

    /** {@inheritDoc} **/
    @Override
    public String getRelativePath(String path, String src) {
        // 处理导入模板的路径
        if (!src.startsWith("/") && !src.startsWith("\\")) {
            // 相对路径, 加上当前模板的上级路径
            src = PathTools.concat(PathTools.removeFileName(path), src);
        }
        return src;
    }

    /** {@inheritDoc} **/
    @Override
    public Date getUpdateTime(String path) {
        VerifyTools.requireNotBlank(path, "path");
        String fullpath = PathTools.concat(folder, path);
        long time = new File(fullpath).lastModified();
        return new Date(time);
    }
}
