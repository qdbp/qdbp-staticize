package com.gitee.qdbp.staticize.tags.core;

import java.util.Map;
import com.gitee.qdbp.staticize.annotation.AdjoinAfter;
import com.gitee.qdbp.staticize.annotation.AttrRequired;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.ITag;
import com.gitee.qdbp.staticize.tags.base.NextStep;

/**
 * when 标签处理类
 *
 * @author zhaohuihua
 * @version 20210424
 */
@AdjoinAfter({ WhenTag.class })
public class WhenTag extends BaseTag {

    /** 判断并列的WHEN标签有没有被执行过 **/
    static final String WHEN_TAG_EVAL_DONE = "WHEN_TAG_EVAL_DONE";

    /** 测试值 **/
    protected Boolean test;

    /**
     * 设置测试值
     * 
     * @param test 测试值
     */
    @AttrRequired
    public void setTest(Boolean test) {
        this.test = test == null ? false : test;
    }

    /** {@inheritDoc} **/
    public NextStep doHandle() throws TagException {
        ITag parent = this.parent();
        Map<String, Object> pstack = parent.stack();
        Boolean done = (Boolean) pstack.get(WHEN_TAG_EVAL_DONE);
        if (done == null) {
            done = false;
        }
        // 更新曾经执行状态
        pstack.put(WHEN_TAG_EVAL_DONE, done ? true : test);

        // executed=true曾经执行过, 本次不执行
        boolean eval = done ? false : test;
        if (eval) {
            return NextStep.EVAL_BODY;
        } else {
            return NextStep.SKIP_BODY;
        }
    }
}
