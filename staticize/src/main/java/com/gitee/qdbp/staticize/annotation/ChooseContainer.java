package com.gitee.qdbp.staticize.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import com.gitee.qdbp.staticize.tags.base.ITag;

/**
 * choose标签容器
 *
 * @author zhaohuihua
 * @version 20200926
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ChooseContainer {

    /** 允许哪些子标签 **/
    Class<? extends ITag>[] children();
}
