package com.gitee.qdbp.staticize.publish;

import java.io.IOException;
import java.util.Map;
import com.gitee.qdbp.staticize.common.IWriter;
import com.gitee.qdbp.staticize.common.IMetaData;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.io.FolderOutputCreator;
import com.gitee.qdbp.staticize.io.IWriterCreator;

/**
 * 根据标签数据发布一个页面, 输出到文件
 *
 * @author zhaohuihua
 * @version 140730
 */
public class FilePublisher {

    /** 发布器 **/
    private final OutputPublisher publisher;

    /** 输出流构建接口 **/
    private final IWriterCreator output;

    /**
     * 构造函数
     *
     * @param root 根节点
     * @param folder 发布页面的保存根路径, 生成的页面保存在这个路径下
     */
    public FilePublisher(IMetaData root, String folder) {
        this.publisher = new OutputPublisher(root);
        this.output = new FolderOutputCreator(folder);
    }

    /**
     * 根据标签数据发布一个页面
     *
     * @param path 页面路径(相对路径)
     * @throws TagException 标签错误
     * @throws IOException 写文件失败
     */
    public void publish(String path) throws TagException, IOException {
        publish(null, path);
    }

    /**
     * 根据标签数据发布一个页面
     *
     * @param preset 预置数据
     * @param path 页面路径(相对路径)
     * @throws TagException 标签错误
     * @throws IOException 写文件失败
     */
    public void publish(Map<String, Object> preset, String path) throws TagException, IOException {
        if (path == null || path.length() == 0) {
            throw new NullPointerException("path is empty");
        }

        // 创建输出流
        try (IWriter out = output.create(path)) {
            publisher.publish(preset, out);
        }
    }

}
