package com.gitee.qdbp.staticize.task;

/**
 * 静态化监听器
 *
 * @author zhaohuihua
 * @version 140809
 */
public interface StaticizeListener {

    /** 任务结束 **/
    void over();
}
