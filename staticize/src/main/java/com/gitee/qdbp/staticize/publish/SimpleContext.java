package com.gitee.qdbp.staticize.publish;

import com.gitee.qdbp.staticize.common.IWriter;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.utils.OgnlUtils;

/**
 * 上下文处理类
 *
 * @author zhaohuihua
 * @version 140523
 */
class SimpleContext extends BaseContext {

    public SimpleContext(IWriter writer) {
        super(writer);
    }

    /**
     * 根据表达式从环境变量中获取值<br>
     * ${xxx}, 从值栈环境变量中获取值, 如${title}, ${image.src}<br>
     * #{xxx}, 从预置环境变量中获取值, 如#{site.id}, #{column.title}<br>
     * &nbsp;&nbsp;#{site}=站点信息, <br>
     * &nbsp;&nbsp;#{column}=栏目信息<br>
     *
     * @param prefix 表达式前缀类型
     * @param expression 表达式
     * @return 环境变量中的对象
     */
    @Override
    protected Object doGetValue(String prefix, String expression) throws TagException {
        if ("#".equals(prefix)) {
            return OgnlUtils.evaluate(expression, true, preset());
        } else if ("$".equals(prefix)) {
            return OgnlUtils.evaluate(expression, true, stack());
        } else {
            throw new TagException("Expression prefix error.");
        }
    }
}
