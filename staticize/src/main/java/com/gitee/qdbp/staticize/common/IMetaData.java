package com.gitee.qdbp.staticize.common;

import java.util.List;
import java.util.Map;
import com.gitee.qdbp.staticize.tags.base.Taglib;

/**
 * 根节点
 *
 * @author zhaohuihua
 * @version 20201004
 */
public interface IMetaData extends BaseMetaData {

    /** 标签库 **/
    Taglib getTaglib();

    /** 将指定节点包装为根节点 (用于将一个模板拆分成多个模板) **/
    IMetaData wrapAsRoot(String tplId, String realPath, NodeMetaData node, List<NodeMetaData> commonNodes,
            Map<String, Object> attrs);
}
