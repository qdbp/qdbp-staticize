package com.gitee.qdbp.staticize.common;


public interface NodeMetaData extends BaseMetaData {

    /**
     * 获取开始标签
     *
     * @return 返回开始标签
     */
    String getOpenTag();

    /**
     * 获取结束标签
     *
     * @return 返回结束标签
     */
    String getCloseTag();
}
