package com.gitee.qdbp.staticize.tags.base;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.gitee.qdbp.staticize.common.IWriter;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.tools.utils.ConvertTools;
import com.gitee.qdbp.tools.utils.VerifyTools;

/**
 * 基础循环迭代处理类<br>
 * setItems()和getItems()设置为protected, 因为某些查询类标签的数据源是数据库<br>
 * 子类可以在initLoadData()中查询数据, 而不是由外部设置<br>
 * <br>
 * status=迭代状态变量名<br>
 * &nbsp;&nbsp;index: 当前迭代对象在集合中的索引(从0开始)<br>
 * &nbsp;&nbsp;count: 迭代次数计数(从1开始)<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;如 begin=20, end=29, step=3, 则index=[20,23,26,29], count=[1,2,3,4]<br>
 * &nbsp;&nbsp;begin: 开始行数<br>
 * &nbsp;&nbsp;rows: 显示行数<br>
 * &nbsp;&nbsp;end: 结束行数(begin+rows-1=end)<br>
 * &nbsp;&nbsp;step: 迭代步长<br>
 * &nbsp;&nbsp;limit: 迭代次数<br>
 * &nbsp;&nbsp;first: 是否为第一次迭代<br>
 * &nbsp;&nbsp;last: 是否为最后一次迭代<br>
 * <br>
 * items=进行迭代的集合<br>
 * 如果没有指定items, 则begin必须, rows/end/limit必填其一<br>
 * 如果指定了items, 又同时指定了begin和end/rows, 如果begin和end在0-items.size范围之外<br>
 * 将会自动根据step是正数还是负数进行调整<br>
 * 反向迭代, 可以不设置begin只设置step="-1"<br>
 * 
 * @author zhaohuihua
 * @version 140521
 */
public abstract class BaseEachTag<T extends IWriter> extends CachingTag<T> {

    protected BaseEachTag(T caching) {
        super(caching);
    }

    /** 保存迭代对象的变量名 **/
    private String var;
    /** 迭代状态的变量名 **/
    private String status;
    /** 保存迭代序号的变量名 **/
    private String index;
    /** 输出在同一行(非首次迭代时删除第一个文本节点前面的换行) **/
    private boolean inline;
    /** 开始行数(从0开始, 可为空, 默认=0) **/
    private Integer begin;
    /** 限制行数(可为空, 默认为不限制) **/
    private Integer rows;
    /** 结束行数(begin+rows-1=end) **/
    private Integer end;
    /** 迭代步长 **/
    private Integer step;
    /** 迭代次数限制 **/
    private Integer limit;
    /** 要进行迭代的集合 **/
    private List<Object> items;
    /** 有没有迭代集合, 如果没有, 则根据begin/rows/end/limit迭代 **/
    private boolean hasItems;
    /** 前置字符 **/
    private String prefix;
    /** 后置字符 **/
    private String suffix;
    /** 每次迭代后的分隔符 **/
    private String separator;
    /** 迭代状态 **/
    protected EachStatus es;
    /** 临时变量, 每次迭代都会清空 **/
    protected Map<String, Object> temp = new HashMap<>();

    /**
     * 字段校验<br>
     * items为空时, 属性items为空时, rows/end/limit必填其一
     * 
     * @param original 原始属性
     * @param parsed 解析后的属性
     * @throws TagException 校验未通过
     */
    public void doValidate(Map<String, Object> original, Map<String, Object> parsed) throws TagException {
        if (!original.containsKey("items")) {
            if (!original.containsKey("begin")) {
                throw new TagException("When 'items' is not set, 'begin' is required.");
            }
            if (!original.containsKey("rows") && !original.containsKey("end") && !original.containsKey("limit")) {
                throw new TagException("When items is not set, rows/end/limit must set one of them.");
            }
        }
    }

    /**
     * 初始化迭代状态
     * 
     * @return 迭代状态
     */
    protected EachStatus initEachStatus() {
        InnerEachStatus es = new InnerEachStatus();
        es.setBegin(this.begin);
        es.setRows(this.rows);
        es.setEnd(this.end);
        es.setStep(this.step);
        es.setLimit(this.limit);
        es.setItems(this.items);
        es.setHasItems(this.hasItems);
        es.init();
        return es;
    }

    /** 初始化导入数据 **/
    protected void initLoadData() throws IOException {
    }

    /** 每次迭代的处理 **/
    protected void handleEach(EachStatus status) throws IOException {
        if (VerifyTools.isNotBlank(this.getSeparator()) && !status.isFirst()) {
            this.print(this.getSeparator());
        }
    }

    /** 开始迭代前的处理 **/
    protected void beforeEach(EachStatus status) throws IOException {
        if (VerifyTools.isNotBlank(this.getPrefix())) {
            this.printToOrigin(this.getPrefix());
        }
    }

    /** 完成迭代后的处理 **/
    protected void afterEach(EachStatus status) throws IOException {
        if (VerifyTools.isNotBlank(this.getSuffix())) {
            this.printToOrigin(this.getSuffix());
        }
    }

    /**
     * 循环迭代处理
     *
     * @return 下一步处理方式
     */
    @Override
    public NextStep doHandle() throws TagException, IOException {
        this.temp.clear();
        if (this.es == null) {
            this.initLoadData();
            this.es = this.initEachStatus();
            // 判断初始状态下能否迭代
            if (this.es == null || !this.es.iterable()) {
                return NextStep.SKIP_BODY;
            }
            this.beforeEach(this.es);
        } else {
            if (this.es.hasNext()) {
                this.es.next();
            } else {
                this.afterEach(this.es);
                return NextStep.SKIP_BODY;
            }
        }

        if (this.var != null) {
            this.addStackValue(this.var, this.es.getItem());
        }
        if (this.status != null) {
            this.addStackValue(this.status, this.es);
        }
        if (this.index != null) {
            this.addStackValue(this.index, this.es.getIndex());
        }
        this.handleEach(this.es);
        return NextStep.LOOP_BODY;
    }

    /**
     * 设置迭代对象的变量名
     * 
     * @param var 迭代对象的变量名
     */
    protected void setVar(String var) {
        this.var = var;
    }

    /**
     * 获取保存迭代对象的变量名
     * 
     * @return 保存迭代对象的变量名
     */
    protected String getVar() {
        return var;
    }

    /**
     * 设置保存迭代状态的变量名
     * 
     * @param status 迭代状态的变量名
     */
    protected void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取保存迭代状态的变量名
     * 
     * @return 迭代状态的变量名
     */
    protected String getStatus() {
        return status;
    }

    /**
     * 获取保存迭代序号的变量名
     * 
     * @return 保存迭代序号的变量名
     */
    protected String getIndex() {
        return index;
    }

    /**
     * 设置保存迭代序号的变量名
     * 
     * @param index 保存迭代序号的变量名
     */
    protected void setIndex(String index) {
        this.index = index;
    }

    /**
     * 设置开始行数(从0开始)
     * 
     * @param begin 开始行数
     */
    protected void setBegin(Integer begin) {
        this.begin = begin; // 可以为空, 可以小于0
    }

    /**
     * 获取开始行数(从0开始)
     * 
     * @return 开始行数
     */
    protected Integer getBegin() {
        return begin;
    }

    /**
     * 设置结束行数(begin+rows-1=end)
     * 
     * @param end 结束行数
     */
    protected void setEnd(Integer end) {
        this.end = end; // 可以为空, 可以小于0
    }

    /**
     * 获取结束行数(begin+rows-1=end)
     * 
     * @return 结束行数
     */
    protected Integer getEnd() {
        return end;
    }

    /**
     * 设置限制行数(可为空, 默认为不限制)
     * 
     * @param rows 限制行数
     */
    protected void setRows(Integer rows) {
        this.rows = rows;
    }

    /**
     * 获取限制行数(可为空, 默认为不限制)
     * 
     * @return 限制行数
     */
    protected Integer getRows() {
        return rows;
    }

    /**
     * 设置迭代步长
     * 
     * @param step 迭代步长
     */
    protected void setStep(Integer step) {
        this.step = step; // 可以为空, 可以小于0
    }

    /**
     * 获取迭代步长
     * 
     * @return 迭代步长
     */
    protected Integer getStep() {
        return step;
    }

    /**
     * 设置迭代次数限制
     * 
     * @param limit 迭代次数限制
     */
    protected void setLimit(Integer limit) {
        this.limit = limit;
    }

    /**
     * 获取迭代次数限制
     * 
     * @return 迭代次数限制
     */
    protected Integer getLimit() {
        return limit;
    }

    protected EachStatus getEashStatus() {
        return this.es;
    }

    /**
     * 设置是否输出在同一行(非首次迭代时删除第一个文本节点前面的换行)
     * 
     * @param inline 是否输出在同一行
     */
    protected void setInline(Boolean inline) {
        if (inline == null) {
            this.inline = false;
        } else {
            this.inline = inline;
        }
    }

    /**
     * 判断是否输出在同一行
     * 
     * @return 是否输出在同一行
     */
    protected boolean isInline() {
        return this.inline;
    }

    /**
     * 设置迭代集合
     * 
     * @param items 要进行迭代的集合
     */
    protected void setItems(Object items) {
        this.hasItems = true;
        this.items = ConvertTools.parseList(items);
    }

    /**
     * 获取迭代集合
     * 
     * @return 要进行迭代的集合
     */
    protected List<Object> getItems() {
        return items;
    }

    /**
     * 判断有没有迭代集合, 如果没有, 则根据begin/rows/end/limit迭代
     * 
     * @return 有没有迭代集合
     */
    protected boolean hasItems() {
        return hasItems;
    }

    /**
     * 获取前置字符
     * 
     * @return 前置字符
     */
    protected String getPrefix() {
        return prefix;
    }

    /**
     * 设置前置字符
     * 
     * @param prefix 前置字符
     */
    protected void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    /**
     * 获取后置字符
     * 
     * @return 后置字符
     */
    protected String getSuffix() {
        return suffix;
    }

    /**
     * 设置后置字符
     * 
     * @param suffix 后置字符
     */
    protected void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    /**
     * 获取每次迭代后的分隔符
     * 
     * @return 分隔符
     */
    protected String getSeparator() {
        return separator;
    }

    /**
     * 设置每次迭代后的分隔符
     * 
     * @param separator 分隔符
     */
    protected void setSeparator(String separator) {
        this.separator = separator;
    }

    protected static class InnerEachStatus implements EachStatus {

        /** 开始行数(从0开始, 可为空, 默认=0) **/
        private Integer begin = 0;
        /** 显示行数(可为空, 默认为不限制) **/
        private Integer rows;
        /** 结束行数(begin+rows-1=end) **/
        private Integer end;
        /** 迭代步长 **/
        private Integer step = 1;
        /** 迭代次数限制 **/
        private Integer limit;
        /** 要进行迭代的集合 **/
        private List<Object> items;
        /** 有没有迭代集合, 如果没有, 则根据begin/rows/end/limit迭代 **/
        private boolean hasItems;
        /** 当前迭代对象在集合中的索引(从0开始) **/
        private int index;
        /** 迭代次数计数(从1开始) **/
        private int count;
        /** 判断能否迭代, 如果不能迭代则一次都不会触发, 也不会执行beforeEach/endEach **/
        private boolean iterable = true;
        /** 是否为首次迭代 **/
        private boolean first = true;

        protected void init() {
            if (this.rows != null && this.rows < 0) {
                // 属性rows不能小于0
                // rows should be greater then 0.
                throw new TagException("Attribute rows cannot be less than 0.");
            }
            if (this.rows != null && this.rows == 0) {
                this.iterable = false; // 不可迭代, 直接退出
                return;
            }
            // 判断迭代次数限制
            if (this.limit != null && this.limit <= 0) {
                this.iterable = false; // 不可迭代, 直接退出
                return;
            }
            if (this.hasItems) { // 有items
                // items为空
                if (this.items == null || this.items.isEmpty()) {
                    this.iterable = false; // 不可迭代, 直接退出
                    return;
                }
            } else { // 没有items
                if (this.begin == null) {
                    throw new TagException("When items is not set, begin is required.");
                }
                if (this.rows == null && this.end == null && this.limit == null) {
                    throw new TagException("When items is not set, rows/end/limit must set one of them.");
                }
            }

            // 未设置步长或步长为0
            if (this.step == null || this.step == 0) {
                if (this.hasItems) {
                    if (this.begin == null || this.begin < 0) {
                        this.begin = 0;
                    }
                }
                // rows只在未设置end时才能用到
                if (this.end == null && this.rows != null) {
                    this.end = this.begin + this.rows - 1;
                }
                if (this.end == null) {
                    this.step = 1;
                } else {
                    // 自动计算步长, begin > end, 则步长设为负数
                    this.step = this.begin > this.end ? -1 : 1;
                }
                if (this.hasItems) {
                    if (this.end == null || this.end > this.items.size() - 1) {
                        this.end = this.items.size() - 1;
                    }
                }
            } else { // 设置了step
                if (this.step < 0) { // 递减
                    if (this.hasItems) {
                        if (this.begin == null || this.begin > this.items.size() - 1) {
                            this.begin = this.items.size() - 1;
                        }
                    }
                    // rows只在未设置end时才能用到
                    if (this.end == null && this.rows != null) {
                        this.end = this.begin - this.rows + 1;
                    }
                    if (this.hasItems) {
                        if (this.end == null || this.end < 0) {
                            this.end = 0;
                        }
                    }
                } else { // 递增
                    if (this.hasItems) {
                        if (this.begin == null || this.begin < 0) {
                            this.begin = 0;
                        }
                    }
                    // rows只在未设置end时才能用到
                    if (this.end == null && this.rows != null) {
                        this.end = this.begin + this.rows - 1;
                    }
                    if (this.hasItems) {
                        if (this.end == null || this.end > this.items.size() - 1) {
                            this.end = this.items.size() - 1;
                        }
                    }
                }
            }

            this.index = this.begin;
            this.count = 1;

            if (this.end == null) {
                // end为空只有一种情况, 就是有limit, 最开始已经处理过limit了
            } else {
                if (this.step < 0) { // 递减
                    if (this.begin < this.end) {
                        this.iterable = false;
                    }
                } else { // 递增
                    if (this.begin > this.end) {
                        this.iterable = false;
                    }
                }
            }
        }

        @Override
        public boolean hasNext() {
            if (this.limit != null && this.count >= this.limit) {
                return false;
            }
            if (this.end != null) {
                if (this.step < 0) {
                    return this.index + this.step >= this.end;
                } else {
                    return this.index + this.step <= this.end;
                }
            }
            if (!this.hasItems) {
                // 没有items又没有end, 则由limit控制迭代次数
                // 因此初始化时需要判断, 没有items时, rows/end/limit必填其一(rows会转换为end)
                // 否则就会出现死循环
                return true;
            } else {
                if (this.step < 0) {
                    if (this.begin < 0) {
                        // begin<0且step<0, 就是一直向下减, 永远到不了0
                        return false;
                    }
                    return this.index + this.step >= 0;
                } else {
                    int size = this.items == null ? 0 : this.items.size();
                    if (this.begin >= size) {
                        // begin>size且step>0, 就是一直在size的后面递增, 永远到不了size
                        return false;
                    }
                    return this.index + this.step <= size - 1;
                }
            }
        }

        @Override
        public Object next() {
            this.first = false;
            this.count++;
            // step不能等于0, 否则会出现死循环
            this.index = this.index + (this.step == 0 ? 1 : this.step);
            return getItem();
        }

        @Override
        public Object getItem() {
            if (!this.hasItems) {
                return this.index;
            } else if (this.items == null || this.items.isEmpty()) {
                return null;
            } else if (this.index < 0 || this.index >= this.items.size()) {
                return null;
            } else {
                return this.items.get(this.index);
            }
        }

        /**
         * 判断能否迭代, 如果不能迭代则一次都不会触发, 也不会执行beforeEach/endEach
         * 
         * @return 能否迭代
         */
        @Override
        public boolean iterable() {
            return this.iterable;
        }

        @Override
        public boolean isFirst() {
            return first;
        }

        @Override
        public boolean isLast() {
            return !hasNext();
        }

        /**
         * 设置开始行数(从0开始)
         * 
         * @param begin 开始行数
         */
        public void setBegin(Integer begin) {
            this.begin = begin; // 可以为空, 可以小于0
        }

        /**
         * 获取开始行数(从0开始)
         * 
         * @return 开始行数
         */
        public Integer getBegin() {
            return begin;
        }

        /**
         * 设置结束行数(begin+rows-1=end)
         * 
         * @param end 结束行数
         */
        public void setEnd(Integer end) {
            this.end = end; // 可以为空, 可以小于0
        }

        /**
         * 获取结束行数(begin+rows-1=end)
         * 
         * @return 结束行数
         */
        public Integer getEnd() {
            return end;
        }

        /**
         * 设置显示行数(可为空, 默认为不限制)
         * 
         * @param rows 显示行数
         */
        public void setRows(Integer rows) {
            this.rows = rows;
        }

        /**
         * 获取显示行数(可为空, 默认为不限制)
         * 
         * @return 显示行数
         */
        public Integer getRows() {
            return rows;
        }

        /**
         * 设置迭代步长
         * 
         * @param step 迭代步长
         */
        public void setStep(Integer step) {
            this.step = step; // 可以为空, 可以小于0
        }

        /**
         * 获取迭代步长
         * 
         * @return 迭代步长
         */
        public Integer getStep() {
            return step;
        }

        /**
         * 设置迭代次数限制
         * 
         * @param limit 迭代次数限制
         */
        public void setLimit(Integer limit) {
            this.limit = limit;
        }

        /**
         * 获取迭代次数限制
         * 
         * @return 迭代次数限制
         */
        public Integer getLimit() {
            return limit;
        }

        /**
         * 设置迭代集合
         * 
         * @param items 要进行迭代的集合
         */
        protected void setItems(List<Object> items) {
            this.items = items;
        }

        /**
         * 获取迭代集合
         * 
         * @return 要进行迭代的集合
         */
        protected List<Object> getItems() {
            return items;
        }

        /**
         * 设置有没有迭代集合, 如果没有, 则根据begin/rows/end/limit迭代
         * 
         * @param hasItems 有没有迭代集合
         */
        protected void setHasItems(boolean hasItems) {
            this.hasItems = hasItems;
        }

        /**
         * 判断有没有迭代集合, 如果没有, 则根据begin/rows/end/limit迭代
         * 
         * @return 有没有迭代集合
         */
        protected boolean hasItems() {
            return hasItems;
        }

        @Override
        public Integer getIndex() {
            return index;
        }

        @Override
        public Integer getCount() {
            return count;
        }

    }
}
