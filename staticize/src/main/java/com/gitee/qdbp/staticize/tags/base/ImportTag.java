package com.gitee.qdbp.staticize.tags.base;

import java.io.IOException;
import com.gitee.qdbp.staticize.common.IWriter;
import com.gitee.qdbp.staticize.exception.TagException;

/**
 * 导入类的标签<br>
 * &lt;import&gt;com.qdbp.xxx.EntityTools&lt;import&gt;
 *
 * @author zhaohuihua
 * @version 20200906
 */
public class ImportTag extends StringCachingTag {

    public NextStep doHandle() throws TagException {
        return NextStep.EVAL_BODY;
    }

    @Override
    protected void doEnded(String content, IWriter writer) throws TagException, IOException {
        if (content == null || content.trim().length() == 0) {
            // 标签体不能为空
            throw new TagException("Tag body can not be empty.");
        }
        this.addImportClass(content.trim());
    }
}
