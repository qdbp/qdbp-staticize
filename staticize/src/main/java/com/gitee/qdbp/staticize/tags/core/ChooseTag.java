package com.gitee.qdbp.staticize.tags.core;

import java.io.IOException;
import com.gitee.qdbp.staticize.annotation.ChooseContainer;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.NextStep;

/**
 * choose 标签处理类, 什么也不干, 只作为when和otherwise和容器
 *
 * @author zhaohuihua
 * @version 20210424
 */
@ChooseContainer(children = { WhenTag.class, OtherwiseTag.class })
public class ChooseTag extends BaseTag {

    @Override
    public NextStep doHandle() throws IOException {
        return NextStep.EVAL_BODY;
    }
}
