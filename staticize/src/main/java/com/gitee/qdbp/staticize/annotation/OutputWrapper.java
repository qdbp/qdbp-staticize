package com.gitee.qdbp.staticize.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标记输出容器<br>
 * 标签分三类: <br>
 * 1.纯容器, 如if/choose等<br>
 * 2.纯输出, 如fmt-date等<br>
 * 3.有子标签, 但是由父标签承担输出任务, 如 include
 *
 * @author zhaohuihua
 * @version 20210914
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface OutputWrapper {
}
