package com.gitee.qdbp.staticize.tags.base;

/**
 * 标签实例工厂类
 *
 * @author zhaohuihua
 * @version 20210701
 */
public interface TagFactory {

    <T extends ITag> T newTagInstance(Class<T> clazz);
}
