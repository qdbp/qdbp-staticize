package com.gitee.qdbp.staticize.common;

import java.util.Map;

/**
 * 值栈容器, 用于处理值栈本身的操作<br>
 * 该类可以在业务侧自定义, 必须有Map入参的构造函数<br>
 * 在taglib中, 通过@StackWrapper=com.gitee.qdbp.able.model.reusable.StackWrapper自定义<br>
 * 该类的所有方法可以在el表达式中以@形式调用, 如@contains('user.account')<br>
 * 在BaseTag.refreshStack方法将stack用StackWrapper封装保存到stack中<br>
 *
 * @author zhaohuihua
 * @version 20210422
 * @deprecated moved to com.gitee.qdbp.able.model.reusable.StackWrapper
 */
@Deprecated
public class StackWrapper extends com.gitee.qdbp.able.model.reusable.StackWrapper {

    public StackWrapper(Map<String, Object> map) {
        super(map);
    }
}
