package com.gitee.qdbp.staticize.parse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.gitee.qdbp.staticize.common.NodeMetaData;
import com.gitee.qdbp.staticize.tags.base.ITag;

/**
 * 标签数据类
 *
 * @author zhaohuihua
 * @version 140520
 */
public class NodeMetaDataImpl implements NodeMetaData {

    /** 标签名称, 如: each, CDATA **/
    protected final String name;
    /** 标签打开标记, 如: &lt;each&gt;, &lt;[CDATA[[ **/
    protected final String openTag;
    /** 标签关闭标记, 如: &lt;/each&gt;, ]]&gt; **/
    protected final String closeTag;

    /** 标签处理类 **/
    protected final Class<? extends ITag> clazz;
    /** 模板ID(相对路径) **/
    protected String tpl;
    /** 模板文件实际路径 **/
    protected String path;
    /** 标签所在的行数 **/
    protected final int row;

    /** 标签所在的列数 **/
    protected final int column;
    /** 父节点 **/
    private NodeMetaDataImpl parent;
    /** 子节点 **/
    private final List<NodeMetaDataImpl> children;
    /** 标签属性 **/
    private final List<AttrData> attributes;

    /**
     * 构造函数
     *
     * @param clazz 标签处理类
     * @param name 标签名称
     * @param tpl 模板ID(文件相对路径)
     * @param path 模板文件实际路径
     * @param row 标签所在的行数
     * @param column 标签所在的列数
     */
    protected NodeMetaDataImpl(Class<? extends ITag> clazz, String name, String tpl, String path, int row, int column) {
        this(clazz, name, "<" + name + ">", "</" + name + ">", tpl, path, row, column);
    }

    /**
     * 构造函数
     *
     * @param clazz 标签处理类
     * @param name 标签名称
     * @param openTag 标签打开标记
     * @param closeTag 标签关闭标记
     * @param tpl 模板ID(文件相对路径)
     * @param path 模板文件实际路径
     * @param row 标签所在的行数
     * @param column 标签所在的列数
     */
    protected NodeMetaDataImpl(Class<? extends ITag> clazz, String name, String openTag, String closeTag, String tpl,
            String path, int row, int column) {
        this.tpl = tpl;
        this.path = path;
        this.name = name;
        this.openTag = openTag;
        this.closeTag = closeTag;
        this.clazz = clazz;
        this.row = row;
        this.column = column;
        this.children = new ArrayList<>();
        this.attributes = new ArrayList<>();
    }

    /**
     * 获取模板ID
     *
     * @return 返回模板ID
     */
    @Override
    public String getTplId() {
        return tpl;
    }

    /**
     * 获取模板文件路径
     *
     * @return 返回模板文件路径
     */
    @Override
    public String getRealPath() {
        return path;
    }

    /**
     * 递归设置tplId和realPath
     * 
     * @param tplId 模板ID
     * @param realPath 模板文件路径
     */
    void resetTplIdAndPath(String tplId, String realPath) {
        this.tpl = tplId;
        this.path = realPath;
        if (this.children != null) {
            for (NodeMetaDataImpl child : this.children) {
                child.resetTplIdAndPath(tplId, realPath);
            }
        }
    }

    /**
     * 取得标签所在的行数
     *
     * @return 返回标签所在的行数
     */
    @Override
    public int getRow() {
        return row;
    }

    /**
     * 取得标签所在的列数
     *
     * @return 返回标签所在的列数
     */
    @Override
    public int getColumn() {
        return column;
    }

    /**
     * 取得标签名称
     *
     * @return 返回标签名称
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * 取得标签打开标记
     *
     * @return 返回标签打开标记
     */
    @Override
    public String getOpenTag() {
        return openTag;
    }

    /**
     * 取得标签关闭标记
     *
     * @return 返回标签关闭标记
     */
    @Override
    public String getCloseTag() {
        return closeTag;
    }

    /**
     * 取得标签处理类
     *
     * @return 返回标签处理类
     */
    @Override
    public Class<? extends ITag> getTagClass() {
        return clazz;
    }

    /**
     * 取得标签的所有属性
     *
     * @return 属性列表
     */
    @Override
    public List<AttrData> getAttributes() {
        return attributes;
    }

    /**
     * 取得标签的指定属性
     *
     * @param attrName 属性名称
     * @return 属性对象
     */
    @Override
    public AttrData getAttribute(String attrName) {
        for (AttrData attr : attributes) {
            if (attrName.equals(attr.getKey())) {
                return attr;
            }
        }
        return null;
    }

    /**
     * 判断标签是否存在指定的属性
     *
     * @param attrName 属性名称
     * @return 是否存在
     */
    @Override
    public boolean hasAttribute(String attrName) {
        for (AttrData attr : attributes) {
            if (attrName.equals(attr.getKey())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 取得标签的指定属性值
     *
     * @param attrName 属性名称
     * @param clazz 属性值类型
     * @return 属性对象
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T> T getAttributeValue(String attrName, Class<T> clazz) {
        for (AttrData attr : attributes) {
            if (attrName.equals(attr.getKey())) {
                return (T) attr.getValue();
            }
        }
        return null;
    }

    /**
     * 重置属性列表
     * 
     * @param attributes 属性列表
     */
    void resetAttributes(List<AttrData> attributes) {
        this.attributes.clear();
        this.attributes.addAll(attributes);
    }

    /**
     * 增加标签属性
     *
     * @param attr 属性
     */
    void addAttribute(AttrData attr) {
        attributes.add(attr);
    }

    /**
     * 增加标签属性
     *
     * @param field 属性名
     * @param value 属性值
     */
    void addAttribute(String field, Object value) {
        attributes.add(new AttrData(field, value));
    }

    /**
     * 删除标签的指定属性
     *
     * @param attrName 属性名称
     * @return 删除掉的属性对象
     */
    AttrData delAttribute(String attrName) {
        Iterator<AttrData> iterator = attributes.iterator();
        while (iterator.hasNext()) {
            AttrData attr = iterator.next();
            if (attrName.equals(attr.getKey())) {
                iterator.remove();
                return attr;
            }
        }
        return null;
    }

    /**
     * 取得子节点
     *
     * @return 返回子节点
     */
    public List<NodeMetaDataImpl> children() {
        return children;
    }

    /**
     * 取得子节点
     *
     * @return 返回子节点
     */
    @Override
    public List<NodeMetaData> getChildren() {
        List<NodeMetaData> list = new ArrayList<>();
        list.addAll(children);
        return list;
    }

    /**
     * 重置子节点列表
     * 
     * @param children 子节点列表
     */
    void resetChildren(List<NodeMetaDataImpl> children) {
        this.children.clear();
        this.children.addAll(children);
    }

    /**
     * 在后面增加子节点
     *
     * @param child 子节点
     */
    void appendChild(NodeMetaDataImpl child) {
        child.parent = this;
        children.add(child);
    }

    /**
     * 在前面增加子节点
     *
     * @param child 子节点
     */
    void prependChild(NodeMetaDataImpl child) {
        child.parent = this;
        children.add(0, child);
    }

    /**
     * 判断有没有子节点
     *
     * @return 有没有子节点
     */
    boolean hasChildren() {
        return !children.isEmpty();
    }

    /**
     * 设置父节点
     *
     * @param parent 父节点
     */
    void setParent(NodeMetaDataImpl parent) {
        this.parent = parent;
    }

    /**
     * 获取父节点
     *
     * @return parent 父节点
     */
    NodeMetaDataImpl getParent() {
        return parent;
    }

    @Override
    public String toString() {
        return this.name == null ? "text" : this.name;
    }
}
