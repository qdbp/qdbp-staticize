package com.gitee.qdbp.staticize.io;

import java.io.IOException;
import java.util.Date;
import com.gitee.qdbp.able.exception.ResourceNotFoundException;
import com.gitee.qdbp.able.model.reusable.Temporary;
import com.gitee.qdbp.staticize.common.IReader;
import com.gitee.qdbp.tools.utils.RandomTools;

/**
 * 临时ReaderCreator
 *
 * @author zhaohuihua
 * @version 20210519
 */
public class TemporaryReaderCreator implements IReaderCreator, Temporary {

    private final String uuid;
    private final IReader reader;

    public TemporaryReaderCreator(String string) {
        this.uuid = RandomTools.generateUuid();
        this.reader = new SimpleReader(uuid, string);
    }

    public String getFilePath() {
        return uuid;
    }

    @Override
    public IReader create(String filePath) throws IOException, ResourceNotFoundException {
        if (this.uuid.equals(filePath)) {
            return reader;
        } else {
            throw new ResourceNotFoundException(filePath + " not found");
        }
    }

    @Override
    public String getRelativePath(String filePath, String newPath) {
        return newPath;
    }

    @Override
    public Date getUpdateTime(String path) {
        return null;
    }
}
