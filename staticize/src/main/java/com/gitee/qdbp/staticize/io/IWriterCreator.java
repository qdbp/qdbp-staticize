package com.gitee.qdbp.staticize.io;

import java.io.IOException;
import com.gitee.qdbp.staticize.common.IWriter;

/**
 * 存储方式的构建接口
 * 
 * @author zhaohuihua
 * @version 140730
 */
public interface IWriterCreator {

    /**
     * 根据文件路径创建存储接口
     * 
     * @author zhaohuihua
     * @param path 文件路径
     * @return 存储接口
     */
    IWriter create(String path) throws IOException;
}
