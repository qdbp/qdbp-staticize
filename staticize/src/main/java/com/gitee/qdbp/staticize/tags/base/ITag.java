package com.gitee.qdbp.staticize.tags.base;

import java.io.IOException;
import java.util.Map;
import com.gitee.qdbp.staticize.common.IContext;
import com.gitee.qdbp.staticize.common.NodeMetaData;
import com.gitee.qdbp.staticize.exception.TagException;

/**
 * 标签处理类
 *
 * @author zhaohuihua
 * @version 140520
 */
public interface ITag {

    /**
     * 标签内容处理方法
     *
     * @return 下一步处理方式
     * @throws TagException 标签处理异常
     * @throws IOException 输出处理异常
     */
    NextStep doHandle() throws TagException, IOException;

    /**
     * 标签结束处理方法
     *
     * @param next doHandle()方法返回的下一步处理方式
     * @throws TagException 标签处理异常
     * @throws IOException 输出处理异常
     */
    void doEnded(NextStep next) throws TagException, IOException;

    /**
     * 计算字段值, 如果是ExpItem或ExpItems将进行计算, 其他值原样返回
     * 
     * @param field 字段名
     * @param value 字段值: ExpItem/ExpItems/其他
     * @return 转换后的字段值
     * @throws TagException 计算出错
     */
    Object doEvalValue(String field, Object value) throws TagException;

    /**
     * 设置基础数据
     *
     * @param parent 父标签
     * @param context 上下文信息
     */
    void doSetBaseData(ITag parent, IContext context);

    /**
     * 字段校验方法, 这里只用于校验各个字段之间的关联关系<br>
     * 如手机号码/电话号码不能同时为空<br>
     * 不要在这个方法做必填字段/字段值类型的校验<br>
     * 必填字段通过在方法上添加AttrRequired注解进行校验<br>
     * 字段值类型在解析时由doEvalValue转换为对应值的方式校验<br>
     *
     * @param metadata 标签元数据
     * @throws TagException 校验未通过
     */
    void doValidate(NodeMetaData metadata) throws TagException;

    /**
     * 获取父标签
     *
     * @return 父标签
     */
    ITag parent();

    /**
     * 获取根节点
     *
     * @return 根节点
     */
    ITag root();

    /**
     * 获取值栈容器
     * 
     * @author zhaohuihua
     * @return 值栈容器
     */
    Map<String, Object> stack();
}
