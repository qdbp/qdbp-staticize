package com.gitee.qdbp.staticize.utils;

import java.util.Map;
import com.gitee.qdbp.able.model.reusable.ExpressionExecutor;
import com.gitee.qdbp.staticize.tags.base.Taglib;

/**
 * Ognl计算处理类<br>
 * 支持静态方法引用; 支持自定义函数<br>
 * 例子详见OgnlEvaluatorTest
 *
 * @author zhaohuihua
 * @version 20210808
 */
public class TaglibOgnlEvaluator extends ExpressionExecutor {

    public TaglibOgnlEvaluator(Taglib taglib) {
        // 获取全局引用静态类名
        addImportClasses(taglib.getGlobalImports());
        // 解析值栈函数
        // 如果方法的参数是字符串, 而表达式没有写引号, 则需要自动追加引号
        // 如将@xxx(user.name)替换为ThisStack.xxx('user.name')
        setStackWrapperConstructor(taglib.getStackWrapperConstructor());
        addStackFunctions(taglib.getStackFunctions());
    }

    @Override
    protected Object evaluate(String expression, Integer resultScale, Integer calcScale, Map<String, Object> root) {
        return OgnlUtils.doEvaluate(expression, resultScale, calcScale, root);
    }
}