package com.gitee.qdbp.staticize.parse;

/**
 * 表达式字符串<br>
 * 如果存放的是普通字符串, evaluatable=false; 如果存放的是表达式, evaluatable=true
 *
 * @author zhaohuihua
 * @version 20200930
 */
public class ExpItem {

    /** 表达式内容 **/
    private final String content;
    /** 是否需要计算 **/
    private final boolean evaluatable;

    public ExpItem(String content) {
        this(content, true);
    }

    public ExpItem(String content, boolean evaluatable) {
        this.content = content;
        this.evaluatable = evaluatable;
    }

    /** 表达式内容 **/
    public String getContent() {
        return content;
    }

    /** 是否需要计算 **/
    public boolean isEvaluatable() {
        return evaluatable;
    }

    @Override
    public String toString() {
        return content;
    }
}
