package com.gitee.qdbp.staticize.common;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.Taglib;

/**
 * 上下文处理接口
 * 
 * @author zhaohuihua
 * @version 140523
 */
public interface IContext {

    /**
     * 根据表达式从环境变量中获取值<br>
     * ${xxx}, 从值栈环境变量中获取值, 如${title}, ${image.src}<br>
     * #{xxx}, 从预置环境变量中获取值, 如#{site.id}, #{column.title}<br>
     * &nbsp;&nbsp;#{site}=站点信息, <br>
     * &nbsp;&nbsp;#{column}=栏目信息
     * 
     * @author zhaohuihua
     * @param expression 表达式
     * @return 环境变量中的值
     * @throws TagException 表达式错误, 表达式取值错误
     */
    Object getValue(String expression) throws TagException;

    /**
     * 获取预置环境变量容器, 通过#{xxx}取值
     * 
     * @return 预置环境变量容器
     */
    Map<String, Object> preset();

    /**
     * 获取值栈环境变量容器, 通过${xxx}取值
     * 
     * @return 值栈环境变量容器
     */
    Map<String, Object> stack();

    /**
     * 增加类的导入信息
     * 
     * @param classFullName 类名全称, 如: com.qdbp.xxx.EntityTools
     */
    void addImportClass(String classFullName) throws TagException;

    /**
     * 获取类的导入信息列表
     * 
     * @return 导入信息列表
     */
    Collection<String> getImportClasses();

    /**
     * 获取类的导入信息
     * 
     * @param shortName &#64;类名简称, 如: &#64;EntityTools
     * @return fullName 类名全称, 如: com.qdbp.xxx.EntityTools
     */
    String getImportClass(String shortName);

    /**
     * 判断是否存在类的导入信息
     * 
     * @param shortName &#64;类名简称, 如: &#64;EntityTools
     * @return 是否存在
     */
    boolean containsImportClass(String shortName);

    /**
     * 向Writer中写入内容
     * 
     * @param content 内容
     * @throws IOException 写入失败
     */
    void write(Object content) throws IOException;

    /**
     * 某些标签需要根据子标签内容决定处理方式<br>
     * 就需要将子标签内容保存到缓冲区, 而不是直接输出到目标Writer<br>
     * 可以在初始化时将context的writer替换为缓冲容器, 在标签结束时再替换回原Writer
     * 
     * @param writer 新的Writer
     * @return 原Writer
     */
    IWriter resetWriter(IWriter writer);

    /**
     * 获取标签库, 从而获得标签库中的一些配置信息
     * 
     * @return 标签库
     */
    Taglib getTaglib();
}
