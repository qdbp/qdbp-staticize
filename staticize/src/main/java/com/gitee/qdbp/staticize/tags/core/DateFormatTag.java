package com.gitee.qdbp.staticize.tags.core;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.gitee.qdbp.staticize.common.NodeMetaData;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.parse.AttrData;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.NextStep;

/**
 * 日期格式化处理类<br>
 * 如果指定了var, 则将格式化后的文本置入该变量, 否则将文本输出到页面<br>
 * 如果没有指定日期, 默认为当前时间<br>
 * 如果没有指定格式字符串, 默认为yyyy-MM-dd HH:mm:ss
 * 
 * @author zhaohuihua
 * @version 140521
 */
public class DateFormatTag extends BaseTag {

    /**
     * 预定义的日期格式
     *
     * @version 140604
     */
    private enum Format {

        /** 默认日期时间格式 */
        DEFAULT("yyyy-MM-dd HH:mm:ss"),

        /** 日期格式 */
        DATE("yyyy-MM-dd"),

        /** 时间格式 */
        TIME("HH:mm:ss");

        private final String pattern;

        Format(String pattern) {
            this.pattern = pattern;
        }
    }

    /** 保存格式化后的字符串的变量名 **/
    private String var;

    /** 要被格式化的日期 **/
    private Date value;

    /** 自定义格式字符串 **/
    private String pattern;

    /**
     * 格式化后的字符串的变量名
     * 
     * @param var 变量名
     */
    public void setVar(String var) {
        this.var = var;
    }

    /**
     * 要被格式化的日期
     * 
     * @param value 日期
     */
    public void setValue(Date value) throws TagException {
        // 参数空指针验证
        if (value == null) {
            throw new TagException("Attribute 'value' cannot be null.");
        }
        this.value = value;
    }

    /**
     * 自定义格式字符串
     * 
     * @param pattern 自定义格式字符串
     */
    public void setPattern(String pattern) throws TagException {
        // 参数空指针验证
        if (pattern == null) {
            throw new NullPointerException("Attribute 'pattern' cannot be null.");
        }

        // 判断参数是不是预定义的格式
        for (Format fmt : Format.values()) {
            if (fmt.name().toLowerCase().equals(pattern)) {
                this.pattern = fmt.pattern;
                return;
            }
        }

        // 不是预定义格式, 将参数设置为自定义格式
        this.pattern = pattern;
    }

    /** {@inheritDoc} **/
    @Override
    public void doValidate(NodeMetaData metadata) throws TagException {
        AttrData pattern = metadata.getAttribute("pattern");
        if (pattern != null && pattern.getValue() instanceof String) {
            // 判断字段值是不是在预定义的列表之中
            for (Format fmt : Format.values()) {
                if (fmt.name().toLowerCase().equals(pattern.getValue())) {
                    return;
                }
            }

            try {
                // 根据字段值创建SimpleDateFormat, 以检查字段值格式是不是正确
                new SimpleDateFormat(pattern.getValue().toString());
            } catch (IllegalArgumentException e) {
                throw new TagException("Attribute 'pattern' format error.", e);
            }
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @throws IOException
     */
    @Override
    public NextStep doHandle() throws IOException {
        Date date = value == null ? new Date() : value;
        String ptn = pattern == null ? Format.DEFAULT.pattern : pattern;

        DateFormat fmt = new SimpleDateFormat(ptn);
        String text = fmt.format(date);

        if (var == null) {
            print(text);
            return NextStep.SKIP_BODY;
        } else {
            this.addStackValue(var, text);
            return NextStep.EVAL_BODY;
        }
    }
}
