package com.gitee.qdbp.staticize.parse;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.gitee.qdbp.able.debug.Debugger;
import com.gitee.qdbp.able.exception.ResourceNotFoundException;
import com.gitee.qdbp.able.model.reusable.Temporary;
import com.gitee.qdbp.staticize.common.IMetaData;
import com.gitee.qdbp.staticize.common.IReader;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.exception.TemplateException;
import com.gitee.qdbp.staticize.io.IReaderCreator;
import com.gitee.qdbp.staticize.parse.NameCollector.TagType;
import com.gitee.qdbp.staticize.parse.TagParser.Options;
import com.gitee.qdbp.staticize.tags.base.ITag;
import com.gitee.qdbp.staticize.tags.base.IncludeTag;
import com.gitee.qdbp.staticize.tags.base.Taglib;
import com.gitee.qdbp.staticize.tags.core.BlockTag;
import com.gitee.qdbp.staticize.tags.core.CommentTag;
import com.gitee.qdbp.staticize.utils.TagUtils;
import com.gitee.qdbp.tools.files.PathTools;
import com.gitee.qdbp.tools.utils.CharTools;
import com.gitee.qdbp.tools.utils.ConvertTools;

/**
 * 标签解析器, 解析模板, 生成标签元数据树, 不支持多线程<br>
 * 关于转义符, 需要转义时在表达式前加斜杠\, 如home-\${root.name}-list, \#{today}<br>
 * 文本中的转义符是在解析时由TagParser在遇到$或#时判断前一个字符进行特殊处理的<br>
 * 否则类似表达式的文本中间的标签无法解析到: <br>
 * \#{item&lt;core:if test="expression"&gt;xxx&lt;/core:if&gt;}<br>
 * 属性中的转义符是在AttrCollector中调用ExpParser.parse()时处理的, <br>
 * &lt;channel:query name="\${item}-${i}"&gt;&lt;/channel:query&gt;<br>
 *
 * @author zhaohuihua
 * @version 140519
 */
class TagParse {

    /** JSP的include标签的正则表达式, &lt;%@ include file="xxx.jsp"%&gt; **/
    private static final Pattern JSP_INCLUDE =
            Pattern.compile("<%@\\s*include\\s+file\\s*=\\s*[\"']([^\"']+)[\"']\\s*%>");

    /** 缓存大小 (读取文件时一次读取多少) **/
    private final int bz;
    /** 标签库 **/
    protected final Taglib taglib;
    /** 模板读取接口 **/
    private final IReaderCreator input;
    /** 是否清除XML注释 **/
    private final boolean clearXmlComment;
    /** 是否解析JSP脚本 **/
    private final boolean parseJspScript;
    /** 调试信息收集接口 **/
    private final Debugger debugger;

    // 使用模板文件时, 一般tpl=path
    // 使用数据库或其他模板输入方式时, tpl=模板ID, path=错误提示时的路径标识
    /** 模板ID(相对路径) **/
    protected final String tpl;
    /** 模板路径(实际路径) **/
    protected String path;

    /** 当前处理的行数 **/
    protected int row;
    /** 当前处理的列数 **/
    protected int column;
    /** 标签容器 **/
    private TagBox box;
    /** 正在取CDATA内容 **/
    private boolean cdata = false;
    /** 正在取XML注释 **/
    private boolean xmlComment = false;
    /** 正在取JSP脚本内容 **/
    private boolean jspScript = false;
    /** 正在打开标签 (还没遇到属性) **/
    private NameCollector open = null;
    /** 正在关闭标签 **/
    private NameCollector close = null;
    /** 正在取标签属性 **/
    private AttrCollector attrs = null;
    /** 正在取表达式 **/
    private ExpCollector exp = null;

    /**
     * 标签解析器构造函数
     *
     * @param options 标签解析的选项
     */
    public TagParse(Taglib taglib, IReaderCreator input, Options options, String tpl) {
        this.bz = options.getBufferSize();
        this.taglib = taglib;
        this.input = input;
        this.clearXmlComment = options.isClearXmlComment();
        this.parseJspScript = options.isParseJspScript();
        this.debugger = options.getDebugger();
        this.tpl = PathTools.formatPath(tpl);
    }

    /** 使用origin的配置信息生成一个新的实例 **/
    private TagParse(TagParse origin, String tpl) {
        this.bz = origin.bz;
        this.taglib = origin.taglib;
        this.input = origin.input;
        this.clearXmlComment = origin.clearXmlComment;
        this.parseJspScript = origin.parseJspScript;
        this.debugger = origin.debugger;
        this.tpl = PathTools.formatPath(tpl);
    }

    /**
     * 解析模板标签
     *
     * @return 标签元数据
     * @throws TagException 解析失败
     * @throws TemplateException 模板不存在/模板读取失败
     */
    public IMetaData parse() throws TagException, TemplateException {
        return parseFile();
    }

    /**
     * 解析模板标签
     *
     * @return 标签元数据
     * @throws TagException 解析失败
     * @throws TemplateException 模板不存在/模板读取失败
     */
    private RootMetaDataImpl parseFile() throws TagException, TemplateException {
        IReader reader;
        try {
            reader = input.create(this.tpl);
        } catch (ResourceNotFoundException e) {
            e.prependMessage("Failed to read template file: [" + this.tpl + "]. ");
            throw e;
        } catch (IOException e) {
            throw new TemplateException("Failed to read template file: [" + this.tpl + "]", e);
        }
        this.path = reader.getRealPath();
        this.box = new TagBox(this);
        try {
            return parseInput(this.path, reader);
        } catch (TagException e) {
            if (!(input instanceof Temporary)) {
                e.prependMessage(this.path + " ");
            }
            throw e;
        } finally {
            try {
                reader.close();
            } catch (IOException ignore) {
            }
        }
    }

    /**
     * 解析模板标签(由于include标签, 一定要知道模板路径, 所以这个方法不能公开)
     *
     * @param realPath 模板路径
     * @param reader 模板内容读取接口
     * @return 标签元数据
     * @throws TagException 解析失败
     * @throws TemplateException 模板不存在/模板读取失败(include引用的模板)
     */
    private RootMetaDataImpl parseInput(String realPath, IReader reader) throws TagException, TemplateException {
        this.row = 1;
        this.column = 0;

        try {
            box.open();
            // 读取模板, 并分批解析
            int length;
            char[] buffer = new char[bz];
            char[] pending = null;
            while ((length = reader.read(buffer, 0, buffer.length)) > 0) {
                char[] realChars;
                int startIndex;
                int realLength;
                if (pending == null) { // 没有待处理内容
                    realChars = buffer;
                    startIndex = 0;
                    realLength = length;
                } else { // 有待处理内容
                    // 合并pending和buffer
                    realChars = new char[pending.length + length];
                    System.arraycopy(pending, 0, realChars, 0, pending.length);
                    System.arraycopy(buffer, 0, realChars, pending.length, length);
                    startIndex = pending.length;
                    realLength = realChars.length;
                }

                // 解析一批字符串
                int endIndex = parseChar(realChars, startIndex, realLength);
                // 判断有没有全部处理掉
                if (endIndex >= realLength) {
                    pending = null;
                } else {
                    // 未处理的部分保存在pending中, 与下次读取到的一起处理
                    pending = new char[realLength - endIndex];
                    System.arraycopy(realChars, endIndex, pending, 0, realLength - endIndex);
                }
            }

            // 将未处理的字符作为文本处理
            // 一批字符在<core:if test="${i}">abcd这个地方结束时
            // text就是后面的这个abcd
            if (pending != null) {
                box.addText(TagUtils.parseEscape(new String(pending)));
            }

            // 当前标签没有回到root, 说明有标签没有写结束标签
            if (box.curr != box.root) {
                // <core:if test="${i}">
                String fmt = "%s:%s, %s is not closed";
                String msg = String.format(fmt, box.curr.row, box.curr.column, box.curr.openTag);
                throw new TagException(msg);
            }

            box.close();
            return box.root;
        } catch (IOException e) {
            throw new TemplateException("Failed to read template file: [" + realPath + "]", e);
        }
    }

    /**
     * 解析模板中的字符
     *
     * @author zhaohuihua
     * @param buffer 新读取到的字符
     * @param start 从哪个位置开始处理
     * @param length 缓存长度
     * @throws TagException 标签解析失败
     * @throws TemplateException 导入其他模板时模板不存在或模板读取失败
     */
    private int parseChar(char[] buffer, int start, int length) throws TemplateException, TagException {
        // index=已经处理掉的字符到哪个索引值了, 从0开始
        // 从index到i之间的字符就是正在处理的
        int index = 0;
        // 每当index变动就记录位置
        // 用于还原未处理字符的行列位置, 和确定正在处理的字符的行列位置
        int irow = this.row;
        int icln = this.column;

        // 追加上次未处理掉的字符
        for (int i = start; i < length; i++) {
            char c = buffer[i];

            if (c == '\n') {
                // 换行了, 行数累加, 列数清零
                this.row++;
                // 从零开始, 因为当前字符是换行符, 还不属于下一行的第1个字符
                this.column = 0;
            } else {
                // 列数累加, TAB字符占4列
                column += c == '\t' ? 4 : 1;
            }

            if (jspScript) { // <% %>
                if (c != '>') {
                    continue;
                }
            }
            if (xmlComment && close == null) {
                // <!-- -->
                if (c == '-') {
                    close = new NameCollector(false, TagType.XML_COMMENT);
                }
            }
            if (cdata && close == null) {
                // <![CDATA[
                if (c == ']') {
                    // ]]>
                    close = new NameCollector(false, TagType.CDATA);
                }
            }

            if (open != null && attrs == null) { // 取开始标签
                if (open.isTagName(c)) {
                    open.parse(c);
                    continue;
                }
                String tagName = open.getName();
                if (tagName == null || tagName.length() == 0) {
                    open = null;
                } else if (open.matches(TagType.CDATA)) {
                    cdata = true;
                    box.newTag("CDATA", "<![CDATA[", "]]>", irow, icln, BlockTag.class);
                    box.open();
                    open = null;
                    index = i;
                    irow = this.row;
                    icln = this.column;
                    if (c == ']') {
                        close = new NameCollector(false, TagType.CDATA);
                        close.parse(c);
                    }
                    continue; // CDATA中的任何标签都不解析
                } else if (open.matches(TagType.XML_COMMENT)) {
                    xmlComment = true;
                    box.newTag("comment", "<!--", "-->", irow, icln, CommentTag.class);
                    box.open();
                    open = null;
                    index = i;
                    irow = this.row;
                    icln = this.column;
                    if (c == '-') {
                        close = new NameCollector(false, TagType.XML_COMMENT);
                        close.parse(c);
                    }
                    continue; // <!-- -->中的任何标签都不解析
                } else if (taglib.containsTag(tagName)) {
                    if (c != '>' && c != '/' && !CharTools.isBlank(c)) {
                        // <core:each! var="i" items="${list}">
                        String fmt = "%s:%s, <%s>, unexpected character %s";
                        String msg = String.format(fmt, row, column, tagName, c);
                        throw new TagException(msg);
                    }
                    box.newTag(tagName, "<" + tagName + ">", "</" + tagName + ">", irow, icln);
                    attrs = new AttrCollector();
                    index = i;
                    irow = this.row;
                    icln = this.column;
                } else {
                    open = null;
                }
            }

            if (attrs != null) { // 取属性
                try {
                    // 正在获取属性, 或者没有遇到结束符, 就继续当作属性处理
                    if (attrs.isOpen() || c != '>') {
                        attrs.parse(c);
                        // 属性值可能包含任何字符
                        // 不再执行后面的结束、<、>的判断, 直接跳到下一字符
                        continue;
                    }
                } catch (TagException e) {
                    // 输出已经取到的属性, 作为DEBUG的依据
                    for (AttrData item : attrs.getAttributes()) {
                        debug("\t{}=\"{}\"", item.getKey(), item.getValue());
                    }
                    e.prependMessage(String.format("%s:%s, %s, ", row, column, box.openTag));
                    throw e;
                }
            }

            if (close != null) { // 取结束标签
                if (close.isTagName(c)) {
                    close.parse(c);
                    continue;
                }
                String tagName = close.getName();
                if (close.matches(TagType.CDATA)) {
                    if (!cdata || c != '>') {
                        close = null; // 文本中包含 ]]>
                    } else {
                        // <![CDATA[ ]]>
                        int end = i - 2; // ]]2个字符, 当前字符是>
                        if (index < end) {
                            box.addText(TagUtils.substring(buffer, index, end, false));
                        }
                        cdata = false;
                        index = i;
                        irow = this.row;
                        icln = this.column;
                    }
                } else if (close.matches(TagType.XML_COMMENT)) {
                    if (!xmlComment || c != '>') {
                        close = null; // 文本中包含 -->
                    } else {
                        // <!-- -->
                        int end = i - 2; // --2个字符, 当前字符是>
                        if (index < end) {
                            box.addText(TagUtils.substring(buffer, index, end, true));
                        }
                        xmlComment = false;
                        index = i;
                        irow = this.row;
                        icln = this.column;
                    }
                } else if (tagName != null && taglib.containsTag(tagName)) {
                    if (c != '>') {
                        // <core:each var="i" items="${list}"></core:each!>
                        String fmt = "%s:%s, error character in the close tag: %s%s";
                        String msg = String.format(fmt, row, column, tagName, c);
                        throw new TagException(msg);
                    }
                    // 根节点下直接遇到结束标签
                    String endTag = "</" + tagName + ">";
                    if (!tagName.equals(box.name) && box.curr == box.root) {
                        // <core:if test="${i}"></core:if></core:each>
                        String fmt = "%s:%s, %s, the open tag not found";
                        String msg = String.format(fmt, row, icln, endTag);
                        throw new TagException(msg);
                    }
                    // 非根节点下结束标签与开始标签不匹配
                    if (!tagName.equals(box.name) && box.curr != box.root) {
                        // <core:each var="i" items="${list}"></core:if>
                        String fmt = "%s(%s:%s) does not match with %s(%s:%s)";
                        int orow = box.curr.row;
                        int ocln = box.curr.column;
                        String msg = String.format(fmt, box.openTag, orow, ocln, endTag, irow, icln);
                        throw new TagException(msg);
                    }
                } else {
                    close = null;
                }
            }

            if (exp != null) { // 取表达式
                // 表达式中间不允许换行
                if (exp.isStringStatus() || (c != '}' && !CharTools.isNewLine(c))) {
                    exp.parse(c);
                    continue;
                }
                if (c != '}') {
                    String fmt = "%s:%s, %s, the expression did not properly close, %s";
                    int orow = box.curr.row;
                    int ocln = box.curr.column;
                    String msg = String.format(fmt, orow, ocln, box.name, exp.getContent());
                    throw new TagException(msg);
                }
            }

            // 表达式标签开始
            if (c == '{') {
                // 当前字符是{, 上一字符是#$, 表示遇到一个表达式了
                char last = i > 0 ? buffer[i - 1] : 0;
                if (last == '#' || last == '$') {
                    // 上上一字符如果是\, 作为转义字符处理
                    if (i > 1 && buffer[i - 2] == '\\') {
                        // 处理斜杠\前面的所有字符, i - 2 跳过斜杠
                        if (index < i - 2) {
                            box.addText(TagUtils.substring(buffer, index, i - 2));
                        }
                        index = i - 1; // 从上一字符开始
                        irow = this.row;
                        icln = this.column + 1;
                    } else {
                        exp = new ExpCollector();
                        exp.parse(last);
                        exp.parse(c);
                        if (index < i - 1) {
                            box.addText(TagUtils.substring(buffer, index, i - 1));
                        }
                        // 从上一字符开始, 都要算在表达式内, 所以-1
                        index = i - 1;
                        irow = this.row;
                        icln = this.column - 1;
                    }
                }
            }
            // 表达式标签结束
            else if (c == '}') {
                // 正在取表达式, 遇到}, 表示表达式结束了
                if (exp != null) {
                    exp.parse(c);
                    // 创建一个表达式标签
                    box.newExpression(exp.getContent());
                    // 当前字符已处理完, index从下一字符开始, 所以+1
                    index = i + 1;
                    irow = this.row;
                    icln = this.column + 1;
                    exp = null;
                }
            }

            if (cdata || xmlComment) {
                // <![CDATA[ ]]> <!-- --> 中间的标签不作识别
                continue;
            }

            if (c == '%' && parseJspScript) {
                if (i > 0 && buffer[i - 1] == '<') {
                    box.flush();
                    jspScript = true;
                }
            } else if (c == '<') { // 遇到<就将index到i之间的字符处理掉
                if (index < i) {
                    box.addText(TagUtils.substring(buffer, index, i));
                }
                index = i;
                irow = this.row;
                icln = this.column;
                // 尝试打开标签
                open = new NameCollector(true, getTagTypes());
            } else if (c == '/') {
                if (i > 0 && buffer[i - 1] == '<') { // 上一个字符是<, 结束标签
                    close = new NameCollector(false, TagType.NORMAL);
                }
            } else if (c == '>') {
                // 正在取注释, 遇到%>, 表示注释结束了
                if (jspScript && i > 0 && buffer[i - 1] == '%') {
                    if (index < i + 1) {
                        String txt = TagUtils.substring(buffer, index, i + 1);
                        parseJspComment(txt, irow, icln);
                    }

                    jspScript = false;
                    // 直接调整index, 放弃中间的所有字符
                    // 当前字符已处理完, index从下一字符开始, 所以+1
                    index = i + 1;
                    irow = this.row;
                    icln = this.column + 1;
                }
                // 正在准备打开标签, 遇到/>, 就是无标签体
                // 如果一批字符正好在<fmt:date/这个地方结束
                // 那么这个/会作为未处理字符遗留到下一个循环
                // 所以一定不会出现i==0且上一字符=/的情况
                else if (open != null && i > 0 && buffer[i - 1] == '/') {
                    if (attrs != null) { // 取标签属性
                        box.addAttributes(attrs.getAttributes());
                        attrs = null;
                    }

                    NodeMetaDataImpl data = box.curr;
                    // 关闭标签, 重置变量
                    box.close();
                    // 无标签体, 在标签结束后检查当前标签是否为include, 如果是则执行include
                    include(data);
                    open = null;
                    close = null;
                    // 当前字符已处理完, index从下一字符开始, 所以+1
                    index = i + 1;
                    irow = this.row;
                    icln = this.column + 1;
                }
                // 正在取标签属性, 遇到>, 表示标签已打开
                else if (attrs != null) {
                    // 将已取到的属性加到当前标签中
                    box.addAttributes(attrs.getAttributes());
                    // 打开当前标签
                    box.open();
                    // 重置变量
                    open = null;
                    attrs = null;
                    // 当前字符已处理完, index从下一字符开始, 所以+1
                    index = i + 1;
                    irow = this.row;
                    icln = this.column;
                }
                // 正在准备结束标签, 遇到>, 表示标签真的结束了
                else if (close != null) {
                    NodeMetaDataImpl data = box.curr;
                    // 关闭当前标签
                    box.close();
                    // 检查当前标签是否为include, 如果是则执行include
                    include(data);
                    // 重置变量
                    close = null;
                    index = i + 1;
                    irow = this.row;
                    icln = this.column + 1;
                }
            }
        }
        return index;
    }

    private List<TagType> getTagTypes() {
        List<TagType> types = ConvertTools.toList(TagType.values());
        if (!clearXmlComment) {
            types.remove(TagType.XML_COMMENT);
        }
        return types;
    }

    /**
     * 解析注释, 处理JSP导入标签, &lt;%@include file="xxx.jsp"%&gt;
     *
     * @author zhaohuihua
     * @param comment 注释内容
     * @throws TagException 解析失败
     * @throws TemplateException 模板不存在/模板读取失败
     */
    private void parseJspComment(String comment, int row, int column) throws TemplateException, TagException {
        Matcher matcher = JSP_INCLUDE.matcher(comment);
        if (!matcher.matches()) {
            debug(comment);
            return;
        }

        debug("{}({})\t{}", row, column, comment);
        String src = matcher.group(1);
        // 创建导入标签
        Class<? extends ITag> clazz = IncludeTag.class;
        NodeMetaDataImpl data =
                new NodeMetaDataImpl(clazz, "include", "<%include%>", "<%include%>", tpl, path, row, column);
        // 设置属性
        data.addAttribute("src", src);
        // 将新标签绑定到标签树上
        box.curr.appendChild(data);
        include(data);
    }

    /**
     * 导入其他模板
     *
     * @param data Include标签数据
     * @throws TagException 解析失败
     * @throws TemplateException 模板不存在/模板读取失败
     */
    private void include(NodeMetaDataImpl data) throws TemplateException, TagException {
        if (!TagUtils.isIncludeTag(data.clazz)) {
            return;
        }

        String key = "src";
        // 从标签属性表中获取模板路径
        String src = null;
        List<AttrData> attrs = data.getAttributes();
        for (AttrData attr : attrs) {
            if (key.equals(attr.getKey())) {
                src = (String) attr.getValue();
                continue;
            }
        }

        if (src == null) {
            String fmt = "%s:%s, %s, tag is missing a required attribute %s";
            String msg = String.format(fmt, data.row, data.column, data.openTag, key);
            throw new TagException(msg);
        }

        String realSrcPath = input.getRelativePath(tpl, src);

        // 创建一个新的模板解析器
        TagParse include = new TagParse(this, realSrcPath);

        // 解析导入的模板
        debug("/----------------------------------------\\");
        NodeMetaDataImpl idata;
        try {
            idata = include.parseFile();
        } catch (TagException e) {
            e.prependMessage(String.format("%s:%s ", data.row, data.column));
            throw e;
        }
        debug("\\----------------------------------------/");

        // 增加一个换行符
        data.appendChild(TagBox.newTextTag("\n", tpl, path, row, column));
        // 将导入模板的内容加到当前标签中
        List<NodeMetaDataImpl> children = idata.children();
        for (NodeMetaDataImpl child : children) {
            data.appendChild(child);
        }
    }

    /**
     * 输出调测日志
     *
     * @param message 消息内容
     * @param args 占位符参数
     */
    protected void debug(String message, Object... args) {
        if (debugger == null) {
            return;
        }
        debugger.debug(message, args);
    }
}
