package com.gitee.qdbp.staticize.common;

/**
 * 缓存字符串的Writer
 *
 * @author zhaohuihua
 * @version 20200928
 */
public class StringCachingWriter implements IWriter {

    private final StringBuilder caching = new StringBuilder();

    @Override
    public void write(Object value) {
        if (value != null) {
            caching.append(value);
        }
    }

    public String getContent() {
        return caching.toString();
    }

    public void clear() {
        this.caching.setLength(0);
    }

    @Override
    public void flush() {
    }

    @Override
    public void close() {
    }
}
