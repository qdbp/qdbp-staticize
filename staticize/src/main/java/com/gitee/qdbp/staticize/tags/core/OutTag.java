package com.gitee.qdbp.staticize.tags.core;

import java.io.IOException;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.NextStep;

/**
 * 输出内容的标签
 *
 * @author zhaohuihua
 * @version 20210829
 */
public class OutTag extends BaseTag {

    /** 变量值 **/
    private Object value;
    /** 默认值 **/
    private Object defaults;

    /**
     * 设置变量值
     *
     * @param value 变量值
     */
    public void setValue(Object value) throws TagException {
        this.value = value;
    }

    /**
     * 设置默认值
     *
     * @param defaults 默认值
     */
    public void setDefault(Object defaults) throws TagException {
        this.defaults = defaults;
    }

    @Override
    public NextStep doHandle() throws TagException, IOException {
        Object realValue = value != null ? value : defaults;
        print(realValue);
        return NextStep.SKIP_BODY;
    }
}
