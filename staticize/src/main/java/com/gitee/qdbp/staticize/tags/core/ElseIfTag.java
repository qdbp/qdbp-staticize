package com.gitee.qdbp.staticize.tags.core;

import java.util.Map;
import com.gitee.qdbp.staticize.annotation.AdjoinAfter;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.ITag;
import com.gitee.qdbp.staticize.tags.base.NextStep;

/**
 * else if 标签处理类
 * 
 * @author zhaohuihua
 * @version 140521
 */
@AdjoinAfter({ IfTag.class, ElseIfTag.class })
public class ElseIfTag extends IfTag {

    /** {@inheritDoc} **/
    public NextStep doHandle() throws TagException {
        ITag parent = this.parent();
        Map<String, Object> pstack = parent.stack();
        Boolean done = (Boolean) pstack.get(IfTag.IF_TAG_EVAL_DONE);
        if (done == null) {
            throw new TagException("No matching 'if' tag was found.");
        }

        // 更新曾经执行状态
        pstack.put(IfTag.IF_TAG_EVAL_DONE, done ? true : test);

        // executed=true曾经执行过, 本次不执行
        boolean eval = done ? false : test;
        if (eval) {
            return NextStep.EVAL_BODY;
        } else {
            return NextStep.SKIP_BODY;
        }
    }
}
