package com.gitee.qdbp.staticize.tags.base;

import java.io.IOException;

/** 
 * 跳过内容的标签
 *
 * @author zhaohuihua
 * @version 20201019
 */
public class IgnoreContentTag extends BaseTag {

    public NextStep doHandle() throws IOException {
        return NextStep.SKIP_BODY;
    }
}
