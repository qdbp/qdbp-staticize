package com.gitee.qdbp.staticize.parse;

import java.util.Arrays;
import java.util.List;
import com.gitee.qdbp.tools.utils.CharTools;

/**
 * 标签称收集器
 * 
 * @author zhaohuihua
 * @version 140603
 */
class NameCollector {

    public enum TagType {
        NORMAL, CDATA, XML_COMMENT
    }

    private static final String CDATA_OPEN = "<![CDATA[";
    private static final String CDATA_CLOSE = "]]>";

    private static final String XML_COMMENT_OPEN = "<!--";
    private static final String XML_COMMENT_CLOSE = "-->";

    /** 标签类型 **/
    private final List<TagType> types;
    /** 是收集开始标签还是收集结束标签 **/
    private final boolean isOpen;
    /** 标签名称 **/
    private final StringBuilder name = new StringBuilder();

    public NameCollector(boolean isOpen, TagType... types) {
        this(isOpen, Arrays.asList(types));
    }

    public NameCollector(boolean isOpen, List<TagType> types) {
        this.isOpen = isOpen;
        this.types = types;
    }

    public void parse(char c) {
        name.append(c);
    }

    public String getName() {
        return name.toString();
    }

    public boolean isTagName(char c) {
        if (types.contains(TagType.CDATA)) {
            if (isOpen) {
                if (CDATA_OPEN.equals('<' + name.toString())) {
                    return false; // 已经满足<![CDATA[了, 后面跟的字符就不会标签名了
                } else if (CDATA_OPEN.startsWith('<' + name.toString() + c)) {
                    return true;
                }
            } else {
                if (CDATA_CLOSE.equals(name.toString() + '>')) {
                    return false; // 已经满足]]>了, 后面跟的字符就不会标签名了
                } else if (CDATA_CLOSE.startsWith(name.toString() + c)) {
                    return true;
                }
            }
        }
        if (types.contains(TagType.XML_COMMENT)) {
            if (isOpen) {
                if (XML_COMMENT_OPEN.equals('<' + name.toString())) {
                    return false; // 已经满足<!--了, 后面跟的字符就不会标签名了
                } else if (XML_COMMENT_OPEN.startsWith('<' + name.toString() + c)) {
                    return true;
                }
            } else {
                if (XML_COMMENT_CLOSE.equals(name.toString() + '>')) {
                    return false; // 已经满足-->了, 后面跟的字符就不会标签名了
                } else if (XML_COMMENT_CLOSE.startsWith(name.toString() + c)) {
                    return true;
                }
            }
        }
        if (types.contains(TagType.NORMAL)) {
            return CharTools.isTagName(c);
        }
        return false;
    }

    public boolean matches(TagType type) {
        if (!this.types.contains(type) || this.name.length() == 0) {
            return false;
        }
        if (type == TagType.NORMAL) {
            return true;
        } else if (type == TagType.CDATA) {
            if (isOpen) {
                return CDATA_OPEN.equals('<' + name.toString());
            } else {
                return CDATA_CLOSE.equals(name.toString() + '>');
            }
        } else if (type == TagType.XML_COMMENT) {
            if (isOpen) {
                return XML_COMMENT_OPEN.equals('<' + name.toString());
            } else {
                return XML_COMMENT_CLOSE.equals(name.toString() + '>');
            }
        } else {
            return false;
        }
    }
}
