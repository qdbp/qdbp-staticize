package com.gitee.qdbp.staticize.parse;

import java.util.ArrayList;

/**
 * 表达式数组<br>
 * 如: home-${root.name}-list, 拆分为3组: [home-] [${root.name}] [-list]
 *
 * @author zhaohuihua
 * @version 20200930
 */
public class ExpItems extends ArrayList<ExpItem> {

    /** serialVersionUID **/
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        for (ExpItem item : this) {
            buffer.append(item.getContent());
        }
        return buffer.toString();
    }
}
