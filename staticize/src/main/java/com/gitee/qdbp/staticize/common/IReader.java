package com.gitee.qdbp.staticize.common;

import java.io.Closeable;
import java.io.IOException;

/**
 * 读取接口
 *
 * @author zhaohuihua
 * @version 20200815
 */
public interface IReader extends Closeable {

    /**
     * Reads characters into an array. <br>
     * This method will block until some input is available, <br>
     * an I/O error occurs, or the end of the stream is reached.
     *
     * @param cbuf Destination buffer
     *
     * @return The number of characters read, or -1 if the end of the stream has been reached
     *
     * @exception IOException If an I/O error occurs
     */
    int read(char[] cbuf) throws IOException;

    /**
     * Reads characters into a portion of an array.
     *
     * @param cbuf Destination buffer
     * @param offset Offset at which to start storing characters
     * @param length Maximum number of characters to read
     *
     * @return The number of characters read, or -1 if the end of the stream has been reached
     *
     * @exception IOException If an I/O error occurs
     */
    int read(char[] cbuf, int offset, int length) throws IOException;
    
    /** 实际保存路径 **/
    String getRealPath();
    
    /** 关闭存储类, 释放资源 **/
    @Override
    void close() throws IOException;
}
