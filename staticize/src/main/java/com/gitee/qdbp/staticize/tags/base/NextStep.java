package com.gitee.qdbp.staticize.tags.base;

/**
 * 下一步处理方式的枚举
 *
 * @author zhaohuihua
 * @version 140520
 */
public enum NextStep {
    /** 执行标签体 **/
    EVAL_BODY,
    /** 跳过标签体 **/
    SKIP_BODY,
    /** 循环执行标签体 **/
    LOOP_BODY
}
