package com.gitee.qdbp.staticize.tags.base;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.gitee.qdbp.able.model.reusable.ExpressionContext;
import com.gitee.qdbp.able.model.reusable.StackWrapper;
import com.gitee.qdbp.staticize.common.IContext;
import com.gitee.qdbp.staticize.common.IWriter;
import com.gitee.qdbp.staticize.common.NodeMetaData;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.parse.AttrSetter;
import com.gitee.qdbp.staticize.parse.AttrSetter.MethodInfo;
import com.gitee.qdbp.staticize.parse.ExpItem;
import com.gitee.qdbp.staticize.parse.ExpItems;
import com.gitee.qdbp.staticize.utils.OgnlUtils;
import com.gitee.qdbp.staticize.utils.TagUtils;
import com.gitee.qdbp.tools.parse.StringParser;
import com.gitee.qdbp.tools.utils.ReflectTools;
import com.gitee.qdbp.tools.utils.StringTools;
import com.gitee.qdbp.tools.utils.VerifyTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 基础标签处理类
 *
 * @author zhaohuihua
 * @version 140520
 */
public abstract class BaseTag implements ITag {

    /** 日志对象 **/
    private static final Logger log = LoggerFactory.getLogger(BaseTag.class);

    private final Map<String, Object> stack;
    private ITag parent;
    private IContext context;
    /** 前置空白文本(换行及缩进) **/
    private String leadingBlank;

    public BaseTag() {
        this.stack = new HashMap<>();
    }

    protected IContext context() {
        return context;
    }

    @Override
    public final Map<String, Object> stack() {
        return this.stack;
    }

    @Override
    public void doEnded(NextStep next) throws TagException, IOException {
    }

    /**
     * 设置基础数据
     *
     * @param parent 父标签
     * @param context 环境变量
     */
    @Override
    public void doSetBaseData(ITag parent, IContext context) {
        this.parent = parent;
        this.context = context;
        this.refreshStack();
    }

    /** 前置空白文本(换行及缩进) **/
    public void setLeadingBlank(String leadingBlank) {
        this.leadingBlank = leadingBlank;
    }

    /** 前置空白文本(换行及缩进) **/
    public String getLeadingBlank() {
        return leadingBlank;
    }

    /** 清除前置空白文本(换行及缩进) **/
    public String clearLeadingBlank() {
        String leadingBlank = this.leadingBlank;
        this.leadingBlank = null;
        return leadingBlank;
    }

    protected void refreshStack() {
        List<Map<String, Object>> stacks = new ArrayList<>();

        // 按父节点在前面的顺序获取数据
        ITag temp = this;
        while (temp != null) {
            Map<String, Object> stack = temp.stack();
            if (stack != null && !stack.isEmpty()) {
                stacks.add(0, stack); // 加到最前面
            }
            temp = temp.parent(); // 查找所有父节点
        }

        // 将所有栈中的数据叠加在一起
        context.stack().clear();
        context.stack().putAll(context.preset());
        for (Map<String, Object> stack : stacks) {
            context.stack().putAll(stack);
        }
        // 将值栈自身以ThisStack为名保存至栈中, 以实现在业务侧自定义值栈自身的函数, 如@contains(user.name)
        // BaseContext.parseStackFunction将@contains(user.name)替换为ThisStack.contains('user.name')
        Constructor<? extends StackWrapper> constructor = context.getTaglib().getStackWrapperConstructor();
        StackWrapper thisStack = ExpressionContext.createStackWrapperInstance(constructor, context.stack());
        context.stack().put("ThisStack", thisStack);
    }

    /**
     * 增加类的导入信息
     *
     * @param classFullName 类名全称, 如: com.qdbp.xxx.EntityTools
     */
    protected void addImportClass(String classFullName) {
        context.addImportClass(classFullName);
    }

    /**
     * 获取类的导入信息列表
     *
     * @return 导入信息列表
     */
    protected Collection<String> getImportClasses() {
        return this.context.getImportClasses();
    }

    /**
     * 获取类的导入信息
     *
     * @param shortName &#64;类名简称, 如: &#64;EntityTools
     * @return fullName 类名全称, 如: com.qdbp.xxx.EntityTools
     */
    protected String getImportClass(String shortName) {
        return this.context.getImportClass(shortName);
    }

    /**
     * 判断是否存在类的导入信息
     *
     * @param shortName &#64;类名简称, 如: &#64;EntityTools
     * @return 是否存在
     */
    protected boolean containsImportClass(String shortName) {
        return this.context.containsImportClass(shortName);
    }

    protected final void addStackValue(String key, Object value) {
        OgnlUtils.setValue(this.stack, key, value);
    }

    protected final void addVariableValue(String key, Object value, String scope) {
        if ("preset".equals(scope)) {
            OgnlUtils.setValue(context.preset(), key, value);
            ITag temp = this;
            while (temp != null) {
                OgnlUtils.setValue(temp.stack(), key, value);
                temp = temp.parent();
            }
        } else if (scope == null || "parent".equals(scope)) {
            OgnlUtils.setValue(this.stack(), key, value);
            OgnlUtils.setValue(this.parent.stack(), key, value);
        } else if ("root".equals(scope)) {
            ITag temp = this;
            while (temp != null) {
                OgnlUtils.setValue(temp.stack(), key, value);
                temp = temp.parent();
            }
        } else if ("this".equals(scope)) {
            OgnlUtils.setValue(this.stack(), key, value);
        } else {
            // TODO: scope="core:if", scopt="core:each"
            OgnlUtils.setValue(this.stack(), key, value);
        }
    }

    /** 从值栈环境变量中获取值 **/
    protected final String getStackString(String key) {
        try {
            return (String) this.context.getValue("${" + key + "}");
        } catch (TagException e) {
            log.warn("Get context stack value exception", e);
            return null;
        }
    }

    /** 从值栈环境变量中获取值 **/
    @SuppressWarnings("unchecked")
    protected final <T> T getStackValue(String key, Class<T> clazz) {
        try {
            return (T) this.context.getValue("${" + key + "}");
        } catch (TagException e) {
            log.warn("Get context stack value exception", e);
            return null;
        }
    }

    /** 从预置环境变量中获取值 **/
    protected final String getPresetString(String key) {
        try {
            return (String) this.context.getValue("#{" + key + "}");
        } catch (TagException e) {
            log.warn("Get context preset value exception", e);
            return null;
        }
    }

    /** 从预置环境变量中获取值 **/
    @SuppressWarnings("unchecked")
    protected final <T> T getPresetValue(String key, Class<T> clazz) {
        try {
            return (T) this.context.getValue("#{" + key + "}");
        } catch (TagException e) {
            log.warn("Get context value exception", e);
            return null;
        }
    }

    /**
     * 根据表达式从环境变量中获取值<br>
     * ${xxx}, 从值栈环境变量中获取值, 如${title}, ${image.src}<br>
     * #{xxx}, 从预置环境变量中获取值, 如#{site.id}, #{column.title}<br>
     *
     * @param expression 表达式
     * @return 环境变量中的值
     * @throws TagException 表达式错误, 表达式取值错误
     */
    protected final String getVariableString(String expression) {
        try {
            return (String) this.context.getValue(expression);
        } catch (TagException e) {
            log.warn("Get context preset value exception", e);
            return null;
        }
    }

    /**
     * 根据表达式从环境变量中获取值<br>
     * ${xxx}, 从值栈环境变量中获取值, 如${title}, ${image.src}<br>
     * #{xxx}, 从预置环境变量中获取值, 如#{site.id}, #{column.title}<br>
     *
     * @param expression 表达式
     * @param clazz 值类型
     * @return 环境变量中的值
     * @throws TagException 表达式错误, 表达式取值错误
     */
    @SuppressWarnings("unchecked")
    protected final <T> T getVariableValue(String expression, Class<T> clazz) {
        try {
            return (T) this.context.getValue(expression);
        } catch (TagException e) {
            log.warn("Get context value exception", e);
            return null;
        }
    }

    /**
     * 获取父标签
     *
     * @return 父标签
     */
    @Override
    public final ITag parent() {
        return this.parent;
    }

    /**
     * 获取根节点
     *
     * @return 根节点
     */
    @Override
    public final ITag root() {
        ITag parent = this.parent;
        while (parent != null && parent.parent() != null) {
            parent = parent.parent();
        }
        return parent;
    }

    /**
     * 获取指定类型的父标签的方法
     *
     * @param type 标签类型
     * @return 父标签
     */
    protected final <T extends ITag> T parent(Class<T> type) {
        return parent(type, false);
    }

    /**
     * 获取指定类型的父标签的方法
     *
     * @param type 标签类型
     * @param identical 恒等, true:父标签等于指定类型, false:父标签继承于指定类型
     * @return 父标签
     */
    @SuppressWarnings("unchecked")
    protected final <T extends ITag> T parent(Class<T> type, boolean identical) {
        ITag parent = this.parent;
        while (parent != null) {
            if (identical) {
                if (type == parent.getClass()) {
                    return (T) parent;
                }
            } else {
                if (type.isAssignableFrom(parent.getClass())) {
                    return (T) parent;
                }
            }
            parent = parent.parent();
        }
        return null;
    }

    /** {@inheritDoc} **/
    @Override
    public void doValidate(NodeMetaData metadata) throws TagException {
    }

    /** {@inheritDoc} **/
    @Override
    public Object doEvalValue(String fieldName, Object fieldValue) throws TagException {
        MethodInfo methodInfo = AttrSetter.instance.getMethodInfo(this.getClass(), fieldName);
        Class<?> fieldType = methodInfo == null ? null : methodInfo.getParamType();
        boolean tryEvalString = methodInfo == null || methodInfo.isTryEvalString();
        if (fieldValue instanceof ExpItem) {
            return doEvalExpression((ExpItem) fieldValue, fieldType);
        } else if (fieldValue instanceof ExpItems) {
            // 多个表达式
            return doEvalExpression((ExpItems) fieldValue, fieldType);
        } else if (tryEvalString && fieldValue instanceof String) {
            // String类型的属性值尝试作为表达式解析
            return tryEvalString((String) fieldValue, fieldType);
        } else {
            return fieldValue;
        }
    }

    protected Object tryEvalString(String string, Class<?> type) {
        // 在新版本中, 属性变量不要求一定用${}包裹, attr="${name}"可以简写为attr="name"
        // -- 如果context中不存在name, 而attr是String, 则将name本身当作字符串赋给attr
        // -- 如果attr是Object, 而context中不存在name, 则将null赋给attr
        if (StringParser.isWrappedBySingleQuotationMark(string)) {
            // 以'包围的字符串, 如 'Let\'s go!'
            return StringParser.unwrapSingleQuotationMark(string);
        } else if (StringParser.isWrappedByDoubleQuotationMark(string)) {
            // 以'包围的字符串, 如 "It's \"me\"!"
            return StringParser.unwrapDoubleQuotationMark(string);
        } else if (TagUtils.isWrappedByCurlyBracket(string)) {
            // 以{}包围的字符串, 如 { @contains('user.userName') ? user.userName : user.userCode }
            return context.getValue('$' + string);
        }
        if (type == Object.class && StringTools.isFieldNames(string)) {
            // 不存在该变量, 说明string只是普通字符串
            // <property name="dataType" value="PROCESS" />
            if (!ReflectTools.containsDepthFields(context.stack(), string)) {
                return string;
            }
        }
        // 目标不是string, 直接当作表达式计算
        if (type != null && type != String.class) {
            // <core:set var="json" value="@JSON.parse(\"{content:'Let\\\\'s go!'}\")" />
            return context.getValue("${" + string + "}");
        }
        // 不是字段名
        if (!StringTools.isFieldNames(string)) {
            return string;
        }
        // 不存在该变量, 说明string只是普通字符串
        if (!ReflectTools.containsDepthFields(context.stack(), string)) {
            return string;
        }
        // 获取变量值
        Object result = context.getValue("${" + string + "}");
        if (result == null) {
            return null;
        } else if (type == null || type.isAssignableFrom(result.getClass())) {
            // 目标类型为空或变量类型是string
            return result;
        } else {
            // 变量类型不是string, 把字段值当作普通字符串处理
            return string;
        }
    }

    protected Object doEvalExpression(ExpItem item, Class<?> type) throws TagException {
        if (item.isEvaluatable()) {
            // 需要计算, 取Ognl表达式的值
            return context.getValue(item.getContent());
        } else {
            // 不需要计算, 返回字符串内容
            return item.getContent();
        }
    }

    /**
     * 计算表达式结果, 如 home-${root.name}-list<br>
     * 需要转义时在表达式前加斜杠\, 如home-\${root.name}-list, \#{today}
     *
     * @param items 表达式数组
     * @param type 字段类型
     * @return 计算结果
     * @throws TagException 表达式错误, 表达式取值错误
     */
    protected String doEvalExpression(ExpItems items, Class<?> type) throws TagException {
        StringBuilder buffer = new StringBuilder();
        for (ExpItem item : items) {
            if (!item.isEvaluatable()) {
                buffer.append(item.getContent());
            } else {
                buffer.append(context.getValue(item.getContent()));
            }
        }
        return buffer.toString();
    }

    protected Object doEvalBoolean(String expression) throws TagException {
        // 需要计算, 取Ognl表达式的值
        return context.getValue(expression);
    }

    /**
     * 向输出处理类写入内容
     *
     * @param content 内容
     */
    protected void print(Object content) throws IOException {
        if (VerifyTools.isBlank(content)) {
            return;
        }

        if (VerifyTools.isNotBlank(leadingBlank)) {
            context.write(leadingBlank);
            leadingBlank = null;
        }
        context.write(content);
    }

    /**
     * 重新设置Writer
     *
     * @param writer 新的Writer
     * @return 原Writer
     */
    protected IWriter resetWriter(IWriter writer) {
        return context.resetWriter(writer);
    }
}
