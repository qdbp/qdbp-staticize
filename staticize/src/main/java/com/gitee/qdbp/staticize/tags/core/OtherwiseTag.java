package com.gitee.qdbp.staticize.tags.core;

import java.util.Map;
import com.gitee.qdbp.staticize.annotation.AdjoinAfter;
import com.gitee.qdbp.staticize.exception.TagException;
import com.gitee.qdbp.staticize.tags.base.BaseTag;
import com.gitee.qdbp.staticize.tags.base.ITag;
import com.gitee.qdbp.staticize.tags.base.NextStep;

/**
 * otherwise 标签处理类
 *
 * @author zhaohuihua
 * @version 20210424
 */
@AdjoinAfter({ WhenTag.class })
public class OtherwiseTag extends BaseTag {

    /** {@inheritDoc} **/
    public NextStep doHandle() throws TagException {
        ITag parent = this.parent();
        Map<String, Object> pstack = parent.stack();
        Boolean done = (Boolean) pstack.get(WhenTag.WHEN_TAG_EVAL_DONE);
        if (done == null) {
            throw new TagException("No matching 'when' tag was found.");
        }

        pstack.remove(WhenTag.WHEN_TAG_EVAL_DONE);
        if (!done) {
            // 没有执行过
            return NextStep.EVAL_BODY;
        } else {
            return NextStep.SKIP_BODY;
        }
    }
}
