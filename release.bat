@echo off
echo ==> versions:set
call mvn versions:set -DnewVersion=3.1.5 -DgenerateBackupPoms=false
echo ==> git push
call git add -u
call git commit -m "release 3.1.5"
call git push origin master
echo ==> git tag
call git tag -a 3.1.5 -m "release 3.1.5"
call git push origin 3.1.5
echo ==> release to oss.sonatype.org
call mvn clean deploy -P release -Dmaven.javadoc.failOnError=false
echo ==> versions:set
call mvn versions:set -DgroupId=com.gitee.qdbp -DoldVersion=3.1.5 -DnewVersion=3.1.6-SNAPSHOT -DgenerateBackupPoms=false
echo ==> git push
call git add -u
call git commit -m "released 3.1.5"
call git push origin master
echo ==> nexus-staging:release
call mvn nexus-staging:release
