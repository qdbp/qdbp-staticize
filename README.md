# 静态化模板库，类似jstl语法
可用于生成html、java、mybatis mapper文件等<br>
输出内容可以是文件 (FilePublisher)，也可以是其他内容<br>
在 https://gitee.com/qdbp/qdbp-jdbc 这个项目就通过模板生成了SqlBuffer对象<br>
https://gitee.com/qdbp/qdbp-jdbc/tree/master/jdbc-core/src/main/java/com/gitee/qdbp/jdbc/sql/parse/SqlBufferPublisher.java

# 使用示例
```java
    // 1.指定文件夹路径, 生成FileInputCreator
    URL path = ErrorIncludeTagPublisher.class.getResource("../template/");
    String folder = PathTools.toUriPath(path);
    System.out.println(folder);
    IReaderCreator input = new FileInputCreator(folder);

    // 2.解析模板, 得到标签元数据树形结构 (正式应用时, metadata可以缓存)
    TagParser parser = new TagParser(input);
    IMetaData metadata = parser.parse("homepage.tpl");

    // 3.准备数据
    Map<String, Object> data = new HashMap<String, Object>();
    Map<String, String> channel = new HashMap<String, String>();
    channel.put("id", "100088");
    channel.put("title", "资讯频道");
    data.put("channel", channel);
    data.put("user", "zhaohuihua");
    data.put("number", new Integer[] { 111, 222, 333, 444, 555, 666, 777, 888, 999, 1000 });

    // 4.发布内容到目标文件
    FilePublisher publisher = new FilePublisher(metadata, folder);
    publisher.publish(data, "homepage.html");
```

# 详尽的错误提示
例如，else标签必须紧跟在if标签后面，但下例中第1行的if结束标签漏写了<br>
就会报错TagException: errortest/include.if.tpl 8:5 errortest/error.if.tpl 6:5, <core:elseif>, 没有找到匹配的前置标签<br>
在include.if.tpl的第8行引用的error.if.tpl的第6行的<core:elseif>标签出现错误
```html
    <core:if test="#{i > 300}">#{i}
    <core:elseif test="#{i < 600}">小于600</core:elseif>
    <core:else>其他</core:else>
```

# 支持静态工具类调用
利用OGNL实现，使用方式是先导入包，再通过@XxxTools.method方式调用
```html
<core:import>com.gitee.qdbp.tools.utils.DateTools</core:import>
<core:import>com.gitee.qdbp.tools.utils.NamingTools</core:import>
<core:import>com.gitee.qdbp.tools.utils.StringTools</core:import>
<core:import>com.alibaba.fastjson.JSON</core:import>

${@DateTools.toNormativeString(@DateTools.parse('2018/8/20 15:25:35'))}

${@NamingTools.toCamelString('user_name')}

${phone}<core:if test="${@StringTools.isPhone(phone)}"> is phone.</core:if>

<core:block>
    <core:set var="json">
        ${@JSON.parse("{content:'Let\\'s go!'}")}
    </core:set>
    json content: ${json.content}
</core:block>
```
支持全局静态类导入，在taglib.txt中配置
```properties
@DateTools    = com.gitee.qdbp.tools.utils.DateTools
@StringTools  = com.gitee.qdbp.tools.utils.StringTools
@VerifyTools  = com.gitee.qdbp.tools.utils.VerifyTools
```


# 特点介绍
* 与jstl最大的不同，是变量的作用域范围<br>
如下示例，内层循环的同名变量i和s不会影响外层的，内层循环结束后，仍能正确输出外层循环的同名变量<br>
```html
    <core:each var="i" status="s" begin="3" rows="3">
        <core:each var="i" status="s" begin="600" rows="4">
            index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
        </core:each>
        index:${s.index} count:${s.count} begin:${s.begin} end:${s.end} first:${s.first} last:${s.last} value:${i}
    </core:each>
```
* 支持else标签
```html
    <core:each var="i" status="s" begin="100" end="1000" step="100">
        ${i}    <core:if test="${i < 300}">小于300</core:if>
                <core:elseif test="${i < 600}">小于600</core:elseif>
                <core:else>其他</core:else>
    </core:each>
```
* 不会因为标签导致空行<br>
解析标签时，进行了leadingBlank的特殊处理
```html
例如这一段内容
    text a
    <core:if test="${xxx}">
        text b
    </core:if>text c
处理后, 相当于这样:
    text a<core:if test="${xxx}">
        text b</core:if>
    text c
```

# 支持的标签
在settings/tags/taglib.txt中配置，可以自定义<br>
前缀可以随便修改，&lt;core:if&gt;可以改成&lt;c:if&gt;，也可以改成&lt;if&gt;<br>
但整个项目都是一致的，不支持每个页面单独定制前缀
```properties
core:include  = com.gitee.qdbp.staticize.tags.base.IncludeTag
core:import   = com.gitee.qdbp.staticize.tags.base.ImportTag
core:block    = com.gitee.qdbp.staticize.tags.core.BlockTag
core:if       = com.gitee.qdbp.staticize.tags.core.IfTag
core:elseif   = com.gitee.qdbp.staticize.tags.core.ElseIfTag
core:else     = com.gitee.qdbp.staticize.tags.core.ElseTag
core:each     = com.gitee.qdbp.staticize.tags.core.EachTag
core:comment  = com.gitee.qdbp.staticize.tags.core.CommentTag
core:set      = com.gitee.qdbp.staticize.tags.core.SetVariableTag

fmt:date      = com.gitee.qdbp.staticize.tags.core.DateFormatTag
```
* 简单标签说明
```html
<core:block>用于定义一个块，限定变量的作用域<core:block>
<core:comment>这个一个注释块<core:comment>
<core:import>com.gitee.qdbp.tools.utils.DateTools</core:import>用于导入静态类，详见前面的示例
<core:if test="${xxx==1}">条件判断</core:if>
<core:elseif test="${xxx==2}">必须紧跟在if或elseif标签后面, 通过@AdjoinAfter注解实现<core:elseif>
<core:else>必须紧跟在if或elseif标签后面<core:elseif>
<core:include src="../common/header.tpl" /> 用于引用其他模板
```
* <fmt:date>标签，日期格式化
```html
<fmt:date pattern="yyyy-MM-dd" value="${createTime}" />
pattern 指定输出的格式，可以是预定义的default/date/time，也可以自定义，与DateFormat格式一致
value 指定日期，如果不指定则默认为当前时间
var 保存日期字符串的变量。如果指定了var, 则将格式化后的文本保存到该变量；否则将文本输出到页面。
```
* <core:set>标签，用于设置变量
```html
<core:each var="userBean" items="${userList}">
    <core:set var="temp.userName" value="${userBean.displayName}" />
    ${temp.userName}
</core:each>
通过scope指定作用域，parent/root/preset; 默认为parent;
如果指定scope=preset, 则可通过#{temp.userName}访问
如果指定了value, 则不允许有标签体
-- <core:set var="tempName" value="${userBean.displayName}" />
-- ${tempName}
如果没有指定value, 则标签体内容就是value, 此时标签体内容必须是一个表达式
-- <core:set var="tempName">${userBean.displayName}</core:set>
```
* <core:each>标签，循环输出内容
```html
status=迭代状态变量名
	index: 从0开始的当前迭代索引
	count: 从1开始的迭代次数计数
		如 begin=20, end=29, step=3, 则index=[20,23,26,29], count=[1,2,3,4]
	begin: 开始行数
	rows: 迭代行数
	end: 结束行数(begin+rows-1=end)
	step: 迭代步长
	limit: 迭代次数
	first: 是否为第一次迭代
	last: 是否为最后一次迭代

items=进行迭代的集合
	如果没有指定items, 则begin必须, rows/end/limit必填其一

	从6开始遍历5次, 6,7,8,9,10
	<core:each var="i" status="s" begin="6" limit="5">
		${s.count}-${i}
	</core:each>

	从6开始
	<core:each items="${list}" var="i" begin="6">
		...
	</core:each>

	只显示第1条
	<core:each items="${list}" var="i" begin="1" rows="1">
		...
	</core:each>
```

# MAVEN仓库地址
https://search.maven.org/artifact/com.gitee.qdbp/qdbp-staticize
```xml
    <dependency>
        <groupId>com.gitee.qdbp</groupId>
        <artifactId>qdbp-staticize</artifactId>
        <version>3.5.5</version>
    </dependency>
```